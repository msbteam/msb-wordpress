<?php

include('wp-blog-header.php'); // FOR TESTING ONLY!

class tm_mu_theme_switch {
	function tm_mu_theme_switch() {
		$themes = $this->get_network_allowed_themes();
		$this->display_theme_form($themes);
	}
	
	function get_network_allowed_themes() {
		global $wpdb;
		$query = "SELECT meta_value FROM ".$wpdb->sitemeta." WHERE meta_key = 'allowedthemes'";
		$results = $wpdb->get_row($query, ARRAY_A);
		$active_themes = array();
		if($results['meta_value']) {
			$themes = unserialize(stripslashes($results['meta_value']));
			foreach($themes as $name=>$value) {
				if($value == 1)
					$active_themes[] .= $name;
			}
		}
		else return false;
		return $active_themes;
	}
	
	function display_theme_form($themes) {
		$path = ABSPATH.'/wp-content/themes/';
		echo '<table>';
		$count = 1;
		foreach($themes as $theme) {
			if($count % 2) // True when count is odd, false when count is even
				echo '<tr>';
			$theme_info = get_theme_data( $path.$theme.'/style.css' );
			if(file_exists($path.$theme.'/screenshot.png'))
			echo '<td><img src="'.get_bloginfo('url').'/wp-content/themes/'.$theme.'/screenshot.png" /><br><input type="radio" name="theme" value="'.$theme.'"> '.$theme_info['Name'].'</td>';
			if(!($count % 2)) // True when count is even, false when count is odd
				echo '</tr>';
			$count++;
		}
		if(count($themes) % 2)
			echo '</tr>';
		echo '</table>';
	}
}

$tm_mu = new tm_mu_theme_switch;
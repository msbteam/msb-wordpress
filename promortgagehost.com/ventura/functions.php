<?php

/* Load the Placester Blueprint Theme Framework. */
require_once( trailingslashit( get_template_directory() ) . 'blueprint/blueprint.php' );
new Placester_Blueprint('2.5');

/**
 * Any modifications to its behavior (add/remove support for features, define 
 * constants etc.) must be hooked in 'after_setup_theme' with a priority of 10 if the
 * framework is a parent theme or a priority of 11 if the theme is a child theme. This 
 * allows the class to add or remove theme-supported features at the appropriate time, 
 * which is on the 'after_setup_theme' hook with a priority of 12.
 * 
 */
add_action( 'after_setup_theme', 'vent_setup', 10 );
function vent_setup() {
	remove_theme_support( 'pls-default-css' );
	remove_theme_support( 'pls-color-options' );
	remove_theme_support( 'pls-typography-options' );
	remove_theme_support( 'pls-sidebars', array( 'footer-widgets' ) );
	add_theme_support( 'pls-sidebars', array( 'primary', 'listings-search' ) );
	add_theme_support( 'pls-routing-util-templates' );

	remove_theme_support( 'pls-meta-data');
    remove_theme_support( 'pls-micro-data');
    remove_theme_support( 'pls-custom-polygons' );
}

	include_once('custom/sidebar-quick-search-widget.php');
	include_once('custom/sidebar-recent-posts-widget.php');
	include_once('custom/sidebar-listings-widget.php');
	include_once('custom/sidebar-agent-widget.php');
	include_once('custom/home-search-form.php');
	include_once('custom/home-listing-list.php');
	include_once('custom/listings-search-form.php');
	include_once('custom/listings-search-list.php');
	include_once('custom/client-profile-search-list.php');
	include_once('custom/property-details.php');

add_action('init','vent_custom_menu');
function vent_custom_menu(){
	register_nav_menu('footer_menu', 'Footer Links');
}

add_action('wp_enqueue_scripts', 'vent_scripts_styles');
function vent_scripts_styles () {
	if (!is_admin()) {
		wp_enqueue_script('ventura', trailingslashit(get_template_directory_uri()) . "js/script.js", array('jquery'), false, true);
		
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
}

/**
 * 	Filter the default prefix used in 'pls_do_atomic' and 'pls_apply_atomic'.
 * 	All add_filters that hook into events set by pls_do_atomic will need to catch
 * 	the prefix_event_name for example:
 *
 *	blueprint will mean that you need to hook against blueprint_close_header, or blueprint_open_header
 */

add_filter( 'pls_prefix', 'vent_prefix' );
function vent_prefix() {
	return 'ventura';
}

function vent_get_user_details ($attribute) {
	$user_details = PLS_Plugin_API::get_user_details();
	if (isset($user_details->$attribute)) {
		return $user_details->$attribute;
	} else {
		return false;
	}
}

add_action( 'wp_print_scripts', 'vent_custom_css', 99);
function vent_custom_css() {
    $custom_css = pls_get_option('pls-custom-css');
    if ( (pls_get_option('pls-css-options')) && $custom_css ) {
        printf( '<style type="text/css">%s</style>', $custom_css );
    }
}


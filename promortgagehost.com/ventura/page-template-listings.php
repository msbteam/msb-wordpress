<?php
/**
 * Template Name: Listings Search
 *
 * This is the template for "Listings" search results page.
 *
 */
?>

<section class="left-content">
	<?php echo PLS_Partials::get_listings_search_form( 'context=listings_search&ajax=1'); ?>


	<div id="placester_listings_listz">
		<h3>Search Results</h3>
		<?php echo PLS_Partials::get_listings_list_ajax('crop_description=1&context=custom_listings_search&table_id=placester_listings_list'); ?>
		<?php PLS_Listing_Helper::get_compliance(array('context' => 'listings', 'agent_name' => false, 'office_name' => false)); ?>
	</div>

</section>

<aside id="sidebar-primary">

	<?php if (is_page("listings")): ?>

		<?php dynamic_sidebar( 'listings' ); ?>

	<?php elseif ( is_active_sidebar( 'primary' ) ) : ?>

		<?php dynamic_sidebar( 'primary' ); ?>

	<?php else: ?>

		<?php PLS_Route::handle_default_sidebar(); ?>

	<?php endif ?>
</aside>

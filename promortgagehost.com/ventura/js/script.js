function update_price_dropdowns () {
  var transation_type = jQuery('#purchase_type_container select').val();
  if (!transation_type) {
    transation_type = 'sale';
  }

  var max_options = jQuery('#' + transation_type + '_max_options select').html();
  var min_options = jQuery('#' + transation_type + '_min_options select').html();  

  jQuery('#max_price_container select').empty().append(max_options);
  jQuery('#min_price_container select').empty().append(min_options);

  jQuery("#min_price_container select, #max_price_container select").trigger("liszt:updated");
}


jQuery( document ).ready( function($) {
  update_price_dropdowns();
  $("#purchase_type_container select").change(function() {
      update_price_dropdowns();    
  });
});

=== Ventura Real Estate for Placester ===
Author: The Placester Team
Contributors: Placester, Matt Barba
Tags: green, orange, blue, two-column, custom-header, custom-background, custom-menu, theme-options, threaded-comments, sticky-post, microformats
Requires at least: 3.0
Tested up to: 3.9
Stable tag: 2.5

A highly customizable theme for real estate professionals complimented by [Real Estate Website Builder](http://http://wordpress.org/extend/plugins/placester/) plugin by [Placester](https://placester.com).

== Description ==

The Ventura theme is the first in Placester's series themes designed from the ground up for real estate professionals. With listing management, Google Maps integration, advanced property search, and complete control over personalization, Placester's themes are all you need to have an instant-on web site quickly.

There are few things as relaxing as a nice day at the beach (or “down the shore,” as some of our New Jersey friends might say). Unfortunately, some of us only get one or two a year. But we at Placester ask, why limit your beach days to the summer? For that matter, why limit them to the real world? Ventura’s blue backgrounds and deep gold accents will have your visitors breaking out their flip-flops and folding chairs as they surf your properties on the web. Perfect for both individual agents and single-office agencies, Ventura is a simple theme that’s ideal for displaying a few featured listings, with a custom slideshow that displays up to five properties, a three-row layout, and top-level page navigation. It’s also fully customizable, so you can make it your own, and SEO optimized, which means you’ll have plenty of people to help you apply that sunscreen to your back. Use with <a href="https://placester.com/">Placester</a>'s <a href="wordpress.org/extend/plugins/placester/">Real Estate Builder plugin</a>.

= Highlights =

* Search by listings location, price, # of bedrooms and bathrooms and more
* Create listings of any type: sales, rentals, vacation, etc
* Customize the listings that appear on the site using the Placester controls
* Track leads using the Placester platform
* Powerful reporting
* Controls for theme customization: logo, tagline, featured properties and more

= Features =

* Fully-featured blog with threaded comment support and more
* Clean and semantic HTML5 and CSS3 code
* Search engine friendly
* Supports native menus controls
* Compatible with all major browsers

= License =

[GNU General Public License](http://www.gnu.org/licenses/gpl-2.0.html)

== Installation ==

= Via WordPress Admin =

1. From WordPress Admin, go to Themes > Install Themes
1. In the search box, type "Placester" and press enter
1. Locate the entry for "Ventura" and click the "Install" link
1. When the installation is finished, click the "Activate" link

= Manual Install =

1. Unzip this file into domain.com/wp-content/themes/ directory, should look like mywebsite.com/wp-content/themes/ventura/
2. Go into WordPress Admin, navigate to Appearance > Themes
3. Find the "Ventura" listing on this page and click "Activate"

== Frequently Asked Questions ==

= Who is Placester for? =

Any professional seeking the competitive advantage of having a robust real estate web site.

= How do I get started? =

After installing the plugin simply follow the instructions in the plugin to obtain an API key and begin either adding listings or [notify us](mailto:support@placester.com) if you already work with a property database company so we can enable the integration for your account.

= Why do I need an API key? =

Placester offers lots of tools on our platform powered by our robust API. So using the API makes your data portable and allows you to use any of our products to interact with your account and your listings.

= How do I get support? =

Easy, simply [reach out](mailto:support@placester.com) to us and let us know how we can help!

= I want a feature that you have not included, can you add it? =

Absolutely, [reach out](mailto:support@placester.com) to us and let us know what features would help you generate more leads easier!


<?php 

PLS_Style::add(array( 
		"name" => "Widget Styles",
		"type" => "heading"));

		PLS_Style::add(array(
				"name" => "Widget Titles",
				"desc" => "",
				"id" => "widget_title_typography",
				"selector" => "aside h3",
				"type" => "typography"));


		PLS_Style::add(array(
				"name" => "Search Widget",
				"desc" => "These options will customize the sidebar search widget.",
				"type" => "info"));

				PLS_Style::add(array(
						"name" => "Quick Search Widget  - label typography",
						"desc" => "",
						"id" => "widget_search_label_typography",
						"selector" => "aside .pls-quick-search label",
						"type" => "typography"));


		PLS_Style::add(array(
				"name" => "Agent Widget",
				"desc" => "These options will customize the sidebar agent widget.",
				"type" => "info"));

				PLS_Style::add(array(
						"name" => "Agent Widget  - agent name typography",
						"desc" => "",
						"id" => "agent_widget_agent_name_typography",
						"selector" => "aside .widget-pls-agent .agent-name",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Agent Widget  - agent phone typography",
						"desc" => "",
						"id" => "agent_widget_agent_phone_typography",
						"selector" => "aside .widget-pls-agent .agent-phone strong",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Agent Widget  - agent email typography",
						"desc" => "",
						"id" => "agent_widget_agent_email_anchor_typography",
						"selector" => "aside .widget-pls-agent .agent-email a, aside .widget-pls-agent .agent-email a:visited",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Agent Widget  - agent email typography",
						"desc" => "",
						"id" => "agent_widget_agent_email_anchor_typography_hover",
						"selector" => "aside .widget-pls-agent .agent-email a:hover",
						"type" => "typography"));

		PLS_Style::add(array(
				"name" => "Listing Widget",
				"desc" => "These options will customize the sidebar listings widget.",
				"type" => "info"));

				PLS_Style::add(array(
						"name" => "Listings Widget  - listings address typography",
						"desc" => "",
						"id" => "widget_listings_address_typography",
						"selector" => "aside .pls-listings a.feat-title",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Listings Widget  - listings details typography",
						"desc" => "",
						"id" => "widget_listings_details_typography_hover",
						"selector" => "aside .pls-listings a.feat-title:hover",
						"type" => "typography"));

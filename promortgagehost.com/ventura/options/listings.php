<?php

PLS_Style::add(array( 
		"name" => "Home Page Styles",
		"type" => "heading"));

PLS_Style::add(array(
			"name" => "Home Page Slideshow Image",
			"desc" => "Upload your desired home page image here. If no image is set, it will default to a slideshow.",
			"id" => "pls_use_image",
			"type" => "upload"));


PLS_Style::add(array( 
		"name" => "Listing Styles",
		"type" => "heading"));

		PLS_Style::add(array( 
				"name" => "General Listing Styles",
				"desc" => "",
				"type" => "info"));
        PLS_Style::add(array(
          "name" => "Default Sort Column",
          "desc" => "Set default column for listings to sort on",
          "id" => "listings_default_sort_by",
          "std" => "cur_data.price",
          "type" => "select",
          "class" => "mini",
          "options" => array(
                  'images' => 'Images',
                  'location.address' => 'Address',
                  'location.locality' => 'City',
                  'location.region' => 'State',
                  'location.postal' => 'Zip',
                  'zoning_types' => 'Zoning',
                  'purchase_types' => 'Purchase Type',
                  'listing_types' => 'Listing Type',
                  'property_type' => 'Property Type',
                  'cur_data.beds' => 'Beds',
                  'cur_data.baths' => 'Baths',
                  'cur_data.price' => 'Price',
                  'cur_data.sqft' => 'Square Feet',
                  'cur_data.avail_on' => 'Date Available'
                )
              )
            );
        
        // I would normally do a 2-item choice as radio button, but decided to mirror how the frontend choice is made -pek
        PLS_Style::add(array(
          "name" => "Default Sort Direction",
          "desc" => "Set default direction for listings sort order",
          "id" => "listings_default_sort_type",
          "std" => "asc",
          "type" => "select",
          "class" => "mini",
          "options" => array(
                  'desc' => 'Descending',
                  'asc' => 'Ascending'
                )
              )
            );
        
        PLS_Style::add(array(
          "name" => "Maximum Listings Per Search",
          "desc" => "Limits total number of listings returned in one search. Max is 50.",
          "id" => "listings_default_list_limit",
          "std" => "50",
          "type" => "text"
              )
            );

				PLS_Style::add(array(
						"name" => "Listing Address Link",
						"desc" => "",
						"id" => "listings_search_list_address",
						"selector" => "#placester_listings_list .single-item .list-thumb-txt a, #placester_listings_list .single-item .list-thumb-txt a:visited",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Listing Address link on hover",
						"desc" => "",
						"id" => "listings_search_list_address_hover",
						"selector" => "#placester_listings_list .single-item .list-thumb-txt a:hover",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Listing Image Border",
						"desc" => "",
						"id" => "listing_image_border",
						"selector" => "#placester_listings_list .single-item .list-thumb-size .thumbs",
						"type" => "border"));

				PLS_Style::add(array(
						"name" => "Listing Image Background",
						"desc" => "",
						"id" => "listing_image_background",
						"selector" => "#placester_listings_list .single-item .list-thumb-size .thumbs",
						"type" => "background"));

				PLS_Style::add(array(
						"name" => "Listing Details Typography",
						"desc" => "",
						"id" => "listing_details_typography",
						"selector" => "#placester_listings_list .single-item .list-thumb-details ul li",
						"type" => "typography"));


		PLS_Style::add(array( 
				"name" => "Single Property Styles",
				"desc" => "",
				"type" => "info"));

				PLS_Style::add(array(
						"name" => "Single Property Address",
						"desc" => "",
						"id" => "single_property_address",
						"selector" => "body.single-property article.property-details h3",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Single Property Basic Details",
						"desc" => "",
						"id" => "single_property_basic_details",
						"selector" => "body.single-property .property-details .basic-info li",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Single Property Main Image background",
						"desc" => "",
						"id" => "single_property_main_image_background",
						"selector" => "body.single-property #property-details-main-image img",
						"type" => "background"));

					PLS_Style::add(array(
							"name" => "Single Property Main Image border",
							"desc" => "",
							"id" => "single_property_main_image_border",
							"selector" => "body.single-property #property-details-main-image img",
							"type" => "border"));

				PLS_Style::add(array(
						"name" => "Single Property Section Titles",
						"desc" => "",
						"id" => "single_property_section_titles",
						"selector" => "body.single-property article.property-details h6",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Single Property Paragraph Text",
						"desc" => "",
						"id" => "single_property_paragraph_text",
						"selector" => "body.single-property article.property-details p",
						"type" => "typography"));

				PLS_Style::add(array(
						"name" => "Single Property Details List",
						"desc" => "",
						"id" => "single_property_paragraph_details_list",
						"selector" => "body.single-property .details-wrapper ul li, body.single-property .amenities ul li",
						"type" => "typography"));



				PLS_Style::add(array( 
					"name" => "SEO Settings",
					"type" => "heading"));

			PLS_Style::add(array(
							"name" => "Display Theme Meta Tags",
							"desc" => "Display the following meta tags at the top of every page: description, author, site name, title, url, and site name.",
							"id" => "pls_header_meta_tags",
							"std" => "1",
							"type" => "checkbox"));

<?php PLS_Route::handle_header(); ?>

<div id="banner">
	<div class="wrapper">
		<div id="slideshow-glow"></div>
	</div>
</div>	

<div class="wrapper">

	<?php PLS_Route::handle_dynamic(); ?>

	<?php PLS_Route::handle_sidebar(); ?>

</div>


<?php PLS_Route::handle_footer(); ?>

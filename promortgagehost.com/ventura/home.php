<?php
/**
 * The main template page.
 *
 * @package Placester
 * @since Placester 0.1
 */

?>
<section id="home-banner">
	<!-- slideshow section -->
	<div class="wrapper">
		<div id="slideshow-glow" class="static">
			<div id="slideshow">
				<div id="slides">
					<?php $img = pls_get_option('pls_use_image', false);  ?>
					<?php if ($img): ?>
						<img src="<?php echo $img ?>">
					<?php else: ?>
						<?php 
							echo PLS_Slideshow::slideshow( 
								array( 
									'animation' => 'fade',
									'animationSpeed' => 800,
									'timer' => true,
									'pauseOnHover' => true,
									'advanceSpeed' => 4000,
									'startClockOnMouseOut' => true,
									'startClockOnMouseOutAfter' => 1000,
									'directionalNav' => true,
									'captions' => 'false',
									'captionAnimation' => 'fade',
									'captionAnimationSpeed' => 800,
									'width' => 620,
									'height' => 320,
									'bullets' => false,
									'context' => 'home',
									'featured_option_id' => 'slideshow-featured-listings',
									'listings' => 'limit=5&is_featured=true&sort_by=price'
								)
							); 
						?>
					<?php endif ?>
					
				</div><!-- end slides -->

<?php
echo PLS_Partials::get_listings_search_form( "class=&context=home&half_baths=0&min_baths=0&max_baths=0&property_type=0&listing_types=0&zoning_types=0&states=0&available_on=0&zips=0&min_beds=0&max_beds=0&neighborhood=0" );
?>

			<div class="clr"></div>

            </div><!-- end slideshow -->
        </div><!-- end slideshow-glow -->
    </div><!-- end of wrapper -->
</section>

<section id="home-listings">
	<div class="wrapper">
		<?php echo PLS_Partials::get_listings( "limit=3&featured_option_id=custom-featured-listings&context=home" ) ?>
			<?php
			// compliance moving to the footer
			// PLS_Listing_Helper::get_compliance(array('context' => 'listings', 'agent_name' => false, 'office_name' => false));
			?>
		<div class="clr"></div>
	</div>
</section> <!-- /#home-listings -->

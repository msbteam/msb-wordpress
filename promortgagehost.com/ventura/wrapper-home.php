<?php PLS_Route::handle_header(); ?>

<script type="text/javascript">
  var search_bootloader;
  jQuery(document).ready(function( $ ) {
    search_bootloader = new SearchLoader ({
    	filter: {
    		context: 'home'
    	}
    });
  });
</script>

<?php  PLS_Route::handle_dynamic(); ?>

<?php PLS_Route::handle_footer(); ?>
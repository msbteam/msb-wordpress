<?php
$favicon = pls_get_option('pls-site-favicon');
$logo = pls_get_option('pls-site-logo');
$title = pls_get_option('pls-site-title');
$subtitle = pls_get_option('pls-site-subtitle');
$desc = get_bloginfo('description');

$agent = PLS_Plugin_API::get_user_details();

$email = pls_get_option('pls-user-email');
$phone = pls_get_option('pls-user-phone');
$email = $email ? $email : $agent['user']['email'];
$phone = $phone ? $phone : $agent['user']['phone'];

?><!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<title><?php wp_title(); ?></title>
	<!-- SEO Tags -->
	<?php if (pls_get_option('pls_header_meta_tags')): 
		$name = get_bloginfo('name'); ?>

		<meta name="description" content="<?php echo $desc; ?>">
		<meta name="author" content="<?php echo vent_get_user_details('first_name') . ' ' . vent_get_user_details('last_name'); ?>">		
		<meta itemprop="name" content="<?php echo $name; ?>">

	<?php endif ?>
	
  <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  <meta name="viewport" content="width=device-width,initial-scale=1">

  <!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

	<?php if ( $favicon ) { ?>
	<link href="<?php echo $favicon; ?>" rel="shortcut icon" type="image/x-icon" />
	<?php } ?>

  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

  <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" media="all" />
  
  <?php wp_head(); ?>
  
</head>

<body <?php body_class(); ?>>

<?php pls_do_atomic( 'open_body' ); ?>

<?php pls_do_atomic( 'before_header' ); ?>

<header>
	<div class="wrapper">

		<section class="placester-logo">
			<?php if ($logo): ?>
				<div id="logo">
					<img src="<?php echo $logo; ?>" alt="Site logo">
				</div>
			<?php endif; ?>

			<?php if ($title): ?>
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php echo $title; ?></a></h1>
				
				<?php if ($subtitle): ?>
					<h2><?php echo $subtitle; ?></h2>
				<?php endif ?>
			<?php endif; ?>

			<?php if ( !$logo && !$title ): ?>
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
				<h2><?php echo $desc; ?></h2>
			<?php endif; ?>

		</section>

		<?php if ( $email ): ?>
			<section class="email">
				<ul>
					<li class="phone-bg-left"></li>
						<li class="phone-bg-mid">Email : <a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></li>
					<li class="phone-bg-right"></li>
				</ul>
			</section>	
		<?php endif ?>
		
		<?php if ( $phone ): ?>
			<section class="phone">
				<ul>
					<li class="phone-bg-left"></li>
						<li class="phone-bg-mid">Call : <?php echo $phone; ?></li>
					<li class="phone-bg-right"></li>
				</ul>
			</section> 	
		<?php endif; ?>
		

		<div id="nav-login">
			<?php echo PLS_Plugin_API::placester_lead_control_panel(array('separator' => '')); ?>
		</div>

		<?php PLS_Route::get_template_part( 'menu', 'primary' ); ?> 


		<div class="clr"></div>
	
	</div><!-- /.wrapper -->

</header>

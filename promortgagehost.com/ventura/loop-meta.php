<?php
/**
 * Loop Meta Template
 *
 * Displays information at the top of the page about archive and search results when viewing those pages.  
 * This is not shown on the front page or singular views.
 *
 * @package PlacesterBlueprint
 * @subpackage Template
 */
?>
	<?php if ( is_home() && ! is_front_page() ) : ?>
		<div class="loop-meta">
			<h3 class="page-title"><?php echo get_post_field( 'post_title', get_queried_object_id() ); ?></h3>

			<div class="loop-description">
				<?php echo apply_filters( 'the_excerpt', get_post_field( 'post_excerpt', get_queried_object_id() ) ); ?>
			</div><!-- .loop-description -->

		</div><!-- .loop-meta -->

	<?php elseif ( is_category() ) : ?>

        <h3 class="page-title"><?php single_cat_title(); ?></h3>

	<?php elseif ( is_tag() ) : ?>

        <h3 class="page-title"><?php single_tag_title(); ?></h3>

	<?php elseif ( is_tax() ) : ?>

        <h3 class="page-title"><?php single_term_title(); ?></h3>

	<?php elseif ( is_author() ) : ?>

		<?php $user_id = get_query_var( 'author' ); ?>

        <h3 class="page-title"><?php single_term_title( the_author_meta( 'display_name', $user_id ) ); ?></h3>

	<?php elseif ( is_search() ) : ?>

        <h3 class="page-title"><?php printf( __( 'Search results for "%s"', pls_get_textdomain() ), esc_attr( get_search_query() ) ); ?></h3>

	<?php elseif ( is_date() ) : ?>

        <h3 class="page-title"><?php _e( 'Blog archives by date', pls_get_textdomain() ); ?></h3>

	<?php elseif ( function_exists( 'is_post_type_archive' ) && is_post_type_archive() ) : ?>

        <h3 class="page-title"><?php post_type_archive_title(); ?></h3>

	<?php elseif ( is_archive() ) : ?>

        <h3 class="page-title"><?php _e( 'Archives', pls_get_textdomain() ); ?></h3>

	<?php elseif ( is_single() ) : ?>

        <h3 class="page-title"><?php the_title(); ?></h3>

	<?php elseif ( is_page() ) : ?>

        <h3 class="page-title"><?php the_title(); ?></h3>

	<?php endif; ?>


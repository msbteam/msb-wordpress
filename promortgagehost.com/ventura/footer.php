<?php
/**
 * Footer Template
 */

$agent = PLS_Plugin_API::get_user_details();

$email = pls_get_option('pls-user-email');
$phone = pls_get_option('pls-user-phone');
$image = pls_get_option('pls-user-image');
$name = pls_get_option('pls-user-name');

$email = strval( $email ? $email : $agent['user']['email'] );
$phone = strval( $phone ? $phone : $agent['user']['phone'] );
$headshot = isset($agent['user']['headshot']) ? $agent['user']['headshot'] : '';
$image = strval( $image ? $image : $headshot );

?>
<div class="clearfix"></div>
<footer>

	<div class="wrapper">

		<section id="footer-about">
			<p class="links-header">About</p>
			<p class="info"><?php echo pls_get_option('pls-user-description') ?></p><br/>
		</section>

		<?php if ($image || $name || $phone || $email) { ?>
		<section id="footer-contact">
			<p class="links-header">Contact</p>
				<?php if ($image) { ?>
					<img src="<?php echo $image; ?>" height=120 alt="<?php echo $name; ?>">
				<?php } ?>

				<?php if ($name) { ?>
					<p class="info"><?php echo $name; ?></p>
				<?php } ?>

				<?php if ($phone) { ?>
					<p class="info blue"><strong><?php echo $phone; ?></strong></p>
				<?php } ?>

				<?php if ($email) { ?>
					<p class="info"><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
				<?php } ?>
		</section>
		<?php } ?>

		<section id="footer-nav">
			<p class="links-header">Site Navigation</p>
			<?php wp_nav_menu (array('menu'=>'footer_menu', 'depth' => 1 ));?>
		</section>

		<div class="clr"></div>

	</div><!-- end of wrapper -->

	<div id="footer-base">
<?php 
if( is_home() && ! pls_has_plugin_error() ) :
?> 
	<div class="wrapper">
      <?php PLS_Listing_Helper::get_compliance(array('context' => 'listings', 'agent_name' => false, 'office_name' => false)); ?>
      <div class="clr"></div>
    </div>
<?php
endif;
?>
		<div class="wrapper">
				<p class="copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> | <a href="https://placester.com/wordpress-themes/ventura/" rel="nofollow">Ventura</a> theme by Placester</p>
		</div>
	</div>

</footer>

<?php
/* Always have wp_footer() just before the closing </body>
 * tag of your theme, or you will break many plugins, which
 * generally use this hook to reference JavaScript files.
 */
wp_footer();
?>

</body>
</html>
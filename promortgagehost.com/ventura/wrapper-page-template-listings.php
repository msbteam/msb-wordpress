<?php PLS_Route::handle_header(); ?>

<script type="text/javascript">
  var search_bootloader;
  jQuery(document).ready(function( $ ) {
    search_bootloader = new SearchLoader ({
    	filter: {
    		context: 'listings_search'
    	},
    	list: {
    		context: 'custom_listings_search'}
    });
  });
</script>

<div id="banner">
	<div class="wrapper">
		<div id="slideshow-glow"></div>
	</div>
</div>	

<div class="wrapper">

	<?php PLS_Route::handle_dynamic(); ?>

	<?php PLS_Route::handle_sidebar(); ?>

</div>


<?php PLS_Route::handle_footer(); ?>

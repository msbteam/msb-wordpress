<?php 
add_filter('pls_listing_get_listings_widget', 'custom_ventura_widget_html_filter', 11, 2);

function custom_ventura_widget_html_filter ($listing_html, $listing_data) {
	
// pls_dump($listing_data);

	ob_start();
    ?>

<section class="feat-listing2">

	<a href="<?php echo $listing_data['cur_data']['url']; ?>" class="feat-title"><?php echo $listing_data['location']['full_address']; ?></a>
	<section class="feat-details">
		<section class="feat-thumb-size">
			<div class="thumbs">
				<a href="<?php echo $listing_data['cur_data']['url']; ?>">
					<?php if ( isset($listing_data['images']) && is_array($listing_data['images']) ): ?>
						<?php echo PLS_Image::load($listing_data['images'][0]['url'], array('resize' => array('w' => 286, 'h' => 145, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true)); ?>
					<?php else: ?>
						<?php echo PLS_Image::load('', array('resize' => array('w' => 120, 'h' => 90, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true)); ?>
					<?php endif ?>
				</a>
			</div>
			<div class="thumbs-shadow"></div>
		</section>
		<?php if (isset($listing_data['rets']['mls_id'])) { ?>
  		<p class="mls">MLS #: <?php echo $listing_data['rets']['mls_id'] ?></p>
  	<?php } ?>
    <?php
    PLS_Listing_Helper::get_compliance(array(
											'context' => 'listings_widget',
											'agent_name' => $listing_data['rets']['aname'],
											'office_name' => $listing_data['rets']['oname'],
											'office_phone' => PLS_Format::phone($listing_data['contact']['phone']),
											'agent_license' => ( isset( $listing_data['rets']['alicense'] ) ? $listing_data['rets']['alicense'] : false ),
											'co_agent_name' => ( isset( $listing_data['rets']['aconame'] ) ? $listing_data['rets']['aconame'] : false ),
											'co_office_name' => ( isset( $listing_data['rets']['oconame'] ) ? $listing_data['rets']['oconame'] : false )
											)
										);
    ?>
		<div class="clr"></div>
	</section>

</section>


<?php
     $listing_html = ob_get_clean();

     return $listing_html;

}
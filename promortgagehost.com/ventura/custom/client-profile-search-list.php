<?php 

add_filter( 'pls_listings_list_ajax_item_html_client', 'custom_client_profile_search_list', 10, 3 );
function custom_client_profile_search_list($listing_item_html, $listing, $context_var  ) {

    // return $listing_item_html;

    /** Start output buffering. The buffered html will be returned to the filter. */
    ob_start();
    // pls_dump($listing);

?>

<section class="single-item">
	
	<section class="list-thumb-size">
		<div class="thumbs">
			<?php if (isset($listing['images'])): ?>
				<?php echo PLS_Image::load($listing['images'][0]['url'], array('resize' => array('w' => 120, 'h' => 70, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
			<?php else: ?>
				<?php echo PLS_Image::load('', array('resize' => array('w' => 120, 'h' => 70, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
			<?php endif; ?>
		</div>
	</section>

	<section class="list-thumb-txt">
		<a href="<?php echo $listing['cur_data']['url']; ?>" class="feat-title"><?php echo $listing['location']['address'] . ' ' . $listing['location']['locality'] . ', ' . $listing['location']['region'] ?></a>
	</section>

	<section class="list-thumb-details">
		<ul class="item-details">
			<li><?php echo PLS_Format::number($listing['cur_data']['price'], array('add_currency_sign' => true, 'abbreviate' => false)); ?></li>
			<li><?php echo $listing['cur_data']['beds']; ?> <span>Beds</span></li>
			<li><?php echo $listing['cur_data']['baths']; ?> <span>Baths</span></li>
			<?php if (isset($listing['cur_data']['sqft'])) { 
				echo '<li>' . PLS_Format::number($listing['cur_data']['sqft'], array('abbreviate' => false, 'add_currency_sign' => false)) . '<span> Sqft</span></li>'; 
			} ?>
		</ul>
<?php //pls_dump($listing['id']); ?>
		<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>

		<a href="<?php echo $listing['cur_data']['url']; ?>" class="seemore-btn-sml">See Details</a>
		<div class="clr"></div>

	</section>

</section>

<?php

    $html = ob_get_clean();

    // current js build throws a fit when newlines are present
    // will need to strip them. 
    // added EMCA tag will solve in the future.
    $html = preg_replace('/[\n\r\t]/', ' ', $html);
    
    return $html;
}

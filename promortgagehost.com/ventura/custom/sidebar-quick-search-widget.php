<?php 
//hook into blueprints search wiget
add_filter('pls_widget_quick_search_context', 'custom_side_search_widget_context');

// set the context so we can modify the search widget without collisions 
function custom_side_search_widget_context () {
	return 'ventura_search_widget';
}


add_filter('pls_listings_search_form_outer_ventura_search_widget', 'custom_side_search_widget_html', 10, 5);

function custom_side_search_widget_html ($form, $form_html, $form_options, $div_title, $form_data) {
	ob_start();
	?>

<section id="search-widget">

	<div class="widget-inner">

		<form method="post" action="<?php echo esc_url( home_url( '/' ) ); ?>listings" id="simple-search">

			<div id="search-widget-location">
				<div class="search-node">
					<label>City</label>
					<?php echo $form_html['cities']; ?>
				</div>
				<div class="search-node">
					<label>State</label>
					<?php echo $form_html['states']; ?>
				</div>
			</div>

			<div id="search-widget-beds-baths">
				<div class="search-node">
					<label>Bedrooms</label>
					<?php echo $form_html['bedrooms']; ?>
				</div>
				<div class="search-node">
					<label>Bathrooms</label>
					<?php echo $form_html['bathrooms']; ?>
				</div>
			</div>

			<div id="search-widget-price">
				<div class="search-node">
					<label>Price from</label>
					<?php echo $form_html['min_price']; ?>
				</div>
				<div class="search-node">
					<label>Price to</label>
					<?php echo $form_html['max_price']; ?>
				</div>
			</div>

			<!-- <div class="clearfix"></div> -->

			<input type="submit" id="search" name="submit" Value="Search" class="seemore-btn-sml" />

			<!-- <div class="clearfix"></div> -->
		</form>

	</div><!--.widget-inner-->

</section><!--#search-widget-->


	<?php
	return trim(ob_get_clean());

}

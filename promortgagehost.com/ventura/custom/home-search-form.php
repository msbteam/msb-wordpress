<?php 

add_filter('pls_listings_search_form_outer_home', 'ventura_home_search_form_html', 10, 6);

function ventura_home_search_form_html ($form, $form_html, $form_options, $section_title, $form_data) {
	// pls_dump($form_html);
	ob_start();
	?>
<div id="form">
  <h2>Quick Search</h2>
	<form action="<?php echo esc_url( home_url( '/' ) ); ?>listings" method="post" id="pls-listings-search-form-home" class="">

      <section class="bedrooms pls_search_form">
				<label for="bedrooms">Bedrooms</label>
					<?php echo $form_html['bedrooms'] ?>
			</section>
			
      <section class="bathrooms pls_search_form">
				<label for="bathrooms">Bathrooms</label>
					<?php echo $form_html['bathrooms'] ?>
			</section>

      <section class="cities pls_search_form">
				<label for="cities">City</label>
					<?php echo $form_html['cities']; ?>
			</section>

      <section class="county pls_search_form">
      <label for="county">County</label>
        <?php echo $form_html['county']; ?>
      </section>

      <section id="purchase_type_container" class="purchase_types pls_search_form">
        <label for="purchase_types">Purchase Type</label>
        <?php echo $form_html['purchase_types'] ?>
      </section>

      <section id="min_price_container" class="min_price pls_search_form">
        <label for="min_price">Min Price</label>
          <?php echo $form_html['min_price'] ?>
      </section>
      
      <section id="max_price_container" class="max_price pls_search_form">
        <label for="max_price">Max Price</label>
          <?php echo $form_html['max_price'] ?>
      </section>

      <input type="submit" value="Search" id="search_page_submit" class="btn-biggest" />
    </form>
  </div>

		<div style="display:none" id="rental_max_options">
		  	<?php echo $form_html['max_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_max_options">
			<?php echo $form_html['max_price'] ?>
		</div>
		<div style="display:none" id="rental_min_options">
		  	<?php echo $form_html['min_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_min_options">
			<?php echo $form_html['min_price'] ?>
		</div>

     <?php
    $search_form = trim(ob_get_clean());
    return $search_form;
}

<?php 

add_filter('pls_listings_search_form_outer_listings_search', 'custom_search_form_html', 10, 6);

function custom_search_form_html ($form, $form_html, $form_options, $section_title, $form_data) {
	// pls_dump($form_html);
	ob_start();
	?>


<section class="gray-box">
	<h3>Search</h3>

	<form id="allure-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>listings" method="post" class="pls_search_form_listings">

		<div id="gray-box-frmfields">
			<div class="tbl-cell-search2">
				<label class="selLabel">Bedrooms</label>
					<?php echo $form_html['bedrooms'] ?>
				<label class="selLabel">Bathrooms</label>
					<?php echo $form_html['bathrooms'] ?>
			</div>

			<div class="tbl-cell-search3">
				<label class="selLabel2">City</label>
					<?php echo $form_html['cities']; ?>
					<div id="purchase_type_container">
				    <label class="selLabel2">Transaction Type</label>
					  <?php echo $form_html['purchase_types'] ?>
					</div>
				<div class="clr"></div>
			</div>

			<div class="tbl-cell-search4">
        <div id="min_price_container">
          <label class="selLabel2">Price</label>
            <?php echo $form_html['min_price'] ?>
        </div>
        <div id="max_price_container">
          <label class="dash">&nbsp; -</label>
            <?php echo $form_html['max_price'] ?>
				</div>
			</div>
		</div>

		<div id="gray-box-btn">
			<input id="search_page_submit" type="submit" Value="Search" class="btn-biggest" />
		</div>
	</form>
		<div style="display:none" id="rental_max_options">
		  	<?php echo $form_html['max_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_max_options">
			<?php echo $form_html['max_price'] ?>
		</div>
		<div style="display:none" id="rental_min_options">
		  	<?php echo $form_html['min_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_min_options">
			<?php echo $form_html['min_price'] ?>
		</div>
	<div class="clr"></div>
</section>

     <?php
	
    $search_form = trim(ob_get_clean());
    return $search_form;
  
}

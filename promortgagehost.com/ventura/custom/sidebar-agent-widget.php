<?php 

add_filter('pls_widget_agent_inner', 'custom_side_agent_widget_html', 10, 4);

function custom_side_agent_widget_html ($post_item, $post_html, $agent_array, $instance) {

	//pls_dump($post_html);
	$agent = PLS_Plugin_API::get_user_details();
	$headshot = isset($agent['user']['headshot']) ? $agent['user']['headshot'] : '';
	
	ob_start();
?>

<section class="side-ctnr">
	<section class="getstarted">
		<section class="gt-thumb-size">
			<?php if ( $instance['photo'] != 0 ) { ?>
	      <img class="option-pls-user-image" src="<?php echo $agent_array['photo']; ?>" alt="" width=100 />
			<?php } elseif ($agent != null && $headshot) { ?>
				<img src="<?php echo $headshot; ?>" height=120 alt="<?php echo $agent['user']['first_name'] . ' ' . $agent['user']['last_name']; ?>">
			<?php } else { } ?>
			<div class="clr"></div>
			<div class="thumbs-shadow"></div>
		</section>

		<section class="gs-txt">
			<?php if ( $instance['name'] != 0 ) { ?>
				<span class="agent-name"><?php echo $agent_array['name']; ?></span><br/>
			<?php } else { ?>
				<span class="agent-name"><?php echo $agent['user']['first_name'] . ' ' . $agent['user']['last_name']; ?></span><br/>
			<?php } ?>

			<?php if ( $instance['phone'] != 0 ) { ?>
				<span class="agent-phone blue"><strong><?php echo $agent_array['phone']; ?></strong></span><br/>
			<?php } else { ?>
				<span class="agent-phone blue"><strong><?php echo $agent['user']['phone']; ?></strong></span><br/>
			<?php } ?>
			
			<?php if ( $instance['email'] != 0 ) { ?>
				<span class="agent-email"><a href="mailto:<?php echo $agent_array['email']; ?>"><?php echo $agent_array['email']; ?></a></span>
			<?php } else { ?>
				<span class="agent-email"><a href="mailto:<?php echo $agent['user']['email']; ?>"><?php echo $agent['user']['email']; ?></a></span>
			<?php } ?>
		</section>

		<div class="clr"></div>
	</section>
	<div class="clr"></div>
</section>

<?php

	return trim(ob_get_clean());
}
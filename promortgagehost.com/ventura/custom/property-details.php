<?php 

add_filter('property_details_filter', 'custom_property_details_page', 10, 2);

function custom_property_details_page ($html, $listing) {

		// pls_dump($listing);

	ob_start();
	
	?>

  <script type="text/javascript">
    jQuery(document).ready(function( $ ) {
      var map = new Map();
      var listing = new Listings({
        single_listing : <?php echo json_encode($listing) ?>,
        map: map
      });
      map.init({
        type: 'single_listing', 
        listings: listing,
        lat : <?php echo json_encode($listing['location']['coords'][0]) ?>,
        lng : <?php echo json_encode($listing['location']['coords'][1]) ?>,
        zoom : 14
      });
      listing.init();
    });
  </script>

<article class="grid_8 alpha property-details" <?php post_class() ?> id="post-<?php the_ID(); ?>">
	<section class="content-list">
		<h3 class="blue"><?php echo $listing['location']['full_address'] ?></h3>
		<ul class="basic-info">
			<li>Beds: <?php echo $listing['cur_data']['beds'] ?></li>
			<li>Baths: <?php echo $listing['cur_data']['baths'] ?></li>
			<li>Half Baths: <?php echo $listing['cur_data']['half_baths'] ?></li>
			<?php if( !empty( $listing['cur_data']['sqft'] ) ) { ?>
			<li>Square Feet: <?php echo number_format( $listing['cur_data']['sqft'] ); ?></li>
			<?php } ?>
			<li>Price: <?php echo PLS_Format::number($listing['cur_data']['price'], array('abbreviate' => false, 'add_currency_sign' => true)); ?></li>
			<?php if (isset($listing['rets']['mls_id'])) { ?>
    		<li>MLS #: <?php echo $listing['rets']['mls_id'] ?></li>
    	<?php } ?>
  		
		</ul>
		<div class="details-thumb"><!--start of details-thumb-->

			<div id="property-details-main-image">
				<?php if ($listing['images']): ?>
					<?php echo PLS_Image::load($listing['images'][0]['url'], array('resize' => array('w' => 600, 'h' => 270, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true)) ?>
				<?php else: ?>
					<?php echo PLS_Image::load(null, array('resize' => array('w' => 611, 'h' => 250, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true, 'html' => array('img_classes' => 'main-banner'))); ?>  
				<?php endif ?>
			</div>



			<section class="right-details">
				<div class="thumbs">
					<?php echo PLS_Map::dynamic($listing, array('lat' => $listing['location']['coords'][0],'lng' => $listing['location']['coords'][1],'zoom' => '14','width' => 340,'height' => 200, 'id' => 'property_details_map' )); ?>
				</div>

				<section class="desc-info">
					<?php if ($listing['cur_data']['desc']): ?>
						<h6>Description</h6>
						<p><?php echo $listing['cur_data']['desc'] ?></p>  
					<?php else: ?>
						<p>No description available at this time.</p>
					<?php endif ?>
				</section>

				<?php $amenities = PLS_Format::amenities_but($listing, array('half_baths', 'beds', 'baths', 'url', 'sqft', 'avail_on', 'price', 'desc')); ?>

				<?php if ( isset($amenities['list']) && $amenities['list'] != null ): ?>
					<?php PLS_Format::translate_amenities($amenities['list']); ?>
					<section class="details-info">
							<h6>Property Amenities</h6>
							<ul class="amenities-info">
								<?php foreach ($amenities['list'] as $amenity => $value): ?>
									<li><span><?php echo $amenity ?></span> <?php echo $value ?></li>
								<?php endforeach ?>
							</ul>
					</section>
				<?php endif; ?>

				<?php if ( isset($amenities['ngb']) && $amenities['ngb'] != null ): ?>
					<?php PLS_Format::translate_amenities($amenities['ngb']); ?>
					<section class="details-info">
							<h6>Neighborhood Amenities</h6>
							<ul class="amenities-info">
								<?php foreach ($amenities['ngb'] as $amenity => $value): ?>
									<li><span><?php echo $amenity ?></span> <?php echo $value ?></li>
								<?php endforeach ?>
							</ul>
					</section>
				<?php endif; ?>

				<?php if ( isset($amenities['uncur']) && $amenities['uncur'] != null ): ?>
					<?php PLS_Format::translate_amenities($amenities['uncur']); ?>
					<section class="details-info">
							<h6>Custom Amenities</h6>
							<ul class="amenities-info">
								<?php foreach ($amenities['uncur'] as $amenity => $value): ?>
									<li><span><?php echo $amenity ?></span> <?php echo $value ?></li>
								<?php endforeach ?>
							</ul>
					</section>
				<?php endif; ?>
			</section>

			<section class="left-details"><!--Start of left-details-thumb-->

				<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>

				<?php
				if( $listing['images'] ) {
				  foreach ($listing['images'] as $image) {
					  echo PLS_Image::load($image['url'], array('resize' => array('w' => 120, 'h' => 95, 'method' => 'crop'), 'fancybox' => true, 'as_html' => false));
					} // end foreach
				} // end if
				?>

				<div class="clr"></div>

			</section><!-- /.left-details -->
			
			<div class="clr"></div>
	<?php
		PLS_Listing_Helper::get_compliance(array(
												'context' => 'listings',
												'agent_name' => $listing['rets']['aname'],
												'office_name' => $listing['rets']['oname'],
												'office_phone' => PLS_Format::phone($listing['contact']['phone']),
												'agent_license' => ( isset( $listing['rets']['alicense'] ) ? $listing['rets']['alicense'] : false ),
												'co_agent_name' => ( isset( $listing['rets']['aconame'] ) ? $listing['rets']['aconame'] : false ),
												'co_office_name' => ( isset( $listing['rets']['oconame'] ) ? $listing['rets']['oconame'] : false )
												)
											);
	?>

		</div><!--end of details-thumb-->

		<div class="clr"></div>
	</section>
</article>

	<?php

	return ob_get_clean();
}

<?php 

add_filter('pls_listing_home', 'ventura_custom_home_listing_list', 10, 2);

function ventura_custom_home_listing_list ($listing_html, $listing_data) {
	
  // pls_dump($listing_data);

	ob_start();
?>

<section class="home-listings-featured">

	<a href="<?php echo $listing_data['cur_data']['url']; ?>" class="feat-title"><?php echo $listing_data['location']['full_address'] ?></a>
	<section class="home-listings-featured-info">
		<section class="feat-thumb-size">
			<div class="thumbs">
				<?php if ( isset($listing_data['images']) && is_array($listing_data['images']) ): ?>
					<?php echo PLS_Image::load($listing_data['images'][0]['url'], array('resize' => array('w' => 120, 'h' => 70), 'fancybox' => true)); ?>
				<?php else: ?>
					<?php echo PLS_Image::load('', array('resize' => array('w' => 120, 'h' => 70))); ?>
				<?php endif ?>
			</div>
			<div class="thumbs-shadow"></div>
			<?php if (isset($listing_data['rets']['mls_id'])) { ?>
    		<p class="mls">MLS #: <?php echo $listing_data['rets']['mls_id'] ?></p>
    	<?php } ?>
  		
		</section>

		<section class="home-listings-featured-info">
			<ul>
				<li><?php echo PLS_Format::number($listing_data['cur_data']['price'], array('add_currency_sign' => true, 'abbreviate' => false)); ?><?php if (isset($listing_data['purchase_types'][0])) { echo '&nbsp;<span>' . ucwords($listing_data['purchase_types'][0]) . '</span>'; } ?></li>
				<li><?php echo $listing_data['cur_data']['beds']; ?> <span>Beds</span><?php echo '&nbsp' . $listing_data['cur_data']['baths'] ?> <span>Baths</span></li>
				<?php if (isset($listing['cur_data']['sqft'])) { 
					echo '<li>' . PLS_Format::number($listing['cur_data']['sqft'], array('abbreviate' => false, 'add_currency_sign' => false)) . '<span> Sqft</span></li>'; 
				} ?>
			</ul>
			<a href="<?php echo $listing_data['cur_data']['url']; ?>" class="learnmore-btn-sml">Learn More</a>
		</section>
    <?php PLS_Listing_Helper::get_compliance(array(
    											'context' => 'inline_search',
												'agent_name' => $listing_data['rets']['aname'],
												'office_name' => $listing_data['rets']['oname'],
												'office_phone' => PLS_Format::phone($listing_data['contact']['phone']),
												'agent_license' => ( isset( $listing_data['rets']['alicense'] ) ? $listing_data['rets']['alicense'] : false ),
												'co_agent_name' => ( isset( $listing_data['rets']['aconame'] ) ? $listing_data['rets']['aconame'] : false ),
												'co_office_name' => ( isset( $listing_data['rets']['oconame'] ) ? $listing_data['rets']['oconame'] : false )
    											)
    										);
    ?>
		<div class="clr"></div>
	</section>

</section> 

<?php
     $listing_html = ob_get_clean();

     return $listing_html;

}
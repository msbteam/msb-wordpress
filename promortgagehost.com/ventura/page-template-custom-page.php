<?php
/**
 *
 * Template Name: Custom Page
 *
 */
?>

<div id="about-page">

	<?php while (have_posts()) : the_post(); ?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

			<h2><?php the_title(); ?></h2>
				<?php the_content(); ?>

		</article>

	<?php endwhile; ?>


</div>
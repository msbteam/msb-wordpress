// Floating div
jQuery(document).ready(function($) {
//this is the floating content

  if(! ($.browser.msie && parseInt($.browser.version, 10) == 7) ) {

    var $floatingbox = $('#floating-box');

    if($('body').length > 0){

      var bodyY = parseInt($('body').offset().top) + 300;
      var originalX = $floatingbox.css('margin-left');

      $(window).scroll(function () { 

        var scrollY = $(window).scrollTop();
        var isfixed = $floatingbox.css('position') == 'fixed';

        if($floatingbox.length > 0){

          // $floatingbox.html("scrollY : " + scrollY + ", bodyY : " + bodyY + ", isfixed : " + isfixed);

          if ( (parseInt(scrollY) - 150) > bodyY && !isfixed ) {
    		    $floatingbox.stop().css({
    		      position: 'fixed',
              top: 50
    		    });
    	    } else if ( (parseInt(scrollY) - 150) < bodyY && isfixed ) {

    	 	    $floatingbox.css({
      		    position: 'relative',
      		    top: 0,
      		    marginLeft: originalX
    	      });
          }
        }
      });
    }
  }
});


function update_price_dropdowns () {
  var transation_type = jQuery('#purchase_type_container select').val();
  if (!transation_type) {
    transation_type = 'sale';
  }

  var max_options = jQuery('#' + transation_type + '_max_options select').html();
  var min_options = jQuery('#' + transation_type + '_min_options select').html();  

  jQuery('#max_price_container select').empty().append(max_options);
  jQuery('#min_price_container select').empty().append(min_options);

  jQuery("#min_price_container select, #max_price_container select").trigger("liszt:updated");
}


jQuery( document ).ready( function($) {

  update_price_dropdowns();

  $("#purchase_type_container select").change(function() {
      update_price_dropdowns();    
  });

});

// Fix Drop down menus over slideshow in ie7
jQuery( document ).ready( function($) {

  if($.browser.msie && parseInt($.browser.version, 10) == 7) {

       var zIndexNumber = 1000;
       // Put your target element(s) in the selector below!
       $(".primary ul, .primary li, .primary a").each(function() {
               $(this).css('zIndex', zIndexNumber);
               zIndexNumber -= 10;
       });
 }
});
<?php 

add_filter( 'pls_listings_list_ajax_item_html_custom_listings_search', 'columbus_custom_listings_search_list', 10, 3 );
function columbus_custom_listings_search_list($listing_item_html, $listing, $context_var  ) {

    // return $listing_item_html;

    /** Start output buffering. The buffered html will be returned to the filter. */
    ob_start();
    // pls_dump($listing);

?>

<section class="lu">	
	<div class="lu-left">
		<?php if (isset($listing['images'])): ?>
			<?php echo PLS_Image::load($listing['images'][0]['url'], array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
		<?php else: ?>
			<?php echo PLS_Image::load('', array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
		<?php endif; ?>
		<p class="price"><span>
			<?php echo PLS_Format::number($listing['cur_data']['price'], array('add_currency_sign' => true, 'abbreviate' => false)); ?></span>
			<?php if (isset($listing['purchase_types'][0])) { echo ucwords($listing['purchase_types'][0]); } ?></p>
			


	</div><!--lu-left-->

	<div class="lu-right">
		<h4><a href="<?php echo $listing['cur_data']['url']; ?>"><?php echo $listing['location']['address'] . ' ' . $listing['location']['locality'] . ', ' . $listing['location']['region'] ?></a></h4>

		<?php if (isset($listing['rets']['mls_id']) && !empty($listing_data['rets']['mls_id']) ) { ?>
		  <?php pls_dump($listing['rets']['mls_id']) ?>
  		<p class="mls">MLS #: <?php echo $listing['rets']['mls_id'] ?></p>
  	<?php } ?>
  	
		<p class="desc"><?php echo $listing['cur_data']['desc']; ?></p>

		<div class="clearfix"></div>

		<a class="details" href="<?php echo $listing['cur_data']['url']; ?>">See Details</a>

		<div class="lu-details">
			<ul>
				<li><?php echo $listing['cur_data']['beds']; ?> <span>Beds</span></li>
				<li><?php echo $listing['cur_data']['baths']; ?> <span>Baths</span></li>
				<?php if (isset($listing['cur_data']['sqft'])) { 
					echo '<li>' . PLS_Format::number($listing['cur_data']['sqft'], array('abbreviate' => false, 'add_currency_sign' => false)) . '<span> Sqft</span></li>'; 
				} ?>
			</ul>

			<div class="lu-links">
				<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>

				<?php $api_whoami = PLS_Plugin_API::get_user_details(); ?>

				<?php if (pls_get_option('pls-user-email')) { ?>
					<a class="info" href="mailto:<?php echo pls_get_option('pls-user-email'); ?>" target="_blank">Request More Information</a>
				<?php } else { ?>
					<a class="info" href="mailto:<?php echo $api_whoami['user']['email']; ?>" target="_blank">Request More Information</a>
				<?php } ?>

			</div><!--lu-links-->
		</div><!--lu-details-->
	</div><!--lu-right-->	
	<?php
	PLS_Listing_Helper::get_compliance(array(
										'context' => 'inline_search',
										'agent_name' => $listing['rets']['aname'],
										'office_name' => $listing['rets']['oname'],
										'office_phone' => PLS_Format::phone($listing['contact']['phone']),
										'agent_license' => ( isset( $listing['rets']['alicense'] ) ? $listing['rets']['alicense'] : false ),
										'co_agent_name' => ( isset( $listing['rets']['aconame'] ) ? $listing['rets']['aconame'] : false ),
										'co_office_name' => ( isset( $listing['rets']['oconame'] ) ? $listing['rets']['oconame'] : false )
										)
									); ?>

<div class="clearfix"></div>

</section><!--LU-->

    <?php

    $html = ob_get_clean();

    // current js build throws a fit when newlines are present
    // will need to strip them. 
    // added EMCA tag will solve in the future.
    $html = preg_replace('/[\n\r\t]/', ' ', $html);
    
    return $html;
}

add_filter('pls_listing_list_ajax_data_request', 'columbus_custom_listing_ajax_data_filter');
function columbus_custom_listing_ajax_data_filter ($listings) {
  
  foreach ($listings as $listing) {

    //format price
    $listing->price = PLS_Format::number($listing->price, array('add_currency_sign' => true, 'abbreviate' => true));
    
    //format images
    if (isset($listing->images) && is_array($listing->images) ) {
      foreach ($listing->images as $index => $image) {
        // pls_dump($image->url);
        // pls_dump(PLS_Image::load($image->url, array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop') ) ));
        // $listings->images[$index]->url = PLS_Image::load($image->url, array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop') ) );
      }
    }
  }
  
  return $listings;
}

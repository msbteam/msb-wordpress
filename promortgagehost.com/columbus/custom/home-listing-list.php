<?php 

add_filter('pls_listing_home', 'columbus_custom_home_listing_list', 10, 2);

function columbus_custom_home_listing_list ($listing_html, $listing_data) {
	
  // pls_dump($listing_data);

	ob_start();
    ?>

<section class="lu">
	<div class="lu-left">
		<?php if ( isset($listing_data['images']) && is_array($listing_data['images']) ): ?>
			<?php echo PLS_Image::load($listing_data['images'][0]['url'], array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true)); ?>
		<?php else: ?>
			<?php echo PLS_Image::load('', array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true)); ?>
		<?php endif ?>

		<p class="price"><span>
		<?php echo PLS_Format::number($listing_data['cur_data']['price'], array('add_currency_sign' => true, 'abbreviate' => false)); ?></span>
		<?php if (isset($listing_data['purchase_types'][0])) { echo ucwords($listing_data['purchase_types'][0]); } ?></p>

		
	</div><!--lu-left-->
	
	<div class="lu-right">
		<h4><a href="<?php echo $listing_data['cur_data']['url']; ?>"><?php echo $listing_data['location']['full_address'] ?></a></h4>

		<?php if (isset($listing_data['rets']['mls_id']) && !empty($listing_data['rets']['mls_id'])) { ?>
  		<p class="mls">MLS #: <?php echo $listing_data['rets']['mls_id'] ?></p>
  	<?php } ?>

		<?php
			if (isset($listing_data['cur_data']['desc'])) {
				echo '<p class="desc">';
				if (strlen($listing_data['cur_data']['desc']) < 200) {
					echo $listing_data['cur_data']['desc'];
				} else {
					$position = strrpos( substr( $listing_data['cur_data']['desc'], 0, 200), ' ' );
					echo substr( $listing_data['cur_data']['desc'], 0, $position ) . '...';
				}
				echo '</p>';
			} ?>

		<div class="clearfix"></div>

		<a class="details" href="<?php echo $listing_data['cur_data']['url']; ?>">See Details</a>

		<div class="lu-details">
			<ul>
				<li><?php echo $listing_data['cur_data']['beds']; ?> <span>Beds</span></li>
				<li><?php echo $listing_data['cur_data']['baths'] ?> <span>Baths</span></li>
				<?php if (isset($listing_data['cur_data']['sqft'])) { 
					echo '<li>' . PLS_Format::number($listing_data['cur_data']['sqft'], array('abbreviate' => false, 'add_currency_sign' => false)) . '<span> Sqft</span></li>'; 
				} ?>
			</ul>

			<div class="lu-links">
				<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing_data['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>

				<?php $api_whoami = PLS_Plugin_API::get_user_details(); ?>

				<?php if (pls_get_option('pls-user-email')) { ?>
					<a class="info" href="mailto:<?php echo pls_get_option('pls-user-email'); ?>" target="_blank">Request More Information</a>
				<?php } else { ?>
					<a class="info" href="mailto:<?php echo $api_whoami['user']['email']; ?>" target="_blank">Request More Information</a>
				<?php } ?>

			</div><!--lu-links-->
		</div><!--lu-details-->
	</div><!--lu-right-->	
    <?php 
    	PLS_Listing_Helper::get_compliance(array(
				'context' => 'inline_search',
				'agent_name' => $listing_data['rets']['aname'],
				'office_name' => $listing_data['rets']['oname'],
				'office_phone' => ( isset($listing_data['rets']['ophone']) ? PLS_Format::phone($listing_data['rets']['ophone']) : false ),
				'agent_license' => ( isset($listing_data['rets']['alicense']) ? $listing_data['rets']['alicense'] : false ),
				'co_agent_name' => ( isset($listing_data['rets']['aconame']) ? $listing_data['rets']['aconame'] : false ),
				'co_office_name' => ( isset($listing_data['rets']['oconame']) ? $listing_data['rets']['oconame'] : false )
			)
		);
    ?>

	<div class="clearfix"></div>
</section><!--LU-->

     <?php
     $listing_html = ob_get_clean();

     return $listing_html;

}
<?php 

add_filter('pls_widget_recent_posts_post_inner', 'columbus_custom_side_recent_posts_widget_html', 10, 2);

function columbus_custom_side_recent_posts_widget_html ($post_item, $post_html) {
	
	//pls_dump($post_html);

	ob_start();
	?>

<section id="recent-widget">

	<div class="post-recent">
    <?php echo $post_html['post_title'] ?>
		<p class="p-info">Posted <?php echo $post_html['author'] ?> <?php echo $post_html['date'] ?></p>
		<p class="p-desc"><?php echo $post_html['excerpt']; ?></p>
	</div>

</section>

	<?php
	return trim(ob_get_clean());

}

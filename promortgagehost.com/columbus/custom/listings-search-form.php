<?php 

add_filter('pls_listings_search_form_outer_listings_search', 'columbus_custom_search_form_html', 10, 6);

function columbus_custom_search_form_html ($form, $form_html, $form_options, $section_title, $form_data) {
	// pls_dump($form_html);
	ob_start();
	?>

<section id="full-search">
	<h3>Search Listing</h3>
	<div id="full-form">

		<form method="post" action="<?php echo esc_url( home_url( '/' ) ); ?>listings" class="pls_search_form_listings">
			
			<div class="form-l">
				<h6>Location</h6>
				<label>City</label>
				<?php echo $form_html['cities']; ?>
				<label>State</label>
				<?php echo $form_html['states']; ?>
				<label>Zipcode</label>
				<?php echo $form_html['zips'] ?>
			</div><!--form-l-->
			
			<div class="form-m">
				<h6>Listing Type</h6>
				<label>Zoning Type</label>
				<?php echo $form_html['zoning_types'] ?>
				<div id="purchase_type_container">
				  <label>Transaction Type</label>
				  <?php echo $form_html['purchase_types'] ?>
				</div>
				<label>Property Type</label>
				<?php echo $form_html['property_type'] ?>
			</div><!--form-m-->
			
			<div class="form-r">
				<div class="form-r-l">
					<h6>Details</h6>
					<label>Bed(s)</label>
					<?php echo $form_html['bedrooms'] ?>
					<label>Bath(s)</label>
					<?php echo $form_html['bathrooms'] ?>
				</div><!--form-r-l-->
				
				<div class="form-r-r">
					<h6>Price Range</h6>
					<div id="min_price_container">
					  <label>Price From</label>
						<select name="metadata[min_price]" id="min_price">
					  		<option></option>
					  	</select>
          		</div>

			    <div id="max_price_container">
					  <label>Price To</label>
					  <select name="metadata[max_price]" id="max_price">
					  	<option></option>
					  </select>
			    </div>
				</div><!--form-r-r-->
				
				<div class="clr"></div>
				<div id="available-on">
					<!--<label>Available On</label>
					<?php //echo $form_html['available_on'] ?> -->
				</div>
			</div><!--form-r-->

			<input type="submit" name="submit" value="Search Now!" id="search-button">
			
			<div class="clearfix"></div>
		</form>
		<div style="display:none" id="rental_max_options">
		  	<?php echo $form_html['max_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_max_options">
			<?php echo $form_html['max_price'] ?>
		</div>
		<div style="display:none" id="rental_min_options">
		  	<?php echo $form_html['min_price_rental'] ?>
		</div>
		<div style="display:none" id="sale_min_options">
			<?php echo $form_html['min_price'] ?>
		</div>
	</div><!--full-form-->
</section>

     <?php
	
    $search_form = trim(ob_get_clean());
    return $search_form;
  
}
  
   
<?php 

// Hook into the slideshow extension
add_filter('pls_slideshow_data_home', 'columbus_custom_slideshow_captions', 10, 1);

function columbus_custom_slideshow_captions ($data) {
	// pls_dump($data);

	if (is_array($data) ) {
	
		unset($data['captions']);

		// each full represents a listing we need to work with.
		// their position in the array matches the other relavent
		// info. 
		if(isset($data['listing'])) {
			foreach ($data['listing'] as $index => $listing) {
			
				// pls_dump($data);
	            /** Get the listing caption. */
			ob_start();
				?>

		<div id="caption-<?php echo $index ?>" class="orbit-caption">
			<p class="address"><a href="<?php echo $listing['cur_data']['url'] ?>"><?php echo $listing['location']['full_address']; ?></a><a class="details-bt" href="<?php echo $listing['cur_data']['url'] ?>">See Details</a></p>
			<p class="basic-details">
				<span class="price-ico"><?php echo PLS_Format::number($listing['cur_data']['price'], array('abbreviate' => false, 'add_currency_sign' => true)); ?></span>
				<span class="beds-ico"><?php echo $listing['cur_data']['beds']; ?> Beds</span>
				<span class="baths-ico"><?php echo $listing['cur_data']['baths']; ?> Baths</span>
			</p>
		
		</div>
	
	<?php 
	
				$data['captions'][] = trim( ob_get_clean() );
			}
		}
	}

	return $data;

}

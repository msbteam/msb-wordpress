<?php 

add_filter('pls_widget_agent_inner', 'columbus_custom_side_agent_widget_html', 10, 2);

function columbus_custom_side_agent_widget_html ($post_item, $post_html) {

	//pls_dump($post_html);
	$agent = PLS_Plugin_API::get_user_details();

	ob_start();
?>

<section id="agent-widget">
	<div class="widget-inner">
		<?php if (pls_get_option('pls-user-image') != null) { ?>
			<img class="option-pls-user-image" src="<?php echo pls_get_option('pls-user-image'); ?>" height=120 alt="<?php echo pls_get_option('pls-user-name'); ?>">
		<?php } elseif (!isset($agent)) { ?>
			<img class="option-pls-user-image" src="<?php echo $agent['user']['headshot']; ?>" height=120 alt="<?php echo pls_get_option('pls-user-name'); ?>">
		<?php } ?>

		<?php if (pls_get_option('pls-user-name')) { ?>
			<h5 class="option-pls-user-name"><?php echo pls_get_option('pls-user-name') ?></h5>
		<?php } else { ?>
			<h5 class="option-pls-user-name"><?php echo $agent['user']['first_name'] . ' ' . $agent['user']['last_name']; ?></h5>
		<?php } ?>

		<?php if (pls_get_option('pls-user-description')) { ?>
			<p class="option-pls-user-description"><?php echo pls_get_option('pls-user-description'); ?></p>
		<?php } ?>

		<?php if (pls_get_option('pls-user-name')) { ?>
			<span class="email"><a href="mailto:<?php echo pls_get_option('pls-user-email') ?>" class="option-pls-user-email"><?php echo pls_get_option('pls-user-email'); ?></a></span>
		<?php } else { ?>
			<span class="email"><a href="mailto:<?php echo $agent['user']['email']; ?>" class="option-pls-user-email"><?php echo $agent['user']['email']; ?></a></span>
		<?php } ?>

		<div class="clearfix"></div>

		<?php if (pls_get_option('pls-user-phone')) { ?>
			<span class="phone option-pls-user-phone"><?php echo pls_get_option('pls-user-phone'); ?></span>
		<?php } else { ?>
			<span class="phone option-pls-user-phone"><?php echo PLS_Format::phone($agent['user']['phone']); ?></span>
		<?php } ?>
	</div><!--inner-widget-->
</section><!--agent-widget-->

<?php

	return trim(ob_get_clean());
}
<?php 

add_filter('property_details_filter', 'columbus_custom_property_details_page', 10, 2);

function columbus_custom_property_details_page ($html, $listing) {

		// pls_dump($listing);

	ob_start();
	
	?>

<section id="fold">

	<div class="fold-l">
		<h2><?php echo $listing['location']['full_address'] ?></h2>
		<p><span><?php echo $listing['cur_data']['sqft'] ?></span> Sqft</p>
		<p><span><?php echo $listing['cur_data']['beds'] ?></span> Bed(s)</p>
		<p><span><?php echo $listing['cur_data']['baths'] ?></span> Bath(s)</p>
		<p><span><?php echo $listing['cur_data']['half_baths'] ?></span> Half Bath(s)</p>
		<div class="clearfix"></div>
		<?php if ($listing['images']): ?>
			<?php echo PLS_Image::load($listing['images'][0]['url'], array('resize' => array('w' => 611, 'h' => 265, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true, 'html' => array('img_classes' => 'main-banner'))); ?>
		<?php else: ?>
			<?php echo PLS_Image::load(null, array('resize' => array('w' => 611, 'h' => 250, 'method' => 'crop'), 'fancybox' => false, 'as_html' => true, 'html' => array('img_classes' => 'main-banner'))); ?>  
		<?php endif ?>
	</div><!--fold-l-->

	<div class="fold-r">
		<p class="price"><?php echo PLS_Format::number($listing['cur_data']['price'], array('abbreviate' => false, 'add_currency_sign' => true)); ?></p>

		<section id="location-widget">
			<h3 class="gr">Location Map</h3>
			<?php echo PLS_Map::dynamic($listing, array(
				'lat' => $listing['location']['coords']['0'],
				'lng' => $listing['location']['coords']['1'],
				'zoom' => '14',
				'width' => 287,
				'height' => 208,
				'canvas_id' => 'map_canvas',
		      	'class' => 'custom_google_map',
		      	'map_js_var' => 'pls_google_map',
		      	'ajax_form_class' => false,
			)); ?>
		</section>
		
	    <script type="text/javascript">
	      jQuery(document).ready(function( $ ) {
	        var map = new Map();
	        var listing = new Listings({
	          single_listing: <?php echo json_encode($listing) ?>,
	          map: map
	        });
	        map.init({
	          type: 'single_listing', 
	          listings: listing,
	          lat : <?php echo json_encode($listing['location']['coords'][0]) ?>,
	          lng : <?php echo json_encode($listing['location']['coords'][1]) ?>,
	          zoom : 14
	        });
	        listing.init();
	      });
    	</script>
	</div><!--fold-r-->

	<div class="clearfix"></div>

</section><!--fold-->

<div id="main">
	<p class="p-info"><span>Listing Type:</span> <?php echo ucwords($listing['purchase_types'][0]); ?></p>
	<?php if($listing['property_type'] != null) { ?>
		<p class="p-info"><span>Property Type:</span> <?php echo PLS_Format::translate_property_type($listing); ?></p>
	<?php } ?>
	<?php if (isset($listing['rets']['mls_id'])): ?>
		<p class="p-info"><span>MLS #:</span> <?php echo $listing['rets']['mls_id'] ?></p>
	<?php else: ?>
		<p class="p-info">Ref # <span><?php echo $listing['id'] ?></p>
	<?php endif ?>

	<div class="clearfix"></div>

	<div class="user-generated">
		<h2>Description</h2>
		<?php if ($listing['cur_data']['desc']): ?>
			<p><?php echo $listing['cur_data']['desc'] ?></p>  
		<?php else: ?>
			<p>No description available at this time.</p>
		<?php endif ?>
	</div><!--user-generated-->

	<?php $amenities = PLS_Format::amenities_but($listing, array('half_baths', 'beds', 'baths', 'url', 'sqft', 'avail_on', 'price', 'desc')); ?>
	<?php if ( isset($amenities['list']) && $amenities['list'] != null ): ?>
		<?php $amenities['list'] = PLS_Format::translate_amenities($amenities['list']); ?>
		<div class="main-widget amenities-widget">
			<h3><?php echo pls_get_option('pls-option-details-prop-amenities', 'Property Amenities'); ?></h3>
			    <div class="widget-inner">
						<ul id="bulleted">
							<?php foreach ($amenities['list'] as $amenity => $value): ?>
								<?php if($value != ' ') { ?>
								<li><span><?php echo $amenity ?></span>: <?php echo $value ?></li>
								<?php } ?>
							<?php endforeach ?>
						</ul>          
						<div class="clearfix"></div>
					</div> <!--widget-inner-->
		</div><!--main-widget-->
	<?php endif; ?>

	<?php if (isset($amenities['ngb']) && $amenities['ngb'] != null ): ?>
		<?php $amenities['ngb'] = PLS_Format::translate_amenities($amenities['ngb']); ?>
		<div class="main-widget amenities-widget">
			<h3><?php echo pls_get_option('pls-option-details-neb-amenities', 'Neighborhood Amenities'); ?></h3>
			    <div class="widget-inner">
						<ul id="bulleted">
							<?php foreach ($amenities['ngb'] as $amenity => $value): ?>
								<?php if($value != ' ') { ?>
								<li><span><?php echo $amenity ?></span>: <?php echo $value ?></li>
								<?php } ?>
							<?php endforeach ?>
						</ul>          
						<div class="clearfix"></div>
					</div> <!--widget-inner-->
		</div><!--main-widget-->
	<?php endif; ?>

	<?php if ( isset($amenities['uncur']) && $amenities['uncur'] != null ): ?>
		<?php $amenities['uncur'] = PLS_Format::translate_amenities($amenities['uncur']); ?>
		<div class="main-widget amenities-widget">
			<h3><?php echo pls_get_option('pls-option-details-cust-amenities', 'Custom Amenities'); ?></h3>
			    <div class="widget-inner">
						<ul id="bulleted">
							<?php foreach ($amenities['uncur'] as $amenity => $value): ?>
								<?php if($value != ' ') { ?>
								<li><span><?php echo $amenity ?></span>: <?php echo $value ?></li>
                <?php } ?>
							<?php endforeach ?>
						</ul>          
						<div class="clearfix"></div>
					</div> <!--widget-inner-->
		</div><!--main-widget-->
	<?php endif; ?>

	<div class="main-widget neighborhood-widget">
		<h3><?php echo pls_get_option('pls-option-details-map', 'Neighborhood'); ?></h3>
		<?php echo PLS_Map::dynamic($listing, array(
			'lat' => $listing['location']['coords'][0],
			'lng' => $listing['location']['coords'][1],
			'zoom' => '14',
			'width' => 610,
			'height' => 280,
			'canvas_id' => 'map_canvas1',
			'class' => 'custom_google_map1',
			'map_js_var' => 'pls_google_map1',
			'ajax_form_class' => false,
		)); ?>
		  
      <script type="text/javascript">
        jQuery(document).ready(function( $ ) {
          var map1 = new Map();
          var listing1 = new Listings({
            single_listing : <?php echo json_encode($listing) ?>,
            map: map1
          });
          map1.init({
            type: 'single_listing', 
            dom_id: 'map_canvas1',
            listings: listing1,
            lat : <?php echo json_encode($listing['location']['coords'][0]) ?>,
            lng : <?php echo json_encode($listing['location']['coords'][1]) ?>,
            zoom : 14
          });
          listing1.init();
        });
      </script>
	</div><!--main-widget-->

	<?php PLS_Listing_Helper::get_compliance(array(
												'context' => 'listings',
												'agent_name' => $listing['rets']['aname'],
												'office_name' => $listing['rets']['oname'],
												'office_phone' => PLS_Format::phone($listing['contact']['phone']),
												'agent_license' => ( isset( $listing['rets']['alicense'] ) ? $listing['rets']['alicense'] : false ),
												'co_agent_name' => ( isset( $listing['rets']['aconame'] ) ? $listing['rets']['aconame'] : false ),
												'co_office_name' => ( isset( $listing['rets']['oconame'] ) ? $listing['rets']['oconame'] : false )
												)
											);
	?>

</div><!--main-->

<aside>

	<div class="sidebar-add-to-favorites-link">
		<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>
	</div>

	<?php if ($listing['images']): ?>
	<section id="gallery-widget">
		<h3 class="grr">Photo Gallery</h3>
		<div class="widget-inner">
			<?php foreach ($listing['images'] as $image): ?>
				<?php echo PLS_Image::load($image['url'], array('resize' => array('w' => 120, 'h' => 95, 'method' => 'crop'), 'fancybox' => true, 'as_html' => false)) ?>
			<?php endforeach ?>
			<div class="clearfix"></div>
		</div><!--widget-inner-->
	</section><!--SEARCH-->
	<?php endif; ?>

<?php
  // This would normally work if we had not already opened the <aside> for the hard photo gallery
  // PLS_Route::handle_sidebar('single-property');
  // Instead, we have to put that widget in here directly, testing for it first
  if ( is_active_sidebar( 'single-property' ) ) {
		dynamic_sidebar( 'single-property' );
  }
?>

</aside>

<?php

	return ob_get_clean();
}

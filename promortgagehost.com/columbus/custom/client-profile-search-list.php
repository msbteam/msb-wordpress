<?php 

add_filter( 'pls_listings_list_ajax_item_html_client', 'columbus_custom_client_profile_search_list', 10, 3 );
function columbus_custom_client_profile_search_list($listing_item_html, $listing, $context_var  ) {

    // return $listing_item_html;

    /** Start output buffering. The buffered html will be returned to the filter. */
    ob_start();
    // pls_dump($listing);

?>

<section class="lu">
	
	<div class="lu-left">
		<?php if (isset($listing['images'])): ?>
			<?php echo PLS_Image::load($listing['images'][0]['url'], array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
		<?php else: ?>
			<?php echo PLS_Image::load('', array('resize' => array('w' => 149, 'h' => 90, 'method' => 'crop'), 'fancybox' => true, 'as_html' => true)); ?>
		<?php endif; ?>
		<p><span>
			<?php echo PLS_Format::number($listing['cur_data']['price'], array('add_currency_sign' => true, 'abbreviate' => false)); ?></span>
			<?php if (isset($listing['purchase_types'][0])) { echo ucwords($listing['purchase_types'][0]); } ?></p>
	</div><!--lu-left-->

	<div class="lu-right">
		<h4><a href="<?php echo $listing['cur_data']['url']; ?>"><?php echo $listing['location']['address'] . ' ' . $listing['location']['locality'] . ', ' . $listing['location']['region'] ?></a></h4>
		<?php if (isset($listing['mls_number'])) { ?>
			<p class="mls">MLS# <?php echo $listing['mls_number'] ?> | <?php echo $listing['mls_group'] ?></p>
		<?php } ?>
		<p class="desc"><?php echo $listing['cur_data']['desc']; ?></p>

		<div class="clearfix"></div>

		<a class="details" href="<?php echo $listing['cur_data']['url']; ?>">See Details</a>

		<div class="lu-details">
			<ul>
				<li><?php echo $listing['cur_data']['beds']; ?> <span>Beds</span></li>
				<li><?php echo $listing['cur_data']['baths']; ?> <span>Baths</span></li>
				<?php if (isset($listing['cur_data']['sqft'])) { 
					echo '<li>' . PLS_Format::number($listing['cur_data']['sqft'], array('abbreviate' => false, 'add_currency_sign' => false)) . '<span> Sqft</span></li>'; 
				} ?>
			</ul>

			<div class="lu-links">
				<?php echo PLS_Plugin_API::placester_favorite_link_toggle(array('property_id' => $listing['id'], 'add_text' => 'Add To Favorites', 'remove_text' => 'Remove From Favorites')); ?>

				<?php $api_whoami = PLS_Plugin_API::get_user_details(); ?>

				<?php if (pls_get_option('pls-user-email')) { ?>
					<a class="info" href="mailto:<?php echo pls_get_option('pls-user-email'); ?>" target="_blank">Request More Information</a>
				<?php } else { ?>
					<a class="info" href="mailto:<?php echo $api_whoami['user']['email']; ?>" target="_blank">Request More Information</a>
				<?php } ?>

			</div><!--lu-links-->
		</div><!--lu-details-->
	</div><!--lu-right-->	

	<div class="clearfix"></div>

</section><!--LU-->

    <?php

    $html = ob_get_clean();

    // current js build throws a fit when newlines are present
    // will need to strip them. 
    // added EMCA tag will solve in the future.
    $html = preg_replace('/[\n\r\t]/', ' ', $html);
    
    return $html;
}
<?php
/**
 * Template Name: Client Profile
 *
 * This is the template for the loggedin area of clients
 *
 * @package PlacesterBlueprint
 * @subpackage Template
 */
?>

<?php if (is_user_logged_in()): ?>
<h1>Your Favorite Listings</h1>
<div class="grid_8 alpha" id="content" role="main">
    <?php echo PLS_Partials::get_listings_list_ajax('crop_description=1&context=custom_listings_search'); ?>
</div>
<?php else: ?>
<h3 id="signup-title">Please Login or Sign Up</h3>
<p id="signup-explanation">Logging in is completely free and helps you keep tracking and save your favorite listings in one place.</p>
<div id="main-area-login">
	<?php echo PLS_Plugin_API::placester_lead_control_panel(array('separator' => '')); ?>
</div>
<?php endif ?>

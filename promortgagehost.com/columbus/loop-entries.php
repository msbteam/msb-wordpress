<?php
/**
 * Loop Entries Templates
 *
 * Loops over a list entries and displays them. It is include on the archive and blog pages.
 *
 * @package PlacesterBlueprint
 * @subpackage Template
 */
 
 // $cache_id = $args;
 // if($_SERVER['REQUEST_METHOD'] === 'POST') {
 //   $cache_id['$_POST'] = $_POST;
 // }

?>
<div id="blog-post-feed">

<?php if (have_posts()) : ?>

  <?php
    $cache = new PLS_Cache('Loop Entries');
    if ($result = $cache->get($wp_query)) {
      return $result;
    }
    ob_start();
  ?>

    <?php while (have_posts()) : the_post(); ?>

<?php
		// needed to honor MORE tags when loop shows in page templates -pek
		global $more;
		$more = 0;
	?>

        <!--BLOG POST STARTS HERE-->
        <div <?php post_class( 'blog-post' ) ?> id="post-<?php the_ID(); ?>">
        <?php
        $current_title = the_title( '', '', false );
        if( empty( $current_title ) ) {
          $current_title = 'No Title';
        }
        ?>
						<div class="post-top">
	          	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf( 'Permalink to %1$s', the_title_attribute( 'echo=false' ) ) ?>"><?php echo $current_title; ?></a></h2>
	            <p class="post-info">Posted on <span><?php the_time('F jS, Y') ?></span> by <?php the_author(); ?></p>
	          </div><!--post-top-->
	          <div class="post-main">
							<?php the_excerpt(); ?>
							<a class="read-more" href="<?php the_permalink() ?>">Read More</a>
	            <!-- <a class="post-comment" href="#">Post a Comment</a>
	            <a class="read-comment" href="#">Read Comment</a> -->

							<div class="blog-tags">
								<?php the_tags(); ?>
							</div>

	            <div class="clearfix"></div>
	          </div><!--post-main-->
          </div><!--BLOG-POST-->

    <?php endwhile; ?>

    <?php $return = ob_get_clean(); ?>
    <?php $cache->save($return); ?>
      
    <?php echo $return; ?>

    <nav class="posts">
        <div class="prev"><?php next_posts_link( __( '&laquo; Older Entries', pls_get_textdomain() ) ) ?></div>
        <div class="next"><?php previous_posts_link( __( 'Newer Entries &raquo;', pls_get_textdomain() ) ) ?></div>
    </nav>
    
<?php else : ?>
    
    <?php get_template_part( 'loop-error' ); ?>
    
<?php endif; ?>

</div><!-- /#blog-post-feed -->
=== Columbus for Placester ===
Author: The Placester Team
Contributors: The Placester Team
Tags: blue, red, green, right-sidebar, fixed-width, custom-menu
Requires at least: 3.0
Tested up to: 3.9
Stable tag: 2.5

A highly customizable theme for real estate professionals complimented by [Real Estate Website Builder](http://http://wordpress.org/extend/plugins/placester/) plugin by [Placester](https://placester.com/wordpress-themes/manchester).

== Description ==

The Columbus theme in Placester's is designed from the ground up for real estate professionals. With listing management, Google Maps integration, advanced property search, and complete control over personalization, Placester's themes are all you need to have an instant-on web site quickly.

It goes without saying that the goal of a real estate website is to drive more leads. But what’s the best way to do this? Is it better to dazzle visitors with digital velvet and mahoghany? Or should you ditch the expensive decor and let your inventory speak for itself? With Columbus, you don’t have to choose. Columbus’ borderless look will give your website an understated elegance, while its green and blue accents will make your listing details jump off the page. Perfect for both individual agents and single-office agencies, Columbus is ideal for displaying multiple property listings, with a two-column layout and top-level page navigation. It’s also fully customizable, so you can make it your own, and SEO optimized, which means you’ll get even more compliments on your feng shui. Use with <a href="https://placester.com/wordpress-themes/columbus">Placester</a>'s <a href="wordpress.org/extend/plugins/placester/">Real Estate Builder plugin</a>.

= Highlights =

* Search by listings location, price, # of bedrooms and bathrooms and more
* Create listings of any type: sales, rentals, vacation, etc.
* Customize the listings that appear on the site using the Placester controls
* Track leads using the Placester platform
* Controls for theme customization: logo, tagline, featured properties and more
* Each listing has it's own landing page automatically created
* MLS integration available

= Features =

* Fully-featured blog with threaded comment support and more
* Clean and semantic HTML5 and CSS3 code
* Search engine friendly
* Supports native menus controls
* Compatible with all major browsers

= License =

[GNU General Public License](http://www.gnu.org/licenses/gpl-2.0.html)

== Installation ==

= Via WordPress Admin =

1. From WordPress Admin, go to Themes > Install Themes
2. In the search box, type "Placester" and press enter
3. Locate the entry for "Columbus" and click the "Install" link
4. When the installation is finished, click the "Activate" link
5. Next, install the Real Estate Builder Plugin found here: http://wordpress.org/extend/plugins/placester/
6. In WordPress admin, visit Plugins > Add New
7. Search for "placester"
8. Click the "Install Now" link and click "Ok" (if necessary) in the pop-up dialog.
9. Once installed, click the "Activate Plugin" link.
10. Via the "Placester" left-hand menu, and Plugin Settings tab, add your API key and configure the plugin.

= Manual Install =

1. Unzip this file into domain.com/wp-content/themes/ directory, should look like mywebsite.com/wp-content/themes/columbus/
2. Go into WordPress Admin, navigate to Appearance > Themes
3. Find the "Columbus" listing on this page and click "Activate"
4. Next, download the Real Estate Builder Plugin found here: http://wordpress.org/extend/plugins/placester/
5. In WordPress admin, visit "Plugins" > "Add New" > "Upload"
6. Upload placester.zip file
7. Once uploaded, click the "Activate Plugin" link
8. Via the "Placester" left-hand menu, and Plugin Settings tab, add your API key and configure the plugin.

== Frequently Asked Questions ==

= Who is Placester for? =

Any professional seeking the competitive advantage of having a robust real estate web site.

= How do I get started? =

After installing the plugin simply follow the instructions in the plugin to obtain an API key and begin either adding listings or [notify us](mailto:support@placester.com) if you already work with a property database company so we can enable the integration for your account.

= Why do I need an API key? =

Placester offers lots of tools on our platform powered by our robust API. So using the API makes your data portable and allows you to use any of our products to interact with your account and your listings.

= How do I get support? =

Easy, simply [reach out](mailto:support@placester.com) to us and let us know how we can help!

= I want a feature that you have not included, can you add it? =

Absolutely, [reach out](mailto:support@placester.com) to us and let us know what features would help you generate more leads easier!

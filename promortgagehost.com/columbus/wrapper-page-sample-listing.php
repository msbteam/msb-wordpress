<?php PLS_Route::handle_header(); ?>

<div id="inner">
	<?php  PLS_Route::handle_dynamic(); ?>

	<aside>
		<div id="floating-box">
			<section id="map">
				<h3 class="gr">Location Map</h3>
				<?php echo PLS_Map::dynamic(null, array('lat' => -74,'lng' => 40,'zoom' => '11','width' => 286,'height' => 230, 'class' => ' ')); ?>
			</section><!--map-widget-->
		</div><!--floating-box-->
	</aside>

	<div class="clearfix"></div>

</div> <!-- end inner -->
 
<?php PLS_Route::handle_footer(); ?>
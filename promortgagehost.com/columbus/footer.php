<footer>
	<div class="f-top">
		<nav class="footer-nav">
			<?php PLS_Route::get_template_part( 'menu', 'primary' ); ?> 
		</nav><!--footer-nav-->

		<?php $api_me = PLS_Plugin_API::get_user_details(); ?>

		<?php if (pls_get_option('pls-user-phone')) { ?>
			<p class="f-phone option-pls-user-phone"><?php echo pls_get_option('pls-user-phone'); ?></p>
		<?php } else { ?>
			<p class="f-phone option-pls-user-phone"><?php echo PLS_Format::phone($api_me['user']['phone']); ?></p>
		<?php } ?>

		<?php if (pls_get_option('pls-user-email')) { ?>
			<p class="f-email"><a class="option-pls-user-email" href="mailto:<?php echo pls_get_option('pls-user-email'); ?>"><?php echo pls_get_option('pls-user-email'); ?></a></p>
			<?php } else { ?>
			<p class="f-email"><a href="mailto:<?php echo $api_me['user']['email']; ?>" class="option-pls-user-email"><?php echo $api_me['user']['email']; ?></a></p>
			<?php } ?>

			<div class="clearfix"></div>
      </div><!--f-top-->
      <div class="f-bot">
        <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"  class="option-pls-site-title"><?php bloginfo( 'name' ); ?></a></h1>
        <h2  class="option-pls-site-subtitle"><?php bloginfo( 'description' ); ?></h2>
        		<?php if (isset($api_me['location'])): ?>
					<p class="address">
						<?php if (pls_get_option('pls-company-street')) { ?>
							<span class="option-pls-company-street"><?php echo pls_get_option('pls-company-street'); ?></span> <br>
						<?php } else { ?>
							<span class="option-pls-company-street"><?php echo $api_me['location']['address']; ?></span> <br>
						<?php } ?>

						<?php if (pls_get_option('pls-company-locality')) { ?>
							<span class="option-pls-company-locality"><?php echo pls_get_option('pls-company-locality'); ?></span>,
						<?php } else { ?>
							<span class="option-pls-company-locality"><?php echo $api_me['location']['locality']; ?></span>
						<?php } ?>

						<?php if (pls_get_option('pls-company-region')) { ?>
							<span class="option-pls-company-region"><?php echo pls_get_option('pls-company-region') ?></span>
						<?php } else { ?>
							<span class="option-pls-company-region"><?php echo $api_me['location']['region']; ?></span>
						<?php } ?>

						<?php if (pls_get_option('pls-company-postal')) { ?>
							<span class="option-pls-company-postal"><?php echo pls_get_option('pls-company-postal') ?></span>
						<?php } else { ?>
							<span class="option-pls-company-postal"><?php echo $api_me['location']['postal']; ?></span>
						<?php } ?>

						</p>
					<?php endif ?>

        <div class="clearfix"></div>
      </div><!--f-bot-->
			<?php 
			$copyright = pls_get_option('pls-footer-copyright');
			if ($copyright) { 
				printf( '<p class="copyright">%s | <a href="https://placester.com/wordpress-themes/columbus/" rel="nofollow">Columbus</a> theme by Placester</p>', $copyright );
			} else {
				printf( '<p class="copyright">&copy; %s %s | <a href="https://placester.com/wordpress-themes/columbus/" rel="nofollow">Columbus</a> theme by Placester</p>',  date('Y'), bloginfo('name') );
			} ?>
    </footer>

  </div> 
  <!-- END OF CONTAINER -->
	
  <!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
       chromium.org/developers/how-tos/chrome-frame-getting-started -->
  <!--[if lt IE 7 ]>
  	<?php wp_enqueue_script('chrome-frame', 'https://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js', array(), false, true); ?>
    <script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
  <![endif]-->

	<!-- Google Analytics -->
<?php wp_footer(); ?>
</body>
</html>

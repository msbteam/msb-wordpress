<?php 

$background_defaults = array('color' => '', 'image' => '', 'repeat' => '', 'position' => '', 'attachment'=> 'scroll');
$typography_defaults = array('color' => '', 'face' => '', 'size' => '', 'style' => '');


PLS_Style::add(array( 
		"name" => "Listings Styles",
		"type" => "heading"));

	PLS_Style::add(array(
		"name" => "Default Sort Column",
		"desc" => "Set default column for listings to sort on",
		"id" => "listings_default_sort_by",
		"std" => "cur_data.price",
		"type" => "select",
		"class" => "mini",
		"options" => array(
						'images' => 'Images',
						'location.address' => 'Address',
						'location.locality' => 'City',
						'location.region' => 'State',
						'location.postal' => 'Zip',
						'zoning_types' => 'Zoning',
						'purchase_types' => 'Purchase Type',
						'listing_types' => 'Listing Type',
						'property_type' => 'Property Type',
						'cur_data.beds' => 'Beds',
						'cur_data.baths' => 'Baths',
						'cur_data.price' => 'Price',
						'cur_data.sqft' => 'Square Feet',
						'cur_data.avail_on' => 'Date Available'
					)
				)
			);

	// I would normally do a 2-item choice as radio button, but decided to mirror how the frontend choice is made -pek
	PLS_Style::add(array(
		"name" => "Default Sort Direction",
		"desc" => "Set default direction for listings sort order",
		"id" => "listings_default_sort_type",
		"std" => "asc",
		"type" => "select",
		"class" => "mini",
		"options" => array(
						'desc' => 'Descending',
						'asc' => 'Ascending'
					)
				)
			);
	
	PLS_Style::add(array(
		"name" => "Maximum Listings Per Search",
		"desc" => "Limits total number of listings returned in one search. Max is 50.",
		"id" => "listings_default_list_limit",
		"std" => "50",
		"type" => "text"
				)
			);
		
    PLS_Style::add(array(
          "name" =>  "Listing Titles/Addresses",
          "desc" => "Change the typography of individual listing titles/addresses",
          "id" => "listings_titles_addresses",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-right h4 a, #placester_listings_list .lu-right h4 a",
          "type" => "typography"));

    PLS_Style::add(array(
          "name" =>  "Listing Titles/Addresses",
          "desc" => "Change the typography of individual listings' description excerpts",
          "id" => "listings_description_excerpt",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-right h4 a, #placester_listings_list .lu-right p.desc",
          "type" => "typography"));

    PLS_Style::add(array(
          "name" =>  "Listing Titles/Addresses - on hover",
          "desc" => "Change the typography of individual listing titles/addresses on hover",
          "id" => "listings_titles_addresses_on_hover",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-right h4 a:hover, #placester_listings_list .lu-right h4 a:hover",
          "type" => "typography"));

    PLS_Style::add(array(
          "name" =>  "Price",
          "desc" => "Change the typography of each listing's price",
          "id" => "listings_price_typography",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-left p, #placester_listings_list .lu-left p",
          "type" => "typography"));
 
    PLS_Style::add(array(
          "name" =>  "Price Background Color",
          "desc" => "Change the background color of individual listing titles/addresses",
          "id" => "listings_price_background_color",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-left p, #placester_listings_list .lu-left p",
          "type" => "background"));

    PLS_Style::add(array(
          "name" =>  "'Request More Information' Link",
          "desc" => "Change the typography of the 'Request More Information' links in each listing.",
          "id" => "listings_request_more_info",
          "std" => $typography_defaults,
          "selector" => "#listing .lu-right .lu-links a.info, #placester_listings_list .lu-right a.info",
          "type" => "typography"));

    PLS_Style::add(array(
          "name" =>  "'See Details' Button Color",
          "desc" => "Change the color of the 'See Details' button.",
          "id" => "see_details_button_color",
          "std" => $typography_defaults,
          "selector" => ".lu-right a.details, .lu-right a.details",
          "type" => "background"));

    PLS_Style::add(array(
          "name" =>  "'See Details' Button Color",
          "desc" => "Change the color of the 'See Details' button.",
          "id" => "see_details_button_border",
          "std" => $typography_defaults,
          "selector" => ".lu-right a.details, .lu-right a.details",
          "type" => "border"));


// Single Property
PLS_Style::add(array( 
			"name" =>  "Single Property Styles",
			"desc" => "The following options are for single property pages/landing-pages.",
			"type" => "info"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Title / Address",
					"desc" => "Change the typography of each property page's title / address.",
					"id" => "single_property_title",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details .fold-l h2",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Title / Address : on hover",
					"desc" => "Change the typography of each property page's title / address on hover.",
					"id" => "single_property_title_hover",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details .fold-l h2 a:hover",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Price",
					"desc" => "Change the typography of the single properties' price",
					"id" => "single_property_price",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details .fold-r p",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - SQFT / Beds / Baths / Half Baths",
					"desc" => "Change the typography of the main detail numbers.",
					"id" => "single_property_main_details_numbers",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details .fold-l p span",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - SQFT / Beds / Baths / Half Baths labels",
					"desc" => "Change the typography of the main detail labels.",
					"id" => "single_property_main_details_labels",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details .fold-l p",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Listing Type / Property Type / MLS / Ref#",
					"desc" => "Change the typography of the minor detail numbers.",
					"id" => "single_property_minor_details_numbers",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details p.p-info",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Listing Type / Property Type / MLS / Ref# labels",
					"desc" => "Change the typography of the main detail labels.",
					"id" => "single_property_minor_details_labels",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details p.p-info span",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Description Title",
					"desc" => "Change the typography of the 'Description' title.",
					"id" => "single_property_description_title",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details #main .user-generated h2",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Description Text",
					"desc" => "Change the typography of the property description text.",
					"id" => "single_property_description_text",
					"std" => $typography_defaults,
					"selector" => "body.single-property .property-details #main .user-generated p",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Neighborhood Title Typography",
					"desc" => "Change the typography of the neighborhood title.",
					"id" => "single_property_neighborhood_title",
					"std" => $typography_defaults,
					"selector" => "body.single-property .neighborhood-widget h3",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Neighborhood Title Background",
					"desc" => "Change the background of the neighborhood title.",
					"id" => "single_property_neighborhood_title_background",
					"selector" => "body.single-property .neighborhood-widget h3",
					"type" => "background"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Neighborhood Title Border",
					"desc" => "Change the border of the neighborhood title.",
					"id" => "single_property_neighborhood_title_border",
					"selector" => "body.single-property .neighborhood-widget h3",
					"type" => "border"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Location Map Title Typography",
					"desc" => "Change the typography of the map title.",
					"id" => "single_property_map_title",
					"std" => $typography_defaults,
					"selector" => "body.single-property #map-widget h3",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Location Map Title Background",
					"desc" => "Change the background of the location map title.",
					"id" => "single_property_map_title_background",
					"selector" => "body.single-property #map-widget h3",
					"type" => "background"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Location Map Title Border",
					"desc" => "Change the border of the location map title.",
					"id" => "single_property_map_title_border",
					"selector" => "body.single-property #gallery-widget h3",
					"type" => "border"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Photo Gallery Title Typography",
					"desc" => "Change the typography of the photo gallery title.",
					"id" => "single_property_gallery_title",
					"std" => $typography_defaults,
					"selector" => "body.single-property #gallery-widget h3",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Photo Gallery Title Background",
					"desc" => "Change the background of the neighborhood title.",
					"id" => "single_property_gallery_title_background",
					"selector" => "body.single-property #gallery-widget h3",
					"type" => "background"));

			PLS_Style::add(array(
					"name" =>  "Single Property - Photo Gallery Title Border",
					"desc" => "Change the border of the photo gallery title.",
					"id" => "single_property_gallery_title_border",
					"selector" => "body.single-property #gallery-widget h3",
					"type" => "border"));

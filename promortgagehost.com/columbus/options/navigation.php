<?php

PLS_Style::add(array( 
    "name" => "Navigation Styles",
    "type" => "heading"));

     PLS_Style::add(array( 
           "name" =>  "Navigation Background Color",
           "desc" => "Change the site's nav bar color.",
           "id" => "nav_background_color",
           "selector" => "header .primary, header .primary ul ul, header nav",
           "type" => "background"));

     PLS_Style::add(array( 
           "name" =>  "Navigation Child Background Color",
           "desc" => "Change the site's nav bar color.",
           "id" => "nav_child_background_color",
           "selector" => ".primary ul ul.sub-menu li a",
           "type" => "background"));

      PLS_Style::add(array( 
          "name" =>  "Navigation Links",
          "desc" => "Change the site navigation links.",
          "id" => "nav_links",
          "selector" => "header nav ul li a",
          "type" => "typography"));

      PLS_Style::add(array( 
          "name" =>  "Navigation Active Link",
          "desc" => "Change the site navigation's single current link.",
          "id" => "nav_active_link",
          "selector" => "header nav li.current_page_item > a",
          "type" => "background"));

      PLS_Style::add(array( 
          "name" =>  "Navigation Link on Hover",
          "desc" => "Change the site navigation's single current link.",
          "id" => "nav_hover_link",
          "selector" => "header nav li a:hover, header nav li a:focus",
          "type" => "background"));
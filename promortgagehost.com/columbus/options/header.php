<?php 

$background_defaults = array('color' => '', 'image' => '', 'repeat' => '', 'position' => '','attachment'=>'scroll');

PLS_Style::add(array( 
    "name" => "Home Details",
    "type" => "heading"));

PLS_Style::add(array(
    "name" => "Home Featured Heading",
    "desc" => "",
    "std" => "Featured Listings",
    "id" => "pls-option-home-featured",
    "type" => "text"));


PLS_Style::add(array( 
		"name" => "Header Styles",
		"type" => "heading"));


		// Add single CSS option for change to site
		PLS_Style::add(array( 
					// div title in Theme Options Menu
					"name" =>  "H1 Title",
					// div descrition in Theme Options Menu
					"desc" => "Change main site title's size, font-family, styling, and color.",
					// div id in Theme Options Menu
					"id" => "h1_title",
					//
					"std" => $background_defaults,
					// selector of targeted tag being changed
					"selector" => "header h1 a",
					// Theme Options Tab (type) which holds option being changed.
					// examples: text, textarea, radio, images, textbox, multicheck,
					// color, upload, typography, background, info, heading
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "H2 Subtitle",
					"desc" => "Change the site subtitle's size, font-family, styling, and color.",
					"id" => "h2_subtitle",
					"std" => $background_defaults,
					"selector" => "header h2",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Header Email",
					"desc" => "Change the header's email size, font-family, styling, and color.",
					"id" => "header_email",
					"std" => $background_defaults,
					"selector" => ".h-email a",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Header Phone",
					"desc" => "Change the header's phone size, font-family, styling, and color.",
					"id" => "header_phone",
					"std" => $background_defaults,
					"selector" => ".h-phone",
					"type" => "typography"));
					

// 
// 
// 

// Search Page Title
PLS_Style::add(array(
		"name" => "Search Page Form Title",
		"desc" => "Options to modify the look of the search page's main form title.",
		"type" => "info"));

    // Search Form Title
    PLS_Style::add(array(
    		"name" =>  "Search Listings Page - Search Form Title Typography",
    		"desc" => "Change the typography of the Search page's form title.",
    		"id" => "search_form_general_title",
    		"selector" => "body.page-template-page-template-listings-php #full-search h3, body.home #main #listing h3",
    		"type" => "typography"));

    PLS_Style::add(array(
    		"name" =>  "Search Listings Page - Search Form Title Background",
    		"desc" => "Change the background of the Search page's form title.",
    		"id" => "search_form_general_title_bg",
    		"selector" => "body.page-template-page-template-listings-php #full-search h3, body.home #main #listing h3",
    		"type" => "background"));

    PLS_Style::add(array(
    		"name" =>  "Search Listings Page - Search Form Title Border",
    		"desc" => "Change the border of the Search page's form title.",
    		"id" => "search_form_general_title_border",
    		"selector" => "body.page-template-page-template-listings-php #full-search h3, body.home #main #listing h3",
    		"type" => "border"));

    // Home/Search page Listing List Titles
    PLS_Style::add(array(
    		"name" => "Property List Title - Home/Search pages",
    		"desc" => "The following options are for lists of properties/listings on the home and search pages.",
    		"type" => "info"));

    PLS_Style::add(array(
          "name" =>  "Property List Title Typography",
          "desc" => "Change the listing list title typography.",
          "id" => "listings_section_title",
          "selector" => "#main #listing h3, body.page-template-page-template-listings-php #main h3",
          "type" => "typography"));

    PLS_Style::add(array( 
          "name" =>  "Property List Title Background",
          "desc" => "Change the listing list title's background.",
          "id" => "listings_section_title_background",
          "selector" => "#main #listing h3, body.page-template-page-template-listings-php #main h3",
          "type" => "background"));

    PLS_Style::add(array(
    		"name" =>  "Property List Title Border",
    		"desc" => "Change the border of the listing list's title.",
    		"id" => "listings_section_title_border",
    		"selector" => "#main #listing h3, body.page-template-page-template-listings-php #main h3",
    		"type" => "border"));


  // Home/Search page Listing List Titles
  PLS_Style::add(array(
  		"name" => "Slideshow Address Banner Styles",
  		"desc" => "Modify the options below to style the slideshow's address banner.",
  		"type" => "info"));

  PLS_Style::add(array(
        "name" =>  "Slideshow Address Title Typography",
        "desc" => "",
        "id" => "slideshow_title",
        "selector" => ".orbit-caption p.address a, .orbit-caption p.address a:visited",
        "type" => "typography"));

  PLS_Style::add(array( 
        "name" =>  "Slideshow Address Title Background",
        "desc" => "",
        "id" => "slideshow_background",
        "selector" => ".orbit-caption p.address",
        "type" => "background"));

  PLS_Style::add(array(
  		"name" =>  "Slideshow Address Title ",
  		"desc" => "",
  		"id" => "slideshow_border",
  		"selector" => ".orbit-caption p.address",
  		"type" => "border"));


PLS_Style::add(array( 
    "name" => "Property Details",
    "type" => "heading"));

PLS_Style::add(array(
    "name" => "Property Amenities Heading",
    "desc" => "",
    "std" => "PROPERTY AMENITIES",
    "id" => "pls-option-details-prop-amenities",
    "type" => "text"));

PLS_Style::add(array(
    "name" => "Neighborhood Amenities Heading",
    "desc" => "",
    "std" => "NEIGHBORHOOD AMENITIES",
    "id" => "pls-option-details-neb-amenities",
    "type" => "text"));

PLS_Style::add(array(
    "name" => "Custom Amenities Heading",
    "desc" => "",
    "std" => "CUSTOM AMENITIES",
    "id" => "pls-option-details-cust-amenities",
    "type" => "text"));

PLS_Style::add(array(
    "name" => "Map Heading",
    "desc" => "",
    "std" => "NEIGHBORHOOD",
    "id" => "pls-option-details-map",
    "type" => "text"));
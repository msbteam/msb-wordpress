<?php

PLS_Style::add(array( 
		"name" => "Search Options",
		"type" => "heading"));

	//price range
	PLS_Style::add(array(
		"name" => "Price Range Options",
		"desc" => "Use the controls below to change what options are displayed when users click the bedrooms filer",
		"type" => "info"));

	PLS_Style::add(array(
		"name" => "Min Search Price",
		"desc" => "",
		"id" => "pls-option-price-min",
		"std" => "0",
		"type" => "text"));

	PLS_Style::add(array(
		"name" => "Max Search Price",
		"desc" => "",
		"id" => "pls-option-price-max",
		"std" => "1000000",
		"type" => "text"));

	PLS_Style::add(array(
		"name" => "Price Increment",
		"desc" => "",
		"std" => "50000",
		"id" => "pls-option-price-inc",
		"type" => "text"));
	
	//beds
	PLS_Style::add(array(
				"name" => "Bedroom Options",
				"desc" => "Use the controls below to change what options are displayed when users click the bedrooms filter",
				"type" => "info"));

	PLS_Style::add(array(
		"name" => "Bedroom Options Start",
		"desc" => "",
		"id" => "pls-option-bed-min",
		"std" => "0",
		"type" => "text"));

	PLS_Style::add(array(
		"name" => "Bedroom Options End",
		"desc" => "",
		"id" => "pls-option-bed-max",
		"std" => "15",
		"type" => "text"));

	//baths
	PLS_Style::add(array(
		"name" => "Bathroom Options",
		"desc" => "Use the controls below to change what options are displayed when users click the bathrooms filter",
		"type" => "info"));

	PLS_Style::add(array(
		"name" => "Bathroom Options Start",
		"desc" => "",
		"id" => "pls-option-bath-min",
		"std" => "0",
		"type" => "text"));

	PLS_Style::add(array(
		"name" => "Bathroom Options End",
		"desc" => "",
		"id" => "pls-option-bath-max",
		"std" => "10",
		"type" => "text"));

	//half-baths
	PLS_Style::add(array(
		"name" => "Half Bath Options",
		"desc" => "Use the controls below to change what options are displayed when users click the bathrooms filter",
		"type" => "info"));

	PLS_Style::add(array(
		"name" => "Half Bath Options Start",
		"desc" => "",
		"id" => "pls-option-half-bath-min",
		"std" => "0",
		"type" => "text"));

	PLS_Style::add(array(
		"name" => "Half Bath Options End",
		"desc" => "",
		"id" => "pls-option-half-bath-max",
		"std" => "5",
		"type" => "text"));
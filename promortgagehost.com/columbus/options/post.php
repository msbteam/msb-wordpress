<?php 

$background_defaults = array('color' => '', 'image' => '', 'repeat' => '', 'position' => '', 'attachment'=> 'scroll');
$typography_defaults = array('color' => '', 'face' => '', 'size' => '', 'style' => '');


PLS_Style::add(array( 
		"name" => "Blog Styles",
		"type" => "heading"));


PLS_Style::add(array(
		"name" => "Blog - Main Page",
		"desc" => "These options will alter the blog posts in list form.",
		"type" => "info"));

		PLS_Style::add(array( 
				"name" => "Blog Headline",
				"desc" => "Change the blog page's headline typography",
				"id" => "blog_headline",
				"std" => $typography_defaults,
				"selector" => "body.page-template-page-template-blog-php #inner #main h2",
				"type" => "typography"));

		PLS_Style::add(array( 
				"name" =>  "Blog - Post Top background in lists.",
				"desc" => "Change the background of the top of posts when in list form.",
				"id" => "blog_post_top_list_background",
				"std" => $background_defaults,
				"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-top",
				"type" => "background"));

		PLS_Style::add(array(
			"name" =>  "Blog - Post title in lists.",
			"desc" => "Change the typography of the posts' when in list form.",
			"id" => "blog_post_top_list_title",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-top h2",
			"type" => "typography"));

		PLS_Style::add(array(
			"name" =>  "Blog : on hover - Post title in lists.",
			"desc" => "Change the typography of the posts' title on hover when in list form.",
			"id" => "blog_post_top_list_title_hover",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-top h2 a:hover",
			"type" => "typography"));

		PLS_Style::add(array(
			"name" =>  "Blog - Post info in lists.",
			"desc" => "Change the typography of the posts' info when in list form.",
			"id" => "blog_post_top_info",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-top p.post-info",
			"type" => "typography"));
			
		PLS_Style::add(array(
			"name" =>  "Blog - Post date in lists.",
			"desc" => "Change the typography of the posts' date when in list form.",
			"id" => "blog_post_top_date",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-top p.post-info span",
			"type" => "typography"));

		PLS_Style::add(array(
			"name" =>  "Blog - Post text in lists.",
			"desc" => "Change the typography of the posts' text when in list form.",
			"id" => "blog_post_list_text",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-main p",
			"type" => "typography"));

		PLS_Style::add(array(
			"name" =>  "Blog - Post text links in lists.",
			"desc" => "Change the typography of the posts' text links when in list form.",
			"id" => "blog_post_list_text_links",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-main p a",
			"type" => "typography"));

		PLS_Style::add(array(
			"name" =>  "Blog - Post 'Read More' link in lists.",
			"desc" => "Change the typography of the posts' 'Read More' link when in list form.",
			"id" => "blog_post_list_read_more",
			"std" => $typography_defaults,
			"selector" => "body.page-template-page-template-blog-php #inner #main .blog-post .post-main a.read-more",
			"type" => "typography"));



// Single Post
PLS_Style::add(array(
		"name" => "Blog - Single Post Page",
		"desc" => "These options will alter individual post pages",
		"type" => "info"));

			PLS_Style::add(array(
					"name" =>  "Single Post - Title",
					"desc" => "Change the typography of the single post's title.",
					"id" => "single_post_title",
					"std" => $typography_defaults,
					"selector" => "body.single-post article header h2",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Post - Text",
					"desc" => "Change the typography of the single post's text.",
					"id" => "single_post_text",
					"std" => $typography_defaults,
					"selector" => "body.single-post article p",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Post - Text Bold",
					"desc" => "Change the typography of the single post's bold/strong text.",
					"id" => "single_post_text_bold",
					"std" => $typography_defaults,
					"selector" => "body.single-post article p strong",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Post - Text Link",
					"desc" => "Change the typography of the single post's link text.",
					"id" => "single_post_text_link",
					"std" => $typography_defaults,
					"selector" => "body.single-post article p a",
					"type" => "typography"));

			PLS_Style::add(array(
					"name" =>  "Single Post - Text Link : on hover",
					"desc" => "Change the typography of the single post's link text on hover.",
					"id" => "single_post_text_link_hover",
					"std" => $typography_defaults,
					"selector" => "body.single-post article p a:hover",
					"type" => "typography"));



















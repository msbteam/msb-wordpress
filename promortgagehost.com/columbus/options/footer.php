<?php 

$background_defaults = array('color' => '', 'image' => '', 'repeat' => '', 'position' => '','attachment'=>'scroll');

PLS_Style::add(array( 
		"name" => "Footer Styles",
		"type" => "heading"));

	PLS_Style::add(array(
		"name" => "Custom Copyright Text",
		"desc" => "Appears in the footer of the site and is typically used to display copyright information",
		"id" => "pls-footer-copyright",
		"std" => "©2012",
		"type" => "text"));



		// Add single CSS option for change to site
		PLS_Style::add(array( 
					// div title in Theme Options Menu
					"name" =>  "H1 Title",
					// div descrition in Theme Options Menu
					"desc" => "Change main site title's size, font-family, styling, and color.",
					// div id in Theme Options Menu
					"id" => "h1_title_footer",
					//
					"std" => $background_defaults,
					// selector of targeted tag being changed
					"selector" => "footer h1 a",
					// Theme Options Tab (type) which holds option being changed.
					// examples: text, textarea, radio, images, textbox, multicheck,
					// color, upload, typography, background, info, heading
					"type" => "typography"));

		PLS_Style::add(array( 
					"name" =>  "Footer Navigation",
					"desc" => "Change the site footer's nav size, font-family, styling, and color.",
					"id" => "footer_nav",
					"std" => $background_defaults,
					"selector" => "footer nav ul li a",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "H2 Subtitle",
					"desc" => "Change the site footer's subtitle's size, font-family, styling, and color.",
					"id" => "h2_subtitle_footer",
					"std" => $background_defaults,
					"selector" => "footer h2",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Footer Email",
					"desc" => "Change the footer's email size, font-family, styling, and color.",
					"id" => "footer_email",
					"std" => $background_defaults,
					"selector" => ".f-email a",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Footer Phone",
					"desc" => "Change the footer's phone size, font-family, styling, and color.",
					"id" => "footer_phone",
					"std" => $background_defaults,
					"selector" => ".f-phone",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Footer Street Address",
					"desc" => "Change the footer's street address size, font-family, styling, and color.",
					"id" => "footer_street_address",
					"std" => $background_defaults,
					"selector" => "footer p.address span",
					"type" => "typography"));


		PLS_Style::add(array( 
					"name" =>  "Footer City/State/Zip",
					"desc" => "Change the footer's city/state/zip size, font-family, styling, and color.",
					"id" => "footer_city_state_zip",
					"std" => $background_defaults,
					"selector" => "footer p.address",
					"type" => "typography"));

















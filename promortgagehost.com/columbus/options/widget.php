<?php 

PLS_Style::add(array(
		"name" => "Sidebar Widgets",
		"type" => "heading"
));


// GENERAL SIDEBAR WIDGETS
PLS_Style::add(array(
		"name" => "Sidebar Widget",
		"desc" => "",
		"type" => "info"
));

PLS_Style::add(array(
		"name" =>  "Sidebar - General Widget Title Typography",
		"desc" => "Change the typography of the sidebar widget titles.",
		"id" => "widget_general_title",
		"selector" => ".widget h3, aside #floating-box #map h3, aside .placester_contact h3",
		"type" => "typography"));

PLS_Style::add(array(
		"name" =>  "Sidebar - General Widget Title Background",
		"desc" => "Change the background of the sidebar widget titles.",
		"id" => "widget_general_title_background",
		"selector" => ".widget h3, aside #floating-box #map h3, aside .placester_contact h3",
		"type" => "background"));

PLS_Style::add(array(
		"name" =>  "Sidebar - General Widget Title Border",
		"desc" => "Change the border of the sidebar widget titles.",
		"id" => "widget_general_title_border",
		"selector" => ".widget h3, aside #floating-box #map h3, aside .placester_contact h3",
		"type" => "border"));
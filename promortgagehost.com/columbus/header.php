<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
  	<?php $api_whoami = PLS_Plugin_API::get_user_details(); ?>
  	<meta charset="utf-8">

  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  	<title><?php bloginfo('name'); ?></title>
	<meta name="description" content="<?php echo get_bloginfo( 'description' ); ?>">
	<meta name="author" content="<?php echo $api_whoami['user']['first_name'] . ' ' . $api_whoami['user']['first_name']; ?>">
	
	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width,initial-scale=1">

  <!--[if lt IE 9]>
  <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
  <?php wp_head(); ?>
	
</head>

<body <?php body_class(); ?>>

	<?php pls_do_atomic( 'open_body' ); ?>

	<div id="container">

		<?php pls_do_atomic( 'before_header' ); ?>

		<header>

			<?php if (pls_get_option('pls-site-logo')): ?>
				<div id="logo">
					<img src="<?php echo pls_get_option('pls-site-logo') ?>" alt="<?php bloginfo( 'name' ); ?>" class="option-pls-site-logo">
				</div>
			<?php endif; ?>

			<?php if (pls_get_option('pls-site-title')): ?>
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="option-pls-site-title"><?php echo pls_get_option('pls-site-title'); ?></a></h1>
				
				<?php if (pls_get_option('pls-site-subtitle')): ?>
					<h2 class="option-pls-site-subtitle"><?php echo pls_get_option('pls-site-subtitle'); ?></h2>
				<?php endif ?>
			<?php endif; ?>

			<?php if (!pls_get_option('pls-site-logo') && !pls_get_option('pls-site-title')): ?>
				<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>"  class="option-pls-site-title"><?php bloginfo( 'name' ); ?></a></h1>
				<h2 class="option-pls-site-subtitle"><?php bloginfo( 'description' ); ?></h2>
			<?php endif; ?>

      <div id="nav-login">
				<?php echo PLS_Plugin_API::placester_lead_control_panel(array('separator' => '|')); ?>
			</div>
			
			<?php if (pls_get_option('pls-user-phone')) { ?>
			<p class="h-phone option-pls-user-phone"><?php echo pls_get_option('pls-user-phone'); ?></p>
			<?php } else { ?>
			<p class="h-phone option-pls-user-phone"><?php echo PLS_Format::phone($api_whoami['user']['phone']); ?></p>
			<?php } ?>
			
			<?php if (pls_get_option('pls-user-email')) { ?>
				<p class="h-email"><a class="option-pls-user-email" href="mailto:<?php echo pls_get_option('pls-user-email'); ?>"><?php echo pls_get_option('pls-user-email'); ?></a></p>
			<?php } else { ?>
				<p class="h-email"><a class="option-pls-user-email" href="mailto:<?php echo $api_whoami['user']['email']; ?>"><?php echo $api_whoami['user']['email']; ?></a></p>
			<?php } ?>
			
      <div class="clearfix"></div>

			<div class="primary">
				<?php PLS_Route::get_template_part( 'menu', 'primary' ); ?>
			</div>

		</header>
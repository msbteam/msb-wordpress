<?php PLS_Route::handle_header(); ?>

<div id="inner">
	<div id="main" role="main">
		<?php  PLS_Route::handle_dynamic(); ?>
	</div><!--main-->

	<?php PLS_Route::handle_sidebar(); ?>
</div><!--inner-->

<?php PLS_Route::handle_footer(); ?>


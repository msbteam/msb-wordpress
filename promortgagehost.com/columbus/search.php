<?php
/**
 * Search Template
 *
 * The search template is loaded when a visitor uses the search form to search for something
 * on the blog.
 *
 * @package PlacesterBlueprint
 * @subpackage Template
 */
?>

<?php PLS_Route::get_template_part( 'loop', 'meta' ); // Loads the loop-meta.php template. ?>

<?php PLS_Route::get_template_part( 'loop', 'entries' ); ?>
<?php

add_theme_support('pls-meta-tags');

/* Load the Placester Blueprint Theme Framework. */
require_once( trailingslashit( get_template_directory() ) . 'blueprint/blueprint.php' );
new Placester_Blueprint('2.5');

/**
 * 	Filter the default prefix used in 'pls_do_atomic' and 'pls_apply_atomic'.
 * 	All add_filters that hook into events set by pls_do_atomic will need to catch
 * 	the prefix_event_name for example:
 *
 *	blueprint will mean that you need to hook against blueprint_close_header, or blueprint_open_header
 */

// Introduce yourself to the class...
add_filter( 'pls_prefix', 'columbus_prefix' );
    function columbus_prefix() {
        return 'columbus';
}


// All our custom functions included for sanity. 

	// custom caption implementation
	include_once('custom/home-slideshow.php');

	// custom caption implementation
	include_once('custom/options.php');

	// custom featured listings list
	include_once('custom/home-listing-list.php');

	// custom sidebar search widget
	include_once('custom/sidebar-quick-search-widget.php');

	// custom sidebar recent posts widget
	include_once('custom/sidebar-recent-posts-widget.php');

	// custom sidebar recent posts widget
	include_once('custom/sidebar-listings-widget.php');

	// custom sidebar search form on listings page
	include_once('custom/listings-search-form.php');

	// custom sidebar ajax listings list on listings search
	include_once('custom/listings-search-list.php');

	// custom list for client profile
	include_once('custom/client-profile-search-list.php');

	// custom property details page.
	include_once('custom/property-details.php');

	// custom property details page featured listings.
	include_once('custom/property-details-featured-listings.php');

	// custom sidebar agent widget
	include_once('custom/sidebar-agent-widget.php');


// Remove Widgets
//
function columbus_remove_some_widgets(){
  unregister_sidebar( 'listings-search' );
  // Remove WordPress Widgets
  unregister_widget('WP_Widget_Meta');
  unregister_widget('WP_Widget_Recent_Comments');
  
}
add_action( 'widgets_init', 'columbus_remove_some_widgets' );


add_action('init', 'columbus_scripts');
function columbus_scripts () {
	wp_register_script('columbus_scripts', trailingslashit(get_template_directory_uri()) . "js/script.js", array('jquery'), false, true);
	wp_enqueue_script('columbus_scripts');
}

add_action('init', 'columbus_add_stylesheet');
function columbus_add_stylesheet() {
  	if (!is_admin()) {
	    
		wp_enqueue_style('columbus-style', get_stylesheet_directory_uri() . '/css/style.css' );
    }
}

/**
 * Any modifications to its behavior (add/remove support for features, define 
 * constants etc.) must be hooked in 'after_setup_theme' with a priority of 10 if the
 * framework is a parent theme or a priority of 11 if the theme is a child theme. This 
 * allows the class to add or remove theme-supported features at the appropriate time, 
 * which is on the 'after_setup_theme' hook with a priority of 12.
 * 
 */
add_action( 'after_setup_theme', 'columbus_setup', 10 );
function columbus_setup() {
	
	add_theme_support( 'post-thumbnails' );

	remove_theme_support( 'pls-default-css' );
	remove_theme_support( 'pls-dragonfly' );

	// adding options
	add_theme_support( 'pls-footer-options' );
	add_theme_support( 'pls-widget-options' );
	add_theme_support( 'pls-routing-util-templates' );

	remove_theme_support( 'pls-meta-data');
    remove_theme_support( 'pls-micro-data');
    remove_theme_support( 'pls-custom-polygons' );
}

// Remove Listings Sidebar
function columbus_remove_listings_search(){
	unregister_sidebar( 'listings-search' );
}
add_action( 'init', 'columbus_remove_listings_search' );

add_action( 'wp_head', 'columbus_comment_reply' );
function columbus_comment_reply() {
	if ( is_singular() ) {
		wp_enqueue_script( "comment-reply" );
	}
}

add_action( 'wp_head', 'columbus_get_favicon' );
function columbus_get_favicon() {
	if (!is_admin()) {
		$favicon = pls_get_option('pls-site-favicon');
		if ( $favicon ) { 
			printf( '<link href="%s" rel="shortcut icon" type="image/x-icon" />' . PHP_EOL, $favicon );
		}
	}
}

add_action( 'wp_print_scripts', 'columbus_custom_css');
function columbus_custom_css() {
	if (!is_admin()) {
		if ( (pls_get_option('pls-css-options')) && (pls_get_option('pls-custom-css')) ) {
			printf( '<style id="pls-custom-css" type="text/css">%s</style>', $custom_css );
		}
	}
}

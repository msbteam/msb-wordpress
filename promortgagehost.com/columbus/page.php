<?php
/**
 * Page Template
 *
 * This is the default page template. It is used when a more specific template can't be found to display 
 * singular views of pages.
 *
 */
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

        <header>
            <?php PLS_Route::get_template_part( 'loop-meta' ) ?>
        </header>

        <?php the_content(); ?>

    </article>

<?php endwhile; else: ?>
    
    <?php PLS_Route::get_template_part( 'loop-error' ); ?>
    
<?php endif; ?>

<?php
/**
 * Template Name: Listings Search
 *
 * This is the template for "Listings" search results page.
 *
 */
?>

<div id="search-form-area" role="main">
	<?php echo PLS_Partials::get_listings_search_form( 'context=listings_search&ajax=1'); ?>
</div>

<div id="main" role="main">
<?php
// no listings table if the plugin is not available
if ( ! pls_has_plugin_error() ) {
?>
	<div id="pls_listings_search_results"><span id="pls_num_results"></span> listings match your search.</div>
	<h3>Search Results</h3>
		<?php echo PLS_Partials::get_listings_list_ajax('crop_description=1&context=custom_listings_search'); ?>
		<?php PLS_Listing_Helper::get_compliance(array('context' => 'search', 'agent_name' => false, 'office_name' => false)); ?>
<?php
} else {
?>
	<div id="pls_listings_search_results">&nbsp;</div>
	<h3>Search Results</h3>
  <p>This site is currently using a Placester theme without the <a href="http://wordpress.org/extend/plugins/placester/">Real Estate Website Builder plugin</a>. This feature will not work without it.
<?php
}
?>
</div>


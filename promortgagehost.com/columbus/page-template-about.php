<?php
/**
 * Template Name: About Us
 *
 * This is the template for the About Us page
 *
 */
?>

<div id="main">
	<div class="user-generated">
    <h2>About <span class="option-pls-company-name"><?php echo pls_get_option('pls-company-name'); ?></span></h2>
    <p id="company-phone" class="option-pls-company-phone"><?php echo pls_get_option('pls-company-phone'); ?></p>
    <p id="company-address" class="option-pls-company-street"><?php echo pls_get_option('pls-company-street'); ?></p>
    <p id="company-city-state"><span class="option-pls-company-locality"><?php echo pls_get_option('pls-company-locality'); ?></span>, <span class="option-pls-company-region"><?php echo pls_get_option('pls-company-region'); ?></span></p>
		<p id="company-decription" class="option-pls-company-description"><?php echo pls_get_option('pls-company-description'); ?></p>

    <?php if(have_posts()) : while(have_posts()) : the_post(); the_content(); endwhile; endif; ?>
  </div><!--user-generated-->

</div><!--main-->

<?php PLS_Route::handle_header(); ?>
<?php
// only load bootloader if plugin is active
if ( ! pls_has_plugin_error() ) {
?>
<script type="text/javascript">
  var search_bootloader;
  jQuery(document).ready(function( $ ) {
    search_bootloader = new SearchLoader ({
      map: {
        dom_id: 'map_canvas',
        filter_by_bounds: false
      },
    	filter: {
    		context: 'listings_search'
    	},
    	list: {
    		context: 'custom_listings_search'
      }
    });
  });
</script>
<?php
}
?>

<div id="inner">
	<?php  PLS_Route::handle_dynamic(); ?>

	<aside>
		<div id="floating-box">
			<section id="map">
				<h3 class="gr">Location Map</h3>
				<?php echo PLS_Map::dynamic(null, array(
				  'lat' => -74,
				  'lng' => 40,
				  'zoom' => '11',
				  'width' => 286,'height' => 230,
				  'canvas_id' => 'map_canvas',
        	'class' => 'custom_google_map',
        	'map_js_var' => 'pls_google_map',
        	'ajax_form_class' => false,
				  )); ?>
			</section><!--map-widget-->
		</div><!--floating-box-->
	</aside>

	<div class="clearfix"></div>

</div> <!-- end inner -->
 
<?php PLS_Route::handle_footer(); ?>


<?php
/*
Plugin Name: MySMARTblog - Disable Welcome E-mail
Description: Disables the email sent to the user when account is created.
Author: MySMARTblog
Version: 0.1
*/

if ( !function_exists('wp_new_user_notification') ) {

function wp_new_user_notification( ) {}

}


if ( !function_exists('wpmu_welcome_user_notification') ) {
	
  function wpmu_welcome_user_notification($user_id, $password, $meta = '') {
      global $current_site;
  
      $welcome_email = get_site_option( 'welcome_user_email' );
  
      $user = new WP_User($user_id);
  
      $welcome_email = apply_filters( 'update_welcome_user_email', $welcome_email, $user_id, $password, $meta);
  
      // Get the current blog name
      $blogname = get_option( 'blogname' );
      $welcome_email = str_replace( 'SITE_NAME', $blogname, $welcome_email );
  
      $welcome_email = str_replace( 'USERNAME', $user->user_login, $welcome_email );
      $welcome_email = str_replace( 'PASSWORD', $password, $welcome_email );
      $welcome_email = str_replace( 'LOGINLINK', wp_login_url(), $welcome_email );
  
      $admin_email = get_site_option( 'admin_email' );
  
      if ( $admin_email == '' )
           $admin_email = 'support@' . $_SERVER['SERVER_NAME'];
  
      $from_name = get_site_option( 'site_name' ) == '' ? 'WordPress' : esc_html( get_site_option( 'site_name' ) );
      $message_headers = "From: \"{$from_name}\" <{$admin_email}>\n" . "Content-Type: text/plain; charset=\"" . get_option('blog_charset') . "\"\n";
      $message = $welcome_email;
  
      $subject = apply_filters( 'update_welcome_user_subject', sprintf(__('New %1$s User: %2$s'), $blogname, $user->user_login) );
      //wp_mail($user->user_email, $subject, $message, $message_headers);
  
      return false; // make sure wpmu_welcome_user_notification() doesn't keep running
  }
  add_filter( 'wpmu_welcome_user_notification', 'bbg_wpmu_welcome_user_notification', 10, 3 );

}


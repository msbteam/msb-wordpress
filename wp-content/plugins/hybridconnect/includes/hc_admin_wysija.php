<?php
$modelList = &WYSIJA::get('list','model');
$wysijaLists = $modelList->get(array('name','list_id'),array('is_enabled'=>1));
?>

<div class="hc_admin_new_connector_top">
    <div class="hc_admin_new_connector_top_close">
        <img class="img_hc_admin_new_connector_top_close" src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/close-icon.png" border="0" />
    </div>
    <div class="clear"></div>
</div>
<div class="hc_admin_new_connector">
    <div class="labelHeader">Sign People Up to WYSIJA</div>

    <div class="labelStep">
        <p>Hybrid Connect now integrates with WYSIJA - simply choose the mailing list that you want to subscribe to below:</p>
    </div>
    <table class="tableConnectorDetails" style="margin-bottom:0px; margin-bottom:20px;">
        <tr>
            <td class="label_column">List(s) to sign up to:</td>
            <td>
            <?php foreach($wysijaLists as $list): ?>
           <div style="padding:10px; border: 1px dashed #E3E3E3; margin:5px; float:left; background: #FBFBF6">
              <input type="checkbox" name="wysijaList" class="wysijaListId" value="<?php echo $list['list_id'];?>"
              <?php if(isset($wysija_active_lists)){ if($wysija_active_lists) { if(in_array( $list['list_id'],$wysija_active_lists)): ?>checked<?php endif; } } ?> /><font style="font-size:14px"> <?php echo $list['name']; ?></font>
           </div>
           <?php endforeach; ?>
            </td>
        </tr>
        <tr>
            <td class="label_column">Thank you page: (include http://):</td>
            <td>
               <input type="text" value="<?php echo $my_connector->custom_ty_page; ?>" id="wysija_sel_hc_connector_thanksPage" name="wysija_sel_hc_connector_thanksPage" style="width:500px;" />
            </td>
        </tr>
    <tr>
    <td class="label_column">
     Signup Type:
    </td>
    <td style="padding:15px;"><input type="radio" name="wysijaoptintype" id="nameAndEmail" value="0" <?php if($my_connector->emailOnly=="0") { echo 'checked="checked"';  } ?> /> Name and Email &nbsp;&nbsp;&nbsp;
    <input type="radio" name="wysijaoptintype" id="emailOnly" value="1" <?php if($my_connector->emailOnly=="1") { echo 'checked="checked"';  } ?> /> Only Email
    </tr>
    </table>
    <button id="hc_wysija_lists" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Save</span></button>
</div>

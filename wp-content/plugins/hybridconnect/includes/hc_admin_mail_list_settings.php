<div class="hc_admin_new_connector_top">
    <div class="hc_admin_new_connector_top_close">
        <img class="img_hc_admin_new_connector_top_close" src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/close-icon.png" border="0" />
    </div>
    <div class="clear"></div>
</div>
<form id="hc_form_mail_settings">
    <div class="hc_admin_new_connector">
    <div class="labelHeader">Mailing List Settings</div>

<?php
    if(get_option("hc_show_api",0)) {
        // api connection already established, show options ?>
         <div class="ui-widget" id="hc_facebook_txt_invalid">
                           <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                                <strong>API integration has been deprecated and we now only support custom HTML submissions. If you've previously set up an API connection then it will continue to work, however we recommend you switch to custom HTML integration for faster signups. </div></strong></p>
                           </div>
        <table class="tableConnectorDetails">
        <tr>
        <td><input type="radio" name="customHTMLConnect" id="mailRadio1" value="0" <?php if($my_connector->apiConnection=="0") { echo 'checked="checked"';  } ?> /> Custom form code (recommended for most users)</td>
        <td><input type="radio" name="customHTMLConnect" id="mailRadio2" value="1" <?php if($my_connector->apiConnection=="1") { echo 'checked="checked"';  } ?> /> Connect through API (advanced users)</td>
        </tr>
        </table>
        <?php
    } else { ?>
     <div style="display:none;"><input type="radio" name="customHTMLConnect" id="mailRadio3" value="0" checked="checked" /></div>
    <?php } ?>

</form>
       <div id="hc_table_custom_signup_code">
         <table class="tableConnectorDetails" >
     <tr>
     <td class="label_column">
   <b>Enter your Autoresponder Code</b> <br/><br/>
  <a href="http://fast.wistia.net/embed/iframe/1ts1kdi1h7?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]"><img src="http://embed.wistia.com/deliveries/b7e60cf6ad38080f4bf5d1c9641c7d754a3b598e.jpg?image_play_button=true&image_play_button_color=278de6e0&image_crop_resized=150x84" alt="" /><br/><b>Watch this Video First</b> </a>
<script charset="ISO-8859-1" src="http://fast.wistia.com/static/popover-v1.js"></script><br/><br/>
    <textarea id="mailingListSignupCode" rows="5" cols="85" style="padding:0px !important; font-size:11px; font-family:courier;">
    <?php echo $my_connector->custom_code; ?>
    </textarea>
    </td>
    </tr>
    </table>
    </div>
<?php
 if(get_option("hc_show_api",0)) {  ?>
    <div class="mailingListAPI" style="margin-top:20px;">
        <div class="labelStep">
            You can use this form to integrate with your mailing list. Firstly, choose the service provider that you use, and then fill out the settings underneath
        </div>
        <?php foreach ($my_services as $s):
              if ($s['validated']): $autoValidated=1 ?>
              <?php endif ?>
        <?php endforeach ?>
        <?php if($autoValidated) { ?>
        <table class="tableConnectorDetails" style="margin-bottom:0px;">
            <tr>
                <td class="label_column">Integrate With*:</td>
                <td>
                    <input type="hidden" class="hc_admin_hidden_idConnector" value="<?php echo $my_connector->IntegrationID ?>" />
                    <?php foreach ($my_services as $s): ?>
                        <?php if ($s['validated']): ?>
                            <input type="radio" name="hc_mail_type" value="<?php echo $s['name'] ?>" class="radio_hc_admin_mail_list_type" <?php if ($my_mailingList && $my_mailingList->autoresponderType == $s['name']): ?>checked<?php endif; ?> />
                            <?php echo $s['label'] ?>
                        <?php endif ?>
                    <?php endforeach ?>
                </td>
            </tr>
        </table>
        <table class="tableConnectorDetails" id="hc_table_admin_mail_list_name" style="margin-top:0px; margin-bottom:0px;">
            <tr>
                <td class="label_column">List Settings:</td>
                <td id="table_column_list_settings">
                </td>
            </tr>
        </table>
        <table class="tableConnectorDetails" id="hc_table_admin_mail_list_typage" style="margin-top:0px; margin-bottom:0px;">
        <tr>
            <td class="label_column">Thank you page*:</td>
            <td>
                <select name="hc_connector_thanksPage" id="sel_hc_connector_thanksPage">
                    <?php foreach ($available_pages as $page): ?>
                        <option <?php if ($my_connector && $my_connector->TyPage == get_permalink(get_page_by_title($page->post_title))): ?>selected<?php endif; ?>  value="<?php echo get_permalink(get_page_by_title($page->post_title)) ?>">
                            <?php echo $page->post_title ?>
                        </option>
                    <?php endforeach; ?>
                </select>
            </td>
        </tr>
        </table>
        </div>

        <?php } else { } ?></strong></p>
                           </div>
                      </div>
                      <?php } ?>

          <div class="hc_admin_new_connector">

         <table class="tableConnectorDetails">
    <tr>
    <td>
    <b>Optin Type:</b>
    </td>
    <td><input type="radio" name="optinType" id="nameAndEmail" value="0" <?php if($my_connector->emailOnly=="0") { echo 'checked="checked"';  } ?> /> Name and Email Address</td>
    <td><input type="radio" name="optinType" id="emailOnly" value="1" <?php if($my_connector->emailOnly=="1") { echo 'checked="checked"';  } ?> /> Only Email Address</td>
    </tr>
    <tr id="customThankYouPageSettings">
    <td>
    <b>Thank You Page (include http://):</b>
    </td>
    <td colspan="2"><input type="text" name="custom_html_typage" id="custom_html_typage" value="<?php if(isset($my_connector->custom_ty_page)): echo $my_connector->custom_ty_page; endif; ?>" style="width:500px;" />
    <br/><font style="font-size:11px;">*If this field is left blank then visitors will be automatically redirected<br/> to the thank you page set in your autoresponder.
    <br/><br/>**Enter the following tag: <br/>
    %samepage%<br/>
    into the thank you page field to keep your visitors on the same page<br/> that the details were submitted.
    </font>
    </td>
    </tr>
    </table>
        </div>
        </div>

           <button id="hc_submit_admin_mail_list_settings2" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Submit</span></button>
                  <button id="hc_remove_admin_mail_list_settings" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default-cancel" role="button" aria-disabled="false"><span class="ui-button-text">Remove Mailing List</span></button>
    </div>
</form>
<div id="hc_admin_ajax_loading_front">
    <center><img src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/ajax-loading.gif"></center>
</div>

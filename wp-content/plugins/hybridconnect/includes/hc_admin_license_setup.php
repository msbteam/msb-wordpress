<?php
$hc_ajax_nonce = wp_create_nonce("hyconspecialsecurityforajaxstring");
$license = get_option('HYBRID_CONNECT_LICENSE_STATUS');
 ?>

<input type="hidden" id="hc_hidden_services_ajaxnonce" value="<?php echo $hc_ajax_nonce ?>" />
<link type="text/css" rel="stylesheet" href="<?php echo HYBRIDCONNECT_CSS_PATH_TEMPLATES ?>/south-street/jquery-ui-1.8.20.custom.css" />


<div style="width:800px;">
    <p><img src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/setup.png" style="margin-left:20px; float:right;">
    <h2>Activate Your License</h2>
    <p>Please activate your license by entering your license key and associated email address.  These details have been sent to you in an email immediately after purchase to your Paypal email address.
</p>
<div style="clear:both;"></div>
<?php if ($license == 'ACTIVE'): ?>
    <div class="ui-widget" style="width:800px;">
        <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <strong>Your license is valid and your product is ready to use  </strong></p>
        </div>
    </div>
<?php else: ?>
    <div class="ui-widget">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <strong>Your license is not valid. </strong></p>
        </div>
    </div>
<?php endif ?>
<?php if (!function_exists('curl_exec')) { ?>
   <div class="ui-widget">
        <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <strong>In order for this plugin to work you need your host to enable CURL.  Please send an email to your host and ask them to enable CURL on your account - it's a fairly common request and they're usually more than happy to do this for you quickly.</strong></p>
        </div>
    </div>

<?php } ?>

<table class="hc_table_fb_settings" style="width:800px; margin:0px;">
    <tr>
        <td class="label_column">License Email</td>
        <td><input type="text" name="hc_license_email" value="<?php if ($license == 'ACTIVE') { echo "***EMAIL ADDRESS VALIDATED***"; } else { echo get_option('hc_license_email'); } ?>" size="55" id="hc_license_email" /></td>
        <td rowspan="2">
       <!--     <input type="button" name="Submit" id="hc_submit_license_settings" value="Validate License" class="hcAdminButton" />      -->
            <button id="hc_submit_license_settings" class="hc_update_service_settings ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Validate License</span></button>
            <input type="hidden" id="hc_hidden_license_ajaxnonce" value="<?php echo $hc_ajax_nonce ?>" />
        </td>
    </tr>
    <tr>
        <td class="label_column">License Key:</td>
        <td><input type="text" name="hc_license_key" value="<?php if ($license == 'ACTIVE') { echo "***LICENSE KEY VALIDATED***"; } else { echo get_option('hc_license_key'); } ?>" size="55" id="hc_license_key" /></td>
    </tr>
</table>
<br/><br/>

</div>
<div id="hc_admin_ajax_loading">
    <center><img src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/ajax-loading.gif"></center>
</div>
<div id="hc_admin_notification_dialog" title="Notification">
    <p>
        <br/>
        <span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
        <span id="hc_admin_notification_dialog_txt">
        </span>
    </p>
</div>
<script>
    jQuery(document).ready(function(){
        jQuery('.ui-state-default').live('mouseover',function () { jQuery(this).addClass('ui-state-hover'); }).live('mouseout',function () { jQuery(this).removeClass('ui-state-hover'); })
    });
</script>
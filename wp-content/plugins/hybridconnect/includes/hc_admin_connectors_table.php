<?php if (count($hc_connectors_list) == 0): ?>
    You haven't created any connectors yet. <a href="/" id="hc_new_first_connector">Click here to create one.</a><br/><br/><br/>
     <center><iframe src="http://fast.wistia.net/embed/iframe/exexkl3ydj?playerColor=278de6&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="800" height="450"></iframe></center>
<?php else: ?>
    <ul id="icons" class="ui-widget ui-helper-clearfix">
    <p align="right"><a href="<?php echo get_admin_url() ?>admin.php?page=hybridConnectAdminAffiliate">Earn money with Hybrid Connect</a></p>
        <table class="hc_admin_connectors_table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Mailing List</th>
                    <th>GotoWebinar</th>
                    <th>Short code / Theme code</th>
                    <th>
                        Post footer <br/>
                        <a href="/" id="hc_admin_remove_postfooter_options"> Remove </a>
                    </th>
                    <th>
                        Comment optin
                    </th>
                    <th>Shortcode <br/>Design
                    <br/><a href="http://fast.wistia.net/embed/iframe/5jvehldiof?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]">
                    <li class="ui-state-default ui-corner-all helpvideos" id="ldl5p55sf0" title="Shortcodes - how do they work?" style="margin-top:5px; margin-left:auto; margin-right:auto;"><span class="ui-icon ui-icon-help"></span></li></span>
                    </a>
                    </th>
                    <th>Widget <br/> Design
                    <br/><a href="http://fast.wistia.net/embed/iframe/txzx6qobdz?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]">
                    <li class="ui-state-default ui-corner-all helpvideos" id="ldl5p55sf0" title="Widgets  - how do they work?" style="margin-top:5px; margin-left:auto; margin-right:auto;"><span class="ui-icon ui-icon-help"></span></li></span>
                    </a>
                    </th>
                    <th>Lightboxes<br/>& Slide Ins
                    <br/><a href="http://fast.wistia.net/embed/iframe/svnj5rg3f4?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]">
                    <li class="ui-state-default ui-corner-all helpvideos" id="ldl5p55sf0" title="Lightboxes - how do they work?" style="margin-top:5px; margin-left:auto; margin-right:auto;"><span class="ui-icon ui-icon-help"></span></li></span>
                    </a>
                    </th>
                    <th>Squeeze <br/>Pages
                    <br/><a href="http://fast.wistia.net/embed/iframe/wrgz688o5m?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]">
                    <li class="ui-state-default ui-corner-all helpvideos" id="ldl5p55sf0" title="Squeeze Pages - how do they work?" style="margin-top:5px; margin-left:auto; margin-right:auto;"><span class="ui-icon ui-icon-help"></span></li></span>
                    </a>
<script charset="ISO-8859-1" src="http://fast.wistia.com/static/popover-v1.js"></script>
                    </th>
                    <th style="width:45px;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($hc_connectors_list as $connector): ?>
                    <tr>
                <input type="hidden" class="hc_table_hidden_connector_id" value="<?php echo $connector->IntegrationID ?>" />
                <td valign="middle" style="vertical-align:top">
                    <a href='/' class="hc_link_connector_edit"> <?php echo $connector->Name ?> </a>
                </td>
                <td valign="middle" style="vertical-align:top">
                    <select name="hc_sel_connector_type" class="hc_sel_connector_type" style="width:88px;">
                        <option <?php if ($connector && $connector->Type == 'Hybrid'): ?>selected<?php endif; ?> value="Hybrid">Hybrid</option>
                        <option <?php if ($connector && $connector->Type == 'Facebook'): ?>selected<?php endif; ?> value="Facebook">Facebook</option>
                        <option <?php if ($connector && $connector->Type == 'Form'): ?>selected<?php endif; ?> value="Form">Form</option>
                        <option <?php if ($connector && $connector->Type == 'None'): ?>selected<?php endif; ?> value="None">No Opt In</option>
                    </select>
                </td>
                <td valign="middle" class="editDesignButton" <?php if ($connector && $connector->Type == 'None'): ?>colspan="2"<?php endif; ?> >
                <?php if ($connector && $connector->Type == 'None'): ?>
                <a href="http://fast.wistia.net/embed/iframe/k5rfc2baba?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]">
                    <li class="ui-state-default ui-corner-all helpvideos" id="ldl5p55sf0" title="Advertisements - How do they work?" style="margin-top:5px; margin-left:auto; margin-right:auto;"><span class="ui-icon ui-icon-help"></span></li></span></a>
                Advert Destination URL:<br/><input type="text" class="adURL" name="adURL" value="<?php echo ($connector->ad_ty_page)? $connector->ad_ty_page : 'http://';?>" /><br/>
                 <a href="/" class="hc_save_ad_url" style="font-size:1.2em; text-decoration:underline;">Save</a>  <?php else: ?>
                    <?php if ($connector->MailingList): ?>
                        <a class="hc_link_connector_toMailingList" href="/"><li class="ui-state-default ui-corner-all" title="Connect to Mailing List" style="width:85px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-mail-closed" style="width:16px; float:left; margin:0px; padding:0px;"></span><?php echo $connector->MailingList ?></li></a>
                    <?php else: ?>
                        <a class="hc_link_connector_toMailingList" href="/"><li class="ui-state-default ui-corner-all" title="Connect to Mailing List" style="width:65px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-mail-closed" style="width:16px; float:left; margin:0px; padding:0px;"></span>Connect</li></a>
                    <?php endif; ?>
                    <?php if(get_option( "hc_wysija_activated" )) :?>
                      <div style="margin-top:5px;"><a class="hc_wysija_settings" href="/">WYSIJA Setup</a></div>
                    <?php endif; ?>
                    <?php endif; ?>
                    <?php if ($connector && $connector->Type != 'None'): ?>
                </td>
                <td valign="middle" class="editDesignButton">
                <?php endif; ?>
                <?php if ($connector && $connector->Type != 'None'): ?>
                    <?php if ($connector->GotoWebinar != ''): ?>
                        <a class="hc_link_connector_toGotoWebinar" href="/"><li class="ui-state-default ui-corner-all" title="Connect to Mailing List" style="width:65px; padding:4px; margin:0px auto;">
                                <span class="ui-icon ui-icon-video" style="width:16px; float:left; margin:0px; padding:0px;"></span>Settings</li></a>
                    <?php else: ?>
                        <a class="hc_link_connector_toGotoWebinar" href="/"><li class="ui-state-default ui-corner-all" title="Connect to Mailing List" style="width:65px; padding:4px; margin:0px auto;">
                                <span class="ui-icon ui-icon-video" style="width:16px; float:left; margin:0px; padding:0px;"></span>Connect</li></a>
                    <?php endif; ?>
                    <?php endif; ?>
                </td>
                <td valign="middle" style="vertical-align:top;" class="editDesignButton">
                    <input class="hc_txt_admin_shortcode" type='text' value='[hcshort id="<?php echo $connector->IntegrationID ?>"]' readonly='true' size="13" style="margin-bottom:5px;"/><br/>
                    <a href="/" class="hc_link_php_snippet" style="font-size:1em;">
                        Get Theme Code
                    </a>
                </td>
                <td valign="middle" style="vertical-align:top;">
                    <input type="hidden" class="hc_table_hidden_connector_id" value="<?php echo $connector->IntegrationID ?>" />
                    <div><label>Post <input class="hc_radio_post_footer" type="checkbox" <?php if ($connector->IntegrationID == get_option('hyConFooterPost')): ?>checked<?php endif ?> /></label></div>
                    <div><label>Page <input class="hc_radio_page_footer" type="checkbox" <?php if ($connector->IntegrationID == get_option('hyConFooterPage')): ?>checked<?php endif ?> /></label></div>
                </td>
                 <td valign="middle" style="vertical-align:top;">
                    <input type="hidden" class="hc_table_hidden_connector_id" value="<?php echo $connector->IntegrationID ?>" />
                    <input type="hidden" class="hc_comment_footer_id" value="<?php echo get_option("hc_comment_connector_id"); ?>" />
                    <div style="margin-left:auto; margin-right:auto;"><label>Active <input class="hc_radio_comment_footer" type="checkbox" <?php if ($connector->IntegrationID == get_option('hc_comment_connector_id')): ?>checked<?php endif ?> /></label></div>
                </td>
                <td valign="middle" class="editDesignButton">
                    <a class="hc_link_connector_shortcode_template_settings" href="/" ><li class="ui-state-default ui-corner-all" title="Shortcode Design" style="width:45px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-pencil" style="width:16px; float:left; margin:0px; padding:0px;"></span>Edit</li></a>
                    <a class="hc_link_connector_statistics" rel="0" href="/"><li class="ui-state-highlight ui-corner-all" title="Statistics" style="width:45px; padding:4px; margin:0px auto; margin-top:5px;"><span class="ui-icon ui-icon-calculator" style="width:16px; float:left; margin:0px; padding:0px;"></span>Stats</li></a>
                </td>
                <td valign="middle" class="editDesignButton">
                    <a class="hc_link_connector_widget_template_settings" href="/"><li class="ui-state-default ui-corner-all" title="Widget Design" style="width:45px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-pencil" style="width:16px; float:left; margin:0px; padding:0px;"></span>Edit</li></a>
                    <a class="hc_link_connector_statistics" rel="1" href="/"><li class="ui-state-highlight ui-corner-all" title="Statistics" style="width:45px; padding:4px; margin:0px auto; margin-top:5px;"><span class="ui-icon ui-icon-calculator" style="width:16px; float:left; margin:0px; padding:0px;"></span>Stats</li></a>
                </td>
             <!--    <td valign="middle" class="editDesignButton">
                    <a class="hc_link_connector_custom_template_settings" href="/"><li class="ui-state-default ui-corner-all" title="Custom Design" style="width:45px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-pencil" style="width:16px; float:left; margin:0px; padding:0px;"></span>Edit</li></a>
                    <a class="hc_link_nogo" rel="1" href="/">&nbsp;</a>
                </td>  -->
                <td valign="middle" class="editDesignButton">
                    <a class="hc_link_connector_lightbox_template_settings" href="/"><li class="ui-state-default ui-corner-all" title="Lightbox Design" style="width:45px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-pencil" style="width:16px; float:left; margin:0px; padding:0px;"></span>Edit</li></a>
                    <a class="hc_link_connector_statistics" rel="2" href="/"><li class="ui-state-highlight ui-corner-all" title="Statistics" style="width:45px; padding:4px; margin:0px auto; margin-top:5px;"><span class="ui-icon ui-icon-calculator" style="width:16px; float:left; margin:0px; padding:0px;"></span>Stats</li></a>
                </td>
                <td valign="middle" class="editDesignButton">
                    <a class="hc_link_connector_squeeze_template_settings" href="/"><li class="ui-state-default ui-corner-all" title="Squeeze Page Design" style="width:45px; padding:4px; margin:0px auto;"><span class="ui-icon ui-icon-pencil" style="width:16px; float:left; margin:0px; padding:0px;"></span>Edit</li></a>
                    <a class="hc_link_connector_statistics" rel="3" href="/"><li class="ui-state-highlight ui-corner-all" title="Statistics" style="width:45px; padding:4px; margin:0px auto; margin-top:5px;"><span class="ui-icon ui-icon-calculator" style="width:16px; float:left; margin:0px; padding:0px;"></span>Stats</li></a>
                 </td>
                <td valign="middle">
                    <a class="hc_link_connector_duplicate" href="/">
                        <li class="ui-state-default ui-corner-all" title="Duplicate" style="float:left; margin:2px;"><span class="ui-icon ui-icon-copy"></span></li>
                    </a>
                    <a href="/" class='hc_link_connector_remove'>
                     <!--   <img src="<?php // echo HYBRIDCONNECT_IAMGES_PATH   ?>/button_remove_large.png" border="0" />  -->
                        <li class="ui-state-default-cancel ui-corner-all" title="Delete" style="width:17px; float:left; margin:2px;"><span class="ui-icon ui-icon-closethick"></span></li>
                    </a>
                </td>
                </tr>
                <tr style="display: none;">
                    <td colspan="13" class="stats_container_background">
                        <div class="hc_container_statistics" id="hc_container_statistics<?php echo $connector->IntegrationID?>">
                        
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <?php if (count($hc_connectors_list) == 1): ?>
        <center>
        <br/>
        <iframe src="http://fast.wistia.net/embed/iframe/exexkl3ydj?playerColor=278de6&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="800" height="450"></iframe>
        </center>
        <?php endif; ?>
    </ul>
    

<?php endif ?>
<script>
    jQuery(document).ready(function(){
        jQuery(":checkbox").checkbox();
    });
</script>
<div class="hc_admin_new_connector_top">
    <div class="hc_admin_new_connector_top_close">
        <img class="img_hc_admin_new_connector_top_close" src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/close-icon.png" border="0" />
    </div>
    <div class="clear"></div>
</div>
<div class="hc_admin_new_connector">
    <div class="labelHeader">GotoWebinar Settings</div>
    
    <table class="tableConnectorDetails">
     <tbody><tr>
     <td class="label_column" style="width:70%">
<a href="http://fast.wistia.net/embed/iframe/d84bon2s11?autoPlay=true&playerColor=278de6&popover=true&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" class="wistia-popover[height=450,playerColor=278de6,width=800]"><img src="http://embed.wistia.com/deliveries/b8f5dfd7c9898d99ce50ad116530f06409a1593c.jpg?image_play_button=true&image_play_button_color=278de6e0&image_crop_resized=150x84" alt="" /><br /><b>Watch this first</b></a>
<script charset="ISO-8859-1" src="http://fast.wistia.com/static/popover-v1.js"></script> </td>
<td style="text-align:left;">
 <b>Help Topics:</b>
        <ol>
        <li><b><a href="http://members.hybrid-connect.com/gotowebinar-integration/" target="_blank">Connecting to the API</a></b></li>
        <li><b><a href="http://members.hybrid-connect.com/connecting-to-a-webinar/" target="_blank">How to Find the Webinar Key</a></b></li>
        </ol>
</td>
</tr>
</table>
<table class="tableConnectorDetails">
        <tr>
            <td class="label_column">Webinar Key:</td>
            <td>
                <input type="text" id="txt_hc_admin_webinar_key" style="width:500px;" value="<?php if($my_webinar):?><?php echo $my_webinar->webinarKey ?><?php endif;?>" />
            </td>
        </tr>
    </table>
 <br/>
    <button id="hc_submit_admin_webinar_settings" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Submit</span></button>
    <button id="hc_remove_admin_webinar_settings" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default-cancel" role="button" aria-disabled="false"><span class="ui-button-text">Remove Webinar</span></button>
</div>
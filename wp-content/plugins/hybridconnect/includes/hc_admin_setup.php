<?php
$hc_ajax_nonce = wp_create_nonce("hyconspecialsecurityforajaxstring");
$license = get_option('HYBRID_CONNECT_LICENSE_STATUS');
?>
<input type="hidden" id="hc_hidden_services_ajaxnonce" value="<?php echo $hc_ajax_nonce ?>" />
<link type="text/css" rel="stylesheet" href="<?php echo HYBRIDCONNECT_CSS_PATH_TEMPLATES ?>/south-street/jquery-ui-1.8.20.custom.css" />
<input type="hidden" id="hc_hidden_setup_page_url" value="<?php echo get_admin_url() . "admin.php?page=hybridConnectLicenseSetup" ?>" />

<div style="width:900px;">

<h1>Hybrid Connect Setup</h1>

<div id="hcsetuptabs">
  <ul>
    <li><a href="#tabs-1">Facebook Setup</a></li>
    <li><a href="#tabs-2">GoToWebinar Setup</a></li>
    <li><a href="#tabs-3">Language Settings</a></li>
  </ul>
  <div id="tabs-1">
    <table class="hc_table_fb_settings" style="width:800px;  border:none; padding:20px;">
        <tr>
            <td class="label_column">Facebook App ID:</td>
            <td><input type="text" name="hc_fb_appid" value="<?php echo get_option('hc_fb_appid') ?>" size="55" id="hc_txt_fb_appid" /></td>
            <td rowspan="2">
                <button id="hc_submit_fb_settings" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Verify Facebook App Settings</span></button>
                <br/> <a href="https://developers.facebook.com/apps" target="_blank">https://developers.facebook.com/apps</a>
                <input type="hidden" id="hc_hidden_fb_ajaxnonce" value="<?php echo $hc_ajax_nonce ?>" /> <br/><br/>
                <span id="hc_admin_fb_txt_valid">
                </span>
            </td>
        </tr>
        <tr>
            <td class="label_column">Facebook App Secret:</td>
            <td><input type="text" name="hc_fb_appsecret" value="<?php echo get_option('hc_fb_appsecret') ?>" size="55" id="hc_txt_fb_appsecret" /></td>
        </tr>
    </table>
   <div id="facebookValidSettings" style="width:800px; display: <?php if(get_option('hc_fb_appvalid')=="1"){ echo "block;"; } else { echo "none;"; } ?>;">
   <center>
  <?php if(get_option('hc_fb_appvalid')=="1") { echo '<b><a href="#" class="getFBAppData">Click Here to Display your application data (Needs a few seconds to load)</a></b>'; } ?>
   </center>
   </div>
   <br/>
     <iframe src="http://fast.wistia.net/embed/iframe/tjtgill0df?playerColor=278de6&version=v1&videoHeight=450&videoWidth=800&volumeControl=true" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" width="800" height="450"></iframe>
  <br/><br/>
  <b>Help Topics:</b>
        <ol>
        <li><b><a href="http://members.hybrid-connect.com/facebook-app-setup/" target="_blank">Facebook App Setup</a></b></li>
        </ol>
  </div>
  <div id="tabs-2">
    <p>If you would like to register people up to GoTowebinar, then you'll need to connect through their API.<br/><br/><a href="https://developer.citrixonline.com/user/register" target="_blank"><b>Click here to create your GoToWebinar API Account</b></a></p>
     <div class="ui-widget" style="display:<?php if(get_option("hc_g2w_valid")=="true"): ?>block; <?php else: ?>none;<?php endif ?> width:800px;" id="hc_service_msg_valid_g2w">
            <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>This service is valid with the current settings</strong></p>
            </div>
        </div>
        <div class="ui-widget" id="hc_service_msg_invalid_g2w" <?php if(get_option("hc_g2w_valid")=="false"): ?>style="display:block; width:800px;"<?php else: ?>style="display:none; width:800px;"<?php endif ?>>
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    <strong>This service couldn't be validated with the current settings</strong></p>
            </div>
        </div>

    <table class="hc_table_fb_settings" style="width:800px;">
    <tbody>
        <tr>
            <td class="label_column">API Key*:</td>
            <td>
            <input type="text" name="g2wAPIKey" id="g2wAPIKey" value="<?php echo get_option('hc_g2w_app_id') ?>" size="60"/>
            </td>
            </tr>
            <tr>
            <td colspan="2">
            <center>
             <button id="hc_button_g2w_connect" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Connect to GoToWebinar</span></button>
            </center>
            </td>
        </tr>
         </tbody>
        </table>
        
      <div id="wistia_d84bon2s11" class="wistia_embed" style="width:800px;height:450px;" data-video-width="800" data-video-height="450">&nbsp;</div>
<script charset="ISO-8859-1" src="http://fast.wistia.com/static/concat/E-v1.js"></script>
<script>
wistiaEmbed = Wistia.embed("d84bon2s11", {
  version: "v1",
  videoWidth: 800,
  videoHeight: 450,
  volumeControl: true,
  playerColor: "278de6"
});
</script>
<br/><br/>
 <b>Help Topics:</b>
        <ol>
        <li><b><a href="http://members.hybrid-connect.com/gotowebinar-integration/" target="_blank">Connecting to the API</a></b></li>
        <li><b><a href="http://members.hybrid-connect.com/connecting-to-a-webinar/" target="_blank">How to Find the Webinar Key</a></b></li>
        </ol>
        
  </div>
  <div id="tabs-3">
 <p>Here you can change the text that is displayed to people when they click the button to be signed up to your mailing list.</p>
 
 <div class="ui-widget" style="display:none; width:800px;" id="hc_update_text_confirmation">
            <div class="ui-state-highlight ui-corner-all" style="margin-top: 20px; padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                    <strong>Your text has been updated!</strong></p>
            </div>
        </div>
        <div class="ui-widget" id="hc_update_text_failure" style="display:none; width:800px;">
            <div class="ui-state-error ui-corner-all" style="padding: 0 .7em;">
                <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                    <strong>Error: For some reason or another we couldn't save your settings.  Please try again and if you're still having problems then contact <a href="http://imimpact.freshdesk.com" target="_blank">support</a></strong></p>
            </div>
        </div>
  <?php
  	// added default messages for language in english
  	$default = array(
		'no_mailing_list_error_text' => "We can't seem to find a mailing list - please check that you have a mailing list set up for this connector",
		'name_email_error_text' => 'Signup Error - Please enter your name and email address',
		'email_error_text' => 'Signup Error - Please enter your email address',
		'valid_email_error_text' => 'Signup Error - Please enter a valid email address'
	);
  	$language_settings = stripslashes_deep(get_option("hc_language_settings", $default));
	$language_settings = array_merge($default, $language_settings);
  ?>
  <table class="hc_table_fb_settings" style="width:800px;">
    <tbody>
        <tr>
            <td class="label_column">Signup Text:</td>
            <td>
            <input type="text" name="hc_signup_text" id="hc_signup_text" value="<?php echo get_option('hc_signup_text', 'Signing you up!'); ?>" size="60"/>
            </td>
        </tr>        
        <tr>
            <td class="label_column">No Mailing List Error Text:</td>
            <td>
            <input type="text" name="no_mailing_list_error_text" class="language-settings" value="<?php echo $language_settings['no_mailing_list_error_text']; ?>" size="60" />
            </td>
        </tr>
        <tr>
            <td class="label_column">Name &amp; Email Error Text:</td>
            <td>
            <input type="text" name="name_email_error_text" class="language-settings" value="<?php echo $language_settings['name_email_error_text']; ?>" size="60" />
            </td>
        </tr>
        <tr>
            <td class="label_column">Email Error Text:</td>
            <td>
            <input type="text" name="email_error_text" class="language-settings" value="<?php echo $language_settings['email_error_text']; ?>" size="60" />
            </td>
        </tr>
        <tr>
            <td class="label_column">Valid Email Error Text:</td>
            <td>
            <input type="text" name="valid_email_error_text" class="language-settings" value="<?php echo $language_settings['valid_email_error_text']; ?>" size="60" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
            <center>
             <button id="hc_signup_text_button" class="ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Update</span></button>
            </center>
            </td>
        </tr> 
       
       
       
         </tbody>
        </table>
  
  </div>
</div>


<div id="hc_admin_ajax_loading">
    <center><img src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/ajax-loading.gif"></center>
</div>
<div id="hc_admin_notification_dialog" title="Notification">
    <p>
        <br/>
        <span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
        <span id="hc_admin_notification_dialog_txt">
        </span>
    </p>
</div>
<script>
    jQuery(document).ready(function(){
        jQuery('.ui-state-default').live('mouseover',function () { jQuery(this).addClass('ui-state-hover'); }).live('mouseout',function () { jQuery(this).removeClass('ui-state-hover'); })
    });
     jQuery(function() {
    jQuery( "#hcsetuptabs" ).tabs();
  });
</script>
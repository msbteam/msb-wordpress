<?php
$hc_ajax_nonce = wp_create_nonce("hyconspecialsecurityforajaxstring");
?>
<link type="text/css" rel="stylesheet" href="<?php echo HYBRIDCONNECT_CSS_PATH_TEMPLATES ?>/south-street/jquery-ui-1.8.20.custom.css" />
<div style="width:820px;">

<div id="wistia_2ucftk7gyd" class="wistia_embed" style="width:800px;height:450px; margin:20px 10px 30px 10px;" data-video-width="800" data-video-height="450" style="margin:auto;"><object id="wistia_2ucftk7gyd_seo" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" style="display:block;height:450px;position:relative;width:800px;"><param name="movie" value="http://embed.wistia.com/flash/embed_player_v2.0.swf?2013-01-16"></param><param name="allowfullscreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="bgcolor" value="#000000"></param><param name="wmode" value="opaque"></param><param name="flashvars" value="banner=true&customColor=278de6&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F14e9b773a1755ddf430063169f5c56e131921882.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=100.63&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F18eda81b36c230f47b0dad7178006fca8cb9506c.jpg%3Fimage_crop_resized%3D800x450&unbufferedSeek=true&videoUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F106d423fdfc83f009a00b2283cd8f525590530ca.bin"></param><embed src="http://embed.wistia.com/flash/embed_player_v2.0.swf?2013-01-16" allowfullscreen="true" allowscriptaccess="always" bgcolor=#000000 flashvars="banner=true&customColor=278de6&hdUrl%5Bext%5D=flv&hdUrl%5Bheight%5D=720&hdUrl%5Btype%5D=hdflv&hdUrl%5Burl%5D=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F14e9b773a1755ddf430063169f5c56e131921882.bin&hdUrl%5Bwidth%5D=1280&mediaDuration=100.63&showVolume=true&stillUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F18eda81b36c230f47b0dad7178006fca8cb9506c.jpg%3Fimage_crop_resized%3D800x450&unbufferedSeek=true&videoUrl=http%3A%2F%2Fembed.wistia.com%2Fdeliveries%2F106d423fdfc83f009a00b2283cd8f525590530ca.bin" name="wistia_2ucftk7gyd_html" style="display:block;height:100%;position:relative;width:100%;" type="application/x-shockwave-flash" wmode="opaque"></embed></object></div>
<script charset="ISO-8859-1" src="http://fast.wistia.com/static/concat/E-v1.js"></script>
<script>
wistiaEmbed = Wistia.embed("2ucftk7gyd", {
  version: "v1",
  videoWidth: 800,
  videoHeight: 450,
  volumeControl: true,
  playerColor: "278de6"
});
</script>
<script charset="ISO-8859-1" src="http://fast.wistia.com/embed/medias/2ucftk7gyd/metadata.js"></script>

<table class="hc_table_fb_settings" style="margin:auto;">
    <tr>
        <td class="label_column">Earn money with Hybrid Connect?</td>
        <td colspan="2">
            <input type="checkbox" name="hc_check_affiliate" <?php if (get_option('hc_earn_money') == 1): ?>checked="checked"<?php endif ?> id="hc_check_affiliate" />
        </td>
    </tr>
</table>
              <br/><br/>
<table class="hc_table_fb_settings" id="affiliate_table" style="margin:auto;<?php if (get_option('hc_earn_money') == 0): ?> display:none;<?php endif ?>">
<tr id="hc_row_affiliate_msg">
        <td colspan="3" id="hc_affiliate_valid_msg">
            <?php if (get_option('hc_affiliate_valid') == 1 && get_option('hc_earn_money') == 1): ?>
                Your affiliate link has been added! (
                <a href="<?php echo get_option('hc_affiliate_link') ?>" target="_blank">
                  <b><?php echo get_option('hc_affiliate_link') ?></b>
                </a>
                )
            <?php elseif (get_option('hc_earn_money') == 1 && get_option('hc_affiliate_valid') != 1): ?>
                We couldn't find an affiliate account with that record, you can sign up or do a password reset <a href="https://www.digiresults.com/marketplace/4928" target="_blank"><b>here</b></a>.
            <?php elseif (get_option('hc_earn_money') == 0): ?>
                If you're not an affiliate, sign up <a href="https://www.digiresults.com/marketplace/4928" target="_blank"><b>here</b></a>.
            <?php endif ?>
        </td>
    </tr>
    <tr id="affiliate_manual">
        <td>
            Enter DigiResults Affiliate link:
        </td>
        <td>
            <input type="text" name="hc_affiliate_manual_link" value="<?php echo get_option('hc_affiliate_link') ?>" size="55" id="hc_affiliate_manual_link" />
        </td>
        <td>
            <button id="hc_submit_affiliate_manual" class="hc_update_service_settings ui-button ui-widget ui-corner-all ui-button-text-only ui-state-default" role="button" aria-disabled="false"><span class="ui-button-text">Update Affiliate Link</span></button>
            <input type="hidden" id="hc_hidden_affiliate_ajaxnonce" value="<?php echo $hc_ajax_nonce ?>" />
        </td>
    </tr>
</table>
<br/><br/>
<div style="width:700px; margin:auto;">
<img src="<?php echo HYBRIDCONNECT_IAMGES_PATH ?>/yourAffiliateLink.png" style="margin-left:20px; float:right;">
<h2>
    Earn money with Hybrid Connect
</h2>
<p>You can effortlessly make commissions with Hybrid Connect by simply ticking the box below and giving us permission to place a small affiliate link underneath each optin box on your site.</p>
<p>The is a very soft sell approach that won't alienate your visitors.  By adding a simple "powered by Hybrid Connect" link below your optin box, you can catch prospects that are interesting
in using Hybrid Connect to power their own optin boxes.  This requires no extra effort on your behalf!</p>
<p>As an affiliate you make <b><u>50% of the full sale</b>.</u></p>
<h3>DigiResults Affiliate Program</h3>
<p>We now use the DigiResults affiliate platform because we believe it's the best platform for you.  <a href="https://www.digiresults.com/marketplace/4928" target="_blank"><b>To get started, visit our affiliate page here and click "Sign Up" at the top right of the page</b></a>.</p>
<p>Before you can start promoting, we will need to approve you as an affiliate.  We'll do this for you within a couple of hours.</p>
<div id="hc_admin_notification_dialog" title="Notification">
    <p>
        <br/>
        <span class="ui-icon ui-icon-info" style="float:left; margin:0 7px 20px 0;"></span>
        <span id="hc_admin_notification_dialog_txt">
        </span>
    </p>
 </div>
</div>
</div>
<script>
    jQuery(document).ready(function(){
        jQuery('.ui-state-default').live('mouseover',function () { jQuery(this).addClass('ui-state-hover'); }).live('mouseout',function () { jQuery(this).removeClass('ui-state-hover'); })
    });
</script>
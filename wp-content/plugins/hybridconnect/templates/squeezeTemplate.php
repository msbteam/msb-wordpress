<!DOCTYPE html>
<?php error_reporting(0); ?>
<html dir="ltr" lang="en-US" style="height:auto;">
<head>
<meta charset="UTF-8" />
<title><?php echo get_the_title(); ?></title>
<?php wp_head(); ?>
</head>
<body style="padding-bottom:20px;">
<div id="hc_ie_bg_fix" style="width:100%; height:100%; top:0px; left:0px; z-index:-20;"></div>
	<?php the_content(); ?>
</body>
<!--</div>-->
 <?php wp_footer(); ?>
</html>
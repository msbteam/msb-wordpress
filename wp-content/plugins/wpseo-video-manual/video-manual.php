<?php
/*
Plugin Name: Video Manual for WordPress SEO by Yoast
Version: 1.0.2
Plugin URI: http://yoast.com/wordpress/seo-video-manual/
Description: This module adds Manual videos for WordPress SEO to your install. Don't waste any time training editors, they can just view the videos and see for themselves!
Author: Joost de Valk
Author URI: http://yoast.com

Copyright 2012-2013 Joost de Valk (email: joost@yoast.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// retrieve our license key from the DB
$options = get_option( 'wpseo_video_manual' );
if ( !is_array( $options ) ) {
	$options = array(
		'dbversion' => 1
	);
	update_option( 'wpseo_video_manual', $options );
}

if ( isset( $options['yoast-video-manual-license'] ) && !empty( $options['yoast-video-manual-license'] ) ) {
	if ( !class_exists( 'EDD_SL_Plugin_Updater' ) ) {
		// load our custom updater
		include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
	}

	$edd_updater = new EDD_SL_Plugin_Updater( 'http://yoast.com/', __FILE__, array(
			'version'   => '1.0.2', // current version number
			'license'   => trim( $options['yoast-video-manual-license'] ), // license key
			'item_name' => 'Video Manual for WordPress SEO', // name of this plugin in the Easy Digital Downloads system
			'author'    => 'Joost de Valk' // author of this plugin
		)
	);
}

/**
 * Add instruction videos for admins and editors to the appropriate places.
 *
 * @package    WordPress SEO
 * @subpackage WordPress SEO Video Manual
 */

/**
 * wpseo_video_Video_Sitemap class.
 *
 * @package WordPress SEO Video Manual
 * @since   0.1
 */
class wpseo_Video_Manual {

	/**
	 * Constructor for the wpseo_Video_Manual class.
	 *
	 * @since 1.0
	 */
	public function __construct() {

		$options = get_option( 'wpseo_video_manual' );

		$this->set_defaults();

		if ( is_admin() ) {
			add_action( 'admin_init', array( $this, 'options_init' ) );
			add_action( 'admin_menu', array( $this, 'register_settings_page' ), 99 );

			add_action( 'update_option_wpseo_video_manual', array( $this, 'activate_license' ) );
			if ( isset( $options['yoast-video-manual-license'] ) ) {
				if ( isset( $options['yoast-video-manual-license-status'] )	&& $options['yoast-video-manual-license-status'] == 'valid'	) {
					add_action( 'wpseo_tab_header', array( $this, 'tab_header' ), 99 );
					add_action( 'wpseo_tab_content', array( $this, 'tab_content' ) );
				}
			}
		}
	}

	function options_init() {
		register_setting( 'yoast_wpseo_video_manual_options', 'wpseo_video_manual' );
	}

	/**
	 * Register the Video SEO submenu.
	 *
	 * @since 1.0
	 */
	function register_settings_page() {
		add_submenu_page( 'wpseo_dashboard', __( 'Manual', 'wordpress-seo' ), __( 'Manual', 'wordpress-seo' ),
			'manage_options', 'wpseo_manual', array( $this, 'admin_panel' ) );
	}

	/**
	 * See if there's a license to activate
	 *
	 * @since 1.0
	 */
	function activate_license() {
		$options = get_option( 'wpseo_video_manual' );

		if ( !isset( $options['yoast-video-manual-license'] ) || empty( $options['yoast-video-manual-license'] ) ) {
			unset( $options['yoast-video-manual-license'] );
			unset( $options['yoast-video-manual-license-status'] );
			update_option( 'wpseo_video_manual', $options );
			return;
		}

		if ( isset( $options['yoast-video-manual-license'] ) && !empty( $options['yoast-video-manual-license'] ) ) {

			// data to send in our API request
			$api_params = array(
				'edd_action' => 'activate_license',
				'license'    => $options['yoast-video-manual-license'],
				'item_name'  => urlencode( 'Video Manual for WordPress SEO' ) // the name of our product in EDD
			);

			// Call the custom API.
			$url      = add_query_arg( $api_params, 'http://yoast.com/' );
			$args     = array(
				'timeout' => 25,
				'rand' => rand(1000, 9999)
			);
			$response = wp_remote_get( $url, $args );

			// make sure the response came back okay
			if ( is_wp_error( $response ) ) {
				return;
			}

			// decode the license data
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			// $license_data->license will be either "valid" or "invalid"

			$options['yoast-video-manual-license-status'] = $license_data->license;
			update_option( 'wpseo_video_manual', $options );
		}
	}

	/**
	 * Register defaults for the video sitemap
	 *
	 * @since 1.0
	 */
	function set_defaults() {
		$options = get_option( 'wpseo_video_manual' );
	}

	/**
	 * Adds the header for the Video tab in the WordPress SEO meta box on edit post pages.
	 *
	 * @since 1.0
	 */
	function tab_header() {
		echo '<li class="manual"><a <a class="wpseo_tablink" href="#wpseo_manual">' . __( 'Manual', 'wordpress-seo' ) . '</a></li>';
	}

	/**
	 * Outputs the content for the Manual tab in the WordPress SEO meta box on edit post pages.
	 *
	 * @since 1.0
	 */
	function tab_content() {
		$content = '<style type="text/css">.video-container { margin: 15px 15px 0 0; text-align: center; }</style><p>'.__('Watch these videos to learn how to use the WordPress SEO plugin:').'</p><div class="video-container alignleft">
<a href="javascript:void(0)" onclick="window.open(\'http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-01-general\',\'\',\'width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0\')">
<img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-01-general.jpg" alt="General"><br>General</a>
</div>
<div class="video-container alignleft">
<a href="javascript:void(0)" onclick="window.open(\'http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-02-page-analysis\',\'\',\'width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0\')">
<img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-02-page-analysis.jpg" alt="Page Analysis"><br>Page Analysis</a>
</div>
<div class="video-container alignleft">
<a href="javascript:void(0)" onclick="window.open(\'http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-03-social\',\'\',\'width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0\')">
<img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-03-social.jpg" alt="Social"><br>Social</a>
</div>
<div class="video-container alignleft">
<a href="javascript:void(0)" onclick="window.open(\'http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-04-profile\',\'\',\'width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0\')">
<img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-04-profile.jpg" alt="Profile"><br>Profile</a>
</div>
<br style="clear: both;">';
		$this->do_tab( 'manual', __( 'Manual', 'wordpress-seo' ), $content );
	}

	/**
	 * Output a tab in the WP SEO Metabox
	 *
	 * @since 1.0
	 *
	 * @param string $id      CSS ID of the tab.
	 * @param string $heading Heading for the tab.
	 * @param string $content Content of the tab.
	 */
	function do_tab( $id, $heading, $content ) {
		?>
    <div class="wpseotab <?php echo $id ?>">
        <h4 class="wpseo-heading"><?php echo $heading ?></h4>
        <table class="form-table">
			<?php echo $content ?>
        </table>
    </div>
	<?php
	}

	/**
	 * Outputs the admin panel for the Video manual
	 *
	 * @since 1.0
	 */
	function admin_panel() {
		$options = get_option( 'wpseo_video_manual' );
		?>
    <div class="wrap">

        <a href="http://yoast.com/wordpress/video-seo/">
            <div id="yoast-icon"
                 style="background: url('<?php echo WPSEO_URL; ?>images/wordpress-SEO-32x32.png') no-repeat;"
                 class="icon32">
                <br/>
            </div>
        </a>

        <h2 id="wpseo-title"><?php _e( "Video Manuals for WordPress SEO", 'wordpress-seo' ); ?></h2>

        <form action="<?php echo admin_url( 'options.php' ); ?>" method="post" id="wpseo-conf">

			<?php

			settings_fields( 'yoast_wpseo_video_manual_options' );

			echo '<h2>' . __( 'License Key' ) . '</h2>';
			echo '<label class="textinput" for="license">' . __( 'License Key', 'wordpress-seo' ) . ':</label> '
				. '<input id="license" class="textinput" type="text" name="wpseo_video_manual[yoast-video-manual-license]" value="'
				. ( isset( $options['yoast-video-manual-license'] ) ? $options['yoast-video-manual-license'] : '' ) . '"/>';

			$license_valid = false;
			if ( !isset( $options['yoast-video-manual-license'] ) || $options['yoast-video-manual-license-status'] != 'valid'
			) {
				echo '<p>' . __( 'Insert the license key you got when you bought the plugin, then click save.', 'wordpress-seo' ) . '</p>';
			} else {
				$license_valid = true;
			}

			?>
            <div class="submit">
                <input type="submit" class="button-primary" name="submit"
                       value="<?php _e( "Save License", 'wordpress-seo' ); ?>"/>
            </div>
        </form >
		<?php
		if ( $license_valid ) {
			?>			<h2>Admin Instruction videos</h2>
            <style>.video-container { margin-right: 20px; float: left; text-align: center;	margin-bottom: 20px; }</style>
            <div class="video-container">
                <a href="javascript:void(0)" onclick="window.open('http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-05-admin-dashboard','','width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0')">
                    <img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-05-admin-dashboard.jpg" alt="Dashboard"><br>Dashboard</a>
            </div>
            <div class="video-container">
                <a href="javascript:void(0)" onclick="window.open('http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-06-admin-titles-meta','','width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0')">
                    <img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-06-admin-titles-meta.jpg" alt="Titles Meta"><br>Titles Meta</a>
            </div>
            <div class="video-container">
                <a href="javascript:void(0)" onclick="window.open('http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-07-admin-social','','width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0')">
                    <img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-07-admin-social.jpg" alt="Social"><br>Social</a>
            </div>
            <div class="video-container">
                <a href="javascript:void(0)" onclick="window.open('http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-08-admin-xml-sitemaps','','width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0')">
                    <img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-08-admin-xml-sitemaps.jpg" alt="XML Sitemaps"><br>XML Sitemaps</a>
            </div>
            <div class="video-container">
                <a href="javascript:void(0)" onclick="window.open('http://wordpress.videousermanuals.com/yoast-video-player.php?&amp;yoast_video=wp-seo-09-admin-permalinks','','width=960,height=600,menubar=0,status=0,location=0,toolbar=0,scrollbars=0')">
                    <img src="http://vum.s3.amazonaws.com/wp/yoast/wordpress-seo-by-yoast/thumb-09-admin-permalinks.jpg" alt="Permalinks"><br>Permalinks</a>
            </div>

			<?php
		}
		?>
    </div>
	<?php

	}
}

/**
 * Throw an error if WordPress SEO is not installed.
 *
 * @since 1.0
 */
function yoast_video_manual_wpseo_missing_error() {
	echo '<div class="error"><p>Please <a href="' . admin_url( 'plugin-install.php?tab=search&type=term&s=wordpress+seo&plugin-search-input=Search+Plugins' ) . '">install &amp; activate WordPress SEO by Yoast</a> and then enable its XML sitemap functionality to allow the Video SEO module to work.</p></div>';
}

/**
 * Initialize the Video SEO module on plugins loaded, so WP SEO should have set its constants and loaded its main classes.
 *
 * @since 1.0
 */
function yoast_video_manual_init() {
	if ( defined( 'WPSEO_VERSION' ) ) {
		$wpseo_video_manual = new wpseo_Video_Manual();
	} else {
		add_action( 'all_admin_notices', 'yoast_video_manual_wpseo_missing_error' );
	}
}
add_action( 'plugins_loaded', 'yoast_video_manual_init' );
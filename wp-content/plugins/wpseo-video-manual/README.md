wpseo-video-manual
==================

Premium add-on for the WordPress SEO plugin that carries instruction video's for editors and admins.

Changelog
=========

1.0.2
-----

* Fix another bug in activation.

1.0.1
-----

* Activating a plugin once is more than enough.

1.0
---

* Initial commit.
<?php
/* Plugin Name: WP Fancy FAQ
Plugin URI: http://www.bitset.org/
Description: Fancy FAQ is a complete Frequently Asked Question Management plugin for WordPress. It provides an Fancy FAQ custom post type With category taxonomy which can be used to effortlessly add FAQ sections to your website. It's easy to install the plugin and adding new questions and categories. After adding questions you can show the faq items in your "Post", "Page", or in "Widget" of your website using shortcode.
Version: 1.0
Author: BitSet Solution
Author URI: http://www.bitset.org/
License: Commercial
*/

define( 'FANCYFAQ_URL', plugin_dir_url(__FILE__) );
define( 'FANCYFAQ_PATH', plugin_dir_path(__FILE__) );

require_once( FANCYFAQ_PATH . 'fancyfaq.posttype.php' );

function fancyfaq_scripts() {
    if (!is_admin()) {
	
	wp_enqueue_script('jquery');
		
	wp_enqueue_script('utility', plugins_url('/js/utility.js', __FILE__));

	wp_enqueue_style ('fancyfaq_style', plugins_url('/css/style.css', __FILE__));
	}
}
add_action('init', 'fancyfaq_scripts', 0);

add_shortcode("fancyfaq", "fancyfaq_display_faq");

function fancyfaq_display_faq($attr, $content) {

    extract(shortcode_atts(array(
                'category' => ''
                    ), $attr));
					
    $plugins_url = plugins_url();

 	$html .= '<div class="sample"><dl class="faqs">';
	
	$tmp = $wp_query;
	$wp_query = new WP_Query('post_type=fancyfaq&orderby=menu_order&order=ASC&fancyfaq_cat='.$category);
	if($wp_query->have_posts()) : while($wp_query->have_posts()) : $wp_query->the_post();
	
	$question = get_the_title();
	$answer = get_the_content();

	$html .= '<dt>';
	$html .= $question;
	$html .= '</dt><dd>';
	$html .= $answer;
	$html .= '</dd>';

	endwhile;
	wp_reset_query();
	endif;
	
	$html .= '</dl></div>';
	
	$wp_query = $tmp;
	
    return $html;
}
?>
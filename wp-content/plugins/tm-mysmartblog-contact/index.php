<?php
/*
Plugin Name: MySmartBlog Contact Form
Description: Plugin for MySmartBlog.com that adds a contact form.
Version: 0.5
Author: David Wood
Author URI: http://iamdavidwood.com/
============================================================================================================
This software is provided "as is" and any express or implied warranties, including, but not limited to, the
implied warranties of merchantibility and fitness for a particular purpose are disclaimed. In no event shall
the copyright owner or contributors be liable for any direct, indirect, incidental, special, exemplary, or
consequential damages (including, but not limited to, procurement of substitute goods or services; loss of
use, data, or profits; or business interruption) however caused and on any theory of liability, whether in
contract, strict liability, or tort (including negligence or otherwise) arising in any way out of the use of
this software, even if advised of the possibility of such damage.

For full license details see license.txt
============================================================================================================ */

require(dirname(__FILE__).'/admin.php');

class tm_mysmartblog_contact {
	function tm_mysmartblog_contact() {
		add_shortcode('mysmartblog_contact', array(&$this, 'mysmartblog_contact'));
	}
	function mysmartblog_contact() {
		$return = '';
		// Display contact information here:
		if($info = get_option('tm-mysmartblog-settings')) {
			if(isset($info['address']) && isset($info['city']) && isset($info['state']) && isset($info['zip'])) {
				$display_address = $info['address'].'<br>';
				if(isset($info['address2']) && $info['address2'] != '') $display_address .= $info['address2'].'<br>';
				$display_address .= $info['city'].', '.$info['state'].' '.$info['zip'];
				$address = preg_replace('/, /', ',', trim($info['address']));
				$address = str_replace(' ', '+', $address);
				if(isset($info['address2']) && $info['address2'] != '') {
					$address2 = preg_replace('/, /', ',', trim($info['address2']));
					$address .= ','.str_replace(' ', '+', $address2);
				}
				$address = str_replace('#', '', $address);
				$city = str_replace(' ', '+', trim($info['city']));
				$state = str_replace(' ', '+', trim($info['state']));
				$zip = str_replace(' ', '+', trim($info['zip']));
				$address .= ','.$city.','.$state.','.$zip;
				$maps_url = 'http://maps.google.com/maps/api/staticmap?center='.$address;
				$maps_url .= '&zoom=14&size=250x250&maptype=roadmap&markers=color:red%7Ccolor:red%7Clabel:A%7C'.$address;
				$maps_url .= '&sensor=false';
			}
			$return .= '<div id="mysmartblog-contactinfo">';
            	if($maps_url && $maps_url != '') $return .= '<a href="http://maps.google.com/?q='.$address.'" target="_blank" title="See in Google Maps">
            	<img src="'.$maps_url.'" style="float:left; border: 1px solid #ccc; padding: 2px; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; margin: 0 20px 15px 50px" class="google_map" border="0" /></a>';
            $return .= '<p style="font-size: 15px; line-height: 22px; margin-top: 20px;">';
				if(isset($info['company']) && $info['company'] != '') $return .= '<b style="font-size: 16px;">'.$info['company'].'</b><br><br>';
				if(isset($display_address) && $display_address != '') $return .= $display_address.'<br>';
				if(isset($info['busphone']) && $info['busphone'] != '') $return .= 'Business: '.$info['busphone'].'<br>';
				if(isset($info['cellphone']) && $info['cellphone'] != '') $return .= 'Cellphone: '.$info['cellphone'].'<br>';
				if(isset($info['fax']) && $info['fax'] != '') $return .= 'Fax: '.$info['fax'];
			$return .= '</p>';
			if(isset($info['facebook']) || isset($info['twitter']) || isset($info['linkedin'])) {
				$return .= '<p>';
				if(isset($info['facebook']) && $info['facebook'] != '') $return .= '<a href="'.$info['facebook'].'" target="_blank"><img src="'.plugins_url('inc/facebook.png', __FILE__).'" alt="Follow me on Facebook" /></a>';
				if(isset($info['twitter']) && $info['twitter'] != '') $return .= '<a href="'.$info['twitter'].'" target="_blank"><img src="'.plugins_url('inc/twitter.png', __FILE__).'" alt="Follow me on Twitter" /></a>';
				if(isset($info['linkedin']) && $info['linkedin'] != '') $return .= '<a href="'.$info['linkedin'].'" target="_blank"><img src="'.plugins_url('inc/linkedin.png', __FILE__).'" alt="Find me on LinkedIn" /></a>';
				$return .= '</p>';
			}
			$return .= '</div>';
		}
		$return .= '<div id="mysmartblog-contact" style="clear: both; background-color: #eeeeee; text-align: center; border-radius: 5px; border: 1px solid #cccccc; padding: 6px;">
		<hr style="background-color: #ccc; border-top: 2px solid #eeeeee; border-bottom: 0px;" />
		<h2 style="font-weight: 900;">Send Us a Message!</h2>';
		// Check if form has been submitted and display form/message accordingly
		if(isset($_REQUEST['mybloginfo-submit']) && $_REQUEST['mybloginfo-submit'] != '') {
			// Form has been submitted! Check for errors
			$errors = array();
			require_once(dirname(__FILE__).'/recaptchalib.php');
			$privatekey = '6LeZVMQSAAAAAHCCHzsBqMh4tuZjw-RGbIPIx7Or';
			$resp = recaptcha_check_answer ($privatekey,
											$_SERVER["REMOTE_ADDR"],
											$_POST["recaptcha_challenge_field"],
											$_POST["recaptcha_response_field"]);
			if (!$resp->is_valid) {
			// What happens when the CAPTCHA was entered incorrectly
				$errors[] = 'You entered the CAPTCHA incorrectly! Please try again.';
			}
			if(isset($_REQUEST['user_name']) && $_REQUEST['user_name'] != '') $user_name = $_REQUEST['user_name'];
				else $errors[] = 'You must enter a name!';
			if(isset($_REQUEST['email']) && $_REQUEST['email'] != '' && preg_match('/^[a-zA-Z0-9][a-zA-Z0-9.+\-]*@([a-zA-Z0-9_\-]+\.)+[a-zA-Z0-9]{2,6}(.[a-zA-Z]{2})?$/', $_REQUEST['email'])) $email = $_REQUEST['email'];
				else $errors[] = 'You must enter a valid email address!';
			if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != '') $phone = $_REQUEST['phone'];
				else $phone = '';
			if(isset($_REQUEST['message']) && $_REQUEST['message'] != '') $user_message = $_REQUEST['message'];
				else $errors[] = 'You must enter a message!';
			// Check if errors!
			if(count($errors) > 0) {
				// There were errors! Display error messages and form again
				$return .= '<p>';
				foreach($errors as $error) $return .= $error.'<br>';
				$return .= '</p>';
				$return .= '<form action="#mysmartblog-contact" method="post">
	<p><label>Name: </label><br>
	<input type="text" name="user_name" style="border: 1px solid #ccc; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 5px;" required value="';
			if(isset($_REQUEST['user_name'])) $return .= $_REQUEST['user_name'];
			$return .= '" /></p>
    <p><label>Email: </label><br>
	<input style="border-radius: 5px; padding: 5px;" type="email" name="email" required value="';
			if(isset($_REQUEST['email'])) $return .= $_REQUEST['email'];
			$return .= '" /></p>
    <p><label>Phone: </label><br>
	<input style="border-radius: 5px; padding: 5px;" type="text" name="phone" value="';
			if(isset($_REQUEST['phone'])) $return .= $_REQUEST['phone'];
			$return .= '" /></p>
    <p><label>Message: </label><br><textarea name="message" cols="50" rows="5" required>';
			if(isset($_REQUEST['message'])) $return .= $_REQUEST['message'];
			$return .= '</textarea></p><p>';
    		require_once(dirname(__FILE__).'/recaptchalib.php');
			$publickey = '6LeZVMQSAAAAAFkIbReOuMF_FbS4zPKBEjv63-tl';
			$return .= recaptcha_get_html($publickey);
			$return .= '</p><p><input type="submit" value="Submit" style="padding: 3px;" name="mybloginfo-submit"  /></p>
</form>';
			} else {
				// No errors! Send email and display confirmation/thank you message!
				if(!($admin_email = get_option('admin_email'))) $return .= 'An unexpected error occured! Message not sent.';
				else {
					$to = $admin_email;
					$headers = "To: {$admin_email}\nFrom: {$user_name} <{$email}>\n";
					$subject = 'Contact form submission';
					$message = "Name: {$user_name}\nEmail: {$email}\nPhone: {$phone}\nMessage:\n{$user_message}\n";
					if(mail($to, $subject, $message, $headers)) { $return .= 'Thank you! Your message has been sent successfully';
						$to = $email;
						$headers = "To: {$user_name} <{$email}>\nFrom: {$user_name} <{$email}>\n";
						$subject = 'Thank you for contacting us!';
						$message = 'Thank you for contacting us!';
						@mail($to, $subject, $message, $headers);
					}
					else $return .= 'There was an error when sending your message! Message not sent.';
				}
			}
		} else {
			// Form not yet submitted, display form.
			$return .= '<form action="#mysmartblog-contact" method="post" style="margin: 0 auto; text-align: center;">
	<p><label>Name: </label><br>
	<input type="text" style="border: 1px solid #ccc; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px; min-width: 54%;" name="user_name" required value="';
			if(isset($_REQUEST['user_name'])) $return .= $_REQUEST['user_name'];
			$return .= '" /></p>
    <p><label>Email: </label><br>
	<input type="email" style="border: 1px solid #ccc; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px; min-width: 54%;" name="email" required value="';
			if(isset($_REQUEST['email'])) $return .= $_REQUEST['email'];
			$return .= '" /></p>
    <p><label>Phone: </label><br>
	<input type="text" style="border: 1px solid #ccc; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px; min-width: 54%;" name="phone" value="';
			if(isset($_REQUEST['phone'])) $return .= $_REQUEST['phone'];
			$return .= '" /></p>
    <p><label>Message: </label><br><textarea style="border: 1px solid #ccc; border-radius: 5px; -moz-border-radius: 5px; -webkit-border-radius: 5px; padding: 10px; max-width: 54%;" name="message" cols="50" rows="5" required>';
			if(isset($_REQUEST['message'])) $return .= $_REQUEST['message'];
			$return .= '</textarea></p><p style="margin: 0 auto; text-align: center;">';
    		require_once(dirname(__FILE__).'/recaptchalib.php');
			$publickey = '6LeZVMQSAAAAAFkIbReOuMF_FbS4zPKBEjv63-tl';
            $return .= '<div style="margin: 0 auto; width: 325px;" align="center">';
			$return .= recaptcha_get_html($publickey);
			$return .= '</p><p><input type="submit" value="Submit" style="width: 140px;" name="mybloginfo-submit"  /></p>

</form>';
            $return .= '</div>';
		}
		$return .= '</div>';
		return $return;
	}
}

new tm_mysmartblog_contact;

<?php

add_action('admin_menu', 'tm_mybloginfo_adminsetup');

function tm_mybloginfo_adminsetup() {
	// Create admin menu item
	add_submenu_page( 'options-general.php', 'Contact Us Setup', 'Contact Us Setup', 'manage_options', 'tm-mybloginfo-contact', 'tm_mybloginfo_contact' );
}

function tm_mybloginfo_contact() {
	// Check if form has been submitted or not
	if(isset($_POST['tm-mysmartblog-update'])) {
		// Save input
		if(is_array($_POST['contact_info']))
			$options = $_POST['contact_info'];
		update_option('tm-mysmartblog-settings', $options);
		echo '<div id="message" class="updated">Settings updated!</div>';
	} elseif(!($options = get_option('tm-mysmartblog-settings'))) {
		$options = array();
	}
	?>
	<div class="wrap">
    	<h2>Contact Us Setup</h2>
        <form action="" method="post">
        	<p><label>Company Name: </label> <input type="text" name="contact_info[company]" value="<?php if(isset($options['company'])) echo $options['company']; ?>" /></p>
            <p><label>Address Line 1: </label> <input type="text" name="contact_info[address]" value="<?php if(isset($options['address'])) echo $options['address']; ?>" /></p>
			<p><label>Address Line 2: </label> <input type="text" name="contact_info[address2]" value="<?php if(isset($options['address2'])) echo $options['address2']; ?>" /></p>
            <p><label>City: </label> <input type="text" name="contact_info[city]" value="<?php if(isset($options['city'])) echo $options['city']; ?>" /></p>
            <p><label>State: </label> <input type="text" name="contact_info[state]" value="<?php if(isset($options['state'])) echo $options['state']; ?>" /></p>
            <p><label>Zip: </label> <input type="text" name="contact_info[zip]" value="<?php if(isset($options['zip'])) echo $options['zip']; ?>" /></p>
            <p><label>Business Phone: </label> <input type="text" name="contact_info[busphone]" value="<?php if(isset($options['busphone'])) echo $options['busphone']; ?>" /></p>
            <p><label>Cell Phone: </label> <input type="text" name="contact_info[cellphone]" value="<?php if(isset($options['cellphone'])) echo $options['cellphone']; ?>" /></p>
            <p><label>Fax Number: </label> <input type="text" name="contact_info[fax]" value="<?php if(isset($options['fax'])) echo $options['fax']; ?>" /></p>
			<p><label><strong>Social Media Links (full URL, include http://)</strong></label><br />
			<label>Facebook:</label> <input type="text" name="contact_info[facebook]" value="<?php if(isset($options['facebook'])) echo $options['facebook']; ?>" /><br />
			<label>Twitter:</label> <input type="text" name="contact_info[twitter]" value="<?php if(isset($options['twitter'])) echo $options['twitter']; ?>" /><br />
			<label>LinkedIn:</label> <input type="text" name="contact_info[linkedin]" value="<?php if(isset($options['linkedin'])) echo $options['linkedin']; ?>" /></p>
            <p><input type="submit" value="Update Contact Info" name="tm-mysmartblog-update" /></p>
        </form>
    </div>
<?php }

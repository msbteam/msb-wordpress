<?php

/**
 * This code is for debugging purposes.
 * Uncomment the lines to force WordPress to check for plugin updates on every page load.
 */
// set_transient( 'update_plugins', null );
// set_site_transient( 'update_plugins', null );

/**
 * Class for sending outbound requests for license key registration, checking for updates, etc.
 *
 * @author Matthew Ruddy
 */
class ESP_HTTP {

    /**
     * Our API Url
     *
     * @var string
     */
    public $api_url = 'http://easingslider.com/api/';

    /**
     * Transient expiry time (default 6 hours)
     *
     * @var int
     */
    public $transient_expiry = 21600;

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {

        // Get the plugin slug
        $slug = plugin_basename( EasingSliderPro::get_file() );

        // Establish variables
        $this->api_url          = apply_filters( 'easingsliderpro_http_api_url', $this->api_url );
        $this->transient_expiry = apply_filters( 'easingsliderpro_http_transient_expiry', $this->transient_expiry );

        // Queue actions & filters
        add_filter( 'pre_set_transient_update_plugins', array( $this, 'modify_updates_transient' ) );
        add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'modify_updates_transient' ) );
        add_filter( 'plugins_api', array( $this, 'get_update_info' ), 10, 3 );
        add_filter( "after_plugin_row_{$slug}", array( $this, 'plugins_registration_message' ) );

    }

    /**
     * Modifies the WordPress updates transient to include our update information
     *
     * @param  object $checked_data The data of checked plugin updates
     * @return object
     */
    public function modify_updates_transient( $checked_data ) {

        // Bail if already checked
        if ( ! isset ( $checked_data->checked ) OR empty( $checked_data->checked ) ) {
            return $checked_data;
        }

        // Get current updates
        $updates = $this->get_updates();

        // Bail if false returned
        if ( ! $updates ) {
            return $checked_data;
        }

        // Bail if we are using the current version
        if ( version_compare( EasingSliderPro::$version, $updates->new_version, '>=' ) ) {
            return $checked_data;
        }

        // Remove plugin's API information (not need for this).
        unset( $updates->information );

        // Get plugin slug
        $slug = plugin_basename( EasingSliderPro::get_file() );

        // Add to WordPress updates transient array
        $checked_data->response[ $slug ] = $updates;

        return $checked_data;

    }

    /**
     * Displays plugin update information on the WordPress Plugins page
     *
     * @param  object $res
     * @param  string $action The action occurring
     * @param  object $args   The arguments
     * @return object
     */
    public function get_update_info( $res, $action, $args ) {

        // Return the plugin information object
        if ( isset( $args->slug ) && $args->slug == 'easingsliderpro' ) {

            // Get available update info stored in database
            $updates = $this->get_updates();

            // Ensure sections are an array (avoids errors)
            $updates->information->sections = (array) $updates->information->sections;

            // Return information (if it exists)
            if ( $updates->information ) {
                return $updates->information;
            }

        }

        return $res;

    }

    /**
     * Displays messages under the Easing Slider "Pro" plugin row
     *
     * @param  string $plugin_name The plugin name
     * @return void
     */
    public function plugins_registration_message( $plugin_name ) {

        // Display messages
        if ( ! get_option( 'easingsliderpro_license_key_is_valid' ) ) {
            echo '</tr><tr class="plugin-update-tr"><td colspan="3" class="plugin-update"><div class="update-message">'. __( 'Please <a href="admin.php?page=easingsliderpro_edit_settings">register</a> your copy of Easing Slider "Pro" to receive access to upgrades and support. Need a license key? <a href="http://easingslider.com/purchase">Purchase one here</a>.', 'easingsliderpro' ) .'</div></td>';
        }

    }

    /**
     * Stores an error response from the API as an option.
     * This is necessary to allow us to display the errors messages on
     * all Easing Slider plugin pages, not just on the consequent request.
     *
     * @param  string $message The response
     * @param  string $type    The response type
     * @return void
     */
    public function store_error( $message ) {

        // Store the response in the database
        update_option( 'easingsliderpro_registration_errors', array(
            'message' => $message,
            'type'    => 'error permanent'
        ) );

    }

    /**
     * Get plugin updates, requesting to server if required.
     *
     * @return array|false
     */
    public function get_updates() {

        // Bail if update have been manually disabled
        if ( ! apply_filters( 'easingsliderpro_enable_updates', __return_true() ) ) {
            return false;
        }

        // Get setting so we can retrieve the license key
        $settings = get_option( 'easingsliderpro_settings' );

        // Bail if there is no license key
        if ( ! isset( $settings['license_key'] ) OR empty( $settings['license_key'] ) ) {
            return false;
        }

        // Make the request
        $request = wp_remote_post( $this->api_url, array(
            'timeout' => 10,
            'body'    => array(
                'action'  => 'get_updates',
                'key'     => trim( $settings['license_key'] ),
                'slug'    => 'easingsliderpro',
                'url'     => home_url(),
                'version' => EasingSliderPro::$version
            )
        ) );

        // Get the response headers
        $headers = (object) wp_remote_retrieve_headers( $request );

        // Get the response body
        $response = json_decode( wp_remote_retrieve_body( $request ) );
                
        // Bail if no response was received (service may temporarily be offline)
        if ( ! isset( $response->response ) ) {
            return false;
        }

        // If the status indicates the request wasn't a success, bail and flag invalid license key.
        if ( $headers->status != 200 ) {

            // Mark the license as invalid
            update_option( 'easingsliderpro_license_key_is_valid', false );

            // Add a notice in the admin area notifying the user
            $this->store_error( $response->response );

            return false;

        }

        // Set license key admin option 
        update_option( 'easingsliderpro_license_key_is_valid', true );

        // Delete any remaining errors
        delete_option( 'easingsliderpro_registration_errors' );

        // Return the updates
        return $response->response;

    }

    /**
     * Registers the license key to the current site
     *
     * @param  string       $key The license key
     * @return object|false
     */
    public function register_license( $key ) {

        // Make the validation request
        $request = wp_remote_post( $this->api_url, array(
            'timeout' => 10,
            'body'    => array(
                'action'      => 'register_license',
                'key'         => trim( $key ),
                'slug'        => 'easingsliderpro',
                'url'         => home_url(),
                'version'     => EasingSliderPro::$version
            )
        ) );

        // Bail if WordPress error was received (HTTP API may have failed or may not have access required)
        if ( is_wp_error( $request ) ) {

            // Store the error
            $this->store_error( $request->get_error_message() );

            return false;

        }

        // Get the response headers
        $headers = (object) wp_remote_retrieve_headers( $request );

        // Get the response body
        $response = json_decode( wp_remote_retrieve_body( $request ) );
                
        // Bail if no response was received (service may temporarily be offline)
        if ( ! isset( $response->response ) ) {

            // Store the error
            $this->store_error( __( 'The license key registration service is temporarily unavailable. Please try again later.', 'easingsliderpro' ) );
        
            return false;

        }

        // Check if the status has reported an error or success. Act appropriately.
        if ( $headers->status == 200 ) {

            // Set license key admin option 
            update_option( 'easingsliderpro_license_key_is_valid', true );

            // Delete any remaining errors
            delete_option( 'easingsliderpro_registration_errors' );

            // Return response object
            return (object) array(
                'message' => __( 'License key has been successfully registered for this site.', 'easingsliderpro' ),
                'status'  => 'updated note'
            );

        }
        else {

            // Flag invalid license key
            update_option( 'easingsliderpro_license_key_is_valid', false );

            // Store the error
            $this->store_error( $response->response );

            return false;

        }

    }

    /**
     * Deactivates a license key
     *
     * @param  string       $key The license key
     * @return object|false
     */
    public function deactivate_license( $key ) {

        // Make the validation request
        $request = wp_remote_post( $this->api_url, array(
            'timeout' => 10,
            'body'    => array(
                'action'      => 'deactivate_license',
                'key'         => trim( $key ),
                'slug'        => 'easingsliderpro',
                'url'         => home_url(),
                'version'     => EasingSliderPro::$version
            )
        ) );

        // Bail if WordPress error was received (HTTP API may have failed or may not have access required)
        if ( is_wp_error( $request ) ) {

            // Store the error
            $this->store_error( $request->get_error_message() );

            return false;

        }

        // Get the response headers
        $headers = (object) wp_remote_retrieve_headers( $request );

        // Check if the status has reported an error or success. Act appropriately.
        if ( $headers->status == 204 ) {

            // Set license key admin option 
            update_option( 'easingsliderpro_license_key_is_valid', false );

            // Delete any remaining errors
            delete_option( 'easingsliderpro_registration_errors' );

            // Tell the user the license has been deactivated
            return (object) array(
                'message' => __( 'Your license for this site has been deactivated.', 'easingsliderpro' ),
                'status'  => 'updated note'
            );

        }

        // If we've gotten here, there's been an error. Tell the user. Store the error.
        $this->store_error( __( 'Your license could not be deactivated successfully. Please contact support to have it deactivated correctly.', 'easingsliderpro' ) );

        return false;

    }

}
<?php

/**
 * Plugin upgrade class. This will become more populated over time.
 *
 * @author Matthew Ruddy
 * @since 2.0
 */
class ESP_Upgrade {

    /**
     * Constructor
     *
     * @return void
     */
    public function __construct() {

        // Get the current plugin version
        $version = get_option( 'easingsliderpro_version' );

        // Do our upgrade actions
        add_action( 'easingsliderpro_do_upgrades', array( $this, '_2_0_6_upgrade' ), 1 );
        
        // Update the plugin version
        do_action( 'easingsliderpro_do_upgrades', $version );

        // Set the new plugin version
        if ( ! version_compare( $version, EasingSliderPro::$version, '=' ) ) {
            update_option( 'easingsliderpro_version', EasingSliderPro::$version );
        }

    }

    /**
     * Plugin upgrade to v2.0.6.
     *
     * @param  string $current_version The current plugin version
     * @return void
     */
    public function _2_0_6_upgrade( $current_version ) {

        // Bail if the current version is v2.0.6 or greater
        if ( ! version_compare( $current_version, '2.0.6', '<' ) ) {
            return;
        }

        // Get the old license key option
        $license_validity = get_option( 'easingsliderpro_license_key' );

        // Act appropriately
        if ( $license_validity == 'valid' ) {
            update_option( 'easingsliderpro_license_key_is_valid', true );
        }
        else {
            update_option( 'easingsliderpro_license_key_is_valid', false );
        }

        // Remove the old option as it is no longer needed
        delete_option( 'easingsliderpro_license_key' );

    }

}
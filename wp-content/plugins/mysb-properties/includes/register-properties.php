<?php
// Include JS - CSS etc - We only want these JS to load on our custom post types


add_action( 'wp_enqueue_scripts', 'add_msbprop_files' );



function add_msbprop_files() {
   if ( 'mysbprop' == get_post_type()) {
        //global $post;
		//$geocoded_address=get_post_meta($post->ID,'martygeocoderlatlng');
		//$geocoded_address=str_replace(array( '(', ')' ), '', $geocoded_address);
        wp_register_style( 'mysbprop-style', plugins_url('/css/mysbprop.css', __FILE__) );
		wp_register_script( 'mysbmapgoog', 'http://maps.googleapis.com/maps/api/js?sensor=false');
	    //wp_register_script( 'mysbmapjs', plugins_url('/mysb-properties/includes/js/map.js'));
	    //wp_register_script( 'mysbmapinit', plugins_url('/mysb-properties/includes/js/mapinit.js'));
		//wp_register_script('mysbmapinit', plugins_url('/mysb-properties/includes/js/mapinit.js'),array(),false,true);
	    //wp_register_script('mysbmapjs', plugins_url('/mysb-properties/includes/js/map.js'),array(),false,true);
        wp_enqueue_style( 'mysbprop-style' );
		wp_enqueue_script( 'mysbmapgoog' );
	    //wp_enqueue_script( 'mysbmapjs' );
	    //wp_localize_script( 'mysbmapjs', 'mysbgmap', $geocoded_address );
		//wp_enqueue_script( 'mysbmapinit' );
		//wp_localize_script( 'mysbmapjs', 'mysbgmap', array('geocoded_address'   => $geocoded_address));
    }
}




// define( 'MYSBPL_URL', plugins_url() . '/mysb-properties/images/' );
// registration code for mysbprop post type
	// registration code for mysbprop post type
	function register_mysbprop_posttype() {
		$labels = array(
			'name' 				=> _x( 'Properties', 'post type general name' ),
			'singular_name'		=> _x( 'Property', 'post type singular name' ),
			'add_new' 			=> __( 'Add New Property' ),
			'add_new_item' 		=> __( 'Property' ),
			'edit_item' 		=> __( 'Property' ),
			'new_item' 			=> __( 'Property' ),
			'view_item' 		=> __( 'View Property' ),
			'search_items' 		=> __( 'Property' ),
			'not_found' 		=> __( 'Property' ),
			'not_found_in_trash'=> __( 'Property' ),
			'parent_item_colon' => __( 'Property' ),
			'menu_name'			=> __( 'Properties' )
		);
		
		$taxonomies = array();
		
		$supports = array('title','editor','author','thumbnail','excerpt','custom-fields','comments','revisions');
		
		$post_type_args = array(
			'labels' 			=> $labels,
			'singular_label' 	=> __('Property'),
			'public' 			=> true,
			'show_ui' 			=> true,
			'publicly_queryable'=> true,
			'query_var'			=> true,
			'capability_type' 	=> 'post',
			'has_archive' 		=> true,
			'hierarchical' 		=> false,
			'rewrite' 			=> array('slug' => 'property', 'with_front' => false ),
			'supports' 			=> $supports,
			'menu_position' 	=> 5,
			'menu_icon' 		=> 'http://www.mysmartblog.com/wp-content/plugins/mysb-properties/images/propery-listing-icon.png',
			'taxonomies'		=> $taxonomies
		 );
		 register_post_type('mysbprop',$post_type_args);
	}
	add_action('init', 'register_mysbprop_posttype');


	// Add Sub Menu
	add_action('admin_menu' , 'mysbprop_help_pages');
 
	function mysbprop_help_pages() {
    	add_submenu_page('edit.php?post_type=mysbprop', 'MySmartBlog Properties - Faq & Help', 'FAQ & Help', 'edit_posts', 'mysbprop-help', 'mysbprop_help_faq');
	}
	
	function mysbprop_help_faq(){
 		echo '<div class="wrap"><h2>MySmartBlog Property Listings Help &amp; FAQ</h2></div><p>Adding properties via MySmartBlog Properties listing tool is very simple. Below you will find a short, but useful guide that will help you get started.</p><br /><strong>Why use the Geocoder for address maps?</strong><br />Using the geocoder is much faster and a more efficient method to load Google maps. Without it - the geocoding takes place in real time - slowing the maps down.<br /><br /><strong>Are these property listings regular posts / page?</strong><br />No, your property listings are stored as custom post types. This allows you more control over what you can do with the listings. For example - you can see the archive of your property listings <a href="">here</a>.
<br /><br /><strong>Why don\'t the images use the built in media library?</strong><br />This is because these custom post types have images that you upload, specifically \'attached\' to them. You can also
<a target="_blank" href="http://en.support.wordpress.com/media/unattached-files/">attach existing images in your media library to a property page</a> , and they will show in the property gallery manager. To learn why images in galleries must be \'attached\' to specific pages or posts (and thus the way this works) - <a target="_blank" href="http://codex.wordpress.org/Media_Library_SubPanel#Actions">read more at the official Wordpress site</a>.<br /><br /><strong>Does this plugin come with shortcodes?</strong><br />You bet! Examples of shortcodes :
<br /><br />
Show 3 properties - <code>&#91;mysbprop posts=3&#93;</code>
<br /><br />
Show single property by post id - <code>&#91;mysbprop property=1712&#93;</code>
<br /><br />
Change the class of the UL element container - <code>&#91;mysbprop class=myclass&#93;</code>
<br /><br />
Show an image with the listing - <code>&#91;mysbprop class=myclass showimage=1&#93;</code>
<br /><br />
Show full description with the listing - <code>&#91;mysbprop full=1&#93;</code><br /><br />

You can use any combination of these shortcodes together.<br /><br />
<strong>Do my listing appear automatically somewhere on my site, and if so - how do I link to them?</strong><br />
Your property listings automatically come with their own dedicated area located at http://youraddress.mysmartblog.com/property/ :<br /><br />
You can link to the property page from your <a href="/wp-admin/nav-menus.php">main menu</a>.

';
	}

// Shorcodes test
add_shortcode('mysbprop', 'mysbprop_shortcode_query');
function mysbprop_shortcode_query($atts, $content){
  extract(shortcode_atts(array( // a few default values
   'posts_per_page' => '1',

   'post_type' => 'mysbprop',
   'caller_get_posts' => 1)
   , $atts));
   

  global $post;
   $posts = new WP_Query( array( 'post_type' => 'mysbprop', 'posts_per_page' => 1 ) );
  //$posts = new WP_Query($atts);
  $output = '';
    if ($posts->have_posts())
        while ($posts->have_posts()):
            $posts->the_post();
            $out = '<div class="test_box">
                <h4>Test: <a href="'.get_permalink().'" title="' . get_the_title() . '">'.get_the_title() .'</a></h4>
                <p class="test_desc">'.get_the_content().'</p>';
                // add here more...
            $out .='</div>';
    /* these arguments will be available from inside $content
        get_permalink()  
        get_the_content()
        get_the_category_list(', ')
        get_the_title()
        and custom fields
        get_post_meta($post->ID, 'field_name', true);
    */
    endwhile;
  else
    echo 'Nothing Found';
    return; // no posts found

  wp_reset_query();
  return html_entity_decode($out);
}
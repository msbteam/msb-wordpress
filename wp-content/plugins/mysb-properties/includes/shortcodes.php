<?php

// Custom Property Shortcode

function mysbprop_display($atts){
   extract(shortcode_atts(array(
      'posts' => 1,
	  'property' => 0,
	  'showimage' => 0,
	  'full' => 0,
	  'class' => none,
   ), $atts));

   $return_string = '<ul class="'.$class.'">';
   query_posts(array('post_type'=> 'mysbprop','orderby' => 'date', 'order' => 'DESC' , 'showposts' => $posts,'p' => $property,'showimage' => $showimage,'full' => $full,));
   if (have_posts()) :
      while (have_posts()) : the_post();
         $return_string .= '<li><a href="'.get_permalink().'">'.get_the_title().'</a></li>';
		 // show image
		 if ($showimage == 1) {
		 $main_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 1,'height' =>365,'width' =>580,'size' =>full ) ); 
		 $return_string .= '<div id="mysbprop-mainimage"><a href="'.$main_get_the_image_as_array[url].'" rel="lightbox[]" class="fancyboxgroup"><img src="'.$main_get_the_image_as_array[url].'" height="365" width="580"></a></div>';
		 }
		// show full listing
		if ($full == 1) {
		 $content = get_the_content();
		 $return_string .= '<div id="mysbprop-info">'.$content.'</div>';
		 }
      endwhile;
   endif;
   $return_string .= '</ul>';

   wp_reset_query();
   return $return_string;
}

add_shortcode('mysbprop', 'mysbprop_display');
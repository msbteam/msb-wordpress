<?php
/*
Plugin Name: Gallery Metabox
Plugin URI: http://wordpress.org/extend/plugins/gallery-metabox/
Description: Displays all the post's attached images on the Edit screen
Author: Bill Erickson
Version: 1.4
Author URI: http://www.billerickson.net
*/

/**
 * Translations
 * @since 1.0
 *
 * @author Bill Erickson
 */
function be_gallery_metabox_translations() {
	load_plugin_textdomain( 'gallery-metabox', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
add_action( 'init', 'be_gallery_metabox_translations' );


/**
 * Add the Metabox
 * @since 1.0
 *
 * @author Bill Erickson
 */
function be_gallery_metabox_add() {
	// Filterable metabox settings. 
	$post_types = apply_filters( 'be_gallery_metabox_post_types', array( 'post', 'page') );
	$context = apply_filters( 'be_gallery_metabox_context', 'normal' );
	$priority = apply_filters( 'be_gallery_metabox_priority', 'high' );
	
	
	// Loop through all post types
	foreach( $post_types as $post_type ) {
		
		// Get post ID
		if( isset( $_GET['noshow'] ) ) $noshow = $_GET['noshow'];
		if( isset( $_GET['post'] ) ) $post_id = $_GET['post'];
		elseif( isset( $_POST['post_ID'] ) ) $post_id = $_POST['post_ID'];
		if( !isset( $post_id ) ) $post_id = false;
		
		// Granular filter so you can limit it to single page or page template
		if( apply_filters( 'be_gallery_metabox_limit', true, $post_id ) )
			add_meta_box( 'be_gallery_metabox', __( 'Gallery Images', 'gallery-metabox' ), 'be_gallery_metabox', $post_type, $context, $priority );

	}
}
add_action( 'add_meta_boxes', 'be_gallery_metabox_add' );

/**
 * Build the Metabox
 * @since 1.0
 *
 * @param object $post
 *
 * @author Bill Erickson
 */
function be_gallery_metabox( $post ) {
	
	$original_post = $post;
	echo be_gallery_metabox_html( $post->ID );
	$post = $original_post;
}

/** 
 * Gallery Metabox HTML 
 * @since 1.3
 *
 * @param int $post_id
 * @return string html output 
 *
 * @author Bill Erickson
 */
 
function remove_media_library_tab($tabs) {
    $noshow = ( $_GET['noshow'] );
	$tab = ( $_GET['tab'] );
    $post_id = !empty( $_GET['post_id'] ) ? (int) $_GET['post_id'] : 0;
    $post_type = get_post_type($post_id);
    if( 'mysbprop' == $post_type && '1' == $noshow) {
    unset($tabs['library']);
	unset($tabs['type_url']);
	//wp_deregister_script('admin-gallery');
	?>
	<style type="text/css">#gallery-settings{display:none !important;}</style>
	<script type="text/javascript">
	jQuery(document).ready(function() {
	jQuery('#gallery-form').attr('action', jQuery('#gallery-form').attr('action') + '&noshow=1');
	});
	</script>

	<?php
	}
    return $tabs;
}
add_filter('media_upload_tabs', 'remove_media_library_tab');
  
function be_gallery_metabox_html( $post_id ) {

	$args = array(
		'post_type' => 'attachment',
		'post_status' => 'inherit',
		'post_parent' => $post_id,
		'post_mime_type' => 'image',
		'posts_per_page' => '-1',
		'order' => 'ASC',
		'orderby' => 'menu_order',
	);
	$args = apply_filters( 'be_gallery_metabox_args', $args );
	$return = '';
    $bname = get_bloginfo('wpurl');
	$intro = '<p><a href="media-upload.php?noshow=1&amp;post_id=' . $post_id .'&amp;type=image&amp;TB_iframe=1&amp;width=640&amp;height=715" id="add_image" class="thickbox" title="Add Image">Upload Property Images</a> | <a href="media-upload.php?noshow=1&amp;post_id=' . $post_id .'&amp;type=image&amp;tab=gallery&amp;TB_iframe=1&amp;width=640&amp;height=715" id="manage_gallery" class="thickbox" title="Manage Gallery">Manage Property Gallery</a> | <strong>After adding images for the first time, save the post to allow them to appear below in your gallery.</strong></p><p>Use this tool to add images to your MySmartBlog property listing. Once added, you can arrange the order by dropping and dragging. The first image will always show and the main image. You can show up to 7 images total - including the main image, and each image after the main image will automatically align with your theme.</p>';
	
	$return .= apply_filters( 'be_gallery_metabox_intro', $intro );

	
	$loop = get_posts( $args );
	if( empty( $loop ) )
		$return .= '<p>No images attached to this property yet.</p>';
			
	foreach( $loop as $image ):
		$thumbnail = wp_get_attachment_image_src( $image->ID, apply_filters( 'be_gallery_metabox_image_size', 'thumbnail' ) );
		$return .= apply_filters( 'be_gallery_metabox_output', '<img src="' . $thumbnail[0] . '" alt="' . $image->post_title . '" title="' . $image->post_content . '" /> ', $thumbnail[0], $image );
	endforeach;
	$return .= 'NOTE : To show your properties on the front end of your site, add a MAIN MENU ITEM that links to your property url, which is located at : <strong>' . $bname . '/property/</strong><br /><br />You can also <a href="http://en.support.wordpress.com/media/unattached-files/" target="_blank">attach existing images in your media library to a property page</a>. To learn why images in galleries must be \'attached\' to specific pages or posts (and thus the way this works) - <a href="http://codex.wordpress.org/Media_Library_SubPanel#Actions" target="_blank">read more at the official Wordpress site</a>.';

	return $return;
}

/**
 * Gallery Metabox - Do AJAX Update
 * @since 1.3
 *
 * This function will add metabox contents via Ajax call. Function is called when an
 * attachment is edited, so we can add/remove image from metabox without refreshing the page.
 *
 * @param object $form_fields
 * @param object $post
 *
 * @author Zlatko Salbut
 *
 */
 
function be_gallery_metabox_do_ajax_update( $form_fields, $post ) {
    $noshow = ( $_GET['noshow'] );
	$tab = ( $_GET['tab'] );
    $post_id = !empty( $_GET['post_id'] ) ? (int) $_GET['post_id'] : 0;
    $post_type = get_post_type($post_id);
    if( 'mysbprop' == $post_type && '1' == $noshow || 'mysbprop' == $post_type && 'gallery' == $tab) {
	?>
	<script type="text/javascript">
	    // <![CDATA[
	    jQuery.ajax({
		    url: "<?php echo admin_url( 'admin-ajax.php' );?>",
		    type: "POST",
		    data: "action=refresh_metabox&post_id=<?php echo $post->post_parent; ?>",
		    success: function(res) {
				jQuery('#be_gallery_metabox .inside', top.document).html(res);
				
		    },
		    error: function(request, status, error) {
				alert("There was an error! Please try again.");
		    }
	   });
	   // ]]>
    </script>
	
	<?php
	}
	return $form_fields;
	
}
add_filter( 'attachment_fields_to_edit', 'be_gallery_metabox_do_ajax_update', 10, 2 );


/**
 * Gallery Metabox AJAX Update
 * @since 1.3
 *
 * Ajax hook for refreshing contents of Gallery Metabox
 *
 * @return void
 *
 * @author Zlatko Salbut
 */
function be_gallery_metabox_ajax_update() {
	if ( !empty( $_POST['post_id'] ) )
	    die( be_gallery_metabox_html( $_POST['post_id'] ) );
}

add_action( 'wp_ajax_refresh_metabox', 'be_gallery_metabox_ajax_update' );


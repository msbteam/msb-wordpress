<?php
/*
Plugin Name: MySmartBlog - Property Listings
Plugin URI: http://www.mysmartblog.com
Description: Add custom property listings to your MySmartBlog website, and showcase links to them with ease via shortcodes
Author: MySmartBlog LLC
Version: 1.00
Author URI: http://www.mysmartblog.com
License: GPL2
*/

// and we're off!

require_once(dirname(__FILE__).'/includes/meta-boxes.php');
require_once(dirname(__FILE__).'/includes/register-properties.php');
require_once(dirname(__FILE__).'/includes/shortcodes.php');
//require_once(dirname(__FILE__).'/externals/gallery-metabox.php');
require_once(dirname(__FILE__).'/externals/get-the-image/get-the-image.php');
require_once(dirname(__FILE__).'/externals/address-geocoder/address-geocoder.php');


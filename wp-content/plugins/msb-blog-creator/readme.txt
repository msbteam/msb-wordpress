=== Automatic Blog Creator ===
Contributors: AAT
Created By: http://mysmartblog.com
Tags: network site, blog, create
Requires at least: 3.4
Tested up to: 3.8.1
Stable tag: 3.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Automated creation of a network blog using the XML-RPC protocol.

== Description ==

Automated creation of a network blog using the XML-RPC protocol.

== Installation ==

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 0.3 =
* API Key Verification Check
* Automatically create users based on e-mail address delivered.
* Theme selector.
* Automated widget creation, placement, and configuration.
* Automated plugin activator and configuration.

= 0.2 =
* Assign template to created blog.

= 0.1 =
* Create blog functions for multi-site networks.
* Check's to see if blog already exists.

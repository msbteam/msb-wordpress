<?php
/*
Plugin Name: MySMARTblog - Automatic Blog Creator
Plugin URI: http://mysmartblog.com
Description: Automated creation of a network blog using the XML-RPC protocol.
Author: MySMARTblog
Author URI: http://mysmartblog.com
Version: 0.3
Network: true
*/

if (!is_multisite()) {
    wp_die('This plugin is intended for Multisite installations only. Please enable before continuing.');
}

if (!class_exists('msb_blog_creator')) {

    class msb_blog_creator
    {
        private $ver_key = '58Sx1ibAp7mJ]4YoD5K3',
                $user_logged_in = NULL;

        function __construct() {
            // Add MSB specific methods
            add_filter('xmlrpc_methods', array($this, 'add_xmlrpc_methods'));
        }

        private function verify_credentials($args)
        {
            global $wp_xmlrpc_server;

            $wp_xmlrpc_server->escape($args);

            $username = $args[1];
            $password = $args[2];
            $content = $args[3];
            $api_key = $args[5];

            $this->user_logged_in = $wp_xmlrpc_server->login($username, $password);

            if (!$this->user_logged_in)
                $wp_xmlrpc_server->error($wp_xmlrpc_server->error);

            if ($api_key !== $this->ver_key)
                $wp_xmlrpc_server->error(500, 'API-KEY submitted is not valid.');

            /*if (!is_super_admin($this->user_logged_in))
                $wp_xmlrpc_server->error(403, 'User credentials failed, user not allowed.');*/

            return $content;
        }

        private function set_plugins($blog_id, $plugins)
        {
            if (switch_to_blog($blog_id)) {

                global $wp_xmlrpc_server;

                // Only activate plugins which are not already active.
                foreach ( $plugins as $i => $plugin ) {
                    $check = get_plugins('/'.$plugin['name']);
                    if (empty($check))
                        $wp_xmlrpc_server->error(500, "Plugin does not exist: " . $plugin['name']);

                    if ( is_plugin_active_for_network( $plugin['name'] ) ) {
                        unset( $plugins[ $i ] );
                    } else {
                        if ( is_plugin_active( $plugin ) || is_network_only_plugin( $plugin['name'] ) ) {
                            unset( $plugins[ $i ] );
                        } else {
                            $plugin_batch[] = $plugin['name'] . '/' . key($check);
                        }
                    }
                }

                if ( empty($plugin_batch) ) {
                    return TRUE;
                }

                if (!activate_plugins( $plugin_batch, self_admin_url('plugins.php?error=true') )) {
                    $wp_xmlrpc_server->error(500, 'There was an error while activating a plugin.');
                }

                $this->configure_plugins($plugins);
                restore_current_blog();
                return TRUE;
            }

            return FALSE;
        }

        private function configure_plugins($plugins) {
            foreach ( $plugins as $plugin ) {
                if (!empty($plugin['data'])) {
                    if (!update_option($plugin['data']['option_name'], $plugin['data']['data_array'])) {
                        return FALSE;
                    }
                }
            }

            return TRUE;
        }

        private function create_nav_menu($blog_id, $menu_name, $location_name) {
            global $wp_xmlrpc_server;

            if (!empty($blog_id) && switch_to_blog($blog_id)) {
                $menu_locations = get_nav_menu_locations();
                $nav_menus = wp_get_nav_menus( array('orderby' => 'name') );

                foreach ( $nav_menus as $menu ) {
                    if ($menu->name == $menu_name) {
                        $menu_set = $menu;
                        break;
                    }
                }

                $menu_locations[$location_name] = $menu_set->term_id;

                // Set menu locations
                set_theme_mod( 'nav_menu_locations', $menu_locations );
                restore_current_blog();

                return TRUE;
            } else {
                $wp_xmlrpc_server->error(500, 'Unable to create the menu, invalid Blog ID.');
            }

            return FALSE;
        }

        private function set_blog_theme($blog_id, $blog_theme)
        {
            global $wp_xmlrpc_server;

            // Switch to theme  by folder name.
            if (isset($blog_id) && isset($blog_theme)) {
                $theme = wp_strip_all_tags($blog_theme);
                $blog_id = (int)$blog_id;
                $my_theme = wp_get_theme($theme);

                if ($my_theme->exists()) {
                    if (!empty($blog_id) && switch_to_blog($blog_id)) {
                        switch_theme($theme);
                        restore_current_blog();
                        return TRUE;
                    }
                } else {
                    $wp_xmlrpc_server->error(500, 'Theme defined does not exist.');
                }
            }

            return FALSE;
        }

        private function notify_user($send, $username, $email, $password) {
            // Notify user of e-mail and password.
            if( $send == TRUE ){
                // Send user email notification of account information
                $subject = 'MySMARTblog - Account Registration Info';
                $msg = "Thank you for registering your account with MySMARTblog!\r\n\r\n";
                $msg .= "You can login to your account here: " . get_bloginfo('url') . ".\r\n";
                $msg .= "Your login information is: \r\n";
				$msg .= "Username: " . $username . " or " . $email . "\r\n";
				$msg .= "Password: " . $password . "\r\n\r\n";
                $msg .= "Please take special note of these as we will not be able to resend this information.";

                wp_mail( $email, $subject, $msg );

                return TRUE;
            }

            return FALSE;
        }

        private function create_user($username, $the_user, $password, $send_email)
        {
            global $wp_xmlrpc_server;

            // if the user_id is the user's e-mail
            if (!is_int($the_user)) {
                if (!($user = get_user_by('email', $the_user))) {
                    $error = wpmu_validate_user_signup(
                        $username,
                        $the_user
                    );

                    if (is_wp_error($error)) {
                        $wp_xmlrpc_server->error(500, $error->get_error_message());
                    }

  					if (empty($password)) {
						$password = wp_generate_password();
					}
                    
                    $user_id = wpmu_create_user(
                        $username,
                        $password,
                        $the_user
                    );

                    // Send e-mail to user on succesful creation
                    if ($send_email !== FALSE)
                        $this->notify_user(TRUE, $username, $the_user, $password);
                } else {
					$user_id = $user->ID;
					wp_set_password($password, $user_id);
					$user_data = get_userdata($user_id);
					
					if ($send_email !== FALSE)
                        $this->notify_user(TRUE, $user_data->user_login, $user_data->user_email, $password);
				}

                return $user_id;
            }

            if (!get_user_by('id', $the_user)) {
                $wp_xmlrpc_server->error(500, 'Could not assign user to blog, invalid User ID');
            } else {
                return $the_user;
            }
        }
		
		private function add_admin_account($blog_id, $admin_account_id) {
            global $wp_xmlrpc_server;

            if ( !add_user_to_blog( $blog_id, $admin_account_id, 'administrator' ) ) {
				$wp_xmlrpc_server->error(500, 'User does not exist.');
			}
			
			return TRUE;
		}

        function create_blog($args)
        {
            $parameters = $this->verify_credentials($args);

            $name = $parameters['path'];
            $the_user = $parameters['user_id'];
            $blog_theme = $parameters['blog_theme'];
            $blog_template = $parameters['blog_template'];
            $plugins = $parameters['plugins'];
            $nav_menu = $parameters['nav_menu'];
            $provided_password = $parameters['ppassword'];
			$admin_account_id = $parameters['admin_poster_id'];

            if (!is_array($parameters)) {
                return new IXR_Error(500, __("Missing an array information, please resubmit."));
            }

            if (!isset($parameters['meta'])) $parameters['meta'] = array('public' => 1);
            if (!isset($parameters['site_id'])) $parameters['site_id'] = 1;

            // Transform path
            if( is_subdomain_install() ):
                $domain = $name . '.' . DOMAIN_CURRENT_SITE;
                $path = '/';
                $blog_id = get_blog_id_from_url($domain);
            else:
                $domain = DOMAIN_CURRENT_SITE;
                $path = PATH_CURRENT_SITE . $name . '/';
                $blog_id = get_blog_id_from_url($domain, $path);
            endif;

            // Create user
            if ($new_user_id = $this->create_user($name, $the_user, $provided_password, FALSE)) {

				if ($blog_id == FALSE) {

                    /*if (class_exists('blog_templates')) {
                        $_POST['blog_template_admin'] = $blog_template;
                    }

                    // Create blog
					if (empty($parameters['site_id'])) {
		                $blog_id = wpmu_create_blog(
		                    $domain,
		                    $path,
		                    $parameters['title'],
		                    $new_user_id,
		                    $parameters['meta']
		                );
					} else {
						$blog_id = wpmu_create_blog(
		                    $domain,
		                    $path,
		                    $parameters['title'],
		                    $new_user_id,
		                    $parameters['meta'],
							$parameters['site_id']
		                );
					}
                    $this->add_admin_account($blog_id, $admin_account_id);

                    if (!class_exists('blog_templates')) {
                        $this->set_blog_theme($blog_id, $blog_theme);
                        $this->set_plugins($blog_id, $plugins);
                        $this->create_nav_menu($blog_id, $nav_menu, 'primary');
                    }*/

                    if (class_exists('blog_templates')) {
                        $_POST['blog_template_admin'] = $blog_template;
                    }

                    // Create blog
					if (empty($parameters['site_id'])) {
		                $blog_id = wpmu_create_blog(
		                    $domain,
		                    $path,
		                    $parameters['title'],
		                    $new_user_id,
		                    $parameters['meta']
		                );
					} else {
						$blog_id = wpmu_create_blog(
		                    $domain,
		                    $path,
		                    $parameters['title'],
		                    $new_user_id,
		                    $parameters['meta'],
							$parameters['site_id']
		                );
					}

                    $this->add_admin_account($blog_id, $admin_account_id);
                    $this->set_blog_theme($blog_id, $blog_theme);
                    $this->set_plugins($blog_id, $plugins);
                    $this->create_nav_menu($blog_id, $nav_menu, 'primary');

				} else {
					return new IXR_Error(500, __("Site already exists."));
				}

                return $blog_id;
            }

            return new IXR_Error(500, __("Failed to create blog, error during creation."));
        }

        function add_xmlrpc_methods($methods)
        {
            $methods['msb.CreateBlog'] = array(new msb_blog_creator(), 'create_blog');

            return $methods;
        }
    }

    class msb_widgets extends WP_Widget {

        private $widget = NULL,
                $data = NULL,
                $bar_type = NULL,
                $sidebars_widgets = NULL;

        function __construct() {
            $this->sidebars_widgets = wp_get_sidebars_widgets();
        }

        private function place_widget () {
            $sidebars_widgets[$sidebar_id] = $sidebar;

            // remove old position
            if ( !isset($_POST['delete_widget']) ) {
                foreach ( $sidebars_widgets as $key => $sb ) {
                    if ( is_array($sb) )
                        $sidebars_widgets[$key] = array_diff( $sb, array($widget_id) );
                }
                array_splice( $sidebars_widgets[$sidebar_id], $position, 0, $widget_id );
            }

            wp_set_sidebars_widgets($sidebars_widgets);
        }

        private function configure_widgeet () {
            $instance = $old_instance;

            /* Strip Tags For Text Boxes */
            $instance['title'] = strip_tags( $new_instance['title'] );
            if ( current_user_can('unfiltered_html') )
                $instance['text'] =  $new_instance['text'];
            else
                $instance['text'] = stripslashes( wp_filter_post_kses( addslashes($new_instance['text']) ) ); // wp_filter_post_kses() expects slashed
            $instance['imgcaption']     = $new_instance['imgcaption'];
            $instance['icon_size']      = $new_instance['icon_size'];
            $instance['icon_pack']      = $new_instance['icon_pack'];
            $instance['animation']      = $new_instance['animation'];
            $instance['icon_opacity']   = $new_instance['icon_opacity'];
            $instance['newtab']         = $new_instance['newtab'];
            $instance['nofollow']       = $new_instance['nofollow'];
            $instance['alignment']      = $new_instance['alignment'];
            $instance['display_titles'] = $new_instance['display_titles'];
            $instance['icons_per_row']  = $new_instance['icons_per_row'];

            $instance['slugtargets']    = (array)$new_instance['slugtargets'] + (array)$instance['slugtargets'];
            $instance['slugtitles'] 	= (array)$new_instance['slugtitles'] + (array)$instance['slugtitles'];
            $instance['slugalts']  		= (array)$new_instance['slugalts'] + (array)$instance['slugalts'];
            $instance['slugorder']  	= (array)$new_instance['slugorder'];

            foreach ($this->networks as $slug => $ndata) {
                $instance[$slug] = !empty($new_instance[$slug]) ? strip_tags( $new_instance[$slug] ) : 'http://';
                // $instance[$slug.'_title'] = strip_tags( $new_instance[$slug.'_title'] );
            }

            foreach ($this->networks_end as $slug => $ndata) {
                $instance[$slug] = strip_tags( $new_instance[$slug] );
                // $instance[$slug.'_title'] = strip_tags( $new_instance[$slug.'_title'] );
            }

            for ($i = 1; $i <= $this->custom_count; $i++) {
                $instance['custom'.$i.'name'] = strip_tags( $new_instance['custom'.$i.'name'] );
                $instance['custom'.$i.'icon'] = strip_tags( $new_instance['custom'.$i.'icon'] );
                $instance['custom'.$i.'url']  = strip_tags( $new_instance['custom'.$i.'url'] );
            }

            $instance['customiconsurl'] = strip_tags( $new_instance['customiconsurl'] );
            $instance['customiconspath'] = strip_tags( $new_instance['customiconspath'] );

            return $instance;
        }
    }

    $msb_blog_creator = new msb_blog_creator();

} // Class check
<?php
/*
Plugin Name: mPress Testimonials
Plugin URI: 
Description: A simple custom post type based plugin for displaying and managing testimonials. Comes with shortcodes and a sidebar widget for easy display.
Version: 0.6
Author: David Wood
Author URI: http://iamdavidwood.com
License: GPL2
*/

class mpress_testimonials {
	function mpress_testimonials() {
		// Create custom post type
		require_once('post_type.php');
		$post_type = 'testimonial';
		$args = array('supports' => array('title', 'editor', 'excerpt'));
		new wordpress_custom_post_type($post_type, array('args' => $args));

		// Add shortcode for display
		add_shortcode('mpress-testimonial', array(&$this, 'mpress_testimonial'));
	}

	function mpress_testimonial($atts) {
		$page = '';
		extract(shortcode_atts(array(
				'page' => 'random'
				), $atts));
		if($page && $page == 'full') return $this->testimonial_page();
		else return $this->random_testimonial();
	}

	function testimonial_page() {
		$return = '';
		$testimonials = get_posts(array('post_type' => 'testimonial'));
		if($testimonials):
			foreach($testimonials as $testimonial):
				$return .= '<div class="testimonial"><blockquote>'.$testimonial->post_content.'</blockquote><cite>- <a href="'.get_permalink($testimonial->ID).'">'.$testimonial->post_title.'</a></cite></div>';
			endforeach;
		endif;
		return $return;
	}

	static function random_testimonial() {
		$return = '';
		$testimonials = get_posts(array(
			'post_type' => 'testimonial',
			'numberposts' => 1,
			'orderby' => 'rand'
		));
		if($testimonials):
			foreach($testimonials as $testimonial):
				$return = self::display_testimonial($testimonial);
			endforeach;
		endif;
		return $return;
	}

	static function display_testimonial($testimonial) {
		if($testimonial->post_excerpt != '')
			$return = '<blockquote>'.$testimonial->post_excerpt.'</blockquote>';
		else
			$return = '<blockquote>'.self::testimonial_excerpt($testimonial->post_content).'</blockquote>';
		return $return.'<cite>- <a href="'.get_permalink($testimonial->ID).'">'.$testimonial->post_title.'</a></cite>';
	}

	static function testimonial_excerpt($content, $length = 50) {
		$words = explode(' ', $content);
		if(count($words) > $length) {
			$excerpt = array_slice($words, 0, $length);
			$content = implode(' ', $excerpt);
			$content .= '...';
			return $content;
		}
		else return $content;
	}
}
new mpress_testimonials;

class mpress_testimonial_widget extends WP_Widget {
	function mpress_testimonial_widget() {
		$name = __('Testimonials'); // The widget name as users will see it
		$description = __('Displays a random testimonial'); // The widget description as users will see it
		$this->WP_Widget($id_base = false,
						$name,
						$widget_options = array('classname' => strtolower(get_class($this)), 'description' => $description),
						$control_options = array());
	}
	//function form() {}
	//function update() {}
	function widget($args, $instance) {
		$before_widget = $after_widget = $before_title = $after_title = '';
		extract($args, EXTR_IF_EXISTS);
		echo $before_widget;
		echo $before_title.$instance['title'].$after_title;
		echo mpress_testimonials::random_testimonial();
		echo $after_widget;
	}
}
add_action('widgets_init', create_function('', 'return register_widget("mpress_testimonial_widget");'));

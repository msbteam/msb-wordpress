<p><?php _e('Welcome!  Create your new website by filling out the information below.'); ?></p>

<?php
    if( $this->errors ){
        echo '<p style="color:red">';
        foreach( $this->errors as $code => $error ){
            switch( $code ){
                case 'blogname':
                    echo 'Site Name: ';
                    break;
                case 'blogtitle':
                    echo 'Site Title: ';
            }
            echo $error . '<br />';
        }
        echo '</p>';
    }
?>

<form method="post">

    <?php if( is_subdomain_install() ): ?>

        <p><label><?php _e('Site Domain'); ?></label><br />
        <input type="text" name="sitename" value="<?php echo esc_attr(stripslashes( @$_POST['sitename'] ) ); ?>" />.<?php echo DOMAIN_CURRENT_SITE; ?></p>

    <?php else: ?>

        <p><label><?php _e('Site Name'); ?></label><br />
        <?php echo 'http://'.DOMAIN_CURRENT_SITE.PATH_CURRENT_SITE; ?> <input type="text" name="sitename" value="<?php echo esc_attr(stripslashes( @$_POST['sitename'] ) ); ?>" /></p>

    <?php endif; ?>

    <p><label><?php _e('Site Title'); ?></label><br />
    <input type="text" name="sitetitle" value="<?php echo esc_attr( stripslashes( @$_POST['sitetitle'] ) ); ?>" /></p>

        <?php
			if( class_exists('blog_templates') ) {
				$blog_templates = new blog_templates();
				if( method_exists( $blog_templates, 'get_user_template_dropdown' ) ) {
					$blog_templates->get_user_template_dropdown('Choose Your Industry');
				} 
			}
		?>

    <p><button type="submit" name="form"><?php _e('Create Site'); ?></button></p>

</form>

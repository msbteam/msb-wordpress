<h3>Create an account</h3>
<form method="post" action="<?php echo site_url('wp-login.php?action=register', 'login_post') ?>" class="wp-user-form">
    <div class="username">
        <label for="user_login"><?php _e('Username'); ?>: </label>
        <input type="text" name="user_login" size="20" id="user_login" />
    </div>
    <div class="password">
        <label for="user_email"><?php _e('Your Email'); ?>: </label>
        <input type="text" name="user_email" size="25" id="user_email" />
    </div>
    <div class="login_fields">
        <?php do_action('register_form'); ?>
        <input type="submit" name="user-submit" value="<?php _e('Sign up!'); ?>" class="user-submit" />
        <input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>?register=true" />
        <input type="hidden" name="user-cookie" value="1" />
    </div>
</form>

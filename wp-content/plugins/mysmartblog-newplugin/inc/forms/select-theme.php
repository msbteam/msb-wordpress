<p><?php _e('Please select a theme for your new site:'); ?></p>

<form method="post" action="<?php echo site_url( $this->endpoint.'/switch-theme/' ); ?>">

    <?php
        foreach( $this->themes as $theme ){
            // Set theme directory and URL for later use
            $theme_dir = WP_CONTENT_DIR.'/themes/'.$theme;
            $theme_url = WP_CONTENT_URL.'/themes/'.$theme;
            // Get theme header info
            $theme_info = @get_theme_data( $theme_dir.'/style.css' );
            // Make sure a screenshot exists for this theme
            if( file_exists($theme_dir.'/screenshot.png') ){
                echo '<div class="alignleft" style="width:280px; margin-right:10px;">';
                    echo '<img src="'.$theme_url.'/screenshot.png" width="280" height="210" /><br />';
                    echo '<p>';
                        echo '<input type="radio" name="theme" value="'.$theme.'" style="padding:3px 10px 0 0; width:auto; height:auto;" />';
                    echo $theme_info['Name'];
                    echo '</p>';
                echo '</div>';
            }
        }
    ?>
    
    <div style="clear:both;"></div>

    <input type="hidden" name="blog_id" value="<?php echo $this->get_primary_blog(); ?>" />

    <p><button type="submit" name="form" ><?php _e('Activate Theme'); ?></button></p>

</form>

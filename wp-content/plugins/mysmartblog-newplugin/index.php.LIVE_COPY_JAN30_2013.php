<?php

/*
Plugin Name: MySmartBlog - New Plugin
Plugin URI: 
Description: A refactored plugin that streamlines the user signup process.
Version: 0.2
Author: Micah Wood
Author URI: http://www.orderofbusiness.net/micah-wood/
License: GPL
License URI: http://www.gnu.org/licenses/gpl.txt
*/

if( !is_multisite() ){
    wp_die('This plugin is intended for Multisite installations only. Please enable before continuing.');
}

/** http://alexking.org/blog/2011/12/15/wordpress-plugins-and-symlinks */
$file = __FILE__;
if ( isset( $plugin ) ) {
	$file = $plugin;
} else if ( isset( $mu_plugin ) ) {
	$file = $mu_plugin;
} else if ( isset( $network_plugin ) ) {
	$file = $network_plugin;
}

// Define constants
define( 'MYSMARTBLOG_VER', '0.2' );
define( 'MYSMARTBLOG_URL', plugin_dir_url( $file ) );
define( 'MYSMARTBLOG_PATH', plugin_dir_path( $file ) );
define( 'MYSMARTBLOG_BASENAME', plugin_basename( $file ) );
define( 'MYSMARTBLOG_FILE', $file );

if( !class_exists('mysmartblog_wp_plugin') ){

    class mysmartblog_wp_plugin{

        private $endpoint = 'action',
                $title = 'Page not found',
                $content = "Sorry, we couldn't find what you were looking for!",
                $themes = array(),
                $errors = false,
                $member_slug = 'membership',
                $member_page = '',
                $non_member_slug = 'not-a-smartblog-member';

        function __construct(){
            // Set member page
            $this->member_page = network_site_url($this->member_slug);
            // Automatically redirect new logins to the member page unless otherwise noted
            add_filter( 'login_form_defaults', array($this, 'login_form_defaults') );
            add_filter( 'login_redirect', array($this, 'login_redirect') );
			// Redirect logouts to the home page
			add_action('wp_logout', array($this, 'logout_redirect'));
            // Register activation hook
            register_activation_hook( MYSMARTBLOG_FILE, array($this, 'activation_hook') );
            // Register deactivation hook
            register_deactivation_hook( MYSMARTBLOG_FILE, array($this, 'deactivation_hook') );
            // Define endpoint
            add_action( 'init', array($this, 'endpoint') );
            // Add handler for redirect
            add_action( 'template_redirect', array($this, 'template_redirect') );
            // Add handler for display of content
            add_filter( 'the_posts', array($this, 'the_posts') );
            // Add handler for loading correct template file
            add_filter( 'template_include', array($this, 'template_include') );
            // Add shortcode for inline login form
            add_shortcode( 'mysmartblog_login', array($this, 'login_out') );
            // Show notes to user about their site, or lack thereof
            add_filter('the_content', array($this, 'user_site_notices'));
        }

		function logout_redirect() {
			// Redirects users to the site homepage when they logout.
			wp_redirect(home_url());
			exit;
		}

        function login_form_defaults( $defaults ){
            if(strpos($_GET['redirect_to'], 'wp-admin') !== false)
                return $defaults;
            $defaults['redirect'] = $this->member_page;
            return $defaults;
        }

        function login_redirect( $url ){
            if(strpos($_GET['redirect_to'], 'wp-admin') !== false)
                return $url;
            global $user;
            $user = new WP_User( @$user->ID );
            // Redirect non-admins to the member page
            if( $user && !$user->has_cap('manage_options') ){
                return $this->member_page;
            }
            return $url;
        }

        function activation_hook(){
            $this->endpoint();
            flush_rewrite_rules();
        }

        function deactivation_hook(){
            flush_rewrite_rules();
        }

        function endpoint(){
            add_rewrite_endpoint( $this->endpoint, EP_ROOT );
        }

        function template_redirect(){
            if( is_page( $this->member_slug ) && !is_user_logged_in() ){
                wp_redirect( network_site_url($this->non_member_slug) );
            }
            $this->process();
        }

        function template_include( $template ){
            $value = get_query_var($this->endpoint);
            // Check if our endpoint is being used
            if( $value ){
                $template = locate_template( array('template-fullwidth.php', 'page.php', 'index.php') );
            }
            return $template;
        }

        function user_site_notices( $content ){
            if( is_page($this->member_slug) ){
                ob_start(); ?>
                    <style>
                        .success{
                            margin: 10px 0;
                            padding: 10px;
                            background: #A7E3A9;
                            border: 1px solid green;
                        }
                        .important-notice{
                            margin: 10px 0;
                            padding: 10px;
                            background: #F99F9F;
                            border: 1px solid red;
                        }
                    </style><?php
                $blog = $this->get_primary_blog();
                if( $blog ){
                    $blog = get_blog_details($blog);
                    echo '<p class="success">You can view your site by clicking here: <a href="'.$blog->siteurl.'" target="_blank">'.$blog->blogname.'</a>.</p>';
                } else{
                    echo '<p class="important-notice">We noticed that you haven\'t created your site yet. There\'s no hurry, but why not <a href="'.site_url('/action/new-site/').'" target="_blank">create your site</a> now?</p>';
                }
                return ob_get_clean() . $content;
            } else{
                return $content;
            }
        }

        function the_posts( $posts ){
            $value = get_query_var($this->endpoint);
            // Check if our endpoint is being used
            if( $value ){
                // Perform the necessary actions
                $this->process();
                remove_all_actions('the_content');
                // Create a fake page
                $post = array(
                    'ID' => 1,
                    'post_author' => 1,
                    'post_date' => current_time('mysql'),
                    'post_date_gmt' => current_time('mysql', 1),
                    'post_content' => $this->content,
                    'post_title' => $this->title,
                    'post_status' => 'static',
                    'comment_status' => 'closed',
                    'ping_status' => 'closed',
                    'post_name' => $this->endpoint . '/' . $value,
                    'post_parent' => 0,
                    'post_type' => 'page'
                );
                $posts = array( (object) $post );
            }
            return $posts;
        }

        function process(){
            $value = get_query_var($this->endpoint);
            // Check if our endpoint is being used
            if( $value ){
                switch( $value ){
                    case 'register':
                        // Verify that the data came from our InfusionSoft form
                        if( isset( $_GET['inf_form_xid'] ) && isset( $_GET['contactId'] ) && isset( $_GET['inf_field_Email'] ) ){

                            $email = ( isset( $_GET['inf_field_Email'] ) ) ? esc_attr( $_GET['inf_field_Email'] ): false;
                            $password = ( isset( $_GET['inf_field_Password'] ) ) ? esc_attr( $_GET['inf_field_Password'] ): false;

                            // Create new user
                            $user_id = $this->create_new_user( $email, $password );
                            if( is_wp_error($user_id) ){
                                $this->title = 'Error';
                                $this->content = $user_id->get_error_message();
                                exit();
                            } elseif( is_array($user_id) ){
                                // Automatically login new user
                                $password = $user_id['password'];
                                $user_id = $user_id['user_id'];
                                $logged_in = $this->auto_login($user_id, $password);
                                if( is_wp_error($logged_in) ){
                                    $this->title = 'Error';
                                    $this->content = $logged_in->get_error_message();
                                    break;
                                }
                            }

                            // Data passed from InfusionSoft - Ensure security
                            $inf_contact_id = esc_attr( $_GET['contactId'] );
                            $fname = ( isset( $_GET['inf_field_FirstName'] ) ) ? esc_attr( $_GET['inf_field_FirstName'] ): false;
                            $lname = ( isset( $_GET['inf_field_LastName'] ) ) ? esc_attr( $_GET['inf_field_LastName'] ): false;
                            $company = ( isset( $_GET['inf_field_Company'] ) ) ? esc_attr( $_GET['inf_field_Company'] ): false;
                            $address1 = ( isset( $_GET['inf_field_StreetAddress1'] ) ) ? esc_attr( $_GET['inf_field_StreetAddress1'] ): false;
                            $address2 = ( isset( $_GET['inf_field_StreetAddress2'] ) ) ? esc_attr( $_GET['inf_field_StreetAddress2'] ): false;
                            $city = ( isset( $_GET['inf_field_City'] ) ) ? esc_attr( $_GET['inf_field_City'] ): false;
                            $state = ( isset( $_GET['inf_field_State'] ) ) ? esc_attr( $_GET['inf_field_State'] ): false;
                            $zip = ( isset( $_GET['inf_field_PostalCode'] ) ) ? esc_attr( $_GET['inf_field_PostalCode'] ): false;
                            $work_phone = ( isset( $_GET['inf_field_Phone1'] ) ) ? esc_attr( $_GET['inf_field_Phone1'] ): false;
                            $cell_phone = ( isset( $_GET['inf_field_Phone2'] ) ) ? esc_attr( $_GET['inf_field_Phone2'] ): false;

                            // Save info as usermeta
                            update_user_meta($user_id, 'infusionsoft_contact_id', $inf_contact_id);
                            if( $fname )        update_user_meta($user_id, 'first_name', $fname);
                            if( $lname )        update_user_meta($user_id, 'last_name', $lname);
                            if( $company )      update_user_meta($user_id, 'company', $company);
                            if( $address1 )     update_user_meta($user_id, 'address1', $address1);
                            if( $address2 )     update_user_meta($user_id, 'address2', $address2);
                            if( $city )         update_user_meta($user_id, 'city', $city);
                            if( $state )        update_user_meta($user_id, 'state', $state);
                            if( $zip )          update_user_meta($user_id, 'zip', $zip);
                            if( $work_phone )   update_user_meta($user_id, 'work_phone', $work_phone);
                            if( $cell_phone )   update_user_meta($user_id, 'cell_phone', $cell_phone);

                            // Direct user to new site setup
                            $this->redirect_user('new-site');

                        } else{
                            $this->redirect_user('log-in');
                        }
                        break;
                    case 'log-in':
                        if( is_user_logged_in() ){
                            $this->redirect_user('new-site');
                            break;
                        }
                        $this->title = 'Log In';
                        $this->content = wp_login_form( array('echo' => false, 'redirect' => site_url( $this->endpoint.'/new-site/' ) ) );
                        break;
                    default:
                        // Require user to login
                        if( !is_user_logged_in() ){
                            $this->redirect_user('log-in');
                        // Perform the desired action
                        } else{
                            switch($value){
                                case 'new-site':
                                    // Don't let users sign up for more than one site
                                    if( $this->user_has_site() ){
                                        $this->redirect_user('member-page');
                                    }
                                    // Process form if it is submitted
                                    if( isset($_POST['sitename']) && isset($_POST['sitetitle']) ){
                                        $blog = $this->create_blog($_POST['sitename'], $_POST['sitetitle']);
                                        if( is_wp_error($blog) ){
                                            $this->errors = $blog->get_error_messages();
                                        } else{
                                            // Save info to auto-fill the contact us page
                                            $user_id = get_current_user_id();
                                            $info = array(
                                                'fname' => get_user_meta($user_id, 'first_name', TRUE),
                                                'lname' => get_user_meta($user_id, 'last_name', TRUE),
												'company' => get_user_meta($user_id, 'company', TRUE),
												'address' => get_user_meta($user_id, 'address1', TRUE),
												'address2' => get_user_meta($user_id, 'address2', TRUE),
												'city' => get_user_meta($user_id, 'city', TRUE),
												'state' => get_user_meta($user_id, 'state', TRUE),
												'zip' => get_user_meta($user_id, 'zip', TRUE),
												'busphone' => get_user_meta($user_id, 'work_phone', TRUE),
												'cellphone' => get_user_meta($user_id, 'cell_phone', TRUE),
                                            );
                                            switch_to_blog($blog);
                                            update_option('tm-mysmartblog-settings', $info);
                                            restore_current_blog();
                                            // Take user to theme selection screen
                                            $this->redirect_user('select-theme');
                                            break;
                                        }
                                    }
                                    // Display form
                                    $this->title = 'Create A Site';
                                    $this->content = $this->display_form('new-site');
                                    break;
                                case 'select-theme':
                                    $this->themes = $this->get_network_allowed_themes();
                                    $this->title = 'Select A Theme';
                                    $this->content = $this->display_form('select-theme');
                                    break;
                                case 'switch-theme':
                                    if( isset($_POST['theme']) && isset($_POST['blog_id']) ){
                                        $theme = wp_strip_all_tags($_POST['theme']);
                                        $blog_id = (int) wp_strip_all_tags($_POST['blog_id']);
                                        $stylesheet = WP_CONTENT_DIR.'/themes/'.$theme.'/style.css';
                                        if( file_exists($stylesheet) ){
                                            $theme_data = get_theme_data( WP_CONTENT_DIR.'/themes/'.$theme.'/style.css');
                                            // Check if we are dealing with a child theme
                                            $template = ( $theme_data['Template'] ) ? $theme_data['Template'] : $theme;
                                            if( switch_to_blog( $blog_id ) ){
												remove_action('after_switch_theme', '_wp_sidebars_changed' );
                                                switch_theme( $template, $theme );
                                                restore_current_blog();
                                                $this->redirect_user('member-page');
                                                break;
                                            }
                                        }
                                    }
                                    $this->redirect_user('select-theme');
                                    break;
                                default:
                                    $this->redirect_user('new-site');
                            }
                        }
                }
            }
        }

        function create_new_user( $email, $password = '' ){
            if( is_user_logged_in() ){
                $current_user = wp_get_current_user();
                return $current_user->ID;
            }
            if( email_exists($email) || username_exists($email) ){
                return new WP_Error('email', 'Sorry, that email address already exists in our system! Please hit the back button and try again.');
            }
            $password = ( $password ) ? $password : wp_generate_password();
            $userdata = array(
                'user_login' => $email,
                'user_email'=> $email,
                'user_pass' => $password,
            );
            $id = wp_insert_user( $userdata );
            if( !is_wp_error($id) ){
                // Send user email notification of account information
                $subject = get_bloginfo('name') . ' - Account Registration Info';
                $msg = 'Thank you for registering your account with '.get_bloginfo('name').'!';
                $msg .= '  You can login to your account here: '.get_bloginfo('url').'.';
                $msg .= '  Your username is '.$email.' and your password is \''.$password.'\'.';
                $msg .= '  Please take special note of these as we will not be able to resend this information.';
                wp_mail( $email, $subject, $msg );
            }
            return array( 'user_id' => $id, 'password' => $password );
        }

        function auto_login( $user_id, $password ){
            if( !is_user_logged_in() ){
                $current_user = new WP_User( $user_id );
                if( is_object($current_user) ){
                    $credentials = array(
                        'user_login' => $current_user->data->user_login,
                        'user_password' => $password,
                        'remember' => true
                    );
                    $logged_in = wp_signon( $credentials );
                    if( ! is_wp_error( $logged_in ) ){
                        wp_set_current_user( $user_id, $current_user->data->user_login );
                        return $user_id;
                    } else{
                        return $logged_in;
                    }
                } else{
                    return new WP_Error('autologin', 'Sorry, an invalid user id was provided during login.  Please contact the site administrator.');
                }
            } else{
                $current_user = wp_get_current_user();
                return $current_user->ID;
            }
        }

        function create_blog($name, $title){
            $valid = wpmu_validate_blog_signup($name, $title);
            $errors = $valid['errors']->errors;
            if( !empty($errors) ){
                return $valid['errors'];
            }
            if( is_subdomain_install() ):
                $domain = $name . '.' . DOMAIN_CURRENT_SITE;
                $path = PATH_CURRENT_SITE;
            else:
                $domain = DOMAIN_CURRENT_SITE;
                $path = PATH_CURRENT_SITE . $name . '/';
            endif;
            $user_id = get_current_user_id();
            $blog = wpmu_create_blog( $domain, $path, $title, $user_id, array('public' => 1) );
            if( !is_wp_error($blog) ){
                update_user_meta( $user_id, 'primary_blog', $blog );
            }
            return $blog;
        }

        function get_primary_blog(){
            return get_user_meta( get_current_user_id(), 'primary_blog', true );
        }

        function goto_primary_blog(){
            $blog = $this->get_primary_blog();
            if( $blog ){
                $url = get_blogaddress_by_id($blog);
                wp_redirect($url);
                exit();
            }
            $this->redirect_user('member-page');
            exit();
        }

        function user_has_site(){
            $site = $this->get_primary_blog();
            if( $site ){
                switch_to_blog( $site );
                $site = false;
                if( current_user_can('manage_options') ){
                    $site = true;
                }
                restore_current_blog();
            }
            return $site;
        }

        function get_network_allowed_themes(){
            global $wpdb;
            $themes = $wpdb->get_var("SELECT meta_value FROM ".$wpdb->sitemeta." WHERE meta_key = 'allowedthemes'");
            if( $themes ){
                $themes = array_keys(unserialize($themes));
                $key = array_search('genesis', $themes);
                if( $key !== false ) {
                    unset( $themes[$key] );
                }
                return $themes;
            }
            return false;
        }

        function display_form( $form ){
            ob_start();
            //include( MYSMARTBLOG_PATH . 'inc/forms/' . $form . '.php' );
			include( '/home/mysmartb/public_html/wp-content/plugins/mysmartblog-newplugin/inc/forms/' . $form . '.php' );
            return ob_get_clean();
        }

        function redirect_user( $slug ){
            if( $slug == 'member-page' ){
                wp_redirect( $this->member_page );
                exit();
            }
            wp_redirect( site_url( $this->endpoint . '/' . $slug . '/' ) );
            exit();
        }

        function login_out(){
            ob_start(); ?>
            <style>
                .inline-login {
                    float:right;
                    margin: 5px 10px 5px 0;
                }
                .inline-login input {
                    font-size: 12px;
                    height: 25px;
                    line-height: 25px;
                    padding: 0 10px;
                    -moz-border-radius: 3px;
                    border-radius: 3px;
                }
                .inline-login button {
                    font-size: 14px;
                    height: 28px;
                    line-height: 25px;
                    margin: 0;
                    padding: 0 5px 5px;
                }
            </style>
            <div class="inline-login"><?php
            if( is_user_logged_in() ){
                $current_user = wp_get_current_user();
                echo '<strong>Welcome ' . ucwords($current_user->user_firstname) . '!</strong> <a href="' . wp_logout_url( site_url() ) . '">Logout</a>';
            } else{ ?>
                <form action="<?php echo wp_login_url(); ?>" method="post">
                    <input type="text" id="user_login" name="log"  value="Username" onfocus="if(this.value=='Username') this.value=''" onblur="if(this.value=='') this.value='Username'" size="15" />
                    <input type="password" id="user_pass" name="pwd" value="Password" onfocus="if(this.value=='Password') this.value=''" onblur="if(this.value=='') this.value='Password'" size="15" />
                    <input type="hidden" name="redirect" value="<?php echo $this->member_page; ?>" />
                    <button type="submit">Login</button>
			    </form><?php
            }
            echo '</div>';
            return ob_get_clean();
        }

		static function is_user_admin() {
			global $current_user, $current_blog;
			$blogs = get_blogs_of_user($current_user->ID);
			$count = 0;
			$admin_of = array();
			foreach($blogs as $blog) {
				if($blog->userblog_id == $current_blog->blog_id)
					continue;
				switch_to_blog($blog->userblog_id);
				if(current_user_can('manage_options')) {
					$admin_of[] = array('ID' => $blog->userblog_id, 'siteurl' => $blog->siteurl, 'blogname' => $blog->blogname);
					$count++;
				}
				restore_current_blog();
			}

			if(count($admin_of) > 0) return $admin_of;
			else return false;
		}

    }

    new mysmartblog_wp_plugin();

}

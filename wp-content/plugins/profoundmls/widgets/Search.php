<?php

require_once('BaseWidget.php');

class ProFoundMLS_Search_Widget extends ProFound_BaseWidget {

	public static $desc = "ProFoundMLS Search Widget";

	/**
	 * Display the Quick Search form wherever the widget has been place
	 *  
	 * @param array $args
	 * @param unknown_type $instance
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		/* Before widget (defined by themes). */
		echo $before_widget;
		
		$opts = get_option('ProFoundMLSAdminOptions');
		$opts_array = json_decode($opts['form_data']);

		# FIXME: move this to the parent class, so we don't have to use a static method on a different class?
		$url = ProFoundMLS::getPageURL("search_results"); 
		
		$fields = array(
			'Price' => 'price',
			'City' => 'city',
			'Sq Feet' => 'sqft',
			'Bedrooms' => 'bedroom',
			'Bathrooms' => 'bathroom',
			'MLS Number' => 'mlsnum',
		);
		$regid = isset($opts['reg_form_id']) ? $opts['reg_form_id'] : 0;
		
		?>		
		<form action="<?= home_url() ?>/<?= $url ?>/" method="get" onSubmit="return showRegForm(this, <?= $regid ?>);">
			<fieldset id="profoundmls-search-widget">
			<h3>Quick Search</h3>
			<div class="fields-wrapper">
			<?php foreach ($fields as $label => $id):
				$any = "Any $label";
				?>
					
				<label for="<?= $id ?>_id"><?= $label ?>: </label>
					
				<?php if ($id == 'mlsnum'): ?>
				<input type="text" size="6" name="<?= $id ?>" id="<?= $id ?>_id" value="" />
				<?php else: ?>
				<select id="<?= $id ?>_id" name="<?= $id ?>">
					<option value=""><?= $any ?></option>
					<?php if ($id == 'city'): ?>
					<option value="">Any City</option>
					<?php endif ?>
				<?php foreach ($opts_array->$id as $val => $result): ?>
					<?php if (isset($result->LongValue)): ?>
					<option value="<?= $result->LongValue ?>"><?= $result->LongValue ?></option>
					<?php else: ?>
					<option value="<?= $val ?>"><?= $result[0] ?></option>
					<?php endif ?>
				<?php endforeach ?>	
				</select>
				<?php endif ?>	
			<?php endforeach ?>
			</div>
			<div class="search-button">
				<input type="submit" value="Search" />
			</div>
			</fieldset>  
 		</form><?php
		echo $after_widget;
	}
}

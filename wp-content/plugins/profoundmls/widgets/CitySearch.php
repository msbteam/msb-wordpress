<?php

/**
 * Include the file containing the base class for this widget
 */
require_once('BaseWidget.php');

/**
 * File containing definition for ProFoundMLS class, which is used to get the URL fragment from the DB for the URL
 */
require_once(dirname(__FILE__) . '/../classes/ProFoundMLS.php');

/**
 * City Search widget for displaying a list of city names with links to a city-specify Category/Neighborhood page
 */
class ProFoundMLS_City_Search_Widget extends ProFound_BaseWidget {

	public static $desc = "ProFoundMLS City Search Widget";

	/**
	 * Render the City List widget HTML
	 * @param $args Default widget arguments from WordPress
	 * @param $instance Instance of ???
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		// Before widget (defined by themes) 
		echo $before_widget;
		?>
		<?php
		$option = get_option('ProFoundMLSAdminOptions');
		$city_names = $option['city_list'];
		sort($city_names);
		
		$cat_url = ProFoundMLS::getPageURL("neighborhood");

		?>
			<div id="city-widget">
				<h3>City Search</h3>
				<ul>
				<?php if (count($city_names)): ?>
					<?php foreach ($city_names as $city):
						$city_url = strtolower(preg_replace('/\s+/', '-', $city)); ?>
					<li>
						<a href="<?= home_url() ?>/<?= $cat_url ?>/<?= $city_url ?>/"><?= $city ?></a>
					</li>
				<?php endforeach ?>
				<?php else: ?>
				    <span style="color: red">Please configure cities in Wordpress Admin under <b>ProFoundMLS</b></span>
				<?php endif ?>
				</ul>
			</div>

		<?php
		echo $after_widget;
	}
}

?>
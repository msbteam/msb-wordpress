<?php

# TODO: do we need all this stuff in the constructor?
/**
 * Base widget for ProFoundMLS WordPress widgets
 *
 * Sets the ID & description based on a static property on the subclasses
 */
abstract class ProFound_BaseWidget extends WP_Widget {

	/**
	 * Widget setup
	 */
	public function __construct() {
		$class = get_class($this);
		parent::__construct(strtolower($class), $class, array('description' => str_replace('_', ' ', $class)));
	}
}

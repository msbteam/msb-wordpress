<?php

require_once('BaseWidget.php');
/**
 * Agent Listing WordPress widget to display featured listings for an agent
 */
class ProFoundMLS_Agent_Listings_Widget extends ProFound_BaseWidget {

	public static $desc = "ProFoundMLS Agent Listings Widget";

	/**
	 * Render the HTML for the widget
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		global $pfmls_object;
		$listings = $pfmls_object->agentListings;

		# TODO: move the HTML construction to the IDX if we want & reuse address generation code

		# TODO: define the content for this in the IDX Admin as well, instead of here?
		
		// Before widget (defined by themes)
		echo $before_widget;
		?>
		<div id="pfmls-agent-listings">
			<h3>Featured Listings</h3>
			<?php foreach ($listings as $prop):
			?><div>
				<a href="<?= home_url() ?>/<?= $prop['uri'] ?>">
					<img alt="Thumbnail" src="<?= $prop['img_url'] ?>" />
				</a>
				<b><?= $prop['ListPrice'] ?></b>
				<a href="<?= home_url() ?>/<?= $prop['uri'] ?>"><?= $prop['ListingID']	?></a>
				<br>
				<?= $prop['StreetNumber'] . ' ' . $prop['StreetDirPrefix'] . ' ' . $prop['StreetName'] . ' ' . $prop['StreetSuffix'] ?><br>
				<?= $prop['City'] . ', ' . $prop['State'] . ' ' . $prop['PostalCode'] ?>
			</div>
			<?php endforeach ?>
		</div>
		<?php

		echo $after_widget;
	}

}


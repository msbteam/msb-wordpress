<?php
// Load the test environment
// https://github.com/nb/wordpress-tests

$inifile = dirname(__FILE__) . '/../config.ini';
$options = parse_ini_file($inifile);
$path = $options['wordpress-tests_bootstrap_path']; 

if (file_exists($path)) {
	$GLOBALS['wp_tests_options'] = array(
		'active_plugins' => array($path)
	);
	require_once $path;
} else {
	exit("Couldn't find wordpress-tests/bootstrap.php\n");
}

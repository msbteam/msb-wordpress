<?php
/**
 * Display a Gravity Form by ID
 * 
 * Similar to the Gravity Form's preview.php script.  Fixes the 404 HTTP return code issue. 
 * Only renders partial HTML - content is meant to be loaded in to a jQuery dialog via the .load() function.
 */

// Load Wordpress
$wppath =
$inifile = dirname(__FILE__) . '/config.ini';
if (file_exists($inifile)) {
	// Development - where plugin source is symlinked in to WordPress wp-content directory
	$options = parse_ini_file($inifile);
	require_once $options['wordpress_dir'] . '/wp-load.php';
} else {
	// Production plugin deployment (no symlink)
	require_once (preg_replace("/wp-content.*/","wp-load.php",__FILE__));
}

// Initialize WordPress and trigger plugin actions
global $wp;
$wp->init();
do_action_ref_array('wp', array(&$this));

# TODO: limit the IDs allowed to ones specified in the ProFoundMLS admin section, for security purposes.

?>
<link rel='stylesheet' href='<?php echo GFCommon::get_base_url() ?>/css/preview.css' type='text/css' />
<link rel='stylesheet' href='<?php echo GFCommon::get_base_url() ?>/css/forms.css' type='text/css' />

<?php
require_once(GFCommon::get_base_path() . "/form_display.php");

$form = RGFormsModel::get_form_meta($_GET["id"]);
GFFormDisplay::enqueue_form_scripts($form);

// TODO: kind of a hack.  special case for this site, as it needs scripts for form validation
if ($_SERVER['HTTP_HOST'] == 'www.phoenix-homes-4sale.com') {
	// Hack the WP_Scripts object so that jquery isn't included
	global $wp_scripts;
	foreach ($wp_scripts->registered as $depobj) {
		$depobj->deps = array_diff($depobj->deps, array('jquery'));
	}

	wp_dequeue_script('jquery');
	remove_filter('wp_print_scripts', 'wp_cycle_scripts');

	wp_print_scripts();
}

// Display the Gravity form
echo RGForms::get_form($_GET["id"], true, true, true);


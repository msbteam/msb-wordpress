//==========================================================
// Pro-Found MLS Javascript functions
//
// Copyright 2011 Pro-Found Marketing, LLC
// All Rights Reserved
//==========================================================

$ = jQuery;

// Define the 'icontains' pseudo-selector.
// See: http://stackoverflow.com/a/12113443/135101
jQuery.expr[':'].icontains = function (a, i, m) {
	return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};

// Initialize a Google Map for a given ID, using the specified options
function initMap(id, options, noinfo, bounds) {
	var currWin;
	var currId = 0;
	
    var latlng = new google.maps.LatLng(mapdata.center.lat, mapdata.center.lng);
    options.center = latlng;
    
    var map = new google.maps.Map(document.getElementById(id), options);
    
    if (bounds) setBounds(map);
    
    jQuery.each(mapdata.properties, function (id, prop) {
	    
	    // Skip properties with empty Latitude and Longitude
	    if (prop.loc.lat == 0 && prop.loc.lng == 0) return;
	    
    	latlng = new google.maps.LatLng(prop.loc.lat, prop.loc.lng);
    	var marker = new google.maps.Marker({
        	    position: latlng, 
            	map: map, 
    	        title: prop.street});
    	
    	if (noinfo) return;
    	
    	// TODO: this is redundant with the server-side code to some degree
    	var html = '<table><tr><td valign="top"><a href="' + wp_site_url + '/' + prop.url + '">' +
    			   '<img class="pfmls_map_thumb" src="' + prop.img + '"></a></td><td>' +
    	           '<a href="' + wp_site_url + '/' + prop.url + '">' + prop.h2 + '</a><p>' + prop.content + '</p>'; 
    	           
    	var infowindow = new google.maps.InfoWindow({
    	    content: html
    	});

    	google.maps.event.addListener(marker, 'click', function() {
    		if (currId != id && currWin) currWin.close(); 
    		infowindow.open(map,marker);
    		currWin = infowindow;
    		currId = id;
    	});
    });   
	
}

// Adjust the bounds for the map
function setBounds(map) {
    var botleft = new google.maps.LatLng(mapdata.bounds[0], mapdata.bounds[1]); 
    var toprght = new google.maps.LatLng(mapdata.bounds[2], mapdata.bounds[3]);
    var bounds = new google.maps.LatLngBounds(botleft, toprght);
    map.fitBounds(bounds);
}

// Search Results page: Initialize the Google Map
function initSearchResults() {
	// Google Map options
	var largeMap = {
      mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	// Add the Google Maps
	initMap('pfmls_map', largeMap, false, true);
	
}

// Property Details page: Initialize the Google Maps and the image gallery
function initPropDetails() {
	// Google Map options
	var largeMap = {
      mapTypeId: google.maps.MapTypeId.HYBRID
	};
	var smallMap = {
      zoom: 13,
      mapTypeControl: false,
      streetViewControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	// Add the Google Maps
	initMap('pfmls_map', largeMap, true, true);
	
	// Initialize the sidebar map
	initMap('pfmls_small_map', smallMap, true, false);
		
	// Add the Property image gallery
	jQuery("#pikame").PikaChoose({carousel:true, carouselVertical:true});
}

/**
 * Display the "Registration Form" in a popup if a cookie hasn't been set yet
 * 
 * @param form DOM object for the original search form
 * @param formid ID of the Gravity Form to display
 */
function showRegForm(form, formid) {
	var $ = jQuery;

	// If the registration cookie has been set already, or a form ID wasn't configured, allow the form submission
	if (!formid || $.cookie("pfmls_registered")) return true;
	
	// Shortcut function to add the 'Skip' button to the form
	var addSkip = function(div) {
		var skipBtn = $('<input style="font-size: 13px; padding: 0px 4px;" type="button" value="Not Now" />');

		// Submit the original form if the 'Skip' button is clicked
		skipBtn.click(function() {
			// Set a temporary 1-day cookie for registration
			setTempCookie();

			if (form) form.submit();
			else div.dialog('close');
		});
		var footer = div.find('.gform_footer');
		footer.append(skipBtn);
	};
	
	showGravityForm(formid, setRegCookie, form, addSkip);
	
	// Prevent the submission of the original form
	return false;
}

/**
 * Set registration cookie indicating user has registered, or skipped registration
 *
 * @param days Number of days the cookie should last before expiring
 */
function setRegCookie(days) {
	var $ = jQuery;
	days = days || 365 * 5;     // Default: 5 years
	$.cookie("pfmls_registered", "true", {expires: days, path: '/'});
}

/**
 * Set a temporary (1 day) cookie for the registration
 */
function setTempCookie() {
	if (no_temp_cookie) return;
	setRegCookie(1);
}

/**
 * Show a specific Gravity form in a modal jQuery popup dialog 
 * @param formid ID of the Gravity Form
 * @param cb Optional callback for successful submission
 */
function showGravityForm(formid, cb, form, addSkip) {
	var $ = jQuery;

	if (!gform_reg_url) return;
	
	// Load the form contents from the server, and display the dialog 
	var el = $('<div />');

	el.load(gform_reg_url + formid, function(responseText, textStatus, xhr) {

		// Insert the MLS ID if this is the 'See This House' dialog
		setTimeout( function() {
			if (parseInt(cb)) {
				var mlswrap = $('.gform_fields').children().last();
				var mlsnum = mlswrap.find('input');
				mlsnum.val(cb);
				mlswrap.hide();
			}
		}, 100);
		
		var orig_form = form;
		var fid = formid;
		var dlg = el;

		// Override the form submission to gather the form data, and submit the form in the background
		var gform = el.find('form');
		gform.submit(function(e) {
			e.preventDefault();

			// Get the form data
			var data = gform.serialize();

			// Submit to the server in the background
			$.post(gform_reg_url + fid, data, function(resp, statusText, xhr) {

				// If the form submission failed, re-display the form
				if (resp.match(/form method/)) {

					// Replace the form HTML with the content sent back from the server, add the "Skip" button
					var form_el = $(resp).find('form');
					gform.html(form_el.html());
					if (addSkip) addSkip(gform);
				}
				// If the form submission was successful, set the cookie and display a link to continue to the results
				else {
					var wrapper = el.find('.gform_wrapper');
					setRegCookie();

					var doc = $(resp);
					wrapper.html('<div>' + doc[doc.length - 1].innerHTML + '</div>');

					var btnText = orig_form ? 'Continue to Search Results' : 'Continue Browsing';
					var resultsBtn = $('<button class="button">' + btnText + '</button>');
					resultsBtn.click(function() {
						if (orig_form) orig_form.submit();
						else dlg.remove();
					});
					wrapper.append(resultsBtn);
				}
			});
		});

		if (addSkip) addSkip(el);

		// Closure
		var closeFunc = function() {
			var oform = orig_form;
			return function() {
				setTempCookie();
				if (oform) oform.submit();
			}
		}();

		var ht = $(window).height();

		// Display the dialog
		var options = {
			modal: true,
			position: ['center', ht * 0.10],
			draggable: false,
			resizable: false,
			minWidth: 580,
			close: closeFunc
		};
		$(this).dialog(options);

		// [We can remove this reminder after this commit.]
		// We no longer used fixed position for two reasons:
		// One, there is a bug when display the jQuery UI Dialog that sometimes places (part of) the dialog
		// above the viewport. In such case, a user could never see the whole dialog.
		// Second, if the dialog is bigger than the viewport, we need to allow the user to scroll to see the
		// entire dialog, especially considering the possibilities for dismissing the dialog (close and 'submit')
		// are at the top and bottom of the dialog.
		// Use fixed positioning for the dialog
		//$('.ui-dialog').css({position:'fixed'});

		// Function to resize & reposition dialog
		var repos = function () {
			var ht = $(window).height();
			var d = $('.ui-dialog');
			var top = Math.min(ht * 0.10, Math.max(0, (ht - d.height()) / 2));

			el.find(".ui-dialog-content")
				.dialog("option", "position", ['center', top])
				.css({'max-height':ht - 120 });
		}

		repos();

		// Automatically resize & reposition
		$(window).resize(repos);

	});
}

// A minimalist version of showGravityForm().
// It copies some of the hacks from the original though.
function showGravityForm2(formid, cb) {
	if (!gform_reg_url) return;

	// Load the form contents from the server, and display the dialog
	var el = $('<div />');
	var dlg = el;

	el.load(gform_reg_url + formid, function (responseText, textStatus, xhr) {
		// Override the form submission to gather the form data, and submit the form in the background
		var gform = el.find('form');
		var this_dialog = $(this);

		// Add a 'cancel' button.
		submitButton = el.find(".gform_footer input.gform_button");
		var btnText = 'Cancel';
		var resultsBtn = $('<button class="button gform_button" style="margin-left:10px;">' + btnText + '</button>');
		resultsBtn.click(function (event) {
			event.preventDefault();
			dlg.remove();
		});
		submitButton.after(resultsBtn);

		gform.submit(function (e) {
			e.preventDefault();

			// Get the form data
			var data = gform.serialize();

			// Submit to the server in the background
			$.post(gform_reg_url + formid, data, function (resp, statusText, xhr) {
				// If the form submission failed, re-display the form
				if (resp.match(/form method/)) {
					alert("Failed to submit form. Please give us a call!");
				} else {
					var wrapper = el.find('.gform_wrapper');

					var doc = $(resp);
					wrapper.html('<div>' + doc[doc.length - 1].innerHTML + '</div>');

					var btnText = 'Continue';
					var resultsBtn = $('<button class="button">' + btnText + '</button>');
					resultsBtn.click(function () {
						dlg.remove();
					});
					wrapper.append(resultsBtn);
				}
			});
		});

		// Display the dialog
		var options = {
			modal:true,
			draggable:false,
			resizable:false,
			minWidth:580
		};
		$(this).dialog(options);

		// [We can remove this reminder after this commit.]
		// We no longer used fixed position for two reasons:
		// One, there is a bug when display the jQuery UI Dialog that sometimes places (part of) the dialog
		// above the viewport. In such case, a user could never see the whole dialog.
		// Second, if the dialog is bigger than the viewport, we need to allow the user to scroll to see the
		// entire dialog, especially considering the possibilities for dismissing the dialog (close and 'submit')
		// are at the top and bottom of the dialog.
		// Use fixed positioning for the dialog
		//$('.ui-dialog').css({position:'fixed'});

		// Function to resize & reposition dialog
		var repos = function () {
			var ht = $(window).height();
			var d = $('.ui-dialog');
			var top = Math.min(ht * 0.10, Math.max(0, (ht - d.height()) / 2));

			el.find(".ui-dialog-content")
				.dialog("option", "position", ['center', top])
				.css({'max-height':ht - 120 });
		}


		repos();

		// Automatically resize & reposition
		$(window).resize(repos);

		if (cb) cb();
	});
}

// Hides given input sections based on field labels.
// Expects an array of field labels.
function hideGravityFields(fieldLabels) {
	fieldLabels.forEach(function(fieldLabel) {
		$fieldLabel = findGravityLabel(fieldLabel);
		$fieldLabel.parent().hide();
	});
}

// Fill in a gravity form field with a value.
// The field label must be passed exactly.
// This has only been tested with basic text inputs.
function fillInGravityFormField(fieldLabel, value, cb) {
    // TODO: Error handling.
    // Check for missing field.
    // Check for multiple fields with the same name.

	//console.log("in fillInGravityFormField");
	$field = findGravityInputField(fieldLabel);
	$field.val(value);
}

// For a given gravity form field's label, return its corresponding input element.
// This has only been testing with basic text inputs.
function findGravityInputField(fieldLabel) {
	$fieldLabel = findGravityLabel(fieldLabel);
	$field = $fieldLabel.next().find('input');
	//console.log($field);
	return $field;
}

function findGravityLabel(fieldLabelString) {
	var containsString = ":icontains(" + fieldLabelString + ")";
	$fieldLabel = $(".gform_fields .gfield label.gfield_label" + containsString);
	//console.log($fieldLabel);
	return $fieldLabel;
}

/**
 * Track number of page hits and display registration form on third page load
 */
jQuery(function() {
	var cname, loadcnt, $ = jQuery;
	
	if (pfmls_cl_form_id) {
		showRegForm(null, pfmls_cl_form_id);	
	} else if (which_reg == 'pf') {
		cname = "pfmls_page_loads";
		loadcnt = 1;	
		formid = window['pfmls_reg_form_id'];
	} else if (which_reg == 'main') {
		cname = "wp_page_loads";
		loadcnt = 2;
		formid = window['main_reg_form_id'];
	} else {
		return;	
	}
	
	var cnt = parseInt($.cookie(cname)) || 0
	$.cookie(cname, cnt + 1, {expires: 365 * 5, path: '/'});	
	
	if (cnt >= loadcnt && formid) {
		showRegForm(null, formid);	
	}
});

/**
 * Update the URL for the category search with the sort order suffix
 */
function updateSortOrder(el, current) {
	var $ = jQuery;
	var url = $(el).val()
	var newurl = location.href.replace(new RegExp('(-' + current + ')?(-page-[0-9]*)?/$'), '-' + url + '/');
	location.href = newurl;
}

// Initialize a Google Map for a given ID, using the specified options
function placeMap(id, options, data) {

	var map = new google.maps.Map(document.getElementById(id), options);

	$.each(data, function (id, obj) {
		var l = new LabelOverlay({
			ll: new google.maps.LatLng(obj[2], obj[3]),
			label: '<a href="' + obj[1] + '">' + obj[0] + '</a>',
			map: map
		})
	});
}


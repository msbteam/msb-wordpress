<?php
/*
Plugin Name: Pro-FoundMLS
Plugin URI: http://www.foundwithprofound.com/getfoundidx/
Version: v1.00
Author: <a href="http://www.foundwithprofound.com/">Pro-Found Marketing</a>
Description: A plugin to perform Pro-FoundMLS feed content in good SEO tactics.

Copyright 2011  Pro-Found Marketing, LLC

This code may not be duplicated or distributed without express permission from Pro-Found Marketing, LLC.
*/

require_once('classes/ProFoundMLS.php');
// require_once('classes/ProFound/UserLogin.php');

require_once('widgets/Search.php');
require_once('widgets/CitySearch.php');
require_once('widgets/AgentListings.php');

$pfmls_object = new ProFoundMLS();

// $inifile = dirname(__FILE__) . '/../config.ini';
// $options = parse_ini_file($inifile);
// $file = "{$options['wordpress_plugin_dir_name']}/{basename(__FILE__)}"
// register_activation_hook($file, function() {

// });

?>
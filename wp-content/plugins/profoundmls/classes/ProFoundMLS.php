<?php
/**
 * Main Pro-Found MLS class that handles all of the plugin functions except the widgets
 *
 * Creates the pages for the plugin, adds the Wordpress hooks, renders the dynamic content,
 * creates the WP Admin pages, queries the Pro-Found IDX.
 *
 * @author Matt Kopala <mkopala@gmail.com>
 *
 */
class ProFoundMLS {
	/**
	 * Our object to connect to the IDX system
	 * @var ProFoundIDX
	 */
	private $IDXWrapper = null;

	/**
	 * Name of option in the options DB used for most settings
	 * @var string
	 */
	private $adminOptionsName = "ProFoundMLSAdminOptions";

	/**
	 * Theme name used for the WP Admin pages
	 * @var string
	 */
	private $themename = "ProFoundMLS";

	/**
	 * Object variable used to store data returned by the IDX
	 * @var array
	 */
	public $data = null;

	/**
	 * Description used for the page meta description as well as the property description on the Property Details page
	 * @var string
	 */
	private $site_description;

	/**
	 * Title to be used for the dynamic page
	 * @var string
	 */
	private $site_title;

	/**
	 * Content for the H1 (page title) tag for the dynamic page
	 * @var string
	 */
	private $site_h1;

	/**
	 * Keywords to be used in the meta keywords in the page HEAD
	 * @var string
	 */
	private $site_keywords;

	private $pageTitle;

	/**
	 * Variable to hold the ID of the current dynamic page, as determined by the query variable after rewrite
	 * @var string|false
	 */
	public $_pfmlsPage = false;

	/**
	 * @var bool This variable is set to 'true' if the search was a category search
	 */
	protected $cat_search = false;

	/**
	 * @var array Property listings to be used for the Agent Listings widget
	 */
	public $agentListings = array();

	public $searchData = array();

	/**
	 * The data that is used to query the IDX
	 * @var  array
	 */		
	public $searchQueryData = null;

	/**
	 * Data to be used for the Google Maps
	 * @var  array
	 */
	private $mapdata;

	/**
	 * The city that was extracted from the property or search URL
	 * @var
	 */

	public $city;

	/**
	 * Flag to indicate whether this account has been disabled on the IDX side
	 * @var bool
	 */
	public $account_disabled = false;

	public $address;

	/**
	 * @var array Sort order URLs, fields, labels, and direction
	 */
	public $sort_mods = array(
		'price-highest-lowest' => array('ListPrice,DESC', "Price, Highest to Lowest"),
		'price-lowest-highest' => array('ListPrice,ASC', "Price, Lowest to Highest"),
		'home-size-sqft-largest-smallest' => array('SquareFeet,DESC', "Home Size, Largest to Smallest"),
		'home-size-sqft-smallest-largest' => array('SquareFeet,ASC', "Home Size, Smallest to Largest"),
		'latest-listings' => array('ListDate,DESC', "Latest Listings"),
	);

	/**
	 * Read the plugin options from the DB, update them with data from the IDX if needed, add WP hooks
	 */
	public function __construct() {

		// Get the data for ProFoundMLS from the option in the database
		$devOptions = get_option($this->adminOptionsName);

		// If there is no API user & key set, we are done
		if ($devOptions['api_key'] == "false" || $devOptions['api_secret'] == "false") {
			return;
		}

		# TODO: need error handling here or stuff could get messed up.  form_data could get clobbered and stay that way
		// Check if the data in the DB needs to be updated; get data from the IDX if needed
		if ($this->needsApiUpdate($devOptions)) {
			require_once('ProFoundIDX.php');
			$api = new ProFoundIDX($devOptions['api_key'], $devOptions['api_secret']);
			$opts_array = $api->getDataForm();

			// Don't overwrite fields if the IDX has an error
			if (is_array($opts_array)) {
				$devOptions['form_data'] = json_encode($opts_array['search_opts']);
				$devOptions['account'] = json_encode($opts_array['account']);

				$devOptions['lastupdated'] = time();
				update_option($this->adminOptionsName, $devOptions);
			}
		}

		// Re-enable IDX access if parameter is set
		if (isset($_GET['pfmls_idx_enable'])) {
			update_option('pfmls_disable_idx', 0);
		}
		$this->account_disabled = get_option('pfmls_disable_idx');

		$this->addHooks();
	}

	/**
	 * Check if the options from the IDX are older than 24 hours, and if so, update them
	 *
	 * Update the options if the Advanced Search settings are missing.
	 * Force an update using the 'pfmls_idx_update' parameter in the GET query string.
	 */
	private function needsApiUpdate($opts) {
		return !isset($opts['lastupdated']) || (time() - $opts['lastupdated'] > 3600 * 24) ||
			!isset($opts['form_data']) || $opts['form_data'] == "false" ||
			isset($_GET['pfmls_idx_update']);
	}

	/**
	 * Add the javascript files to the list of files to be included in the head
	 */
	private function addMapFilesToHead() {
		wp_register_script('google_maps', "http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false");
		wp_register_script('pikachoose', plugins_url('/profoundmls/js/jquery.pikachoose.js'));

		wp_enqueue_script('google_maps');
		wp_enqueue_script('pikachoose');

	}

	/**
	 * Generate the JSON code for the Google Map
	 */
	private function addMapData() {
		#$base_url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=";

		$data = array('properties' => array());

		$max_lat = $max_lng = -180;
		$min_lat = $min_lng = 180;

		// Go through the properties, and look up the latitude & longitude
		foreach ($this->mapdata as $result) {
			// TODO: should change IDX to return properties under a specific key

			// Skip unless object is a property
			# TODO: restructure the IDX data so we don't have to do hacks like this
			if (!is_array($result) || !isset($result['ListingID'])) continue;

			#$address = $this->getAddress($result);

			// TODO: some error checking needed here
			// Query Google
			//$geo = json_decode(file_get_contents($base_url . urlencode($address)));
			#print_r($geo); exit;

			//$loc = $geo->results[0]->geometry->location;
			#print_r($loc);
			$loc = (object) array('lat' => doubleval($result['Latitude']), 'lng' => doubleval($result['Longitude']));

			# TODO: clean this code up: remove junk from IDX response & use full array, or something
			# TODO: merge with getAddress code
			# TODO: Just use Google's return data?  Or at least verify against Google's data?
			$data['properties'][$result['ListingKey']] = array(
				'loc' => $loc,
				'url' => $result['uri'],
				'img' => $result['img_url'],
				'h2' => $result['prop_h2'],
				'content' => $result['prop_content'],
				'type' => $result['DwellingType'],
				'street' => implode(' ', array($result['StreetNumber'], $result['StreetDirPrefix'], $result['StreetName'], $result['UnitNumber'], $result['StreetSuffix'])),
				'city_zip' => implode(' ', array($result['City'], $result['State'], $result['PostalCode'])),
				'broker' => $result['ListOfficeName'],
			);

			// Update the bounds
			$min_lat = min($min_lat, $loc->lat);
			$min_lng = min($min_lng, $loc->lng);
			$max_lat = max($max_lat, $loc->lat);
			$max_lng = max($max_lng, $loc->lng);
		}

		# TODO: refactor this method and split it up for the case where we just have one property
		#   i.e. the Property Details page

		// Get the center of the map
		if ($max_lat == $min_lat) {
			$data['center'] = array(
				'lat' => $max_lat,
				'lng' => $max_lng,
			);
		} else {
			$data['center'] = array(
				'lat' => ($max_lat - $min_lat) / 2,
				'lng' => ($max_lng - $min_lng) / 2,
			);
		}
		$data['bounds'] = array($min_lat, $min_lng, $max_lat, $max_lng);

		// Print the <script> block
		?><script type="text/javascript">var mapdata = <?= json_encode($data) ?></script>
		<?php
	}

	/**
	 * This code was mostly taken from the VirtualPages::detectPost() method from the VirtualPages plugin.
	 *
	 * @see http://wordpress.org/extend/plugins/virtual-pages/
	 */
	public function detectPost($posts) {
		global $wp;
		global $wp_query;

		$this->city = false;

		# TODO: not sure if this is the right place for this
		if (!$this->account_disabled) {
			$this->getAgentListings();
		}

		if (isset($wp->query_vars) && array_key_exists('pfmls', $wp->query_vars)) {

				// Make sure this method isn't called again for this request
				remove_filter('the_posts',array(&$this,'detectPost'));

				remove_action('wp_head','rel_canonical');

				remove_action('wp_head','genesis_canonical');       // Genesis 1.7
				remove_action('wp_head','genesis_canonical', 5);    // Genesis 1.8

				// Shorthand
				$q = &$wp->query_vars;

				// TODO: This is a bit of a hack here as well - if "city" part of URL was actually a category, then this is a search results page
				if ($q['pfmls'] == 'neighborhood' && isset($q['city'])) {
					$city_name = ucwords(preg_replace('/-/', ' ', $q['city']));
					if (!$this->getCityId($city_name)) {
						$q['pfmls'] = 'search_results';
						$q['pfmls_cat'] = $q['city'];
						unset($q['city']);
					}
				}

				$this->_pfmlsPage = $q['pfmls'];

				// Set the page title
				switch ($q['pfmls']) {
					case 'disclosure':
						$this->renderDisclosure();
						exit;
					case 'search_results':
						if ($this->account_disabled) break;
						$this->getSearchResults();
						$this->addMapFilesToHead();
						break;
					case 'neighborhood':
						$this->site_h1 = "Houses for Sale";
						if (isset($q['city'])) {
							$city_name = ucwords(preg_replace('/-/', ' ', $q['city']));
							$this->site_h1 .= ' ' . $city_name;
							$this->city = $city_name;
						}
						$this->site_h1 .= " by Property Type";
						$this->site_title = $this->site_h1;
						break;
					case 'advsearch':
						$this->site_title = $this->site_h1 = "Advanced Search for Properties";
						break;
					case 'property_detail':
						if ($this->account_disabled) break;
						$this->getPropertyDetails();
						$this->addMapFilesToHead();
						$this->addHeadersForPDP();

						# TODO: this is quite a bit of a hack to redirect ourselves.  will this cause other issues?
						$url = $_SERVER['REQUEST_URI'];
						if (substr($url, -1, 1) == '/') {
							header ('HTTP/1.1 301 Moved Permanently');
							header("Location: " . untrailingslashit($url));
							exit;
						}
						remove_action('template_redirect', 'redirect_canonical');

						break;
				}

				// Replace the current WP_Query with our own
				$wp_query = new WP_Query();

				// Put something in the array so a 404 isn't generated; set the post count
				$wp_query->posts = array(null);
				$wp_query->post_count = 1;

				// Trick WordPress in to thinking this is a page - needed for wp_title() filter
				$wp_query->is_page = true;

				// This controls the CSS classes for the body tag and nav menu highlighting
				$page_id = get_option('pfmls_' . $q['pfmls'] . '_page_id');
				$wp_query->post->ID = $page_id;
				$wp_query->is_singular = true;

				// Set the page title
				$wp_query->post->post_title = $this->site_title;

				// Disable comments
				$wp_query->post->comment_status = 'closed';

				return $wp_query->posts;
		}

		return $posts;
	}

	// Only show the mortgage calculator if an admin has set up at least one of the gravity forms.
	protected function shouldShowMortgageCalculator() {
		$options = get_option($this->adminOptionsName);
		$connect_with_loan_officer_form_id = $options["connect_with_loan_officer_form_id"];
		$connect_with_real_estate_agent_form_id = $options["connect_with_real_estate_agent_form_id"];
		return $connect_with_loan_officer_form_id || $connect_with_real_estate_agent_form_id;
	}

	/**
	 * Render header details for the Property Details Page. This section was moved to its
	 * own function due to its verbosity.
	 */
	protected function addHeadersForPDP() {
		if (!$this->shouldShowMortgageCalculator()) return;

		wp_register_script('accounting', plugins_url('/profoundmls/js/accounting.min.js'));
		wp_enqueue_script('accounting');

		wp_register_script('tools', plugins_url('/profoundmls/js/tools.js'));
		wp_enqueue_script('tools');

		wp_register_script('jquery.ui-1.9.1.js', "http://code.jquery.com/ui/1.9.1/jquery-ui.js");
		wp_enqueue_script('jquery.ui-1.9.1.js');

		wp_register_script('calculator', plugins_url('/profoundmls/js/calculator.js'));
		wp_enqueue_script('calculator');
		wp_register_style('calculator', WP_PLUGIN_URL . '/profoundmls/css/calculator.css');
		wp_enqueue_style('calculator');

		wp_register_script('bootstrap', plugins_url('/profoundmls/js/bootstrap.min.js'));
		wp_enqueue_script('bootstrap');
		wp_register_style('bootstrap', WP_PLUGIN_URL . '/profoundmls/css/bootstrap.css');
		wp_enqueue_style('bootstrap');
	}

	/**
	 * Render HTML that contains the ARMLS disclosure and information about the ProFound IDX
	 */
	public function renderDisclosure() {
		$options = $this->getAdminOptions();

		date_default_timezone_set('MST');
		$last_updated = date('n/j/Y g:i A T', (int) get_option('pfmls_last_updated'));

		$acct = json_decode($options['account']);

		$img_base =	site_url() . "/wp-content/plugins/profoundmls/images/";
		$cdate = date('Y');

		switch ($acct->mls) {
			case 'armls':
				$img = $img_base . 'ARMLS_logo.jpg';
				$text = "

						<img src=\"$img\">
						<p>
						Copyright $cdate Arizona Regional Multiple Listing Service, Inc. All rights reserved.<br><br>
						All information should be verified by the recipient and none is guaranteed as accurate by ARMLS.<br><br>
						IDX Solution Provided by GetFoundIDX<br><br>
						ARMLS Listing Data last updated $last_updated .
						</p>
						";
				break;
			case 'trendmls':
				$img = $img_base . 'TREND_logo.jpg';
				$company = $acct->company;
				$text = "
					The data relating to real estate for sale on this website appears in part through the TREND Internet Data Exchange
					 program, a voluntary cooperative exchange of property listing data between licensed real estate brokerage firms in
					 which $company participates, and is provided by TREND through a licensing agreement.<br><br>
					The information provided by this website is for the personal, non-commercial use of consumers and may not be used
					 for any purpose other than to identify prospective properties consumers may be interested in purchasing.<br><br>
					Some properties which appear for sale on this website may no longer be available because they are under contract,
					 have sold or are no longer being offered for sale.<br><br>
					Some real estate firms do not participate in IDX and their listings do not appear on this website. Some properties
					 listed with participating firms do not appear on this website at the request of the seller.<br><br>
					Real estate listings held by brokerage firms other than are marked with the IDX icon and detailed information about
					 each listing includes the name of the listing broker.<br><br>
					Information Deemed Reliable But Not Guaranteed.<br><br>
					Data from Google Maps is not provided by TREND and should be considered informative, but not accurate.<br><br>
					© $cdate TREND, All Rights Reserved
					Data last updated: $last_updated.<br>
					<div>
						<img src=\"$img\">
					</div>
					<div style=\"clear: both;\">
					<a target=\"_blank\" href=\"http://foundwithprofound.com/getfoundidx/\">WordPress Real Estate Website</a> IDX Solution
					provided by Pro-Found Marketing, LLC.<br>
					</div>
						";
				break;
		}

		?>
		<html>
			<head>
				<style>
				 body { font-size: 14px; }
					img { float: left; margin-top: 12px; margin-bottom: 12px; }
					p { margin-left: 200px; }
				</style>
			</head>
			<body>
				<?= $text ?>
			</body>
		</html><?php
	}

	/**
	 * Generate the dynamic content for the Pro-Found MLS pages
	 *
	 * @param string $content Original content from the DB field for the page
	 * @return string Original or dynamic content
	 */
	public function renderPage($content) {
		global $wp;

		if (isset($wp->query_vars) && array_key_exists('pfmls', $wp->query_vars)) {

			ob_start();
			# TODO: match the function names with the query string variables, and remove switch statement?
			// Set the page title
			# TODO: move these keys to be class constants
			switch ($wp->query_vars['pfmls']) {
				case 'search_results':
					$this->pageSearch();
					break;
				case 'neighborhood':
					$this->pageNeighborhood();
					break;
				case 'property_detail':
					$this->pagePropertyDetail();
					break;
				case 'advsearch':
					$this->pageAdvancedSearch();
					break;
			}
			return ob_get_clean();
		}
		return $content;
	}

	/**
	 * Initialize the sidebar, add the dynamic pages to the DB, add/refresh the rewrite rules
	 */
	public function init() {
		$this->getAdminOptions();

		// Bail out if this is the gravity forms display script
		if (strpos($_SERVER['SCRIPT_NAME'], 'gravity_form_display') > 0) return;

		# TODO: move to the plugin install?
		$this->addSidebar();

		# TODO: move these to the initialization function once we have a hook that checks for edits to
		#  the page URLs
		$this->addDynamicPages();
		$this->addRewriteRules();

		if (!is_admin()) {
			wp_register_script('profound_mls', plugins_url('/profoundmls/js/profound_mls.js'));
			wp_register_script('jquery-cookie', plugins_url('/profoundmls/js/jquery.pookie.js'));

			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-cookie');
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_script('profound_mls');

			wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
		}
	}

	/**
	 * Add a sidebar that will be used for the Property Details page
	 *
	 * @uses register_sidebar() Adds the sidebar using the Wordpress API
	 */
	private function addSidebar() {
		$desc = "
		This sidebar contains a small map for the Property Details page at the top,
		followed by any widgets that you add here.";

		$args = array(
			'name' => 'Pro-Found MLS Sidebar',
			'id' => 'sidebar-pfmls-prop-detail',
			'description' => $desc,
		);
		register_sidebar($args);
	}

	# FIXME: clean this up - reuse page keys from elsewhere
	public function getPageIds() {
		$keys = array('search_results', 'advsearch', 'property_detail', 'neighborhood');
		$ids = array();
		foreach ($keys as $key) {
			$ids[] = get_option('pfmls_' . $key . '_page_id');
		}
		return $ids;
	}

	/**
	 * Get the URL (name) for the PF MLS page specified by the key
	 * @param string $key Key for the page
	 */
	public static function getPageURL($key) {
		$id = get_option('pfmls_' . $key . '_page_id');

		$post = get_post($id);

		return $post->post_name;
	}

	# TODO: this should only be called when the plugin is installed/activated,
	#  or when someone updates the URLs in the options via the admin pages
	/**
	 * Add rewrite rules to the WordPress options DB for the Pro-Found MLS dynamic pages
	 */
	private function addRewriteRules() {
		# TODO: get this from an object variable
		$opts = $this->getAdminOptions();

		$search_fields = array('price', 'city', 'sqft', 'bedroom', 'bathroom', 'mlsnum', 'garage', 'zipcode', 'proptype', 'pricerange', 'schooldistrict');

		// Rewrite rules for search URLs
		$query_vars = array();
		foreach ($search_fields as $index => $fname) {
			add_rewrite_tag("%$fname%",'([^&]+)');

			// Construct the final URL
			$query_vars[] = $fname . '=$matches[' . ($index + 1) . ']';
		}
		$url = 'index.php?pfmls=search_results&' . implode('&', $query_vars);

		add_rewrite_tag("%pfmls%",'([^&]+)');
		add_rewrite_tag("%pp%",'(\d+)');
		add_rewrite_tag("%pfmls_cat%",'([^&]+)');
		add_rewrite_tag("%listing_id%",'([^&]+)');

		// Get the URLs for the pages from the DB
		$results_url = ProFoundMLS::getPageURL("search_results");
		$cat_url = ProFoundMLS::getPageURL("neighborhood");
		$advsearch_url = ProFoundMLS::getPageURL("advsearch");

		$rules = array(
			// Search results
            # TODO: remove this, it's old. (Matt)
			'^' . $results_url . '/([^/]*)/([^/]*)/([^/]*)/([^/]*)/([^/]*)/?' =>
				$url,

			// Category Search results, paged
			'^' . $cat_url . '/([^/]+)-page-(\d+)/?' =>
				'index.php?pfmls=search_results&pfmls_cat=$matches[1]&pp=$matches[2]',

			// Search results, regular GET request
			'^' . $results_url . '/?$' =>
				'index.php?pfmls=search_results',

			// Neighborhood w/ city - or Category Search results
			'^' . $cat_url . '/([^/]*)/?' =>
				'index.php?pfmls=neighborhood&city=$matches[1]',

			// Neighborhood page
			'^' . $cat_url . '/?' =>
				'index.php?pfmls=neighborhood',

			// Property Search page
			'^' . $advsearch_url . '/?' =>
				'index.php?pfmls=advsearch',

			// Property Detail page
			'^([^/]*)-(\d{7})/?' =>
			'index.php?pfmls=property_detail&listing_id=$matches[2]',

			// ARMLS Disclosure
			'^pfmls-disclosure/?' =>
			'index.php?pfmls=disclosure',
		);

		foreach ($rules as $regex => $new_url) {
			add_rewrite_rule($regex, $new_url, 'top');
		}

		flush_rewrite_rules();
	}

	/**
	 * Add all of the WordPress actions and filters for this plugin
	 */
	private function addHooks() {
		add_action('init',                    array(&$this, 'init'));
		add_action('admin_init',              array(&$this, 'adminInit'));
		add_action('admin_menu',              array(&$this, 'adminMenu'));

		add_action('wp_head',                 array(&$this, 'addHeaderCode'));
		add_filter('wp_title',                array(&$this, 'setPageTitle'));

		// Add hooks to allow setting a custom title in the H1 tags
		add_filter('the_title',               array(&$this, 'setPageH1'));

		add_filter('the_posts',               array(&$this, 'detectPost'));
		add_filter('the_content',             array(&$this, 'renderPage'));

		add_action('widgets_init',            array(&$this, 'registerWidgets'));

		add_filter('get_sidebar',             array(&$this, 'sidebarPropDetail'));
		add_filter('sidebars_widgets',        array(&$this, 'widgetFilter'));

		add_action('get_footer',              array(&$this, 'addDisclosure'), 1);

		add_shortcode('pfmls_city_list',      array(&$this, 'renderCityList'));
		add_shortcode('pfmls_map',            array(&$this, 'simpleGoogleMap'));
	}

	/**
	 * Add the Pro-Found MLS dynamic pages to the DB if they don't exist already
	 *
	 * Add the Property Detail, Neighborhood, Search Results, and Property Search
	 * pages to the database, and save the page IDs in the options table.
	 */
	private function addDynamicPages() {

		$expired = "This property is not available for IDX display for any number of reasons, but thousands of others are.

			Browse through these properties with our comprehensive property search: <a href=\"/property-search/\">Start New Search</a>.
		";

		$pages = array(
			'property_detail' => array('Property Detail', ''),
			'neighborhood' => array('Neighborhood', 'neighborhood'),
			'search_results' => array('Search Results', ''),
			'advsearch' => array('Property Search', 'property-search'),
			'unavailable' => array('Listing Not Available', 'removed-from-idx-display', $expired),
		);

		foreach ($pages as $key => $data) {
			list($title, $url) = $data;

			$optionid = 'pfmls_' . $key . '_page_id';
			$pageid = get_option($optionid);

			$post_name = ProFoundMLS::getPageURL($key);

			if (!$pageid || !$post_name) {

				// Create post object
				$post = array(
					'post_title' => $title,
					'post_name' => $url,
					'post_content' => count($data) == 3 ? $data[2] : '',
					'post_status' => 'publish',
					'post_type' => 'page',
					'comment_status' => 'closed',
					'ping_status' => 'closed',
				);
				# TODO: set name (url), guid, and menu_order?

				// Insert or update the post ID in the database
				$pageid = wp_insert_post($post);

				if ($pageid) {
					update_option($optionid, $pageid);
				} else {
					add_option($optionid, $pageid);
				}
			}
		}
	}

	/**
	 * Override the <title> for the page if this is a PF MLS page
	 *
	 * @param string $title Original title from WordPress
	 * @return string Title to use for the page
	 */
	public function setPageTitle($title) {
		return $this->site_title ? $this->site_title : $title;
	}

	/**
	 * Override the <h1> for the page if this is a PF MLS page
	 *
	 * Only modify the title if the ID of the post is a PF MLS page
	 * @param string $title Original title as determined by WordPress
	 * @return string Page title to be shown in the H1 tags
	 */
	public function setPageH1($title) {
		# FIXME: hack for case 173
		if ($this->site_h1) {
			global $wp_query;
			$wp_query->is_singular = true;
		}

		return in_the_loop() && $this->site_h1 ? $this->site_h1 : $title;
	}

	/**
	 *  Exclude the Agent Listings widget if we did not find any agent listings on the IDX
	 *
	 * @param $widgets Original widget list from WordPress
	 * @return  Modified widget list with Agent Listings widget excluded, if applicable
	 */
	public function widgetFilter($widgets) {
		if (!is_admin()) {

			if (empty($this->agentListings)) {

				// Go through each container, and remove the widget if present
				foreach ($widgets as $container => $cwidgets) {
					if (!is_array($cwidgets)) continue;

					// Skip the inactive widgets list
					if ($container == 'wp_inactive_widgets') continue;

					// Filter the list
					$widgets[$container] = array_merge(array_filter($cwidgets, array($this, 'skipAgentListingsWidget')));
				}
			}
		}
		return $widgets;
	}

	/**
	 * Remove any widget names from the array that are Agent Listings widgets
	 * @param $name Widget name
	 * @return bool True to include this widget in the filtered array
	 */
	private function skipAgentListingsWidget($name) {
		return strpos($name, 'profoundmls_agent_listings_widget') === false;
	}

	/**
	 * Get the Agent Listings from the IDX and save them for use by the widget
	 */
	protected function getAgentListings() {
		# TODO: use a constant defined for all files, or a shared method
		$opts = get_option($this->adminOptionsName);

		// Bail out if the list of IDs is empty, or is not a comma-separated list of integers
		$listids = $opts['feat_listings'];
		if (!$listids || !preg_match('/^\s*(\d+\s*,?\s*)+$/', $listids)) return;

		// Get the cached listings, if it exists
		$fl_obj = $opts['fl_cache'];

		$now = time();

		// If we don't have any featured listings cached, or if the timestamp is older than 10 minutes, fetch them from the IDX server
		if (!$fl_obj || $now - $fl_obj['updated'] > 600 || isset($_GET['pfmls_feat_listings'])) {

			require_once(dirname(__FILE__) . '/../classes/ProFoundIDX.php');
			$api = new ProFoundIDX($opts['api_key'], $opts['api_secret']);

			$data = array(
				'listids' => $listids
			);

			# TODO: only fetch these if the widget is supposed to be displayed on the page.  Also, wrap this up with the other IDX queries.
			$res =  $api->getAgentListings($data);

			# Cache the listings
			$fl_obj = array(
				'updated' => $now,
				'data' => $res
			);
			$opts['fl_cache'] = $fl_obj;

			update_option($this->adminOptionsName, $opts);
		}

		$this->agentListings = $fl_obj['data'];

		return $fl_obj['data'];
	}

	/**
	 * If this is the PF MLS property details page, use the PF MLS sidebar
	 *
	 * @param string $content Original sidebar content
	 * @return string Original or modified sidebar content
	 */
	public function sidebarPropDetail($content) {
		if ($this->_pfmlsPage == 'property_detail') {

			remove_filter('get_sidebar', array(&$this, 'sidebarPropDetail'));

			$body = htmlentities("I am interested in seeing this house!");
			$subject = htmlentities("See This House : " . $this->address . " (" . $this->data['LIST_105'] . ")");
			$imgdir = plugins_url() . '/profoundmls/images';

			$search_url = home_url() . '/' . ProFoundMLS::getPageURL("advsearch");
			?>
			<div id="pfmls-link-images">
				<div class="pfmls-btn-goback" onclick="javascript:history.back(1)"></div>
				<div class="pfmls-btn-newsearch" onclick="javascript:location.href='<?= $search_url ?>'"></div>
			</div>
            <?php

            // If there is a youtube video id, display it above the map on the right.
            //$this->data['youtube_id'];
            if ($this->data['youtube_id'] != null) {
            ?>
                <div id="pfmls-youtube">
                    <iframe src="http://www.youtube.com/embed/<?=$this->data['youtube_id'];?>?wmode=transparent" frameborder="0" allowfullscreen></iframe>
                </div>
            <?php
            }
            ?>
			<div id="pfmls_small_map"></div>

            <?= $this->renderPropertyDetailCalculator() ?>

			<div id ="pfmls-action-images">
			<?

			// Add the "Call to Action" images
			$this->renderActionButton('seenow');
			$this->renderActionButton('help');

			?></div>
			<?php

			ob_start();
			dynamic_sidebar('sidebar-pfmls-prop-detail');
			return ob_get_clean();
		}
		return $content;
	}

	private function renderPropertyDetailCalculator() {
		if (!$this->shouldShowMortgageCalculator()) return;

		?>

		<div class="calculator calculator_sidebar" data-mls-number="<?= $this->data['ListingID'] ?>"
		     data-zip-code="<?= $this->data['PostalCode'] ?>"
		     data-school-district="<?= $this->data['SchoolDistrict'] ?>" data-city="<?= $this->getCityId($this->data['City']) ?>"
		     data-search-url="<?= home_url() ?>/<?= ProFoundMLS::getPageURL("search_results") ?>/">
		    <div class="header">
		    	Calculate payment
		    	<br/>
		    	Search by budget
		    </div>
			<div class="inputs">
				<div class="line">
					<label>List price:</label> <input type="text" class="list_price" disabled="disabled"
					                                  value="<?= $this->data['ListPrice'] ?>"/>
				</div>
				<div class="line">
					<label for="down_payment">Down payment:</label>
					<select class="down_payment_select">
						<option value="va" selected="selected">VA (0%)</option>
						<option value="fha">FHA (3.5%)</option>
						<option value="conventional">Conventional (5%)</option>
						<option value="jumbo">Jumbo (10%)</option>
						<option value="other">Other</option>
					</select>
				</div>
				<div class="line">
					<label></label>

					<div class="input-prepend">
						<span class="add-on">$</span>
						<input id="down_payment" type="text" class="down_payment"/>
					</div>
				</div>
				<div class="line">
					<label>Loan amount:</label> <input type="text" class="loan_amount" disabled="disabled"/><br/>
					<label for="interest_rate">Interest rate:</label>

					<div class="input-append">
						<input id="interest_rate" type="text" class="interest_rate"/>
						<span class="add-on">% APR</span>
					</div>
				</div>
				<div class="line">
					<label>Annual taxes:</label>
					<input type="text" class="taxes" disabled="disabled" value="<?= $this->data['TotalTaxes']?>"/>
				</div>
				<div class="line">
					<label for="insurance">Annual insurance:</label>

					<div class="input-prepend">
						<span class="add-on">$</span>
						<input id="insurance" type="text" class="insurance"/>
					</div>
				</div>
			</div>
			<div class="actions">
				<label></label>
				<input type="submit" value="Calculate!" class="launch_button btn btn-primary"/>
			</div>
		</div>


		<div class="calculator calculator_popup" data-mls-number="<?= $this->data['ListingID'] ?>"
		     data-zip-code="<?= $this->data['PostalCode'] ?>"
		     data-school-district="<?= $this->data['SchoolDistrict'] ?>" data-city="<?= $this->getCityId($this->data['City']) ?>"
		     data-search-url="<?= home_url() ?>/<?= ProFoundMLS::getPageURL("search_results") ?>/">
			<div class="left">
				<div class="inputs">
					<div class="line">
						<label>List price:</label> <input type="text" class="list_price" disabled="disabled"
						                                  value="<?= $this->data['ListPrice'] ?>"/>
					</div>
					<div class="line">
						<label for="down_payment">Down payment:</label>
						<select class="down_payment_select">
							<option value="va" selected="selected">VA (0%)</option>
							<option value="fha">FHA (3.5%)</option>
							<option value="conventional">Conventional (5%)</option>
							<option value="jumbo">Jumbo (10%)</option>
							<option value="other">Other</option>
						</select>
					</div>
					<div class="line">
						<label></label>

						<div class="input-prepend">
							<span class="add-on">$</span>
							<input id="down_payment" type="text" class="down_payment"/>
						</div>
					</div>
					<div class="line">
						<label>Loan amount:</label> <input type="text" class="loan_amount" disabled="disabled"/><br/>
						<label for="interest_rate">Interest rate:</label>

						<div class="input-append">
							<input id="interest_rate" type="text" class="interest_rate"/>
							<span class="add-on">% APR</span>
						</div>
					</div>
					<div class="line">
						<label>Annual taxes:</label>
						<input type="text" class="taxes" disabled="disabled" value="<?= $this->data['TotalTaxes']?>"/>
					</div>
					<div class="line">
						<label for="insurance">Annual insurance:</label>

						<div class="input-prepend">
							<span class="add-on">$</span>
							<input id="insurance" type="text" class="insurance"/>
						</div>
					</div>
				</div>
				<div class="actions">
					<label></label>
					<input type="submit" value="Calculate!" class="calculate btn btn-primary"/>
				</div>
			</div>
			<div class="right">
				<div class="outputs">
					<label>Monthly payment:</label>
					<div class="input-prepend">
						<span class="add-on">$</span>
						<input type="text" class="monthly_payment" disabled="disabled"/>
					</div>
				</div>
				<div class="questions">
					<div class="line fit question">
						<label>Does this payment fit your budget?</label>

						<div class="right-side">
							<button class="yes btn">Yes</button>
							<button class="no btn">No</button>
						</div>
					</div>
					<div class="line fit_yes question">
						<div class="excellent"><span class="label label-success">Excellent!<br/> What would you like to do now?</span>
						</div>
						<div>
							<ul>
								<?
								$options = get_option($this->adminOptionsName);
								$connect_with_loan_officer_form_id = $options["connect_with_loan_officer_form_id"];
								if ($connect_with_loan_officer_form_id) {
									?>
									<li><a href="#" class="connect_with_loan_officer"
									       data-gravity-form-id="<?=$connect_with_loan_officer_form_id?>">Talk to a financing
										expert about pre qualifying for this home</a></li><?
								}
								?>
								<?
								$connect_with_real_estate_agent_form_id = $options["connect_with_real_estate_agent_form_id"];
								if ($connect_with_real_estate_agent_form_id) {
									?>
									<li><a href="#" class="connect_with_real_estate_agent"
									       data-gravity-form-id="<?=$connect_with_real_estate_agent_form_id?>">Speak to an agent
										about this home</a></li><?
								}
								?>
								<li><a href="#" class="see_others">See other homes with similar monthly payments in the same
									school district, zip code, or city</a></li>
							</ul>
						</div>
						<div class="similar question">
							<div>Show me similar homes based on:</div>
							<div>
								<ul>
									<li><a href="#" class="school_district">School district</a></li>
									<li><a href="#" class="zip_code">Zip code</a></li>
									<li><a href="#" class="city">City</a></li>
								</ul>
							</div>
						</div>
					</div>
					<div class="line target question">
						<label>What is your preferred monthly payment amount?</label>

						<div class="input-prepend">
							<span class="add-on">$</span>
							<input type="text" class="target_monthly"/>
						</div>
						<br/>
						<label></label>
						<button class="go btn">Search homes</button>
					</div>
				</div>
			</div>
		</div>

		<?
	}

	/**
	 * Render HTML for the "Call to Action" image with onClick handler to display Gravity Form
	 *
	 * @param string $form_key Key name in the options table for the form
	 */
	private function renderActionButton($form_key) {
		$options = get_option($this->adminOptionsName);
		$formid = $options[$form_key . '_form_id'];

		if ($formid) {
			?><div class="pfmls-btn-<?= $form_key ?>" onClick="showGravityForm(<?= $formid ?>, <?= $this->data['ListingID'] ?>);" /></div><?php
		}
	}

	/**
	 * Register the Quick Search, City Search and Agent Listings widgets with WordPress
	 */
	public function registerWidgets() {
		$widgets = array(
			'ProFoundMLS_Search_Widget',
			'ProFoundMLS_City_Search_Widget',
			'ProFoundMLS_Agent_Listings_Widget'
		);
		array_map('register_widget', $widgets);
	}

	/**
	 * Returns an array of admin options
	 */
	private function getAdminOptions() {
		return get_option($this->adminOptionsName);
	}

	/**
	 * Create the ProFoundMLS plugin menu in the WordPress Admin area
	 */
	public function adminMenu() {
		# FIXME: what is this for???
		/*
		if ( $_GET['page'] == basename(__FILE__) ) {
			if ( 'save' == $_REQUEST['action'] ) {
				foreach ($this->adminOptionsName as $value) {
					update_option( $value['id'], $_REQUEST[ $value['id'] ] );
				}
				foreach ($options as $value) {
					if( isset( $_REQUEST[ $value['id'] ] ) ) {
						update_option( $value['id'], $_REQUEST[ $value['id'] ]  );
					}
				}

				header("Location: admin.php?page=functions.php&saved=true");
				die;

			}
			else if( 'reset' == $_REQUEST['action'] ) {

				foreach ($options as $value) {
					delete_option( $value['id'] );
				}

				header("Location: admin.php?page=functions.php&reset=true");
				die;

			}
		}
		*/

		add_menu_page($this->themename, $this->themename, 'administrator', 8, array($this,'adminSettings'));

		$menus = array(
			array('Settings', 'adminSettings'),
			array('City Configuration', 'adminCityConfiguration'),
			array('Category/Neighborhood', 'adminCategoryNeighborhood'),
		);

		for ($i = 1; $i <= 6; $i++) {
			add_submenu_page(8, $this->themename, $menus[$i][0], 'administrator', $i, array($this, $menus[$i][1]));
		}

	}

	/**
	 * Update the SEO meta data for the Property Details page
	 */
	private function updatePropDetails() {
		# TODO: just get/access this from an object variable, instead of calling a function
		$devOptions = $this->getAdminOptions();
		if (isset($_POST['update_ProFoundMLSSettings'])) {
			if (isset($_POST['property_detail_page_title'])) {
				$property_detail_page_title = $_POST['property_detail_page_title'];
				$devOptions['property_detail_page_title'] = $property_detail_page_title;
			}

			if (isset($_POST['property_detail_page_h1'])) {
				$property_detail_page_h1 = $_POST['property_detail_page_h1'];
				$devOptions['property_detail_page_h1'] = $property_detail_page_h1;
			}

			if (isset($_POST['property_detail_page_description'])) {
				$property_detail_page_description = $_POST['property_detail_page_description'];
				$devOptions['property_detail_page_description'] = $property_detail_page_description;
			}

			if (isset($_POST['property_detail_page_keywords'])) {
				$property_detail_page_keywords = $_POST['property_detail_page_keywords'];
				$devOptions['property_detail_page_keywords'] = $property_detail_page_keywords;
			}

			$detail_request_meta = $_POST['detail_request_meta'];
			$devOptions['detail_request_meta'] = $detail_request_meta;

			update_option($this->adminOptionsName, $devOptions);
			$this->showUpdateMessage();
		}
	}

	/**
	 * Show the Property Details admin page and save updates if necessary
	 */
	public function adminPropertyDetails() {
		$this->updatePropDetails();
		$this->renderCustomSEOform("property_detail", "Property Details");
	}

	/**
	 * Update the Categories and SEO meta data for the Neighborhood/Category page
	 */
	private function updateCategoryOptions() {
		$devOptions = $this->getAdminOptions();
		if (isset($_POST['update_category'])) {

			$fields = array('category_name', 'search_query', 'category_image_uri', 'category_desc');
			foreach ($fields as $fname) {
				$devOptions[$fname] = isset($_POST[$fname]) ? $_POST[$fname] : null;
			}

			update_option($this->adminOptionsName, $devOptions);
			$this->showUpdateMessage();
		}
	}

	/**
	 * Show the Category settings form
	 */
	private function renderCategorySettings() {
		?>
		<div class="wrap">
			<div class="content" id="categories">
				<form method="post" action="<?= $_SERVER["REQUEST_URI"]; ?>">
					<h2 class="head-city-list">Category / Neighborhood Configuration</h2>
					<p>The Category/Neighborhood Configuration creates a grid layout
						within the Category page.</p>
					<p>Choose a page template and set the SEO Meta Data here.</p>
					<div class="tabs">
						<ul>
							<li><a class="tab" href="javascript:void(0)" title="#categories"
								class="active">Categories</a></li>
							<li><a class="tab" href="javascript:void(0)"
								title="#category-template">Category Template & Meta</a></li>
						</ul>
					</div>
					<p>
						<span class="add-category">Add Category</span>
					</p>
					<ul class="category-lable">
						<li><span>Category Name:</span></li>
						<li><span>Search Query Word(s):</span></li>
						<li><span>Category Image URI:</span></li>
					</ul>
								<?php
								$devOptions = $this->getAdminOptions();
								$categories_name = $devOptions['category_name'];
								$categories_desc = $devOptions['category_desc'];
								$category_queries = $devOptions['search_query'];
								$category_image_uri = $devOptions['category_image_uri'];
							   ?>
									<?php for($i = 0;$i < count($categories_name);$i++) { ?>
									<ul class="category-input">
										<li><input type="text" name="category_name[]" value="<?= $categories_name[$i] ?>"  /></li>
										<li><input type="text" name="search_query[]" value="<?= $category_queries[$i] ?>" /></li>
										<li><input type="text" name="category_image_uri[]" value="<?= $category_image_uri[$i] ?>" /></li>
										<?php $delete_icon = WP_PLUGIN_URL . '/profoundmls/images/gnome_edit_delete.png'; ?>
										<textarea rows="3" cols="60" name="category_desc[]"><?= stripslashes($categories_desc[$i]) ?></textarea>
										<li class="delete"><img class="delete-item" alt="Delete" src="<?= $delete_icon; ?>" /></li>
									</ul>
									<?php } ?>
								<div class="submit">
									<input type="submit" name="update_category" value="<?php _e('Save Changes', 'ProFoundMLS') ?>" />
								 </div>
							</form>
				<ul id="category-input" class="category-input" style="display: none">

					<li><input type="text" name="category_name[]" /></li>
					<li><input type="text" name="search_query[]" /></li>
					<li><input type="text" name="category_image_uri[]" /></li>
					<?php $delete_icon = WP_PLUGIN_URL . '/profoundmls/images/gnome_edit_delete.png'; ?>
					<textarea rows="3" cols="60" name="category_desc[]"></textarea>
					<li class="delete"><img class="delete-item" alt="Delete" src="<?= $delete_icon; ?>" /></li>
				</ul>
				<script type="text/javascript">
								jQuery(document).ready(function(){
									jQuery('.add-category').live('click',function(){
										var category_clone = jQuery('#category-input').clone();
										jQuery(category_clone).removeAttr('id');
										jQuery(category_clone).css('display','block');
										jQuery('.category-lable').after(category_clone);
										return false;
									});
									jQuery('.delete').live('click',function(){
										jQuery(this).parent().remove();
										return false;
									});
								});
							</script>
				<div id="notes">
					<hr />
					<h3>Notes :</h3>
					<p>Enter as many categories as you would like.</p>
					<p>Each category requires a title,which can contain a city name (if
						the category is used flowing a city choice),</p>
					<p>a set of keywords to query against the MLS database, and an image
						to category in the layout grid.</p>
				</div>
			</div><?php
	}

	/**
	 * Render the Custom SEO form for the specified page
	 *
	 * @param string $key ID for the dynamic page
	 * @param string $name Name of the dynamic page
	 */
	private function renderCustomSEOform($key, $name) {
		# TODO: refactor some of this code to a generic form builder function; combine with adminAdvancedSearch
		/*
		?>
		<div class="wrap">
			<div id="search-result">
				<form method="post" action="<?= $_SERVER["REQUEST_URI"]; ?>">
					<h2 class="head-city-list"><?= $name ?> Configuration</h2>
					<p>The <?= $name ?> Configuration designates the Page Template and
						Page META Data.</p>
					<h3>SEO META Data</h3>
					<div id="search-result-content">
							<label>Request meta data from the server :</label>
							<input type="checkbox" name="<?= $key ?>_request_meta" id="<?= $key ?>_request_meta"  <?php if($devOptions['detail_request_meta']){ ?> checked="checked" <?php } ?> />
							<br />
							<p>Enter the structure of the SEO Meta Data for the <?= $name ?> page</p>
							<label>Page Title</label>
							<input type="text"  name="<?= $key ?>_page_title" class="text"  value="<?= $devOptions[$key . '_page_title'] ?>"/>
							<label>Page H1:</label>
							<input type="text"  name="<?= $key ?>_page_h1" class="text" value="<?= $devOptions[$key . '_page_h1'] ?>"/>
							<label>Page Description:</label>
							<textarea name="<?= $key ?>_page_description" class="text-area"><?= $devOptions[$key . '_page_description'] ?></textarea>
							<label>Page Keywords:</label>
							<textarea name="<?= $key ?>_page_keywords" class="text-area"><?= $devOptions[$key . '_page_keywords'] ?></textarea>
							<div class="submit">
							<input type="submit" name="update_ProFoundMLSSettings" value="<?php _e('Save Changes', 'ProFoundMLS') ?>" /></div>
							</div>
			</div>
			<div id="notes">
				<hr />
				<h3>Notes :</h3>
						<p>If you don't know what you're doing,don't mess with the META data</p>
			</div>
			</form>
		</div>

		</div>
		<div class="clear"></div><?php
		*/
	}

	/**
	 * Show the Category/Neigborhood page and update the settings if necessary
	 */
	public function adminCategoryNeighborhood() {
		$this->updateCategoryOptions();
		$this->renderCategorySettings();

		#$desc = "The Category/Neighborhood Configuration creates a grid layout within the Category page.";
		$this->renderCustomSEOform("neighborhood", "Category/Neighborhood");
	}

	private function updateSearchResultsOptions() {
		$devOptions = $this->getAdminOptions();
		if (isset($_POST['update_ProFoundMLSSettings'])) {

			if (isset($_POST['search_result_page_title'])) {
				$search_result_page_title = $_POST['search_result_page_title'];
				$devOptions['search_result_page_title'] = $search_result_page_title;
			}

			if (isset($_POST['search_result_page_h1'])) {
				$search_result_page_h1 = $_POST['search_result_page_h1'];
				$devOptions['search_result_page_h1'] = $search_result_page_h1;
			}

			if (isset($_POST['search_result_page_description'])) {
				$search_result_page_description = $_POST['search_result_page_description'];
				$devOptions['search_result_page_description'] = $search_result_page_description;
			}

			if (isset($_POST['search_result_page_keywords'])) {
				$search_result_page_keywords = $_POST['search_result_page_keywords'];
				$devOptions['search_result_page_keywords'] = $search_result_page_keywords;
			}

			$request_meta = $_POST['request_meta'];
			$devOptions['request_meta'] = $request_meta;


			update_option($this->adminOptionsName, $devOptions);
			$this->showUpdateMessage();
		}
	}

	private function updateSEOsettings($key) {
	}


	public function adminSearchResults() {
		$this->updateSearchResultsOptions();
		$this->renderCustomSEOform("search_results", "Search Results");
	}

	/**
	 * Update the city list used by the City Search widget and City List shortcode
	 */
	private function updateCityOptions() {
		$devOptions = $this->getAdminOptions();
		if (isset($_POST['update_ProFoundMLSSettings'])) {
			if (isset($_POST['city_list'])) {
				$all_city = $_POST['city_list'];
				$all_city = array_unique($all_city);

				$devOptions['city_list'] = $all_city;
			} else {
				$devOptions['city_list'] = null;
			}

			update_option($this->adminOptionsName, $devOptions);
			$this->showUpdateMessage();
		}
	}

	/**
	 * Show the admin page for the City Configuration and save the settings if necessary
	 */
	public function adminCityConfiguration() {
		$this->updateCityOptions();

		$devOptions = $this->getAdminOptions();
		$form_data = $devOptions['form_data'];
		$opts_array = json_decode($form_data);
		$all_city = $devOptions['city_list'];
		$delete_icon = WP_PLUGIN_URL . '/profoundmls/images/gnome_edit_delete.png';

		?>
		<div class="wrap">
			<div id="city">
				<form method="post" action="<?= $_SERVER["REQUEST_URI"]; ?>">
					<h2 class="head-city-list">City List Configuration</h2>
					<p>
						The City List Configuration drives the available cities list with in
						the City Search Widget as well as the City Search shortcode.
					</p>
					<div id="list-all-city">
					<?php
					?>
						<select name="city_list[]" id="city-list" multiple="" size="5">
						<?php if (($opts_array->city)): ?>
							<?php foreach($opts_array->city as $result): ?>
							<option value="<?= $result[0] ?>"><?= $result[0] ?></option>
							<?php endforeach ?>
						<?php endif ?>
						</select>
					</div>
					<div id="city-visible">
						<?php  if($all_city): ?>
						<ul>
							<?php foreach($all_city as $city): ?>
							<li>
								<span><?= ($city); ?> </span>
								<input type="hidden" name="city_list[]" value="<?= $city; ?>" />
								<img class="delete-item" alt="Delete" src="<?= $delete_icon; ?>" />
							</li>
							<?php endforeach ?>
						</ul>
						<script type="text/javascript">
							jQuery('.delete-item').live('click',function() {
								jQuery(this).parent().remove();
								return false;
							});
						</script>
						<?php endif ?>
						<p>Select cities from the list on the left to added them to the list <br />
							Control+Click to select multiple(Option+Click on mac).</p>
						<div class="submit">
							<input type="submit" name="update_ProFoundMLSSettings" value="<?php _e('Save Changes', 'ProFoundMLS') ?>" /></div>
						</div>
				</form>
			</div>
		</div><?php
	}

	/**
	 * Add the stylesheet for the WP Admin pages to the list of stylesheets to be linked
	 */
	public function adminInit() {
		# FIXME: need to display a Warning notice if permalinks are not enabled

		$myStyleUrl = WP_PLUGIN_URL . '/profoundmls/css/default.css';
		$myStyleFile = WP_PLUGIN_DIR . '/profoundmls/css/default.css';
		if ( file_exists($myStyleFile) ) {
			wp_register_style('profoundmlsStyleSheets', $myStyleUrl);
			wp_enqueue_style( 'profoundmlsStyleSheets');
		}
	}

	/**
	 * Display a message on the WP Admin pages when settings have been saved
	 */
	private function showUpdateMessage() {
		?><div class="updated">
			<p>
				<strong><?php _e("Settings Updated.", "ProFoundMLS");?> </strong>
			</p>
		</div><?php
	}

	/**
	 * Display & update the Settings admin page
	 *
	 * This page is used to set & update the Pro-Found MLS account ID & API key
	 */
	public function adminSettings() {
		$devOptions = $this->getAdminOptions();


		$inputs = array(
			array(
				'id' => 'api_key',
				'label' => 'Account ID',
				'info' => ''
			),
			array(
				'id' => 'api_secret',
				'label' => 'API Key',
				'info' => 'This information must be entered exactly as it appears in your welcome letter settings',
			),
			array(
				'id' => 'main_reg_form_id',
				'label' => 'Main Registration Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for <b>"Registration"</b> on main site pages after 3 visits.
				           Leaving this field blank will disable the registration dialog.'
			),

			array(
				'id' => 'reg_form_id',
				'label' => 'Registration Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for <b>"Registration"</b> on new searches and after 2 visits to the Search Results or Property Details pages.
				           Leaving this field blank will disable the registration dialog.'
			),
			array(
				'id' => 'help_form_id',
				'label' => 'Need Help Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for
				           <b>"Need Help? Questions"</b> call-to-action button on the Property Details page.
				           Leaving this field blank will disable the display of the button.'
			),
			array(
				'id' => 'seenow_form_id',
				'label' => 'See This House Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for
				           <b>"See This House TODAY!"</b> call-to-action button on the Property Details page.
				           Leaving this field blank will disable the display of the button.'
			),
			array(
				'id' => 'feat_listings',
				'label' => 'Featured Listings MLS IDs',
				'info' => 'Enter a list of MLS IDs separated by commas and/or spaces, to be displayed in the Featured Listings widget',
			),
			array(
				'id' => 'connect_with_loan_officer_form_id',
				'label' => 'Talk to Financing Expert Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for
				           <b>"Talk to a financing expert about pre qualifying for this home"</b> call-to-action link on the Property Details page\'s mortgage calculator.
				           Leaving this field blank will disable the display of the link.'
			),
			array(
				'id' => 'connect_with_real_estate_agent_form_id',
				'label' => 'Connect to Real Estate Agent Form ID',
				'info' => 'Enter the ID for the form created with Gravity Forms to be used for
				           <b>"Speak to an agent about this home"</b> call-to-action link on the Property Details page\'s mortgage calculator.
				           Leaving this field blank will disable the display of the link.'
			),
			array(
				'id' => 'craigstlist_visitor_form_id',
				'label' => 'Craigslist Visitor Form ID',
				'info' => 'Enter the ID for the Gravity Form to be displayed if a vistor arrives from Craigslist.  Leaving this field blank will disable the display of the form'
			),
		);

		# TODO: need to validate this data, use form library (?) and/or change to foreach loop
		$fields = array();
		foreach ($inputs as $input) {
			$fields[] = $input['id'];
		}
		$fields[] = 'no_temp_cookie';

		if (isset($_POST['update_ProFoundMLSSettings'])) {

			// Clear the featured listings cache if the value has changed
			if ($devOptions['feat_listings'] != $_POST['feat_listings']) {
				unset($devOptions['fl_cache']);
			}

			foreach ($fields as $fname) {
				if (isset($_POST[$fname])) {
					$devOptions[$fname] = $_POST[$fname];
				} else {
					$devOptions[$fname] = false;
				}
			}
			update_option($this->adminOptionsName, $devOptions);
			$this->showUpdateMessage();
		}

		?>
		<?php # TODO: move the inline CSS to a stylesheet? ?>
		<style>
			#reg_form_id, #help_form_id, #seenow_form_id, #main_reg_form_id, #craigstlist_visitor_form_id,
			#connect_with_loan_officer_form_id, #connect_with_real_estate_agent_form_id
			 {
				width: 30px;
			}
			#feat_listings { width: 400px; }
		</style>
		<div class=wrap>
			<form method="post" action="<?= $_SERVER["REQUEST_URI"]; ?>">
				<h2>Pro-Found MLS Settings</h2>
				<?php foreach ($inputs as $input): ?>
					<p>
						<label for="<?= $input['id'] ?>"><b><?= $input['label'] ?>: </b></label>
						<input type="text" name="<?= $input['id'] ?>" id="<?= $input['id'] ?>"
						       value="<?= $devOptions[$input['id']] ?>"><br>
						<span><?= $input['info'] ?></span>
					</p>
				<?php endforeach ?>
					<p>
						<input type="checkbox" name="no_temp_cookie" id="no_temp_cookie" <?= ($devOptions['no_temp_cookie']) ? 'checked' : '' ?>>
						<label for="no_temp_cookie"><b>DO NOT</b> set a one-day cookie if <b>Not Now</b> button is pressed</label>
					</p>

				<div class="submit">
					<input type="submit" name="update_ProFoundMLSSettings" value="<?php _e('Save Changes', 'ProFoundMLS') ?>" />
				</div>
			</form>
		</div>

		<hr>
		<h3>Notes</h3>
		<p>
			To utilize the Pro-Found MLS Plug-in you must subscribe to the
			Pro-Found IDX Service <br /> and validate your WordPress Site with
			valid account information. <br /> Please contact Pro-Found Marketing at
			(480) 226-3020 to get started.
		</p><?php
	}

	/**
	 * Strip out $ and , from the category name
	 * @param $name Raw category name
	 * @return mixed Cleaned version of category name
	 */
	private function _cleanCatName($name) {
		return preg_replace('/[,$-\s]+/', ' ', $name);
	}

	/**
	 * Extract the Category keywords and optional city from the category search URL fragment
	 *
	 * @param string $catid URL fragment for the category search
	 * @return array Search query string for category, City, Category Name
	 */
	public function getKeywordsForCat($catid) {
		$option = get_option($this->adminOptionsName);
		$city = null;

		// Extract any sorting modifiers from the URL
		$sort = null;
		foreach ($this->sort_mods as $str => $val) {
			$catid = preg_replace("/-$str\$/", '', $catid, -1, $count);
			if ($count) {
				$sort = array($str, $val[0]);
			}
		}

		// Replace - with space to get original category name in DB
		$catname = preg_replace('/-/', ' ', $catid);

		if (empty($option['category_name'])) return;

		foreach ($option['category_name'] as $index => $name) {
			// Check for string without any city
			$name2 = str_replace('-', ' ', strtolower($name));
			if ($catname == $name2) {
				return array($option['search_query'][$index], null, $name, $option['category_desc'][$index], $sort);
			}

			// Check the string w/ and without the city
			$regexps = array(
				preg_replace('/{city}/i', '(.*)', $name),
			);
			# FIXME: could probably use a callback here to clean this up a bit
			// Generate a regexp for multiple cities
			if (preg_match('/{(.*?)}/', $name, $m) && $m[1] != 'city') {
				$str = '(' . implode('|', explode(',', $m[1])) . ')';
				$regexps[] = preg_replace('/{.*?}/i', $str, $name);
			}

			foreach ($regexps as $regex) {

				$regex = $this->_cleanCatName($regex);
				#echo "R: $regex : $catname\n";

				// Check if we matches a category, and if so, extract the keywords and city
				if (preg_match("/^$regex$/i", $catname, $matches)) {
					if (isset($matches[1])) {
						$city = ucwords($matches[1]);
					}
					return array($option['search_query'][$index], $city, $name, $option['category_desc'][$index], $sort);
				}
			}
		}
		# TODO: what to do if we don't find it ...
	}

	public function getIDXWrapper() {
		if (!isset($this->IDXWrapper)) {
			$devOptions = get_option($this->adminOptionsName);
			require_once('ProFoundIDX.php');
			return new ProFoundIDX($devOptions['api_key'], $devOptions['api_secret']);
		}

		return $this->IDXWrapper;
	}

	/**
	 * Build the property search query from the GET parameters and query the IDX
	 *
	 * Save the data return by the IDX for later use by the page header, dynamic pages,
	 * and Google Maps data.
	 */
	# TODO: this method should be private.  changed for testing purposes.
	public function getSearchResults() {
		global $wp;

		# TODO: no need to fetch/set this more than once.  set as object variable on plugin init.
		$option = get_option($this->adminOptionsName);
		$form_data = json_decode($option['form_data']);

		$q = $wp->query_vars;

		# TODO: this URL cleanup is kind of hacky.  clean it up if possible.
		#  merge with code that creates the URL rewrite rules

		// Send URL to IDX so it can fill in the paging information - clean old paging stuff from the URL
		$regexp = array('/\/$/', '/-page-\d+\/?$/', '/&?pp=\d+/');
		# TODO: also hacky, but works. BugzID 125 (pagination issue in a subdirectory)
		if($_SERVER['HTTPS'] == "on") {
			$protocol = "https://";
		} else {
			$protocol = "http://";
		}

		$q['p_url'] = $protocol . $_SERVER['HTTP_HOST'] . preg_replace($regexp, '', $_SERVER['REQUEST_URI']);

		if (!isset($q['pp']))
			$q['pp'] = 1;

		// Encode the features in to an array for the IDX request
		$feats = array();
		if (isset($form_data->features)) {
			foreach ($form_data->features as $fname) {
				$fname2 = preg_replace('/ /', '_', $fname);
				if (isset($_REQUEST[$fname2]) && $_REQUEST[$fname2] == 'on') {
					$feats[] = $fname;
				}
			}
			$q['features'] = serialize($feats);
		}

		// Don't send empty query variables
		foreach ($q as $key => $val) {
			if (trim($val) == '') unset($q[$key]);
		}

		$api = $this->getIDXWrapper();

		// If it was a category search, look up the keywords for the category, and use those
		if (isset($q['pfmls_cat'])) {
			list($q['words'], $city_name, $cat_name, $desc, $sort) = $this->getKeywordsForCat($q['pfmls_cat']);
			$this->cat_search = true;
			$this->current_sort = $sort[0];

			// Save the Category/Neighborhood description for later
			$this->resultsInfo = stripslashes($desc);

			// Lookup the city ID from the name
			if ($city_name) {
				$q['city'] = $this->getCityId($city_name);
			}

			// Custom sort order
			if ($sort) {
				$q['sort'] = $sort[1];
			}

			$q['cat_name'] = preg_replace('/{.*?}/', $city_name, $cat_name); #ucwords(preg_replace('/-/', ' ', $q['pfmls_cat']));

			// Override the paging URL for search results from a category
			$q['p_url'] .= '-page-%s/';
		} else {
			$q['p_url'] .= '&pp=%s';
		}
		#print_r($q);

		if ($q['pricerange']) {
			list($low, $high) = explode(',', $q['pricerange']);
			if ($q['words']) $q['words'] .= ";";
			$q['words'] .= "LIST_22>$low;LIST_22<$high";
		}

		if ($q['schooldistrict']) {
			$schooldistrict = $q['schooldistrict'];
			if ($q['words']) $q['words'] .= ";";
			$q['words'] .= "LIST_111=$schooldistrict";
		}

		$this->searchQueryData = $q;

		$this->mapdata = $this->searchData = $api->search($q);

		# TODO: fix this SEO meta override stuff
		#if($request_meta) {
		if (isset($this->searchData['authorized']) && $this->searchData['authorized'] === 0) {
			# FIXME: move this to a method
			update_option('pfmls_disable_idx', 1);
			$this->account_disabled = true;
		} else {
			$this->setLastUpdated($this->searchData);

			$seo = $this->searchData['seo'];

			$this->site_title = $seo['title'];
			$this->site_h1 = $seo['h1'];
			$this->site_description = $seo['description'];
			$this->site_keywords = $seo['keywords'];
		}
		#}
	}

	protected function setLastUpdated($data) {
		update_option('pfmls_last_updated', $data['last_updated']);
	}

	/**
	 * Construct an address using the fields from the IDX for the given array
	 *
	 * @param array $result Array of data
	 * @return string Complete address
	 */
	private function getAddress($result) {
		// Construct the address
		return implode(' ', array(
									   $result['StreetNumber'],
									   $result['StreetDirPrefix'],
									   $result['StreetName'],
									   $result['UnitNumber'],
									   $result['StreetSuffix'],
									   $result['City'],
									   $result['State'],
									   $result['PostalCode']));
	}

	# TODO: should store the cities on the plugin side so that they are keyed by city name
	/**
	 * Look up the IDX-friendly ID for the given city name
	 *
	 * @param string $city_name Name of the city to lookup
	 * @return string IDX-friendly city ID
	 */
	private function getCityId($city_name) {
		$opts = get_option($this->adminOptionsName);
		$form_data = json_decode($opts['form_data']);

		foreach ($form_data->city as $id => $city) {
			if ($city_name == $city[0]) return $id;
		}
		return false;
	}

	private function showDisabledNotice() {
		?>
		<div class="pfmls-idx-disabled">
			<p>
		The service that provides the particular home search page, or home for sale detail page
		that you were trying to find has been temporarily suspended. We expect to have this situation
		remedied shortly. </p>
			<p>Please continue to browse this site and/or check back frequently. We
		apologize for this inconvenience. Thanks.</p>
		</div>
		<?
	}

	/**
	 * Show a message if no results were found, with a link to start a new search.
	 */
	protected function showNoResults() {
		?><div class="pfmls-no-results-msg"><?php
		if ($this->cat_search) {
			$this->showNoResultsForCategory();
		} else {
			?>
				Oops.  There were no homes for sale found matching your search criteria.<br><br>
				If you want to continue looking at homes for sale, you can start a new search here: <a href="/property-search/">Start new home search</a>
			<?php
		}
		?>		
		</div>
		<?php
	}

	protected function getSearchResultsTotal() {
		return $this->searchData['paging']['total'];
	}

	/*
	 * Rather than simply show a 'sorry there were no results' type
	 * of message, we'll show a more helpful page allowing the user
	 * to more easily see homes similar to what they were hoping to 
	 * see from the given category. At this time, that means we'll 
	 * show them the latest listings from the city that was part of
	 * the category, if there was a city. Otherwise we'll just show
	 * them the latest listings from the MLS.
	 */
	protected function showNoResultsForCategory() {
		$q = array();
		$area = "area";

		// TODO: This is another implementation that only works for armls.
		// This will break for TREND. We need to fix this by adding the concept
		// of field-mapping on the plugin side.

		// Do we have a city mentioned in the category filter?
		preg_match("/LIST_39:([^;]*)/", $this->searchQueryData['words'], $matches);
		$city_name = isset($matches[1]) ? $matches[1] : "";

		if ($city_name) {
			$area = "city where you were looking";
			$q['words'] = "LIST_39:$city_name";
		}

		?>
		Currently, there are no homes for sale in the category that you are looking for. However, in an effort to serve you better below is a list of the latest listings in the <?= $area ?>. You can also personalize your search here: <a href="<?= home_url() . '/' . ProFoundMLS::getPageUrl('advsearch') ?>">Start home search</a>.
		<br/><br/>
		<?php

		$q['latest'] = "true";

		$listings = $this->getIDXWrapper()->search($q);

		$this->renderSearchResultListings($listings);
	}

	/**
	 * Render the content for the Search Results dynamic page
	 */
	public function pageSearch() {
		if ($this->account_disabled) {
			$this->showDisabledNotice();
			return;
		}
		$prop_array = $this->searchData;

		// No results
		if ($this->getSearchResultsTotal() == 0) {
			return $this->showNoResults();
		}

		// Paging information
		$paging = $prop_array['paging'];

		// If this page is showing search results for a Category/Neighborhood, show the summary
		if (isset($this->resultsInfo)) {
			?><p class="pfmls-cat-info"><?= $this->resultsInfo ?></p><?
		}

		if (is_array($paging)) {
			unset($prop_array['paging']);
			?><p>Displaying <?= $paging['status'] ?> results<br><?= $paging['paging'] ?></p><?php
		}

		# FIXME: only show if category page?
		#if (isset($))

		?>
		<div class="pfmls-sort-select">
			<label>Sort By:</label>
			<select onChange="updateSortOrder(this, '<?= $this->current_sort ?>')">
				<?php foreach ($this->sort_mods as $url => $data):
					$selected = preg_match("/-$url(-page-\d+)?\//", $_SERVER['REQUEST_URI']);
				?>
				<option <? if ($selected) echo "selected "?> value="<?= $url ?>"><?= $data[1]?></option>
			<?php endforeach ?>
			</select>
		</div>
		<?php

		$this->renderMapContainer();

		$this->renderSearchResultListings($prop_array);

		if (is_array($paging)) {
			?><p><?= $paging['paging'] ?></p><?php
		}
	}

	protected function renderSearchResultListings($listings) {
		$options = $this->getAdminOptions();
		$acct = json_decode($options['account']);

		?>
			<div id="property-wrapper">
				<?php foreach($listings as $id => $result): ?>
				<?php
					# TODO: should move all of the properties in to their own array in the IDX response
					// Skip if this isn't a property
					if (!is_array($result) || !isset($result['ListingID'])) continue;
				?>
				<div id="data_<?= $id ?>" class="pfmls_property">
					<strong><a href="<?= home_url() ?>/<?=$result['uri'];?>"><?=$result['prop_h2'];?></a></strong>
					<?php if (isset($result['img_url'])): ?>
					<a href="<?= home_url() ?>/<?= $result['uri'] ?>">
						<img class="pfmls_prop_img" width="190" src="<?= $result['img_url'] ?>">
					</a>
					<?php endif ?>
					<div class="pfmls_prop_data">
						<?php if ($acct->mls == 'armls'): ?>
						<img class="pfmls-armls-small" alt="ARMLS Logo" src="<?= site_url() ?>/wp-content/plugins/profoundmls/images/ARMLS_logo_small.jpg" />
						<?php endif ?>
						<?=$result['prop_content']?>
					</div>
					<div class="clear"></div>
					<hr>
				</div>
				<?php endforeach ?>
			</div>
		<?php
	}

	/**
	 * Render an iframe to contain the disclosures HTML page
	 */
	public function addDisclosure() {
		$options = $this->getAdminOptions();
		$acct = json_decode($options['account']);

		$height = ($acct->mls == 'armls') ? 250 : 510;


		if (in_array($this->_pfmlsPage, array('search_results', 'property_detail')) && $this->searchData != "No Results") {
			?>
			<iframe id="pfmls_disclosure" style="height: <?= $height ?>px; width: 650px;" src="<?= home_url() ?>/pfmls-disclosure"></iframe>
			<?php
		}
	}

	/**
	 * Fetch the details from the IDX on the property specified in the URL
	 */
	private function getPropertyDetails() {
		global $wp;
		$listing_id = $wp->query_vars['listing_id'];

		$devOptions = $this->getAdminOptions();
		require_once('ProFoundIDX.php');
		$api = new ProFoundIDX($devOptions['api_key'], $devOptions['api_secret']);
		$data['q'] = $listing_id;
		$this->data = $prop_array = $api->getDetail($data);

		// If the listing has expired, redirect to the expired listings page
		if ($prop_array['expired']) {
			$url = $this->getPageURL('unavailable');
			// Previous we return a 301. For the moment, we'll change this to a 302 in case we want the benefit of SEO.
			header("HTTP/1.1 302 Found");
			header('Location: /' . $url );
			exit;
		}

		# TODO: need to clean this up & remove extra class variables.  Data returned from IDX needs structured better too.
		$this->mapdata = array($this->data);

		$this->address = $this->getAddress($this->data);

		$this->setLastUpdated($this->data);

		$request_meta = $devOptions['detail_request_meta'];
		#if($request_meta) {
			$seo = $prop_array['seo'];
			$this->site_title = $seo['title'];
			$this->site_h1 = $seo['h1'];
			$this->site_description = $seo['description'];
			$this->site_keywords = $seo['keywords'];
		#}
	}

	/**
	 * Render a section of the Property Details, showing the label and value
	 *
	 * @param string $title Title for the section to show in the H4 tag
	 * @param string $list Key indicating which list of details to show for the section
	 */
	private function renderDetailList($title, $list) {
		$key_map = array(
			'general' => array(
				'Status' => 'ListingStatus',
				'Special Listing Condition' => 'AdditionalSaleTerms',
				'Listing Updated' => 'ModificationTimestamp',
				'Floors' => 'UnitFloors',
				'List Price' => 'ListPrice',
				'Square Feet' => 'SquareFeet',
				'Bedrooms' => 'Beds',
				'Bathrooms' => 'Bathrooms',
				'Full Baths' => 'BathsFull',
				'Half Baths' => 'BathsHalf',
				'Levels' => 'Design',
				'Year Built' => 'YearBuilt',
				'Property Age (years)' => 'PropertyAge',
				'Builder Name' => 'BuilderName',
				'Marketing Name' => 'MarketingName',
				'Model' => 'Model',
				'Lot Size (sq feet)' => 'LotSize',
				'Lot Size (Acres)' => 'LotAreaAcre',
				'County' => 'County',
				'Taxes' => 'TotalTaxes',
				'Tax Year' => 'TaxYear',
			),

			'school' => array(
				'Elementary School' => 'ElementarySchool',
				'Junior High School' => 'MiddleSchool',
				'High School' => 'HighSchool',
				'District' => 'SchoolDistrict',
			),

			'community' => array(
				'Subdivision' => 'Subdivision',
				'Planned Community' => 'GF20080207210325432062000000',
				'Name' => 'FEAT20070914205338315648000000',
				#'Home Owners' => 'LIST_96',
				#'Associations' => 'FEAT20070914205202933704000000',
				#'HOA Dues' => 'FEAT20070914205229919954000000',
				#'HOA Rules' => 'GF20070914134415075069000000',
				#'HOA Fees Include' => 'GF20070914134345186288000000',
			),

			'features' => array(
				'Interior Features' => 'InteriorFeatures',
				'Exterior Features' => 'ExteriorFeatures',
				'Kitchen Features' => 'Appliances',
				'Dining Area' => 'DiningKitchen',
				'Master Bath Features' => 'MasterBathFeatures',
				'Main Bedroom Features' => 'MainBedroom',
				'Additional Bedroom Features' => 'ExtraBedFeatures',
				'Other rooms' => 'RoomList',
				'Additional Room' => 'AdditionalRoom',
				'Flooring' => 'Floor',
				'Laundry' => 'LaundryType',
				'Energy Features' => 'Certifications',
				'Technology' => 'Technology',
				'Fireplaces' => 'FireplaceCount',
				'Fireplace Features' => 'FireplaceFeatures',
				'Cooling' => 'Cooling',
				'Heating' => 'Heating',
				'Central Air' => 'CentralAir',
				'Hot Water' => 'HotWater',
				'Cooking Fuel' => 'CookingFuel',
				'Utilities' => 'Utilities',
				'Pool' => 'PoolType',
				'Spa' => 'Spa',
				'Construction Finish' => 'Exterior',
				'Roofing' => 'Roof',
				'Porch / Deck' => 'PorchDeck',
				'Basement' => 'BasementType',
				'Attics' => 'Attics',
			),

			'lot' => array(
				'Property Description' => 'PropDescription',
				'Landscaping' => 'Landscaping',
				'Water' => 'Water',
				'Sewer, Public' => 'SewerSeptic',
				'Horse' => 'Horse',
				'Slab Parking Spaces' => 'SlabParkingSpaces',
				'Carport Spaces' => 'CarportSpaces',
				'Garage Spaces' => 'GarageSpaces',
				'Garage Type' => 'Garage Type',
				'Fencing' => 'Fencing',
				'Structures' => 'StructureList',
				'Waterfront' => 'WaterFront',
				'Lot Description' => 'LotDescription',
				'Lot Dimensions' => 'LotDimensions',
				'Lot Area (Acres)' => 'LotAreaAcre',
			),

			'demographics' => array(
				'Population',
				'Households',
				'White Population',
				'Black Population',
				'Hispanic Population',
				'Asian Population',
				'Hawaiian Population',
				'Indian Population',
				'Other Population',
				#'Male Population',
				#'Female Population',
				'Persons Per Household',
				'Average House Value',
				'Income Per Household',
				'Median Age',
				'Median Age Male',
				'Median Age Female',
			),

			'listing_info' => array(
				'Listing Broker' => 'ListOfficeName'
			)
		);

		$prop_array = $this->data;

		?>
							<h4><?= $title ?></h4>
							<table>
								<?php foreach ($key_map[$list] as $label => $key): ?>
								<?php
									if (!array_key_exists($key, $prop_array)) continue;
									$val = $prop_array[$key];

									// Don't display fields that are blank
									if (!$val) continue;

									// Special case for demographics for now
									if ($list == 'demographics') {
										$label = $key;
										$key = str_replace(' ', '', $label);
										if ($label == 'Households') $key = 'HouseholdsPerZipCode';
										$val = $prop_array['demographics'][$key];
									}
								?>
								<tr><th><?= $label ?></th><td><?= $val ?></td></tr>
								<?php endforeach ?>
							</table><?php
	}

	/**
	 * Render a set of sections of the Property Details page
	 *
	 * @param string $label Label to use for the DIV id attribute
	 * @param array $sections Sections to render (title => key)
	 */
	private function renderDetailSections($label, $sections) {
		?>
						<div id="props-<?= $label ?>">
							<?foreach ($sections as $title => $listid):
								// Skip Demographics section if data is empty
								if ($listid == 'demographics' &&
									(empty($this->data[$listid]) || $this->data[$listid]['Population'] == "0"))
										continue; ?>
							<?= $this->renderDetailList($title, $listid); ?>
							<?endforeach ?>
						</div><?
	}

	/**
	 * Show the Property Details page
	 */
	public function pagePropertyDetail() {
		if ($this->account_disabled) {
			$this->showDisabledNotice();
			return;
		}
		$prop_array = $this->data;

		$main = array(
			'List Price' => 'ListPrice',
			'MLS #' => 'ListingID',
			'Bedrooms' => 'Beds',
			'Bathrooms' => 'Bathrooms',
			'Full Baths' => 'BathsFull',
			'Half Baths' => 'BathsHalf',
			'Sq Feet' => 'SquareFeet',
			'HOA' => 'HOA',
			#'City, Zip' => array('City', 'PostalCode'),
			'City' => 'City',
			'Zip' => 'PostalCode',
			'Sub Division' => 'Subdivision',
		);

		$left = array(
			"General Information" => 'general',
			"School Information" => 'school',
			"Community Information" => 'community',
			"Lot Information" => 'lot',
			"Demographic Information by Zip Code" => 'demographics',
		);
		$right = array(
			"Features" => 'features',
			"Broker Info" => 'listing_info'
		);

		#if (!$prop_array || !is_array($prop_array['images'])) return;

		?>
			<div id="property-detail">
				<div id="property-main-features">
					<?php foreach ($main as $label => $key):

						// Set the value to be used
						if (is_array($key)) {
							$stuff = array();
							foreach ($key as $id) $stuff[] = $prop_array[$id];
							$val = implode(', ', $stuff);
						} else {
							$val = isset($prop_array[$key]) ? $prop_array[$key] : null;
						}
					?>
					<?php if ($val): ?>
					<strong><?= $label ?></strong>: <?= $val ?><br/>
					<?php endif ?>
					<?php endforeach ?>
				</div>

				<?php $this->addImageGallery(); ?>

				<div id="property-subfeature">
					<?php if ($prop_array['Contingent'] == 'UCB (Under Contract-Backups)'): ?>
					<h4 class="pfmls_ucb">
						This property is Under Contract accepting Backup offers. To start a new search please click:
						<a href="<?= home_url() . '/' . ProFoundMLS::getPageUrl('advsearch') ?>">Start New Search</a>
					</h4>
					<?php endif ?>
					<h4>Property Description</h4>
					<p><?= $prop_array['Remarks'] ?></p>
					<h4>Cross-streets</h4>
					<p><?= $prop_array['CrossStreet'] ? $prop_array['CrossStreet'] : "<span class='data-not-entered'>Cross street information not entered</span>" ?></p>
					<h4>Directions</h4>
					<p><?= $prop_array['Directions'] ? $prop_array['Directions'] : "<span class='data-not-entered'>Directions not entered</span>" ?></p>
				</div>

				<div id="props-general-info">
					<?php $this->renderDetailSections("left", $left); ?>
					<?php $this->renderDetailSections("right", $right); ?>
				</div>


				<h4>Map : <?= $this->address ?></h4>
				<?php $this->renderMapContainer(); ?>
			</div>
		<?php
	}

	/**
	 * Render the HTML for the Property Details image gallery
	 */
	private function addImageGallery() {
		$prop_array = $this->data;
		?>
		<div class="pikachoose">
			<ul id="pikame" class="jcarousel-skin-pika">
		<?php if(is_array($prop_array['images'])): ?>
			<?php foreach($prop_array['images'] as $id=>$result): ?>
				<li><img src="<?=$result['img_url'];?>"/><span><?=$result['description'];?></span></li>
			<?php endforeach ?>
		<?php endif ?>
			</ul>
		</div><?php
	}

	/**
	 * Render the HTML to be included in the page HEAD
	 *
	 * Add the meta description and keywords, Google Maps property data, JavaScript initialization
	 * function, and the plugin CSS stylesheets.
	 *
	 * Define a JavaScript variable for the Gravity Forms registration form URL, if the option was set in WP Admin.
	 */
	public function addHeaderCode() {
		$devOptions = $this->getAdminOptions();

		$regid = isset($devOptions['reg_form_id']) && $devOptions['reg_form_id'] ? $devOptions['reg_form_id'] : 0;
		$regid2 = isset($devOptions['main_reg_form_id']) && $devOptions['main_reg_form_id'] ? $devOptions['main_reg_form_id'] : 0;

		$cl_form_id = isset($devOptions['craigstlist_visitor_form_id']) && preg_match('/craigslist\.org/', $_SERVER['HTTP_REFERER'])
			? $devOptions['craigstlist_visitor_form_id']
			: 0;

		// Determine which registration form & cookie to use for this page
		if ($this->_pfmlsPage && in_array($this->_pfmlsPage, array('search_results', 'property_detail'))) {
			$which = 'pf';
		} else if (!$this->_pfmlsPage) {
			$which = 'main';
		} else {
			$which = 'none';
		}

		$pluginurl = parse_url(WP_PLUGIN_URL);

		$css_files = array('front', 'right');
		?>
		<?php foreach ($css_files as $fname): ?>
		<link rel="stylesheet" type="text/css" media="all" href="<?= plugins_url() . '/profoundmls/css/' . $fname . '.css' ?>"/>
		<?php endforeach ?>
		<?php if($this->site_description): ?>
			<meta name="description" content="<?= $this->site_description ?>" />
		<?php endif ?>
		<?php if($this->site_keywords): ?>
			<meta name="keywords" content="<?= $this->site_keywords ?>" />
		<?php endif ?>
		<?php // Fix for issue: http://stackoverflow.com/questions/658238/debug-message-resource-interpreted-as-other-but-transferred-with-mime-type-appli ?>
		<meta http-equiv="content-script-type" content="text/javascript" />
		<script type="text/javascript">
			var pfmls_cl_form_id = <?= $cl_form_id ?>;
			var pfmls_reg_form_id = <?= $regid ?>;
			var main_reg_form_id = <?= $regid2 ?>;
			var no_temp_cookie = <?= ($devOptions['no_temp_cookie'] ? 'true' : 'false') ?>;
			var which_reg = '<?= $which ?>';
			var gform_reg_url = '<?= $pluginurl['path'] . '/profoundmls/gravity_form_display.php?id=' ?>';
		</script>
		<?php

		// Include the JSON for the Property Map
		if (!empty($this->mapdata) && $this->mapdata != "No Results") {
			$this->addMapData();
		}

		switch ($this->_pfmlsPage) {
			case 'search_results':
				$this->jsInit("initSearchResults");
				break;
			case 'property_detail':
				$this->jsInit("initPropDetails");
				break;
		}
	}

	/**
	 * Add the <script> block to the header to initialize the page with the given Javascript function
	 *
	 * @param string $funcname Name of function to call when the document is loaded
	 */
	private function jsInit($funcname) {
		# TODO: need to add CDATA comments in here?
		?><script type="text/javascript">
			// Add callback to setup everything after page is loaded
			var wp_site_url = '<?= home_url() ?>';
			jQuery(document).ready(<?= $funcname ?>);
		</script><?php
	}

	/**
	 * Render the HTML for the Advanced Search dynamic page
	 *
	 * The Advanced Search contains select boxes for common features, and checkboxes for
	 * additional features that will generate keywords for a search.
	 */
	public function pageAdvancedSearch() { ?>
		<?php
		$option = get_option($this->adminOptionsName);
		$devOptions = $this->getAdminOptions();
		$form_data = $devOptions['form_data'];
		$opts_array = json_decode($form_data);

		$fields = array(
			'proptype' => array('Property Type', 'Any Type'),
			'mlsnum' => 'MLS Number',
			'city' => 'City',
			'price' => 'Price',
			'zipcode' => 'ZIP Code',
			'sqft' => 'Sq Feet',
			#'yrbuilt' => 'Year Built',
			'bedroom' => 'Bedrooms',
			'garage' => 'Garage',
			'bathroom' => 'Bathrooms',
		);

		$i = 0;		// Count for fields, used for splitting table in to columns

		// Select boxes
		$regid = isset($devOptions['reg_form_id']) ? $devOptions['reg_form_id'] : 0;
		?>
		<form id="pfmls_adv_search_form" action="<?= home_url() ?>/<?= ProFoundMLS::getPageURL("search_results"); ?>/" method="get" onSubmit="return showRegForm(this, <?= $regid ?>);">
			<table id="pfmls-adv-search-main">
			<?php foreach ($fields as $name => $data):
						// Set the label
						if (is_array($data)) {
							list($label, $any) = $data;
						} else {
							$label = $data;
							$any = "Any $label";
						}
				?>
				<?php if ($i % 2 == 0): ?>
				<tr>
				<?php endif ?>
					<td>
						<label for="<?= $name ?>_id"><?= $label ?>: </label>
						<?php if(($opts_array->$name)): ?>
						<select id="<?= $name ?>_id" name="<?= $name ?>">
							<option value=""><?= $any ?></option>
							<?php foreach($opts_array->$name as $key => $val): ?>
							<?php if (isset($val->LongValue)): ?>
							<option value="<?= $val->Value ?>"><?= $val->LongValue ?></option>
							<?php else: ?>
							<option value="<?= $key ?>"><?= $val[0] ?></option>
							<?php endif ?>
							<?php endforeach ?>
						</select>
						<?php else: ?>
						<input type="text" name="<?= $name ?>" id="<?= $name ?>_id" value="" />
						<?php endif ?>
					</td>
				<?php if ($i % 2 != 0): ?>
				</tr>
				<?php endif ?>
				<?php $i++ ?>
			<?php endforeach ?>
				<td><input type="submit" value="Search"></td>
				</tr>
			</table><?php

		$i = 0;

		// TODO: Add the features ?>
		<h3>Features:</h3>
			<table>
			<?php foreach ($opts_array->features as $fname): ?>
				<?php if ($i % 3 == 0): ?>
				<tr>
				<?php endif ?>
					<td>
						<input type="checkbox" name="<?= $fname ?>">
						<label for="<?= $id ?>"><?= $fname ?></label>
					</td>
				<?php if ($i % 3 == 2): ?>
				</tr>
				<?php endif ?>
				<?php $i++ ?>
			<?php endforeach ?>
			</table>

			<div id="submit_btn_box">
				<input type="submit" name="submit2" value="Search">
			</div>
		</form>
		<?php

	}

	/**
	 * Go through the configured categories, and return the Text, URLs, and image URLs
	 * @return void
	 */
	public function getCategories() {
		$option = get_option($this->adminOptionsName);
		$category_names = $option['category_name'];
		$category_image_uri = $option['category_image_uri'];
		$url_base = ProFoundMLS::getPageURL('neighborhood');

		$cats = array();

		for ($i = 0; $i < count($category_names); $i++) {
			$catstr = $category_names[$i];

			# FIXME: remove the redundancy below
			if ($this->city && preg_match('/{(.*?)}/', $catstr, $matches)) {
				$cities = strtolower($matches[1]);
				if ($cities == 'city' || in_array(strtolower($this->city), explode(',', $cities))) {
					$name = preg_replace('/{(.*?)}/', ucwords($this->city), $catstr);
					$cats[] = array(
						'name' => $name,
						'url' => home_url() . "/$url_base/" . preg_replace('/\s+/', '-', strtolower($this->_cleanCatName($name))) . '/',
						'img' => $category_image_uri[$i],
					);
				}
			} else if (!$this->city && !preg_match('/{(.*?)}/', $catstr, $matches)) {
				$cats[] = array(
					'name' => $catstr,
					'url' => home_url() . "/$url_base/" . preg_replace('/\s+/', '-', strtolower($this->_cleanCatName($catstr))) . '/',
					'img' => $category_image_uri[$i],
				);
			}
		}
		return $cats;
	}

	/**
	 * Render the HTML for the Category/Neighborhood dynamic page
	 */
	public function pageNeighborhood() {
		?>
		<ul class="category-input">
		<?php foreach ($this->getCategories() as $cat): ?>
			<li>
				<a href="<?= $cat['url'] ?>">
				<?php if ($cat['img'] != null): ?>
					<img alt="<?= $cat['name'] ?>"
						 src="<?= site_url() ?><?= $cat['img']  ?>" />
				<?php endif ?>
				</a>
				<a href="<?= $cat['url'] ?>"><?= $cat['name'] ?></a>
			</li>
		<?php endforeach ?>
		</ul>
		<?php
	}

	/**
	 * Include the HTMl that will be used for the Google Map on different pages
	 *
	 */
	public function renderMapContainer() {
		?>
		<p class="pfmls_clear"></p>
		<noscript>
		The Property Map uses <a href="http://maps.google.com">Google Maps</a> and requires Javascript to be enabled.
		<p>
		Please enable Javascript in your browser &amp; reload this page.
		</p>
		</noscript>

		<div id="pfmls_map"></div>
		<?php
	}

	/**
	 * Generate the HTML for the [pfmls_city_list] shortcode
	 *
	 * @return string Content to insert in place of the WP shortcode
	 */
	public function renderCityList() {
		$option = get_option($this->adminOptionsName);
		$city_names = $option['city_list'];
		sort($city_names);

		$columns = 4;
		$count = count($city_names);
		$cities_per_col = ceil($count / $columns);

		ob_start();
		?>
		<div class="pfmls-city-list">
			<?php
			if ($count > 0) {
				for ($i = 0; $i < $columns; $i++) {
					$this->cityColumn($i + 1, array_slice($city_names, $i * $cities_per_col, $cities_per_col));
				}
			} else {
			?>
				<span style="color: red">Please configure cities in Wordpress Admin under <b>ProFoundMLS</b></span>
			<?php
			}
			?>
			</div>
			<div class="pfmls_clear"></div>
			<?php

		return ob_get_clean();
	}

	/**
	 * Render an unstructured list of the cities specified
	 *
	 * @param integer $i Column number to be used for the HTML id attribute
	 * @param array $cities Cities and their IDX-friend IDs to be used for the links
	 */
	private function cityColumn($i, $cities) {
		$option = get_option($this->adminOptionsName);
		$cat_url = ProFoundMLS::getPageURL("neighborhood");
		?>
		<ul id="pfmls-cities-col-<?= $i ?>">
			<?php foreach ($cities as $city):
				# TODO: this code is duplicated in the CitySearch widget
				$city_url = strtolower(preg_replace('/\s+/', '-', $city));
			?>
			<li>
				<a href="<?= home_url() ?>/<?= $cat_url ?>/<?= $city_url ?>/"><?= $city ?></a>
			</li>
			<?php endforeach ?>
		</ul><?php
	}

	public function simpleGoogleMap($attrs, $content) {
		#print_r($attrs);

		$defaults = array(
			'width' => 500,
			'height' => 400,
			'center' => '33.5, -112',
			'zoom' => 10
		);
		foreach ($defaults as $key => $val) {
			$$key = isset($attrs[$key]) ? $attrs[$key] : $val;
		}

		$lines = explode("\n", trim($content));

		$data = array();
		foreach ($lines as $optline) {
			$opts = preg_split('/\s*,\s*/', strip_tags(trim($optline)));
			$data[] = "['" . trim($opts[0]) . "','" . $opts[1] . "'," . $opts[2] . "," . trim($opts[3]) . "]\n";
		}

		?>
		<link rel="stylesheet" href="<?= plugins_url() ?>/profoundmls/css/gmaps-labels.css" />
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=geometry&sensor=false"></script>
		<script type="text/javascript" src="<?= plugins_url() ?>/profoundmls/js/gmaps-labels.js"></script>
		<script type="text/javascript">
			function showPfMap () {
				var data = [
					<?= implode(",\n", $data); ?>
				];
				var options = {
					zoom: <?= $zoom ?>,
					mapTypeId: google.maps.MapTypeId.ROADMAP,
					center: new google.maps.LatLng(<?= $center ?>)
				};
				placeMap('gmap', options, data);
			}
			google.maps.event.addDomListener(window, 'load', showPfMap);
		</script>
		<div style="width: <?=$width?>px; height: <?= $height ?>px; background: #bdc" id="gmap"></div>
		<?php

	}
}


<?php

# TODO: don't use multi-part/form-data MIME type for requests?
# TODO: use the built-in functions in WordPress for HTTP requests?

class ProFoundIDX {
	private $api_url_base       = 'http://get.profoundidx.com/';
	private $api_key 			= null;
	private $api_secret 		= null;
	private $ch 				= null;
	
	public function __construct($key, $secret) {
		// set the api key and secret based on passed parameters
		$this->api_key = $key;
		$this->api_secret = $secret;
		
		// initialize cURL for use later
		$this->ch = curl_init();

		$this->debugOptions();

		curl_setopt($this->ch, CURLOPT_HEADER, false);
		curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->ch, CURLOPT_TIMEOUT, 0);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);

	}
	public function __destruct() {
		// clean cURL up
		if ($this->ch) curl_close($this->ch);
	}
	
	# TODO: For development only ...
	private function debugOptions() {
		$inifile = realpath(dirname(__FILE__) . '/../config.ini'); 
		if (file_exists($inifile)) {
			$options = parse_ini_file($inifile);
			if (isset($options['curl_proxy'])) {
				curl_setopt($this->ch, CURLOPT_PROXY, $options['curl_proxy']);
			}
			if (isset($options['idx_url'])) {
				$this->api_url_base = $options['idx_url'];
			}
		}
	}

	/**
	 * Make the HTTP request to the IDX server.
	 *
	 * @param $url
	 * @param $data
	 * @param $api string The api name and version and response type
	 * @return var false if no results, otherwise the response to be sent
	 */
	private function _makeRequest($url, $data = array(), $api = null) {
		$rawdata = $data;

		$data['BrowserIP'] = $_SERVER['REMOTE_ADDR'];
		$data['UserAgent'] = $_SERVER['HTTP_USER_AGENT'];
		$data['Referer'] = $_SERVER['HTTP_REFERER'];
		$data['RequestURL'] = $_SERVER['REQUEST_URI'];
		$data['Hostname'] = $_SERVER['HTTP_HOST'];

		$data['accountid'] = $this->api_key;
		$data['apikey'] = $this->api_secret;

		$full_url = $this->api_url_base . $url;
		curl_setopt($this->ch, CURLOPT_URL, $full_url);
		curl_setopt($this->ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($this->ch, CURLOPT_POST, 1);
		if ($api) {
			curl_setopt($this->ch, CURLOPT_HTTPHEADER, array("Accept: application/vnd.profoundmls.idx.$api"));
		}
		curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);

		// Debugging support for deployed versions of the plugin
		$result = curl_exec ($this->ch);
		if (isset($_GET['debug'])) {
			echo "==> URL: $full_url\n==> Request:\n";
			print_r($rawdata);
			echo "==> Response:\n$result\n";
			print_r(json_decode($result));
		}

		if (!$result) {
			return false;
		} else {
			return  json_decode($result, true);
		}
	}

	public function getDataForm() {
		return $this->_makeRequest('q/advopts/');
	}

	public function search($data = null) {
		return $this->_makeRequest('q/search/', $data, "search-v1.01+json");
	}

	public function getDetail($data = null) {
		return $this->_makeRequest('q/prop/', $data);
	}

	public function getAgentListings($data = null) {
		return $this->_makeRequest('q/myprops/', $data);
	}
}
?>

<script type="text/javascript" src="<? echo $this->plugin_url; ?>themes/lightbox-video/flowplayer/example/flowplayer-3.2.6.min.js"></script>
	<link rel="<? echo $this->plugin_url; ?>themes/lightbox-video/flowplayer/example/stylesheet" type="text/css" href="style.css">

<div class="popup-dom-lightbox-wrapper" id="<?php echo $lightbox_id?>"<?php echo $delay_hide ?>>
	<div class="lightbox-main lightbox-color-<?php echo $color ?>">
	<a href="#" class="lightbox-close" id="<?php echo $lightbox_close_id?>"><span>Close</span></a>
	
		<div class="lightbox-top">
			<div class="lightbox-top-content">
				<div class="lightbox-top-text">
					<p class="heading"><?php echo $fields['title'] ?></p>
				</div>
				<div class="video">
                <?php 
				if(isset($fields['video-embed']) && !empty($fields['video-embed'])){
					echo $fields['video-embed'];
				}
				else if(isset($fields['video']) && !empty($fields['video'])){
					echo '<a  
			 href="'.$fields['video'].'"
			 style="display:block;width:700px;height:330px"  
			 id="player"> 
		</a> 
		<script>
			flowplayer("player", "'.$this->plugin_url.'themes/lightbox-video/flowplayer/flowplayer-3.2.7.swf");
		</script>';
				}
				?>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		
		<div class="lightbox-middle-bar">
            <form method="post" action="<?php echo $form_action ?>"<?php echo $target ?>>
                <div>
                    <?php echo $inputs['hidden'].$fstr; ?>
                    <input type="submit" value="<?php echo $fields['submit_button'] ?>" src="<?php echo $theme_url?>images/trans.png" class="<?php echo $button_color?>-button" />
                </div>
            </form>
		</div>
		
		<div class="lightbox-bottom">
			<p class="secure"><?php echo $fields['footer_note'] ?></p>
		</div>
			<?php echo $promote_link ?>
	</div>

</div>
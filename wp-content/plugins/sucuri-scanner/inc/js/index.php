<?php

/**
 * Avoid directory listing.
 *
 * @package   Sucuri Plugin - SiteCheck Malware Scanner
 * @author    Yorman Arias <yorman.arias@sucuri.net>
 * @author    Daniel Cid   <dcid@sucuri.net>
 * @copyright Since 2010-2014 Sucuri Inc.
 * @license   Released under the GPL - see LICENSE file for details.
 * @link      https://wordpress.sucuri.net/
 * @since     File available since Release 0.1
 */

if( !defined('SUCURISCAN') ){ exit(0); }

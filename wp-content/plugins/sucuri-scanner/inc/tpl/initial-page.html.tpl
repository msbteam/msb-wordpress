
<div id="poststuff">

    <div class="postbox">
        <h3>Sucuri SiteCheck</h3>
        <div class="inside sucuriscan-clearfix">
            <div class="sucuriscan-column-left">
                <p>
                    <a href="http://sitecheck.sucuri.net/" target="_blank">Sucuri SiteCheck</a>
                    scanner will check your website for known malware, blacklisting status, website
                    errors, and out-of-date software.
                </p>
            </div>
            <div class="sucuriscan-column-right">
                <form method="post">
                    <input type="hidden" name="wpsucuri-doscan" value="wpsucuri-doscan" />
                    <input type="submit" name="wpsucuri_doscanrun" value="Scan this site now" class="button button-primary button-hero" />
                </form>
            </div>
        </div>
        <div class="sucuriscan-disclaimer">
            <p>
                <strong>Disclaimer</strong>: Sucuri SiteCheck is a free and remote scanner.
                Although we do our best to provide the best results, 100% accuracy is not
                realistic, and not guaranteed.
            </p>
        </div>
    </div>


    <div class="postbox">
        <h3>1-Click Hardening</h3>
        <div class="inside sucuriscan-clearfix">
            <div class="sucuriscan-column-left">
                <p>
                    In our experience a high-percentage of the infections we see every day come from
                    poor management on the end-user's part. This feature uses common hardening
                    measures that can be taken at any time and helps reduce infection risk.
                </p>
            </div>
            <div class="sucuriscan-column-right">
                <a href="%%SUCURI.URL.Hardening%%" class="button button-primary button-hero">Harden this site now</a>
            </div>
        </div>
    </div>


    <div class="postbox">
        <h3>WordPress Integrity</h3>
        <div class="inside sucuriscan-clearfix">
            <div class="sucuriscan-column-left">
                <p>
                    This feature compares your core install against a clean version of core. In
                    other words, if it is not a 1-to-1 match with core you will be notified of a
                    problem.
                </p>
            </div>
            <div class="sucuriscan-column-right">
                <a href="%%SUCURI.URL.CoreIntegrity%%" class="button button-primary button-hero">Check site integrity now</a>
            </div>
        </div>
    </div>


    <div class="postbox">
        <h3>Post-Hack</h3>
        <div class="inside sucuriscan-clearfix">
            <div class="sucuriscan-column-left">
                <p>
                    After being hacked or infected with malware, we recommend that you update your
                    wp-config keys, and also reset all your user passwords. Do it with ease using
                    Sucuri Post-Hack.
                </p>
            </div>
            <div class="sucuriscan-column-right">
                <a href="%%SUCURI.URL.PostHack%%" class="button button-primary button-hero">Run Post-Hack resets</a>
            </div>
        </div>
    </div>


    <div class="postbox">
        <h3>Last Logins</h3>
        <div class="inside sucuriscan-clearfix">
            <div class="sucuriscan-column-left">
                <p>
                    It's always good to know who is logging into your site. This feature allows you
                    to view logins, where they came from, and when they logged in.
                </p>
            </div>
            <div class="sucuriscan-column-right">
                <a href="%%SUCURI.URL.LastLogins%%" class="button button-primary button-hero">View Last Logins</a>
            </div>
        </div>
    </div>

</div>


<table class="wp-list-table widefat sucuri-%%SUCURI.SettingsDisplay%%">
    <tbody>
        <tr class="alternate">
            <td>Sucuri Plugin version</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.PluginVersion%%</span></td>
        </tr>
        <tr>
            <td>Sucuri Plugin MD5Sum (sucuri.php)</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.PluginMD5%%</span></td>
        </tr>
        <tr class="alternate">
            <td>Sucuri Plugin Last-time scan</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.PluginRuntimeDatetime%%</span></td>
        </tr>
        <tr>
            <td>Operating System</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.OperatingSystem%%</span></td>
        </tr>
        <tr class="alternate">
            <td>Server</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.Server%%</span></td>
        </tr>
        <tr>
            <td>Memory usage</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.MemoryUsage%%</span></td>
        </tr>
        <tr class="alternate">
            <td>MYSQL Version</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.MySQLVersion%%</span></td>
        </tr>
        <tr>
            <td>SQL Mode</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.SQLMode%%</span></td>
        </tr>
        <tr class="alternate">
            <td>PHP Version</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.PHPVersion%%</span></td>
        </tr>
        <tr>
            <td>PHP Safe Mode</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.SafeMode%%</span></td>
        </tr>
        <tr class="alternate">
            <td>PHP Allow URL fopen</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.AllowUrlFopen%%</span></td>
        </tr>
        <tr>
            <td>PHP Memory Limit</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.MemoryLimit%%</span></td>
        </tr>
        <tr class="alternate">
            <td>PHP Max Upload Size</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.UploadMaxFilesize%%</span></td>
        </tr>
        <tr>
            <td>PHP Max Post Size</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.PostMaxSize%%</span></td>
        </tr>
        <tr class="alternate">
            <td>PHP Max Script Execute Time</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.MaxExecutionTime%%</span></td>
        </tr>
        <tr>
            <td>PHP Max Input Time</td>
            <td><span class="sucuriscan-monospace">%%SUCURI.MaxInputTime%%</span></td>
        </tr>
    </tbody>
</table>

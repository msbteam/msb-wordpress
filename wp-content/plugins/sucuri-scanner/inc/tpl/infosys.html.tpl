
<div class="sucuriscan-tabs">
    <ul>
        <li>
            <a href="#" data-tabname="server-info">Plugin & Server Info</a>
        </li>
        <li>
            <a href="#" data-tabname="loggedin-users">Logged In Users</a>
        </li>
        <li>
            <a href="#" data-tabname="wordpress-cronjobs">WordPress Cronjobs</a>
        </li>
        <li>
            <a href="#" data-tabname="htaccess-integrity">HTAccess Integrity</a>
        </li>
        <li>
            <a href="#" data-tabname="wpconfig-vars">WP Config Variables</a>
        </li>
    </ul>

    <div class="sucuriscan-tab-containers">
        <div id="sucuriscan-server-info">
            %%SUCURI.ServerInfo%%
        </div>

        <div id="sucuriscan-loggedin-users">
            %%SUCURI.LoggedInUsers%%
        </div>

        <div id="sucuriscan-wordpress-cronjobs">
            %%SUCURI.Cronjobs%%
        </div>

        <div id="sucuriscan-htaccess-integrity">
            %%SUCURI.HTAccessIntegrity%%
        </div>

        <div id="sucuriscan-wpconfig-vars">
            %%SUCURI.WordpressConfig%%
        </div>
    </div>
</div>


<table class="wp-list-table widefat sucuri-wpconfig-rules">
    <thead>
        <th colspan="7" class="thead-with-button">
            <span>WP-Config Variables</span>
            <div class="thead-topright-action">
                <a href="%%SUCURI.WordpressConfig.ThickboxURL%%" title="WordPress Config Variables" class="button button-primary thickbox">View File</a>
            </div>
        </th>
        <tr>
            <th>Variable Name</th>
            <th>Value</th>
        </tr>
    </thead>
    <tbody>
        %%SUCURI.WordpressConfig.Rules%%
    </tbody>
</table>

<div id="sucuriscan-wpconfig-content" style="display:none">
    <textarea class="sucuriscan-full-textarea sucuriscan-wpconfig-textarea sucuriscan-monospace">%%SUCURI.WordpressConfig.Content%%</textarea>
</div>

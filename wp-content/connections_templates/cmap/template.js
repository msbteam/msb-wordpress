jQuery(document).ready(function () {
	var $j = jQuery.noConflict(); 
	
	$j('a.toggle-map').click( function() {
		var $this = $j(this);
		var uuid = $this.attr('data-uuid');
		var strShow = $this.attr('data-str-show');
		var strHide = $this.attr('data-str-hide');
		
		if ( $this.data('toggled') )
		{
			$this.data('toggled', false);
			$j( '#map-' + uuid ).slideUp('normal', function() { $j(this).remove() } );
			//$j( '#map-' + uuid ).remove();
			$this.text( strShow );
		}
		else
		{
			$this.data('toggled', true);
			$this.text( strHide );
			$j( $this.attr('data-gmap') ).appendTo( '#entry-id-' + uuid );
			$j( '#map-' + uuid ).fadeIn();
		}
		
		return false
	});
	
	$j('a.toggle-div').click( function() {
		var $this = $j(this);
		var uuid = $this.attr('data-uuid');
		var strShow = $this.attr('data-str-show');
		var strHide = $this.attr('data-str-hide');
		var div = $this.attr('data-div-id');
		
		if ( $this.data('toggled') )
		{
			$this.data('toggled', false);
			$this.text( strShow );
			$j( '#' + div ).slideUp();
		}
		else
		{
			$this.data('toggled', true);
			$this.text( strHide );
			$j( '#' + div ).slideDown();
		}
		
		return false
	});
});
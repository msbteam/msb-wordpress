<?php
	if ( !isset($cMap) ) global $cMap;
	
	$uuid = uniqid($entry->getId(),FALSE);
	$gMapFrame = '';
?>
<div id="entry-id-<?php echo $uuid; ?>" class="cn-entry">
	<table border="0px" bordercolor="#E3E3E3" cellspacing="0px" cellpadding="0px">
	    <tr>
	        <td align="left" width="55%" valign="top">
	        	<?php $entry->getLogoImage( array( 'style' => array('-moz-border-radius' => '4px', 'background-color' => '#FFFFFF', 'border' => '1px solid #E3E3E3' ) ) ); ?>
	        </td>
	        <td align="right" valign="top" style="text-align: right;">
	        	
				<div style="clear:both; margin: 5px 5px;">
		        	<div style="margin-bottom: 5px;">
						<?php $entry->getNameBlock() ?>
						<?php echo $entry->getTitleBlock() ?>
						<?php echo $entry->getOrgUnitBlock() ?>
					</div>
					
					<?php
						if ( $cMap->showAddresses ) echo $entry->getAddressBlock();
						
						if ( $cMap->showPhoneNumbers ) echo $entry->getPhoneNumberBlock();
						
						if ( $cMap->showEmail ) echo $entry->getEmailAddressBlock();
						if ( $cMap->showIM ) echo $entry->getImBlock();
						if ( $cMap->showSocialMedia ) echo $entry->getSocialMediaBlock();
						
						if ( $cMap->showBirthday ) echo $entry->getBirthdayBlock('F jS');
						if ( $cMap->showAnniversary ) echo $entry->getAnniversaryBlock('F jS');
					?>
					
				</div>
	        </td>
	    </tr>
	    
	    <tr>
	        <td valign="bottom" style="text-align: left;">
				
				<?php
					
					if ( $cMap->enableNote && $entry->getNotes() != '' )
					{
						echo '<a class="cn-note-anchor toggle-div" id="note-anchor-' , $uuid , '" href="#" data-uuid=\'' , $uuid , '\' data-div-id=\'note-block-' . $uuid . '\' data-str-show=\'' , $cMap->strNoteShow , '\' data-str-hide=\'' , $cMap->strNoteHide , '\'>' , $cMap->strNoteShow , '</a>';
					}
					
					if ( ( $cMap->enableNote && $entry->getNotes() != '' ) && ( $cMap->enableBio && $entry->getBio() != '' ) )
					{
						echo ' | ';
					}
					
					if ( $cMap->enableBio && $entry->getBio() != '' )
					{
						echo '<a class="cn-bio-anchor toggle-div" id="bio-anchor-' , $uuid , '" href="#" data-uuid=\'' , $uuid , '\' data-div-id=\'bio-block-' . $uuid . '\' data-str-show=\'' , $cMap->strBioShow , '\' data-str-hide=\'' , $cMap->strBioHide , '\'>' , $cMap->strBioShow , '</a>';
					}
				?>
				
	        </td>
			
			<td align="right" valign="bottom"  style="text-align: right;">
				
				<?php
					if ( $entry->getWebsites() )
					{
						$website = $entry->getWebsites();
						if ($website[0]->address != NULL) echo '<a class="url" href="' , $website[0]->address , '" target="_blank">' , $cMap->strVisitWebsite , '</a>' , "\n";
						
						echo ' | ';
					}
					
					if ( $cMap->enableMap )
					{
						$gMapFrame = $cMap->buildGmapFrame( $entry->getAddresses() );
							
						if ( !empty($gMapFrame) )
						{
							$mapDiv= '<div class="cn-gmap" id="map-' . $uuid . '" style="display: none;">' . $gMapFrame . '</div>';
					
							echo '<a class="cn-map-anchor toggle-map" id="map-anchor-' , $uuid , '" href="#" data-uuid=\'' , $uuid , '\' data-str-show=\'' , $cMap->strMapShow , '\' data-str-hide=\'' , $cMap->strMapHide , '\' data-gmap=\'' , $mapDiv , '\'>' , $cMap->strMapShow , '</a>';
							
							echo ' | ';
						}
					}
				?>
				
				<span class="cn-return-to-top"><?php echo $entry->returnToTopAnchor() ?></span>
	        </td>
	    </tr>
		
		<tr>
			<td colspan="2">
				<?php
					
					if ( $cMap->enableNote && $entry->getNotes() != '' )
					{
						echo '<div class="cn-notes" id="note-block-' , $uuid , '" style="display: none;">';
							if ( $cMap->enableNoteHead )
							{
								echo '<h4>' , $cMap->strNoteHead , '</h4>';
							}
							
							echo $entry->getNotesBlock();
						echo '</div>';
					}
					
					if ( $cMap->enableBio && $entry->getBio() != '' )
					{
						echo '<div class="cn-bio" id="bio-block-' , $uuid , '" style="display: none;">';
							
							if ( $cMap->enableBioHead )
							{
								echo '<h4>' , $cMap->strBioHead , '</h4>';
							}
							
							if ( $entry->getImageLinked() && $entry->getImageDisplay() )
							{
								echo '<img class="photo" alt="Photo of ' , $entry->getFirstName() , ' ' , $entry->getLastName() , '" src="' , CN_IMAGE_BASE_URL , $entry->getImageNameCard() , '" />' , "\n";
							}
							
							echo $entry->getBioBlock();
						echo '</div>';
					}
				?>
			</td>
		</tr>
		
	</table>
</div>
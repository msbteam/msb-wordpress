<?php
if ( !class_exists('cMap') )
{
	class cMap
	{
		/* Enable/Disable template features. */
			
		// Turn on or off the pagination. Valid values are TRUE or FALSE.
		public $enablePagination = TRUE;
		
		// Pagination limit [Number of entries per page]. $enablePagination MUST be set to TRUE.
		public $pageLimit = 20;
		
		// Put the pagination links before or after the list. Valid values are 'before' or 'after'.
		public $paginationPosition = 'before';
		
		// Turn on or off the category select drop down. Valid values are TRUE or FALSE.
		public $enableCategorySelect = TRUE;
		
		// Show categories with a zero entry count. Valid values are TRUE or FALSE.
		public $showEmptyCategories = TRUE;
		
		// Show category count. Valid values are TRUE or FALSE.
		public $showCategoryCount = FALSE;
		
		// Put the category select drop down before or after the list. Valid values are 'before' or 'after'.
		public $categorySelectPosition = 'before';
		
		// Limit the category select drop down to a root parent category ID.
		// Set using the shortcode option: category='X'
		// Valid values are TRUE or FALSE.
		public $enableCategoryByRootParent = FALSE;
		
		// Turn on or off the Google Map embed. Valid values are TRUE or FALSE.
		public $enableMap = TRUE;
		
		// Turn on or off the Bio tray. Valid values are TRUE or FALSE.
		public $enableBio = TRUE;
		
		// Turn on or off the header text for the Bio tray. Valid values are TRUE or FALSE.
		public $enableBioHead = TRUE;
		
		// Turn on or off the Note tray. Valid values are TRUE or FALSE.
		public $enableNote = TRUE;
		
		// Turn on or off the header text for the Note tray. Valid values are TRUE or FALSE.
		public $enableNoteHead = TRUE;
		
		// Turn on or off the addresses. Valid values are TRUE or FALSE.
		public $showAddresses = TRUE;
		
		// Turn on or off the phone numbers. Valid values are TRUE or FALSE.
		public $showPhoneNumbers = TRUE;
		
		// Turn on or off the email addresses. Valid values are TRUE or FALSE.
		public $showEmail = TRUE;
		
		// Turn on or off the IM IDs. Valid values are TRUE or FALSE.
		public $showIM = TRUE;
		
		// Turn on or off the social media IDs. Valid values are TRUE or FALSE.
		public $showSocialMedia = TRUE;
		
		// Turn on or off the birthday. Valid values are TRUE or FALSE.
		public $showBirthday = TRUE;
		
		// Turn on or off the anniversary. Valid values are TRUE or FALSE.
		public $showAnniversary = TRUE;
		
		
		// Google Map options
		public $mapType = 'm';			// t= Map Type. The available options are "m" map, "k" satellite, "h" hybrid, "p" terrain.
		public $mapZoom = '13';		// z= Sets the zoom level.
		public $mapFrameWidth = '580';		// Map iFrame width
		public $mapFrameHeight = '400';	// Map iFrame Height
		
		/* Customize the template strings */
		public $strSelect = 'Select Category';
		public $strSelectAll = 'All';
		
		public $strMapShow = 'Show Map';
		public $strMapHide = 'Close Map';
		
		public $strBioHead = 'Biography';
		public $strBioShow = 'Show Bio';
		public $strBioHide = 'Close Bio';
		
		public $strNoteHead = 'Notes';
		public $strNoteShow = 'Show Notes';
		public $strNoteHide = 'Close Notes';
		
		public $strHomePhone = 'Home Phone';
		public $strHomeFax = 'Home Fax';
		public $strCellPhone = 'Cell Phone';
		public $strWorkPhone = 'Work Phone';
		public $strWorkFax = 'Work Fax';
		
		public $strPersonalEmail = 'Personal Email';
		public $strWorkEmail = 'Work Email';
		
		public $strVisitWebsite = 'Visit Website';
		
		/**
		 * Load the template filters.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 */
		public function __construct()
		{
			add_filter( 'cn_list_index', array($this, 'listIndex'), 10, 2 );
			add_filter( 'cn_phone_number', array($this, 'phoneLabels') );
			add_filter( 'cn_email_address', array($this, 'emailLabels') );
			
			// If pagination is enabled add the appropiate filters.
			if ( $this->enablePagination )
			{
				add_filter( 'cn_list_results', array($this, 'limitList'), 10 );
				add_filter( 'cn_list_' . $this->paginationPosition, array($this, 'listPages'), 10, 2 );
			}
			
			// If the category select/filter feature is enabled, add the appropiate filters.
			if ( $this->enableCategorySelect )
			{
				add_filter( 'cn_list_' . $this->categorySelectPosition, array($this, 'categorySelect'), 9, 2 );
				add_filter( 'cn_list_atts', array($this, 'setCategory') );
			}
		}
		
		/**
		 * Build the iframe for a Google Map embed from the first address supplied by cnEntry::getAddresses.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param object $address
		 * @param object $options
		 * @return string
		 */
		public function buildGmapFrame($address)
		{
			if ( !empty($address) )
			{
				$mapLink = "http://maps.google.com/?q=";
				
				if ($address[0]->line_one != null) $mapLink .= $address[0]->line_one . '+';
				if ($address[0]->line_two != null) $mapLink .= $address[0]->line_two . '+';
				if ($address[0]->city != null) $mapLink .= $address[0]->city . '+';
				if ($address[0]->state != null) $mapLink .= $address[0]->state . '+';
				if ($address[0]->zipcode != null) $mapLink .= $address[0]->zipcode;
				if ($address[0]->latitude != null) $latitude .= $address[0]->latitude;	
				if ($address[0]->longitude != null) $longitude .= $address[0]->longitude;
				
				if ( !empty($latitude) && !empty($longitude) ) $geoCode = '&sll=' . $latitude . ',' . $longitude;
				
				// Convert link for Google Maps query.
				// Google Maps parameters
		        // t= Map Type. The available options are "m" map, "k" satellite, "h" hybrid, "p" terrain.
		        // z= Sets the zoom level.
		        $iFrameSource = str_replace(' ', '+', $mapLink) . $geoCode . '&amp;ie=UTF8' . '&amp;t=' . $this->mapType . '&amp;z=' . $this->mapZoom . '&amp;output=embed';
				
				return '<iframe width="' . $this->mapFrameWidth . '" height="' . $this->mapFrameHeight . '" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="' . $iFrameSource . '"></iframe>';
			}
			
			return '';
		}
		
		/**
		 * Alter the Phone Labels.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param object $data
		 * @return object
		 */
		public function phoneLabels($data)
		{
			switch ($data->type)
			{
				case 'homephone':
					$data->name = $this->strHomePhone;
					break;
				case 'homefax':
					$data->name = $this->strHomeFax;
					break;
				case 'cellphone':
					$data->name = $this->strCellPhone;
					break;
				case 'workphone':
					$data->name = $this->strWorkPhone;
					break;
				case 'workfax':
					$data->name = $this->strWorkFax;
					break;
			}
			
			return $data;
		}
		
		/**
		 * Alter the Email Labels.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param object $data
		 * @return object
		 */
		public function emailLabels($data)
		{
			switch ($data->type)
			{
				case 'personal':
					$data->name = $this->strPersonalEmail;
					break;
				case 'work':
					$data->name = $this->strWorkEmail;
					break;
				
				default:
					$data->name = 'Email';
				break;
			}
			
			return $data;
		}
		
		/**
		 * Limit the returned results.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param array $results
		 * @return array
		 */
		public function limitList($results)
		{
			$limit = $this->pageLimit; // Page Limit
			
			( !isset($_GET['cn-pg']) ) ? $offset = 0 : $offset = ( $_GET['cn-pg'] - 1 ) * $limit;
			
			return array_slice($results, $offset, $limit, TRUE);
		}
		
		/**
		 * Limit the returned results.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param string
		 * @param array $results
		 * @return string
		 */
		public function listPages($out, $results = NULL)
		{
			global $connections;
			$i = 1;
			
			$limit = $this->pageLimit; // Page Limit
			$pageCount = ceil( $connections->resultCount / $limit );
			
			$out .= '<ul id="cn-pages">';
				while ($i <= $pageCount)
				{
					$out .= '<li><a href="' . $this->modifyUrl( array('cn-pg' => $i) ) . '">' . $i . '</a></li>';
					$i++;
				}
			$out .= '</ul>';
			$out .= '<div style="clear: both;"></div>';
			return $out;
		}
		
		/**
		 * Modifies, replaces or removes the url query.
		 * 
		 * @author solenoid && jesse
		 * @link http://us2.php.net/manual/en/function.parse-url.php#100114
		 * @param array $mod
		 * @return string
		 */
		public function modifyUrl($mod)
		{
		    $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		    $query = explode("&", $_SERVER['QUERY_STRING']);
		    if (!$_SERVER['QUERY_STRING']) {$queryStart = "?";} else {$queryStart = "&";}
		    // modify/delete data
		    foreach($query as $q)
		    {
		        list($key, $value) = explode("=", $q);
		        if(array_key_exists($key, $mod))
		        {
		            if($mod[$key])
		            {
		                $url = preg_replace('/'.$key.'='.$value.'/', $key.'='.$mod[$key], $url);
		            }
		            else
		            {
		                $url = preg_replace('/&?'.$key.'='.$value.'/', '', $url);
		            }
		        }
		    }
		    // add new data
		    foreach($mod as $key => $value)
		    {
		        if($value && !preg_match('/'.$key.'=/', $url))
		        {
		            $url .= $queryStart.$key.'='.$value;
		        }
		    }
		    return $url;
		}
		
		/**
		 * Returns the options for the category select list.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param object $category
		 * @param integer $level
		 * @param integer $selected
		 * @return string
		 */
		public function buildOptionRowHTML($category, $level, $selected)
		{
			$selectString = NULL;
			
			$pad = str_repeat('&nbsp;&nbsp;&nbsp;', max(0, $level));
			if ($selected == $category->term_id) $selectString = ' SELECTED ';
			
			($this->showCategoryCount) ? $count = ' (' . $category->count . ')' : $count = '';
			
			if ( ($this->showEmptyCategories && empty($category->count)) || ($this->showEmptyCategories || !empty($category->count)) || !empty($category->children) ) $out .= '<option value="' . $category->term_id . '"' . $selectString . '>' . $pad . $category->name . $count . '</option>';
			
			if ( !empty($category->children) )
			{
				foreach ( $category->children as $child )
				{
					++$level;
					$out .= $this->buildOptionRowHTML($child, $level, $selected);
					--$level;
				}
				
			}
			
			return $out;
		
		}
		
		/**
		 * Returns the form for the category select list.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param string $out
		 * @param array $results
		 * @return string
		 */
		public function categorySelect($out, $results = NULL)
		{
			global $connections;
			$baseURL = get_permalink();
			$selected = esc_attr( $_GET['cn-cat'] );
			$level = 0;
			
			$categories = $connections->retrieve->categories();
			
			$out .= '<form id="cn-cat-select" action="' . esc_url($baseURL) . '" method="get">';
			$out .= '<select name="cn-cat" id="cn-category-select" onchange="this.form.submit()">';
			
			$out .= '<option value="">' . $this->strSelect . '</option>';
			$out .= '<option value="">' . $this->strSelectAll . '</option>';
			
			foreach ( $categories as $key => $category )
			{
				if ( isset($connections->limitCategoryTree) )
				{
					if ( $connections->limitCategoryTree === TRUE )
					{
						if ( !is_array($connections->categoryTreeIDs) )
						{
							// Trim the space characters if present.
							$connections->categoryTreeIDs = str_replace(' ', '', $connections->categoryTreeIDs);
							
							// Convert to array.
							$connections->categoryTreeIDs = explode(',', $connections->categoryTreeIDs);
						}
						
						if ( !in_array($category->term_id, $connections->categoryTreeIDs) ) continue;
					}
				}
				
				$out .= $this->buildOptionRowHTML($category, $level, $selected);
			}
			
			$out .= '</select>';
			$out .= '</form>';
			
			return $out;
		}
		
		/**
		 * Alters the shortcode attribute values before the query is processed.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param array $atts
		 * @return array
		 */
		public function setCategory($atts)
		{
			global $connections;
			
			if ( $this->enableCategoryByRootParent )
			{
				$connections->limitCategoryTree = TRUE;
				$connections->categoryTreeIDs = esc_attr( $atts['category'] );
			}
			
			if ( isset($_GET['cn-cat']) && $_GET['cn-cat'] != '' ) $atts['category'] = $_GET['cn-cat'];
			
			return $atts;
		}
		
		/**
		 * Dynamically builds the alpha index based on the available entries.
		 * 
		 * @author Steven A. Zahm
		 * @version 1.0
		 * @param string $index
		 * @param array $results
		 * @return string
		 */
		public function listIndex($index, $results = NULL)
		{
			$previousLetter = NULL;
			$setAnchor = NULL;
			
			foreach ( (array) $results as $row)
			{
				$entry = new cnEntry($row);
				$currentLetter = strtoupper(mb_substr($entry->getFullLastFirstName(), 0, 1));
				if ($currentLetter != $previousLetter)
				{
					$setAnchor .= '<a href="#' . $currentLetter . '">' . $currentLetter . '</a> ';
					$previousLetter = $currentLetter;
				}
			}
			
			return '<div class="cn-alphaindex" style="text-align:right;font-size:larger;font-weight:bold">' . $setAnchor . '</div>';
		}
	}
	
	global $cMap;
		
	$cMap = new cMap();
}
?>
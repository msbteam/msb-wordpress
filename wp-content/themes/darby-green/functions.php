<?php
// Start the engine
require_once(TEMPLATEPATH.'/lib/init.php');

// Child theme library
//require_once(CHILD_DIR.'/lib/custom-header.php');
add_theme_support( 'genesis-custom-header', array( 'width' => 960, 'height' => 200, 'textcolor' => 'ffffff') );
require(CHILD_DIR.'/lib/style.php');

// Child theme (do not remove)
define('CHILD_THEME_NAME', 'Lifestyle Theme');
define('CHILD_THEME_URL', 'http://www.studiopress.com/themes/lifestyle');

// Add support for custom background
if ( function_exists('add_custom_background') ) {
	add_custom_background();
}

// Add new image sizes
add_image_size('Mini Square', 70, 70, TRUE);
add_image_size('Square', 110, 110, TRUE);

// Force layout on homepage
add_filter('genesis_pre_get_option_site_layout', 'lifestyle_home_layout');
function lifestyle_home_layout($opt) {
	if ( is_home() )
    $opt = 'content-sidebar';
	return $opt;
}  

// Add advertising code after single post
add_action('genesis_after_post_content', 'lifestyle_include_adsense', 9); 
function lifestyle_include_adsense() {
    if ( is_single() )
    require(CHILD_DIR.'/adsense.php');
}

// Add two sidebars underneath the primary sidebar
add_action('genesis_after_sidebar_widget_area', 'lifestyle_include_bottom_sidebars'); 
function lifestyle_include_bottom_sidebars() {
    require(CHILD_DIR.'/sidebar-bottom.php');
}

// Customize the footer section
add_filter('genesis_footer_creds_text', 'lifestyle_footer_creds_text');
function lifestyle_footer_creds_text($creds) {
	$creds = __('Copyright', 'genesis') . ' [footer_copyright] [footer_childtheme_link] '. __('on', 'lifestyle') .' [footer_genesis_link] &middot; [footer_wordpress_link] &middot; [footer_loginout]';
	return $creds;
}  

// Register widget areas
genesis_register_sidebar(array(
	'name'=>'Sidebar Bottom Left',
	'description' => 'This is the bottom left column in the sidebar.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Sidebar Bottom Right',
	'description' => 'This is the bottom right column in the sidebar.',
	'before_title'=>'<h4 class="widgettitle">','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Top Left',
	'description' => 'This is the featured top left column of the homepage.',
	'before_title'=>'<h4 class="widgettitle"><span class="corner-left"></span>','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Top Right',
	'description' => 'This is the featured top right column of the homepage.',
	'before_title'=>'<h4 class="widgettitle"><span class="corner-right"></span>','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Featured Bottom',
	'description' => 'This is the featured bottom section of the homepage.',
	'before_title'=>'<h4 class="widgettitle"><span class="corner-left"></span>','after_title'=>'</h4>'
));
genesis_register_sidebar(array(
	'name'=>'Additional Info',
	'id' => 'additionalinfo',
	'description' => 'This is for additional info (such as legal disclaimers) that appears below the main footer.',
	'before_title'=>'<h4 class="widgettitle"><span class="corner-left"></span>','after_title'=>'</h4>'
));


/* Different Favicon for Each Site in MS */

function favicon4sites() {
 echo '<link rel="Shortcut Icon" type="image/x-icon" href="' . get_bloginfo('wpurl') . '/files/favicon.ico" />';
}

add_action( 'admin_head', 'favicon4sites' );

add_action( 'login_head', 'favicon4sites' );

remove_action('wp_head', 'genesis_load_favicon');
remove_action('genesis_meta', 'genesis_load_favicon');

add_action( 'wp_head', 'favicon4sites' );


// Custom footer
remove_action('genesis_footer', 'genesis_do_footer');
add_action('genesis_footer', 'custom_footer');
function custom_footer() {
    require(CHILD_DIR.'/custom-footer.php');
}  
// Custom Post Metas 
add_action ( 'genesis_post_content' , 'msb_post_meta_check' );

function msb_post_meta_check() {
	global $post;

	if ( ( $post->id != '87' ) || ( $post->post_type != 'post' ) || ( genesis_site_layout() == 'full-width-content' ) )
		remove_action( 'genesis_after_post_content', 'genesis_post_meta' );
		
}

add_filter('genesis_post_info', 'msb_post_info_filter');
function msb_post_info_filter($post_info) {
	if ( ( $post->id != '87' ) || ( $post->post_type != 'post' ) || ( genesis_site_layout() == 'full-width-content' ) )
		$post_info = '';
	return $post_info;
}

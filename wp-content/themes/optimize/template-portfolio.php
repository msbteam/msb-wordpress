<?php
/*
Template Name: Portfolio
*/
?>

<?php global $woo_options; ?>

<?php get_header(); ?>

        <div id="featured">
            <div id="page-title" class="col-full">
                <span class="archive_header blog"><span class="fl cat"><?php if ( is_category() ) single_cat_title(); else the_title(); ?></span></span>        
            </div>
        </div> 

        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl">
                	<?php if ( $woo_options['woo_portfolio_tags'] ) { 
					$tags = explode(',',$woo_options['woo_portfolio_tags']); // Tags to be shown
					foreach ($tags as $tag){
						$tag = trim($tag); 
						$displaytag = $tag;
						$tag = str_replace (" ", "-", $tag);	
						$tag = str_replace ("/", "-", $tag);
						$tag = strtolower ( $tag );
						$link_tags[] = '<a href="#'.$tag.'" rel="'.$tag.'">'.$displaytag.'</a>'; 
					}
					$new_tags = implode(' ',$link_tags);
					?>
                    <span class="port-cat"><?php _e('Select a category:', 'woothemes'); ?>&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" rel="all"><?php _e('All','woothemes'); ?></a>&nbsp;<?php echo $new_tags; ?></span>
					<?php } else { if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); } ?>
                </div>
                <a class="subscribe fr" href="<?php if ( $woo_options['woo_feedburner_url'] <> "" ) { echo $woo_options['woo_feedburner_url']; } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        

                <div class="<?php if ( function_exists( "yoast_breadcrumb" ) ) echo 'fr'; else echo 'fl'; ?>">
					<span class="fr catrss"><a class="subscribe fr" href="<?php if ( $woo_options['woo_feedburner_url'] <> "" ) { echo $woo_options['woo_feedburner_url']; } else { echo get_bloginfo_rss('rss2_url'); } ?>"></a></span>
                </div>
			</div>
        </div>              
    
	</div><!-- /#top -->
       
    <div id="content">
    <div class="col-full">
    
		<div id="portfolio">
		
		<?php if ( get_query_var('paged') ) $paged = get_query_var('paged'); elseif ( get_query_var			('page') ) $paged = get_query_var('page'); else $paged = 1;
						$args = array(
							'post_type' => 'portfolio', 
							'showposts' => '-1',
							'paged' => $paged, 
						);
			query_posts( $args ); ?>
			
				        <?php if (have_posts()) : while (have_posts()) : the_post(); $count++; ?>		        					

				<?php 
					// Portfolio tags class
					$porttag = ""; 
					$posttags = get_the_tags(); 
					if ($posttags) { 
						foreach($posttags as $tag) { 
							$tag = $tag->name;
							$tag = str_replace (" ", "-", $tag);	
							$tag = str_replace ("/", "-", $tag);
							$tag = strtolower ( $tag );
							$porttag .= $tag . ' '; 
						} 
					} 
				?>                                                                        
                <!-- Post Starts -->
                <div class="post block fl <?php echo $porttag; ?>">

					<?php
						// Grab large portfolio image
					 	$large = get_post_meta($post->ID, 'portfolio-large', $single = true); 

						if (empty($large) && $woo_options['woo_portfolio_lightbox'] == "true") { 
	                    	// Check if there is a gallery in post
	                    	$exclude = get_post_meta($post->ID, 'portfolio', $single = true);
	                    	$gallery = woo_get_post_images();
	                    	if ( $gallery ) {
	                    		// Get first uploaded image in gallery
	                    		$large = $gallery[0]['url'];
	                    		$caption = $gallery[0]['caption'];
		                    } 
	                    }
	                    
	                    // Set rel on anchor to show lightbox
	                    if ( $woo_options['woo_portfolio_lightbox'] == "true") 
                    		$rel = 'rel="prettyPhoto['. $post->ID .']"';
					 ?>
	                    
                    <a <?php echo $rel; ?> title="<?php echo $caption; ?>" href="<?php echo $large; ?>" class="thumb">
						<?php 
						if ( $woo_options['woo_portfolio_resize'] ) {
							woo_image('key=portfolio&width=450&height=210&class=portfolio-img&link=img'); 
						} else { ?>
                        	<img class="portfolio-img" src="<?php echo get_post_meta($post->ID, 'portfolio', $single = true); ?>" alt="" />
                        <?php } ?>
                    </a>
                    
                    <?php 
                    	// Output image gallery for lightbox
                    	if ( $gallery ) {
	                    	foreach ( array_slice($gallery, 1) as $img => $attachment ) {
	                    		echo '<a '.$rel.' title="'.$attachment['caption'].'" href="'.$attachment['url'].'" class="gallery-image"></a>';	                    
	                    	}
	                    	unset($gallery);
	                    }
                    ?>

                    <h2 class="title"><?php the_title(); ?></h2>

                    <div class="entry">
	                    <?php the_content(); ?>                        
	                </div><!-- /.entry -->

                </div><!-- /.post -->
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  

            <div class="fix"></div>
        
            <div class="more_entries">
                <?php if (function_exists('wp_pagenavi')) wp_pagenavi(); else { ?>
                <div class="fl"><?php previous_posts_link(__('&laquo; Newer Entries ', 'woothemes')) ?></div>
                <div class="fr"><?php next_posts_link(__(' Older Entries &raquo;', 'woothemes')) ?></div>
                <br class="fix" />
                <?php } ?> 
            </div>		
                
		</div><!-- /#portfolio -->

    </div><!-- /.col-full -->
    </div><!-- /#content -->
<?php get_footer(); ?>
	<div id="footer">
    <div class="col-full">
    
        <!-- Footer Widget Area Starts -->
        <div id="footer-widgets">
    
            <div class="block">
                <?php woo_sidebar('footer-1'); ?>		           
            </div>
            <div class="block">
                <?php woo_sidebar('footer-2'); ?>		           
            </div>
            <div class="block">
                <?php woo_sidebar('footer-3'); ?>		           
            </div>
            <div class="block last">
                <?php woo_sidebar('footer-4'); ?>		           
            </div>

	        <div class="fix"></div>       
        </div>
        <!-- Footer Widget Area Ends -->

        <div id="footer-credits">
	
		<div id="copyright" class="col-left">
            <?php if(get_option('woo_footer_left') == 'true'){
                echo stripslashes(get_option('woo_footer_left_text'));	
            } else { ?>
                <p>&copy; <?php echo date('Y'); ?> <?php bloginfo(); ?>. <?php _e('All Rights Reserved.', 'woothemes') ?></p>
            <?php } ?>
		</div>
		
		<div id="credit" class="col-right">
            <?php if(get_option('woo_footer_right') == 'true'){
                echo stripslashes(get_option('woo_footer_right_text'));
            } else { ?>
				<p><?php _e('Powered by', 'woothemes') ?> <a href="http://www.wordpress.org">WordPress</a>. <?php _e('Designed by', 'woothemes') ?> <a href="<?php $aff = get_option('woo_footer_aff_link'); if(!empty($aff)) { echo $aff; } else { echo 'http://www.woothemes.com'; } ?>"><img src="<?php bloginfo('template_directory'); ?>/images/woothemes.png" width="74" height="19" alt="Woo Themes" /></a></p>
            <?php } ?>
		</div>

		</div><!-- /#footer-widgets -->
		
	</div><!-- /.col-full -->
	</div><!-- /#footer -->
	
</div><!-- /#container -->

<?php wp_footer(); ?>

<?php if ( get_option('woo_twitter') && $GLOBALS['ishome'] == "true" ) { ?>
<script type="text/javascript" src="http://twitter.com/javascripts/blogger.js"></script>
<script type="text/javascript" src="http://api.twitter.com/1/statuses/user_timeline/<?php echo get_option('woo_twitter'); ?>.json?callback=twitterCallback2&amp;count=<?php echo get_option('woo_twitter_count'); ?>&amp;include_rts=t"></script>
<?php } ?>

</body>
</html>
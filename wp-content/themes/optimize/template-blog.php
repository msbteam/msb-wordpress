<?php
/*
Template Name: Blog
*/
?>

<?php get_header(); ?>


        <div id="featured">
            <div id="page-title" class="col-full">
                <span class="archive_header blog"><span class="fl cat"><?php the_title(); ?></span></span>        
            </div>
        </div>

        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl"><?php if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); ?></div>
                <a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        
                <div class="<?php  if ( function_exists( "yoast_breadcrumb" ) ) echo 'fr'; else echo 'fl'; ?>">
					<span class="fr catrss"><a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>"><?php _e('RSS feed for this section', 'woothemes'); ?></a></span>
                </div>
			</div>
        </div>              
    
	</div><!-- /#top -->
       
    <div id="content">
    <div class="col-full">
    
		<div id="main" class="col-left">
            
			<?php
			// WP 3.0 PAGED BUG FIX
			if ( get_query_var('paged') )
				$paged = get_query_var('paged');
			elseif ( get_query_var('page') ) 
				$paged = get_query_var('page');
			else 
				$paged = 1;
			?>
			<?php query_posts("post_type=post&paged=$paged"); ?>
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?><?php if (in_category('600')) continue; ?>
                                                                        
                <!-- Post Starts -->
                <div class="post">

	                <?php if ( $woo_options[ 'woo_post_content' ] != "content" ) woo_image( 'width='.$woo_options[ 'woo_thumb_w' ].'&height='.$woo_options[ 'woo_thumb_h' ].'&class=thumbnail '.$woo_options[ 'woo_thumb_align' ]); ?> 

                    <h2 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
                    
                    <p class="post-meta">
                    	<?php _e('Posted on', 'woothemes') ?> <?php the_time(get_option('date_format')); ?>
                    	<?php _e('by', 'woothemes') ?>  <?php the_author_posts_link(); ?>
                    	<?php _e('in', 'woothemes') ?> <?php the_category(', ') ?>
                    </p>
                    
                    <div class="entry">
						<?php global $more; $more = 0; ?>	                    
                        <?php if ( get_option('woo_excerpt') == "true" ) the_excerpt(); else the_content(); ?>                        
	                </div><!-- /.entry -->
                    
                   	<span class="comments"><?php comments_popup_link(__('0 Comments', 'woothemes'), __('1 Comment', 'woothemes'), __('% Comments', 'woothemes')); ?></span>
                    

                </div><!-- /.post -->
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
                <div class="more_entries">
                    <?php if (function_exists('wp_pagenavi')) wp_pagenavi(); else { ?>
                    <div class="fl"><?php previous_posts_link(__('&laquo; Newer Entries ', 'woothemes')) ?></div>
                    <div class="fr"><?php next_posts_link(__(' Older Entries &raquo;', 'woothemes')) ?></div>
                    <br class="fix" />
                    <?php } ?> 
                </div>		
                
		</div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /.col-full -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>
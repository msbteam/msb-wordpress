<?php
/*
Template Name: Sitemap
*/
?>
<?php get_header(); ?>
       
        <div id="featured">
            <div id="page-title" class="col-full">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl"><?php if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); ?></div>
                <a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        
			</div>
        </div>              
	</div><!-- /#top -->  
       
    <div id="content">
    <div class="col-full">
		<div id="main" class="col-left">
					
	        <div class="post">
	        	        	
	        	<div class="entry">
	            
	            	<h3><?php _e('Pages', 'woothemes') ?></h3>
	
	            	<ul>
	           	    	<?php wp_list_pages('depth=1&sort_column=menu_order&title_li=' ); ?>		
	            	</ul>				
	    
		            <h3><?php _e('Categories', 'woothemes') ?></h3>
	
		            <ul>
	    	            <?php wp_list_categories('title_li=&hierarchical=0&show_count=1') ?>	
	        	    </ul>
			        
			        <h3>Posts per category</h3>
			        
			        <?php
			    
			            $cats = get_categories();
			            foreach ($cats as $cat) {
			    
			            query_posts('cat='.$cat->cat_ID);
			
			        ?>
	        
	        			<h4><?php echo $cat->cat_name; ?></h4>
						
			        	<ul>	
	    	        	    <?php while (have_posts()) : the_post(); ?>
	        	    	    <li style="font-weight:normal !important;"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> - <?php _e('Comments', 'woothemes') ?> (<?php echo $post->comment_count ?>)</li>
	            		    <?php endwhile;  ?>
			        	</ul>
	    
	    		    <?php } ?>
	    		
	    		</div><!-- /.entry -->
	    						
	        </div><!-- /.post -->                    
	                
        </div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /.col-full -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>

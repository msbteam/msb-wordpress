<?php get_header(); ?>

        <div id="featured">
            <div id="page-title" class="col-full">
                    <h2 class="title"><?php _e('Error 404 - Page not found!', 'woothemes') ?></h2>
            </div>
        </div>
    
	</div><!-- /#top -->
       
    <div id="content">
    <div class="col-full">
    
		<div id="main" class="col-left">
                                                                                    
            <div class="post">

                <p><?php _e('The page you trying to reach does not exist, or has been moved. Please use the menus or the search box to find what you are looking for.', 'woothemes') ?></p>

            </div><!-- /.post -->
                                                                            
        </div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /.col-full -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>
<?php
/*
Template Name: Full Width
*/
?>

<?php get_header(); ?>

        <div id="featured">
            <div id="page-title" class="col-full">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl"><?php if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); ?></div>
                <a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        
			</div>
        </div>              
	</div><!-- /#top -->  
       
    <div id="content">
    <div class="col-full">
		<div id="main" class="fullwidth">
            
            <?php if (have_posts()) : $count = 0; ?>
            <?php while (have_posts()) : the_post(); $count++; ?>
                                                                        
                <div class="post">

                    
                    <div class="entry">
	                	<?php the_content(); ?>
	               	</div><!-- /.entry -->

                </div><!-- /.post -->
                                                    
			<?php endwhile; else: ?>
				<div class="post">
                	<p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
                </div><!-- /.post -->
            <?php endif; ?>  
        
		</div><!-- /#main -->
		
    </div><!-- /.col-full -->
    </div><!-- /#content -->
		
<?php get_footer(); ?>
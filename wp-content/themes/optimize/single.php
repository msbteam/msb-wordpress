<?php get_header(); ?>

	<?php if (have_posts()) : the_post(); ?>
       
        <div id="featured">
            <div id="page-title" class="col-full">
                <h1><?php the_title(); ?></h1>
            </div>
        </div>
        <div id="breadcrumb">
        	<div class="col-full">
                <div class="fl"><?php if ( function_exists( "yoast_breadcrumb" ) ) yoast_breadcrumb('', ''); ?></div>
                <a class="subscribe fr" href="<?php if ( get_option('woo_feedburner_url') <> "" ) { echo get_option('woo_feedburner_url'); } else { echo get_bloginfo_rss('rss2_url'); } ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/images/ico-rss.png" alt="Subscribe" class="rss" />
                </a>        
        		<?php if(get_post_type() == 'post'){ ?>                
                <div class="<?php if ( function_exists( "yoast_breadcrumb" ) ) echo 'fr'; else echo 'fl'; ?>">
                    <?php _e('Posted on', 'woothemes') ?> <?php the_time(get_option('date_format')); ?>
                    <?php _e('by', 'woothemes') ?>  <?php the_author_posts_link(); ?>
                </div>
                <?php } ?>
			</div>
        </div>
	</div><!-- /#top -->

    <div id="content">
    <div class="col-full">
    
		<div id="main" class="col-left">
		                        
                <div class="post">
                                        
                    <div class="entry">
                    	<?php the_content(); ?>
<!--BEGIN .author-bio-->
<div class="author-bio">
<div style="float: left;"> <?php echo get_avatar( get_the_author_meta('email'), '60' ); ?> </div>

<div class="author-info">
<h4 class="author-title" style="float: left; margin-top: 29px; margin-left: 10px;">Written by <?php the_author_link(); ?></h4>

<div style="clear: both;"></div>

<p class="author-description" style="float: left; margin-top: 5px;"><?php the_author_meta('description'); ?>
<br /><br />Website: <a href="<?php the_author_meta('user_url');?>"><?php the_author_meta('user_url');?></a>
</div>
<!--END .author-bio-->

</div>
					</div>
										
					<?php the_tags('<p class="tags">'.__('Tags: ', 'woothemes'), ', ', '</p>'); ?>

                </div><!-- /.post -->

                
                <?php if ('open' == $post->comment_status) : ?>
	                <?php comments_template(); ?>
				<?php endif; ?>
                                                            
		</div><!-- /#main -->

        <?php get_sidebar(); ?>

    </div><!-- /.col-full -->
    </div><!-- /#content -->
    
	<?php else: ?>
    <div id="content">
    <div class="col-full">
            <p><?php _e('Sorry, no posts matched your criteria.', 'woothemes') ?></p>
        </div><!-- /.post -->             
    </div><!-- /.col-full -->
    </div><!-- /#content -->
    <?php endif; ?>  
    
		
<?php get_footer(); ?>
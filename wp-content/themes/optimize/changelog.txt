*** Optimize Changelog ***

2011.10.06 - version 2.3.6
 * style.css - tweaked alignleft, alignright, aligncenter for images

2011.09.28 - version 2.3.5
 * template-portfolio.php - set showposts to -1 to show all posts on portfolio page, as paging breaks the tag filtering.

2011.09.01 - version 2.3.4
 * /includes/theme-js.php - Optimised code and replaced use of bloginfo() with get_template_directory_uri().
 * /includes/js/scripts.js - Added optimisation on Twitter and Testimonials faders to prevent fade stacking when switching between tabs.

2011.08.23 - version 2.3.3
 * index.php - Added ID to "About" Info box

2011.08.05 - version 2.3.2
 * includes/theme-options.php, 
   includes/theme-js.php - updated slider speed options

2011.07.25 - version 2.3.1
 * /includes/js/scripts.js - New JS for twitter and testimonial feeds
 * /includes/js/slides.min.jquery.js - Replaced innerfade with slidesjs
 * /includes/theme-js.php - New JS for twitter and testimonial feeds
 * /includes/theme-options.php - New options for JS for twitter and testimonial feeds
 * index.php - Modified markup for replacing of innerfade with slidesjs
 * style.css - modified css for twitter stream text wrap
 
2011.07.01 - version 2.3
 * /includes/js/portfolio.js - Added hash-based portfolio navigation JavaScript.
 * template-portfolio.php - changed the link for the tags to include hash tag so it is easier to copy the URL

2011.05.09 - version 2.2
 * includes/theme-widgets.php - added WooTabs
 * style.css - added 4.3 WooTabs

2011.03.31 - version 2.1.2 
 * template-portfolio.php - fixed query to support pagination
 * includes/theme-functions.php - changed Portfolio CPT rewrite portfolio slug (line 180)

2010.12.05 - version 2.1.1
 * header.php - fixed prettyphoto to work on homepage too
 * includes/theme-js.php - fixed prettyphoto to work on homepage too

2010.11.19 - version 2.1
 * includes/js/prettyPhoto.js - Version 3.0 of prettyPhoto http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/
 * css/prettyPhoto.css - updated css

2010.11.18 - version 2.0.9
 * includes/theme-functions.php - fixed gallery function for new framework
 * template-portfolio.php - fixed gallery function for new framework 

2010.11.12 - version 2.0.8
 * includes/theme-functions.php - fixed bug with saving custom field images

2010.11.09 - version 2.0.7
 * footer.php - fixed footer affil link
 * style.css - change bg color on body and #container
 * index.php - added class="entry" to mainpage divs
 * includes/theme-options.php - added options icon
 * template-archives.php - removed </div> in breadcrumb
 * template-fullwidth.php - removed </div> in breadcrumb
 * template-sitemap.php - removed </div> in breadcrumb

2010.11.02 - version 2.0.6
 * includes/js/innerfade.js - update to auto resize testimonials

2010.10.19 - version 2.0.5
 * template-portfolio.php - added caption text to gallery images
 * includes/theme-functions.php - added caption text to gallery images function

2010.10.07 - version 2.0.4
 * index.php, template-portfolio.php - re-removed meta_value from query since it got back in there for some reason

2010.09.02 - version 2.0.3
 * template-blog.php - fixed query to only show post_type=post

2010.08.31 - version 2.0.2 
 * index.php - fixed twitter if statement and added class="twitter-if"
 * style.css - added .twitter-if class
 * index.php - removed meta_value from query of mini-features

2010.08.30 - version 2.0.1
 * template-portfolio.php - Added check for empty post image array.

2010.08.19 - version 2.0
 * /functions/ - updated to latest framework
 * archive-portfolio.php - REMOVED
 * footer.php - added new footer customization options
 * header.php - added woo_head(). Fixed RSS link.
 * --- CUSTOM POST TYPES ---
 * includes/theme-functions.php - Added custom post types for testimonials, mini-features, portfolio
 * includes/theme-options.php - Removed redundant options. Added Custom Post Types fields.
 * index.php - Added query for custom post types. Fixed homepage content query to work with new options.
 * template-portfolio.php - Added custom post types support
 * single.php - removed post meta for custom post types
 * template-blog.php - removed old portfolio settings from query
 * archive.php - removed old portfolio settings from query 
 * template-portfolio.php - moved code from archive-portfolio.php. Added functionality for multiple portfolio images per item.
 * includes/images/ - new custom post type icons

2010.08.12 - version 1.7.1
 * includes/theme-widgets.php - Made search widget multi-instance

2010.08.10 - version 1.7
 * includes/theme-actions.php - Removed yoast breadcrumb. Grab plugin here: http://yoast.com/wordpress/breadcrumbs/
 * includes/single.php - Test if function_exists yoast_breadcrumb()
 * includes/page.php - Test if function_exists yoast_breadcrumb()
 * includes/archive.php - Test if function_exists yoast_breadcrumb()
 * includes/template-sitemap.php - Test if function_exists yoast_breadcrumb()
 * includes/template-fullwidth.php - Test if function_exists yoast_breadcrumb()
 * includes/template-blog.php - Test if function_exists yoast_breadcrumb()
 * includes/archive-portfolio.php - Test if function_exists yoast_breadcrumb()
 * includes/template-contact.php - Test if function_exists yoast_breadcrumb()
 * includes/template-archives.php - Test if function_exists yoast_breadcrumb()
 * includes/theme-options.php - removed option to show/hide breadcrumb. Activated when installing plugin.

2010.06.28 - version 1.6.1
 * style.css - Fixed Gravity forms styling bug

2010.06.21 - version 1.6.0
 * header.php - Added theme support for WordPress 3.0 Menu Management
 * /includes/theme-functions.php - Added theme support for WordPress 3.0 Menu Management
 
2010.06.15 - version 1.5.3
 * style.css - added section 3.4 Info Boxes
 * /images/ico-*.png - Added info box icons

2010.06.05 - version 1.5.2
 * style.css - added styling for Gravity forms
 * effects.css - added styling for Gravity forms  

2010.06.04 - version 1.5.1
 * archive.php - fixed archive layout bug if portfolio wasn't set in options

2010.05.26 - version 1.5
 * header.php - exclude portfolio category from RSS (line 11)
 * template-*.php - exclude portfolio category from RSS (line 16)
 * single.php - exclude portfolio category from RSS (line 13)
 * page.php - exclude portfolio category from RSS (line 11)
 * archive-portfolio.php - exclude portfolio category from RSS (line 25)
 * archive.php - exclude portfolio category from RSS (line 36)
 * includes/theme-functions.php - added function to store portfolio ID
 * archive.php - added IF conditional to show portfolio template if in portfolio category

2010.05.21 - version 1.4.3
 * index.php added stripslashes to sub featured text

2010.04.29 � version 1.4.2
 * search.php - Sanitized search query return
 * header.php  - Modified WooNav call

2010.04.19 � version 1.4.1
 * template-contact.php - Fixed JS issue.

2010.04.07 � version 1.4
 * /functions/* � MAJOR UPDATE � Framework V.2.7.0
 * header.php � Added SEO tags, woo_title(); & woo_meta();
 * functions.php � Changed layout for loading required files.

2010.03.18 - version 1.3.4 
 * archive-portfolio.php - made all portfolio tags lowercase in code

2010.03.17 - version 1.3.3
 * style.css - added bold to #logo .site-title a

2010.03.02 - version 1.3.2
 * archive-portfolio.php - fixed so that tags with spaces or / are allowed

2010.02.07 - version 1.3.1
 * functions/admin-setup.php - Custom css wasn't outputted with text title enabled

2010.01.27 - version 1.3
 * functions.php - Added Woo Custom Nav
 * /functions/ - Updated WooFramework
 * header.php - Added Woo Custom Nav (line 63-78)

2010.01.26 - version 1.2
 * index.php - New Option to change the alignment of the featured area
 * includes/theme-options.php - - New Option to change the alignment of the featured area

2010.01.15 - version 1.1
 * includes/js/prettyphoto.js - updated to v2.5.6
 * css/prettyphoto.css - updated
 * images/prettyphoto/ - updated

2010.01.12 - version 1.0.9
 * archive-portfolio.php - added "posts_per_page => -1" to query to show all posts (no paging)

2010.01.08 - version 1.0.8
 * archive-portfolio.php - fixed quality with image resizer in use + made title of page page_title()

2009.12.30 - version 1.0.7
 * template-contact.php - Added contact form page template
   includes/theme-options.php - Added contact form e-mail option
   css/contact.css - New file for contact form page template styling

2009.12.28 - version 1.0.6
 * archive-portfolio.php - lightbox fix when disabled in options panel

2009.12.21 - version 1.0.5
 * functions/* - core framework update

2009.12.18 - version 1.0.4
 * comments.php - fixed pingbacks
 * includes/theme-comments.php - added pingbacks function
 * style.css - added pingback styling (section 5.3)

2009.12.17 - version 1.0.3
 * functions/* - core framework update
 * css/ie6.css - removed unclickable play button
 * includes/theme-options.php - Added option for text-title
 * css/effects.css - added text-shadow for text-title
 * includes/js/scripts.js - IE fix for portfolio jquery

2009.12.16 - version 1.0.2
 * index.php - added $GLOBALS['ishome'] (line 1)
   footer.php - added $GLOBALS['ishome'] (line 45)

2009.12.15 - version 1.0.1
 * index.php - removed "cricket's" wording line 16-19
 * index.php - added stripslashes to info text and quotes text
	
2009.12.15 - version 1.0
 * First release!
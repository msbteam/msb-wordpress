<?php

add_action('init','woo_global_options');
function woo_global_options() {
	// Populate WooThemes option in array for use in theme
	global $woo_options;
	$woo_options = get_option('woo_options');
}

add_action( 'admin_head','woo_options' );  
if (!function_exists('woo_options')) {
function woo_options() {

// VARIABLES
$themename = "Optimize";
$manualurl = 'http://www.woothemes.com/support/theme-documentation/optimize/';
$shortname = "woo";

$GLOBALS['template_path'] = get_bloginfo('template_directory');

//Access the WordPress Categories via an Array
$woo_categories = array();  
$woo_categories_obj = get_categories('hide_empty=0');
foreach ($woo_categories_obj as $woo_cat) {
    $woo_categories[$woo_cat->cat_ID] = $woo_cat->cat_name;}
$categories_tmp = array_unshift($woo_categories, "Select a category:");    
       
//Access the WordPress Pages via an Array
//$woo_pages = array();
//$woo_pages_obj = get_pages('sort_column=post_parent,menu_order'); 
//foreach ($woo_pages_obj as $woo_page) {
    //$woo_pages[$woo_page->ID] = $woo_page->post_name;}
//$woo_pages_tmp = array_unshift($woo_pages, "Select a page:");

// Image Alignment radio box
$options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 

// Image Links to Options
$options_image_link_to = array("image" => "The Image","post" => "The Post"); 

//Testing 
$options_select = array("one","two","three","four","five"); 
$options_radio = array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five"); 

//URL Shorteners
if (_iscurlinstalled()) {
	$options_select = array("Off","TinyURL","Bit.ly");
	$short_url_msg = 'Select the URL shortening service you would like to use.'; 
} else {
	$options_select = array("Off");
	$short_url_msg = '<strong>cURL was not detected on your server, and is required in order to use the URL shortening services.</strong>'; 
}

//Stylesheets Reader
$alt_stylesheet_path = TEMPLATEPATH . '/styles/';
$alt_stylesheets = array();

if ( is_dir($alt_stylesheet_path) ) {
    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) { 
        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) {
            if(stristr($alt_stylesheet_file, ".css") !== false) {
                $alt_stylesheets[] = $alt_stylesheet_file;
            }
        }    
    }
}

//More Options


$other_entries = array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
$body_repeat = array("no-repeat","repeat-x","repeat-y","repeat");
$body_pos = array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");

// THIS IS THE DIFFERENT FIELDS
$options = array();   

$options[] = array( "name" => "General Settings",
					"icon" => "general",
                    "type" => "heading");
                        
$options[] = array( "name" => "Theme Stylesheet",
					"desc" => "Select your themes alternative color scheme.",
					"id" => $shortname."_alt_stylesheet",
					"std" => "default.css",
					"type" => "select",
					"options" => $alt_stylesheets);

$options[] = array( "name" => "Custom Logo",
					"desc" => "Upload a logo for your theme, or specify an image URL directly.",
					"id" => $shortname."_logo",
					"std" => "",
					"type" => "upload");    
                                                                                     
$options[] = array( "name" => "Text Title",
					"desc" => "Enable if you want Blog Title and Tagline to be text-based. Setup title/tagline in WP -> Settings -> General.",
					"id" => $shortname."_texttitle",
					"std" => "false",
					"type" => "checkbox");

$options[] = array( "name" => "Custom Favicon",
					"desc" => "Upload a 16px x 16px <a href='http://www.faviconr.com/'>ico image</a> that will represent your website's favicon.",
					"id" => $shortname."_custom_favicon",
					"std" => "",
					"type" => "upload"); 
                                               
$options[] = array( "name" => "Tracking Code",
					"desc" => "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.",
					"id" => $shortname."_google_analytics",
					"std" => "",
					"type" => "textarea");        

$options[] = array( "name" => "RSS URL",
					"desc" => "Enter your preferred RSS URL. (Feedburner or other)",
					"id" => $shortname."_feedburner_url",
					"std" => "",
					"type" => "text");
                    
$options[] = array( "name" => "Contact Form E-Mail",
					"desc" => "Enter your E-mail address to use on the Contact Form Page Template.",
					"id" => $shortname."_contactform_email",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "Custom CSS",
                    "desc" => "Quickly add some CSS to your theme by adding it to this block.",
                    "id" => $shortname."_custom_css",
                    "std" => "",
                    "type" => "textarea");
                    
$options[] = array( "name" => "Styling Options",
					"icon" => "styling",
					"type" => "heading");
					
$options[] = array( "name" =>  "Body Background Color",
					"desc" => "Pick a custom color for background color of the theme e.g. #697e09",
					"id" => "woo_body_color",
					"std" => "",
					"type" => "color");
					
$options[] = array( "name" => "Body Background Image",
					"desc" => "Upload an image for the theme's background.",
					"id" => $shortname."_body_img",
					"std" => "",
					"type" => "upload");
					
$options[] = array( "name" => "Background Image Repeat",
                    "desc" => "Select how you would like to repeat the background-image.",
                    "id" => $shortname."_body_repeat",
                    "std" => "no-repeat",
                    "type" => "select",
                    "options" => $body_repeat);

$options[] = array( "name" => "Background Image Position",
                    "desc" => "Select how you would like to position the background.",
                    "id" => $shortname."_body_pos",
                    "std" => "top",
                    "type" => "select",
                    "options" => $body_pos);

$options[] = array( "name" =>  "Link Color",
					"desc" => "Pick a custom color for links or add a hex color code e.g. #697e09",
					"id" => "woo_link_color",
					"std" => "",
					"type" => "color");   

$options[] = array( "name" =>  "Link Hover Color",
					"desc" => "Pick a custom color for links hover or add a hex color code e.g. #697e09",
					"id" => "woo_link_hover_color",
					"std" => "",
					"type" => "color");  

$options[] = array( "name" => "Featured Area",
					"icon" => "featured",
					"type" => "heading");    

$options[] = array(	"name" => "Featured Area Alignment",
					"desc" => "Check this if you want this section to be aligned left instead of the default right alignment.",
					"id" => $shortname."_featured_right",
					"std" => "false",
					"type" => "checkbox");

$options[] = array(	"name" => "Title (Slogan)",
					"desc" => "Enter the title of  your featured area (or slogan)",
					"id" => $shortname."_featured_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Intro Text",
					"desc" => "Enter some intro text to have under your title. You can use HTML here ex. &lt;em>Text&lt;/em> for italics.",
					"id" => $shortname."_featured_text",
					"std" => "",
					"type" => "textarea");

$options[] = array(	"name" => "Button #1 Text (optional)",
					"desc" => "Enter the button text for the first button under the intro text",
					"id" => $shortname."_featured_btn1_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Button #1 URL",
					"desc" => "Enter the URL for the first button",
					"id" => $shortname."_featured_btn1_url",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Button #2 Text (optional)",
					"desc" => "Enter the button text for the second button under the intro text",
					"id" => $shortname."_featured_btn2_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Button #2 URL",
					"desc" => "Enter the URL for the second button",
					"id" => $shortname."_featured_btn2_url",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Featured Image",
					"desc" => "Upload an image to show in the featured area. We suggest using a transparent PNG image with a size of 475x390px.",
					"id" => $shortname."_featured_image",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Featured Image URL",
					"desc" => "Enter the URL that the featured image will point to, or upload an image. Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_featured_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Featured URL Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_featured_image_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Use lightbox?",
					"desc" => "Show the Featured Image URL in a javascript lightbox.",
					"id" => $shortname."_featured_lightbox",
					"std" => "true",
					"type" => "checkbox");		

$options[] = array(	"name" => "Show play icon?",
					"desc" => "Show a play button overlay over the image.",
					"id" => $shortname."_featured_play",
					"std" => "true",
					"type" => "checkbox");		


$options[] = array( "name" => "Sub Featured",
					"icon" => "featured",
					"type" => "heading");    


$options[] = array(	"name" => "Enable Sub Featured Area",
					"desc" => "Enable area below featured which holds 5 thumbnails and some info text e.g. Product Features",
					"id" => $shortname."_sub",
					"std" => "false",
					"type" => "checkbox");		

$options[] = array(	"name" => "Use lightbox?",
					"desc" => "Open the thumbnails links in a lightbox. If disabled, the link will open in the browser window.",
					"id" => $shortname."_sub_lightbox",
					"std" => "true",
					"type" => "checkbox");		

$options[] = array(	"name" => "Lightbox gallery?",
					"desc" => "Show the lightbox as gallery with Prev/Next navigation?",
					"id" => $shortname."_sub_gallery",
					"std" => "false",
					"type" => "checkbox");		

$options[] = array(	"name" => "Title",
					"desc" => "Enter the title to show in the sub featured area (right of the thumbnails)",
					"id" => $shortname."_sub_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Text",
					"desc" => "Enter the text to show in the sub featured area (right of the thumbnails)",
					"id" => $shortname."_sub_text",
					"std" => "",
					"type" => "textarea");

$options[] = array(	"name" => "Thumb #1",
					"desc" => "Upload a thumbnail (size 60x60px). ",
					"id" => $shortname."_sub_thumb1",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #1 URL",
					"desc" => "Enter the URL that the thumbnail image will point to OR upload an image (max width 900px). Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_sub_thumb1_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #1 Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_sub_thumb1_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Thumb #2",
					"desc" => "Upload a thumbnail (size 60x60px). ",
					"id" => $shortname."_sub_thumb2",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #2 URL",
					"desc" => "Enter the URL that the thumbnail image will point to OR upload an image (max width 900px). Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_sub_thumb2_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #2 Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_sub_thumb2_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Thumb #3",
					"desc" => "Upload a thumbnail (size 60x60px). ",
					"id" => $shortname."_sub_thumb3",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #3 URL",
					"desc" => "Enter the URL that the thumbnail image will point to OR upload an image (max width 900px). Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_sub_thumb3_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #3 Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_sub_thumb3_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Thumb #4",
					"desc" => "Upload a thumbnail (size 60x60px). ",
					"id" => $shortname."_sub_thumb4",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #4 URL",
					"desc" => "Enter the URL that the thumbnail image will point to OR upload an image (max width 900px). Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_sub_thumb4_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #4 Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_sub_thumb4_title",
					"std" => "",
					"type" => "text");

$options[] = array(	"name" => "Thumb #5",
					"desc" => "Upload a thumbnail (size 60x60px). ",
					"id" => $shortname."_sub_thumb5",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #5 URL",
					"desc" => "Enter the URL that the thumbnail image will point to OR upload an image (max width 900px). Read <a target='_blank' href='http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/'>prettyPhoto documentation</a> for info on how to add video or flash in this URL.",
					"id" => $shortname."_sub_thumb5_url",
					"std" => "",
					"type" => "upload");

$options[] = array(	"name" => "Thumb #5 Title",
					"desc" => "Add a title to the link (will show in Lightbox)",
					"id" => $shortname."_sub_thumb5_title",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "Main Area",
					"icon" => "homepage",
					"type" => "heading");    

$options[] = array( "name" => "Twitter Username",
					"desc" => "Enter your Twitter username if you want to display latest tweet in the homepage",
					"id" => $shortname."_twitter",
					"std" => "",
					"type" => "text");

$options[] = array(    "name" => "Number of tweets",
                    "desc" => "Select the number of tweets you want to show in your twitter box (using jquery innerfade).",
                    "id" => $shortname."_twitter_count",
                    "std" => "3",
                    "type" => "select",
                    "options" => $other_entries);

//$options[] = array( "name" => "Homepage content #1",
         // "desc" => "(Optional) Select a page that you'd like to display on the front page <strong>below Twitter and above the mini features area</strong>.",
          //"id" => $shortname."_main_page1",
          //"std" => "Select a page:",
			//"type" => "select",
			//"options" => $woo_pages);   

//$options[] = array( "name" => "Homepage content #2",
          //"desc" => "(Optional) Select a page that you'd like to display on the front page <strong>below the mini features area and above the info box.</strong>",
          //"id" => $shortname."_main_page2",
          //"std" => "Select a page:",
			//"type" => "select",
			//"options" => $woo_pages);   

$options[] = array( "name" => "Mini-Features Area",
          "desc" => "Enable the front page Mini-Features features area.",
          "id" => $shortname."_main_pages",
          "std" => "true",
          "type" => "checkbox");
					
$options[] = array( "name" => "Custom permalink",
          "desc" => "This option allows you to change the permalink on the individual mini-features pages. (e.g /infobox/pagename to /features/pagename/). Please update <a href='". admin_url('options-permalink.php')."'>Permalinks</a> after any changes.",
          "id" => $shortname."_infobox_rewrite",
          "std" => "infobox",
          "type" => "text");                          

$options[] = array( "name" => "Info Box",
					"icon" => "homepage",
					"type" => "heading");    

$options[] = array(	"name" => "Homepage - Info Box",
					"desc" => "Show the info box at the bottom of homepage. Add info below.",
					"id" => $shortname."_info",
					"std" => "false",
					"type" => "checkbox");	

$options[] = array( "name" => "About Title",
					"desc" => "Edit the title of the About section",
					"id" => $shortname."_info_about_title",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "About Text",
					"desc" => "Edit the text of the About section",
					"id" => $shortname."_info_about_text",
					"std" => "",
					"type" => "textarea");

$options[] = array( "name" => "About link text",
					"desc" => "Add a link text to read more at the bottom of About section (optional)",
					"id" => $shortname."_info_about_link",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "About URL",
					"desc" => "Add an URL to read more at the bottom of About section (optional)",
					"id" => $shortname."_info_about_url",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "Testimonials Title",
					"desc" => "Edit the title of the testimonials section",
					"id" => $shortname."_info_quote_title",
					"std" => "",
					"type" => "text");

$options[] = array( "name" => "Slider Settings",
					"icon" => "slider",
					"type" => "heading"); 
					
$options[] = array( "name" => "Enable Auto Fade",
					"desc" => "Enable automatic fading between Testimonials & Twitter Stream items.",
					"id" => $shortname."_testimonials_autofade",
					"std" => "true",
					"type" => "checkbox");

$options[] = array( "name" => "Auto Fade Interval",
                    "desc" => "The time in <strong>seconds</strong> each testimonial & twitter stream item pauses for, before fading to the next.",
                    "id" => $shortname."_testimonials_interval",
                    "std" => "6",
					"type" => "select",
					"options" => array( '1', '2', '3', '4', '5', '6', '7', '8', '9', '10' ) );
                    
$options[] = array( "name" => "Portfolio",
					"icon" => "portfolio",
					"type" => "heading");    

$options[] = array(	"name" => "Use Lightbox?",
					"desc" => "Show the portfolio URL or large image in a javascript lightbox.",
					"id" => $shortname."_portfolio_lightbox",
					"std" => "true",
					"type" => "checkbox");

$options[] = array(	"name" => "Use Dynamic Image Resizer?",
					"desc" => "Use the dynamic image resizer (thumb.php) to resize the portfolio thumbnail. Remember to CHMOD your cache folder to 777. <a href='http://www.woothemes.com/2008/10/troubleshooting-image-resizer-thumbphp/'>Need help?</a>",
					"id" => $shortname."_portfolio_resize",
					"std" => "false",
					"type" => "checkbox");		

$options[] = array( "name" => "Portfolio Tags",
					"desc" => "Enter comma seperated tags for portfolio sorting (e.g. web, print, icons). You must add these tags to the portfolio items you want to sort.",
					"id" => $shortname."_portfolio_tags",
					"std" => "",
					"type" => "text");    

					
$options[] = array( "name" => "Layout Options",
					"icon" => "layout",
					"type" => "heading");    

$options[] = array(	"name" => "Blog Excerpt",
					"desc" => "Show only the excerpt in the blog section. ",
					"id" => $shortname."_excerpt",
					"std" => "false",
					"type" => "checkbox");

$options[] = array( "name" => "Dynamic Images",
					"icon" => "image",
				    "type" => "heading");    

$options[] = array( "name" => "Enable Dynamic Image Resizer",
					"desc" => "This will enable the thumb.php script. It dynamicaly resizes images on your site.",
					"id" => $shortname."_resize",
					"std" => "true",
					"type" => "checkbox");    
                    
$options[] = array( "name" => "Automatic Image Thumbs",
					"desc" => "If no image is specified in the 'image' custom field then the first uploaded post image is used.",
					"id" => $shortname."_auto_img",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Enable WordPress Post Images Support",
					 "desc" => "Use WordPress 2.9+ built in thumbnail/post-image support.",
					 "id" => $shortname."_post_image_support",
					 "std" => "false",
					 "class" => "collapsed",
					 "type" => "checkbox"); 
     
$options[] = array( "name" => "Hard Crop?",
					 "desc" => "Use WordPress 2.9+ built in thumbnail/post-image support.",
					 "id" => $shortname."_pis_hard_crop",
					 "std" => "false",
					 "class" => "hidden last",
					 "type" => "checkbox");

$options[] = array( "name" => "Thumbnail Image Dimensions",
					"desc" => "Enter an integer value i.e. 250 for the desired size which will be used when dynamically creating the images.",
					"id" => $shortname."_image_dimensions",
					"std" => "",
					"type" => array( 
									array(  'id' => $shortname. '_thumb_w',
											'type' => 'text',
											'std' => 100,
											'meta' => 'Width'),
									array(  'id' => $shortname. '_thumb_h',
											'type' => 'text',
											'std' => 100,
											'meta' => 'Height')
								  ));
                                                                                                
$options[] = array( "name" => "Thumbnail Image alignment",
					"desc" => "Select how to align your thumbnails with posts.",
					"id" => $shortname."_thumb_align",
					"std" => "alignleft",
					"type" => "radio",
					"options" => $options_thumb_align); 

$options[] = array( "name" => "Show thumbnail in Single Posts",
					"desc" => "Show the attached image in the single post page.",
					"id" => $shortname."_thumb_single",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Single Image Dimensions",
					"desc" => "Enter an integer value i.e. 250 for the image size. Max width is 576.",
					"id" => $shortname."_image_dimensions",
					"std" => "",
					"type" => array( 
									array(  'id' => $shortname. '_single_w',
											'type' => 'text',
											'std' => 200,
											'meta' => 'Width'),
									array(  'id' => $shortname. '_single_h',
											'type' => 'text',
											'std' => 200,
											'meta' => 'Height')
								  ));

$options[] = array( "name" => "Add thumbnail to RSS feed",
					"desc" => "Add the the image uploaded via your Custom Settings to your RSS feed",
					"id" => $shortname."_rss_thumb",
					"std" => "false",
					"type" => "checkbox");  
					
//Footer
$options[] = array( "name" => "Footer Customization",
					"icon" => "footer",
                    "type" => "heading");
					
					
$options[] = array( "name" => "Custom Affiliate Link",
					"desc" => "Add an affiliate link to the WooThemes logo in the footer of the theme.",
					"id" => $shortname."_footer_aff_link",
					"std" => "",
					"type" => "text");	
									
$options[] = array( "name" => "Enable Custom Footer (Left)",
					"desc" => "Activate to add the custom text below to the theme footer.",
					"id" => $shortname."_footer_left",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Custom Text (Left)",
					"desc" => "Custom HTML and Text that will appear in the footer of your theme.",
					"id" => $shortname."_footer_left_text",
					"std" => "<p></p>",
					"type" => "textarea");
						
$options[] = array( "name" => "Enable Custom Footer (Right)",
					"desc" => "Activate to add the custom text below to the theme footer.",
					"id" => $shortname."_footer_right",
					"std" => "false",
					"type" => "checkbox");    

$options[] = array( "name" => "Custom Text (Right)",
					"desc" => "Custom HTML and Text that will appear in the footer of your theme.",
					"id" => $shortname."_footer_right_text",
					"std" => "<p></p>",
					"type" => "textarea");					                      

// Add extra options through function
if ( function_exists("woo_options_add") )
  $options = woo_options_add($options);                                              

if ( get_option('woo_template') != $options) update_option('woo_template',$options);      
if ( get_option('woo_themename') != $themename) update_option('woo_themename',$themename);   
if ( get_option('woo_shortname') != $shortname) update_option('woo_shortname',$shortname);
if ( get_option('woo_manual') != $manualurl) update_option('woo_manual',$manualurl);

                                     
// Woo Metabox Options
$woo_metaboxes = array();

if( get_post_type() == 'post' || !get_post_type()){

$woo_metaboxes[] = array (	"name" => "image",
							"label" => "Image",
							"type" => "upload",
							"desc" => "Upload image for use with blog posts");
} // End post

if( get_post_type() == 'infobox' || !get_post_type()){	

$woo_metaboxes['mini'] = array (	
				"name" => "mini",
				"label" => "Mini-features Icon",
				"type" => "upload",
				"desc" => "Upload icon for use with the Mini-Feature on the homepage (optimal size: 32x32px) (optional)"
			);
 
$woo_metaboxes['mini_excerpt'] = array (	
				"name" => "mini_excerpt",
				"label" => "Mini-features Excerpt",
				"type" => "textarea",
				"desc" => "Enter the text to show in your Mini-Feature. "
			);

$woo_metaboxes['mini_readmore'] = array (	
				"name" => "mini_readmore",
				"std" => "",
				"label" => "Mini-features URL",
				"type" => "text",
				"desc" => "Add an URL for your Read More button in your Mini-Feature on homepage (optional)"
			);

} // End mini

if( get_post_type() == 'testimonial' || !get_post_type()){	

$woo_metaboxes['testimonial_author'] = array (	
				"name" => "testimonial_author",
				"label" => "Testimonial Author",
				"type" => "text",
				"desc" => "Enter the name of the author of the testimonial e.g. Joe Bloggs"
			);
 
$woo_metaboxes['testimonial_url'] = array (	
				"name" => "testimonial_url",
				"label" => "Testimonial URL",
				"type" => "text",
				"desc" => "(optional) Enter the URL to the testimonial author e.g. http://www.woothemes.com"
			);

} // End testimonial

if( get_post_type() == 'portfolio' || !get_post_type()){

$woo_metaboxes['portfolio'] = array (	"name" => "portfolio",
							"label" => "Portfolio Thumbnail",
							"type" => "upload",
							"desc" => "Upload an image for use in the portfolio (optimal size: 450x210)");

$woo_metaboxes['portfolio-large'] = array (	"name" => "portfolio-large",
							"label" => "Portfolio Large",
							"type" => "upload",
							"desc" => "Add an URL OR upload an image for use as the large portfolio image");

} // End portfolio

// Add extra metaboxes through function
if ( function_exists("woo_metaboxes_add") )
	$woo_metaboxes = woo_metaboxes_add($woo_metaboxes);
    
if ( get_option('woo_custom_template') != $woo_metaboxes) update_option('woo_custom_template',$woo_metaboxes);

}
}
?>
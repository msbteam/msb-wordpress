<?php
/*
Template Name: Facebook Custom Page Template
*/
?>

<head>


<style type="text/css">
   .gototop, .creds {
        display: none;
    }
    #wrap {
        border: 0px !important;
    }
</style>
    </head>

<body>
<?php
// Add custom body class to the head
add_filter( 'body_class', 'add_body_class' );
function add_body_class( $classes ) {
    $classes[] = 'landing-page';
    return $classes;
}


//REMOVE PAGE TITLE
remove_action('genesis_post_title', 'genesis_do_post_title');

// Remove header, navigation, breadcrumbs, footer widgets, footer
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
remove_action( 'genesis_header', 'genesis_header_markup_open', 5 );
remove_action( 'genesis_header', 'genesis_do_header' );
remove_action( 'genesis_header', 'genesis_header_markup_close', 15 );
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs');
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );
remove_action( 'genesis_footer', 'genesis_footer_markup_open', 5 );
remove_action('genesis_footer', 'genesis_custom_footer');
remove_action( 'genesis_footer', 'genesis_do_footer' );
remove_action( 'genesis_footer', 'genesis_footer_markup_close', 15 );

/**
 * Force layout
 *
 * @author Greg Rickaby
 * @since 1.0.0
 */
function child_do_layout( $opt ) {
    $opt = 'full-width-content'; // You can change this to any Genesis layout
    return $opt;
}

add_filter('genesis_footer_output', 'footer_output_filter', 10, 1);
function footer_output_filter($footer_content) {
    $footer_content = '&nbsp;';
    return $footer_content;
}

genesis();
?>
</body>

<?php
/*
Template Name: MYSB Property Listings
*/
?>

<?php

remove_action('genesis_post_title', 'genesis_do_post_title');//We're not using the title for this post type
remove_action('genesis_loop', 'genesis_do_loop');//remove genesis loop
add_action('genesis_loop', 'msb_proploop');//add the special loop

function msb_proploop() {
    global $post;
	$lide = $post->ID;
    $loop = new WP_Query( array( 'post_type' => 'mysbprop','posts_per_page' => 1,'p' => "$lide" ) ); ?>
    
    <?php while ( $loop->have_posts() ) : $loop->the_post();?>
	     
        <div id="post-<?php the_ID(); ?>" class="post-<?php the_ID(); ?> mysbprop type-mysbprop status-publish hentry">
		<h1 class="entry-title"><?php the_title();?></h1>
			<?php if ( function_exists( 'get_the_image' ) ) 
					$pid=get_the_ID();
					$images =& get_children( "post_type=attachment&post_mime_type=image&post_parent=$pid" );
					$count = count($images);
					
					$main_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 1,'height' =>365,'width' =>580,'size' =>full ) ); 
					
					$one_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 2,'height' =>75,'width' =>75,'size' =>full ) ); 

					$two_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 3,'height' =>75,'width' =>75,'size' =>full ) ); 

					$three_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 4,'height' =>75,'width' =>75,'size' =>full ) ); 

					$four_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 5,'height' =>75,'width' =>75,'size' =>full ) ); 

					$five_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 6,'height' =>75,'width' =>75,'size' =>full ) );
					
					$six_get_the_image_as_array = get_the_image( array('format' => 'array','order_of_image' => 7,'height' =>75,'width' =>75,'size' =>full ) ); 

				?>
				
			<div id="mysbprop-mainimage"><a href="<?php echo $main_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $main_get_the_image_as_array[url]; ?>" height="365" width="580"></a></div>
			<div id="mysbprop-otherimages"><?php if( count($images) >= 2 ) { ?><a href="<?php echo $one_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $one_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?><?php if( count($images) >= 3 ) { ?><a href="<?php echo $two_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $two_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?><?php if( count($images) >= 4 ) { ?><a href="<?php echo $three_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $three_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?><?php if( count($images) >= 5 ) { ?><a href="<?php echo $four_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $four_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?><?php if( count($images) >= 6 ) { ?><a href="<?php echo $five_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $five_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?><?php if( count($images) >= 7 ) { ?><a href="<?php echo $six_get_the_image_as_array[url]; ?>" rel="lightbox[<?php the_ID(); ?>]" class="fancyboxgroup"><img src="<?php echo $six_get_the_image_as_array[url]; ?>" height="75" width="75"></a><?php } ?>
			</div>
            <div id="mysbprop-info"><?php the_content(); ?></div>
			<p class="mysbprop-pinfo">Property Info / <?php echo ' ' . genesis_get_custom_field('ecpt_address1') . '&nbsp;'; echo genesis_get_custom_field('ecpt_address'); echo ' ' . genesis_get_custom_field('ecpt_city') . ',&nbsp;';echo genesis_get_custom_field('ecpt_state');echo '&nbsp;'. genesis_get_custom_field('ecpt_zipcode') . ' '; ?></p>
            <p class="mysbprop-price">Price&nbsp;:&nbsp;$<?php echo genesis_get_custom_field('ecpt_price'); ?></p>
			<p class="mysbprop-bedrooms">Bedrooms&nbsp;:&nbsp;<?php echo genesis_get_custom_field('ecpt_bedrooms'); ?></p>
            <p class="mysbprop-baths">Baths&nbsp;:&nbsp;<?php echo genesis_get_custom_field('ecpt_baths'); ?></p>
            <p class="mysbprop-sqft">Sq. Ft.&nbsp;:&nbsp;<?php echo genesis_get_custom_field('ecpt_sqft'); ?></p>
            <p class="mysbprop-address-full">Address&nbsp;:&nbsp;<?php echo ' ' . genesis_get_custom_field('ecpt_address1') . '&nbsp;'; echo genesis_get_custom_field('ecpt_address'); echo ' ' . genesis_get_custom_field('ecpt_city') . ',&nbsp;';echo genesis_get_custom_field('ecpt_state');echo '&nbsp;'. genesis_get_custom_field('ecpt_zipcode') . ' '; ?></p>
            <p class="mysbprop-pinfo">Property Map</p>
		
			<!-- lets' show a map if we have one -->			
			<div style="display: none;">
					<?php //if ( get_post_meta($post->ID, 'latlng', true) !== '' ) : ?>
						<div id="item<?php the_ID(); ?>">
							<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
							<?php the_content(); ?>
						</div>
					<?php //endif; ?>
					
			</div><div id="map" style="width: 100%; height: 300px;"></div>
			<script type="text/javascript"> 
     				 var myOptions = {
         				zoom: 18,
         				center: new google.maps.LatLng<?php echo genesis_get_custom_field('martygeocoderlatlng');?>,
         				mapTypeId: google.maps.MapTypeId.ROADMAP
      								};

      				var map = new google.maps.Map(document.getElementById("map"), myOptions);
					var marker = new google.maps.Marker({    
      				position: new google.maps.LatLng<?php echo genesis_get_custom_field('martygeocoderlatlng');?>,    
      				map: map    
    				});  
   			</script>

			
			<!-- End map -->
			
            </div><!--end proppost -->
    <?php endwhile;?>
	
    <?php
    }
 
    genesis();